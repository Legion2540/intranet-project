<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnnounceController extends Controller
{
    public function announce_index(){
        return view('pages/announce/index');
    }

    public function announce_create(){
        return view('/pages/announce/admin/create_announce');
    }
}
