<?php

namespace App\Http\Controllers;

use App\BookingRoom;
use Illuminate\Http\Request;

class ManagerEmpController extends Controller
{
    public function index_approval()
    {
        return view('pages/booking/approval/approval_1/index_room_car');
    }
    public function approved_car()
    {
        return view('pages/booking/approval/approval_1/approve_car1');
    }
    public function approved_room(Request $request)
    {
        $data = [];
        $room = BookingRoom::find($request['book_id']);
        if ($room) {

            $room['device_id'] = json_decode($room['device_id']);
            $room['food_detail'] = json_decode($room['food_detail']);
            return view('pages/booking/approval/approval_1/approve_room1', compact('room'));
        } else {
            return back()->with('message', 'Not have this booking');
        }
    }

    public function approved_room_approved(Request $request){

        BookingRoom::where('id',$request['book_id'])->update([
            'manager_approved' => 'approved',
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return back();


    }

    public function approved_room_cancelled(Request $request){

        BookingRoom::where('id',$request['book_id'])->update([
            'status_booking' => 'cancelled',
            'manager_approved' => 'cancelled',
            'reason' => $request['reason'],
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return back();


    }



    public function manager_approve()
    {
        return view('/pages/booking/approval/approval_car/approved_car');
    }
}
