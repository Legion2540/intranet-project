<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MarketController extends Controller
{
    public function market_index(){
        return view('pages/market/user/index');
    }
    public function market_myproduct(){
        return view('pages/market/user/myproduct');
    }
    public function market_product(){
        return view('pages/market/user/product');
    }
    public function market_sell(){
        return view('pages/market/user/sell');
    }
}
