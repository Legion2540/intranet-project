<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){
        return view('/pages/blog/blog');

    }
    public function blog_other(){
        return view('/pages/blog/blog_other');

    }
    public function myblog(){
        return view('/pages/blog/myblog');

    }
    public function content_blog($id){
        return view('/pages/blog/mycontent');

    }
    public function createBlog(){
        return view('/pages/blog/createblog');

    }
}
