<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\User;
use App\BookingDevices;
use App\TypeDevices;
use Illuminate\Support\Facades\Auth;
use Image;
use QrCode;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Symfony\Component\VarDumper\Cloner\Data;

class EquipmentController extends Controller
{
    public function equipment_index()
    {

        $bookking_deveces = BookingDevices::leftJoin('users', 'booking_devices.who_approve', '=', 'users.id')
            ->where('user_id', Auth::user()->id)
            ->where('booking_devices.type', 'lend')
            ->where('status', 'approved')
            ->select(
                'booking_devices.id',
                'user_id',
                'title',
                'to',
                'objective',
                'pattern',
                'devices',
                'booking_devices.type',
                'status',
                'status_print',
                'form_status',
                'who_approve',
                'booking_devices.created_at',
                'booking_devices.updated_at',
                'users.name as user_name'
                )
            ->get();

        if (count($bookking_deveces) > 0) {
            foreach ($bookking_deveces as $key_booking=> $item_booking){
                $data['device_lend'][$key_booking]['booking_id'] = $item_booking['id'];

                $data['device_lend'][$key_booking]['date_lend'] = $item_booking['created_at'];
                $data['device_lend'][$key_booking]['who_app'] = $item_booking['user_name'];

                $devices_decode = json_decode($item_booking['devices']);


                $device_id_all = [];
                foreach ($devices_decode as $key_device=> $item_device){
                    foreach ($item_device->devices_id as $key_devices_id=> $item_devices_id){
                        array_push($device_id_all, $item_devices_id);
                    }
                }


                $device = Device::whereIn('id', $device_id_all)
                    ->get();
                foreach ($device as $key_device_full => $device_full){
                    $data['device_lend'][$key_booking]['content'][$key_device_full]['device_id'] = $device_full['id'];
                    $data['device_lend'][$key_booking]['content'][$key_device_full]['name'] = $device_full['name'];
                    $data['device_lend'][$key_booking]['content'][$key_device_full]['device_no'] = $device_full['device_no'];
                }

                $data['device_lend'][$key_booking]['count_device'] = count($device);

            }
        }else{
            $data['device_lend'] = NULL;
        }

        // dd($data);

        return view('pages.equipment.myequipment', $data);
    }

    public function equipment_index_update(Request $request){
        if (!is_null($request['order_id'])){

            foreach($request['order_id'] as $key_booking_id => $item_booking_id){

                $arr_device_id = [];
                foreach($item_booking_id as $key_device_id => $device_id){
                    array_push($arr_device_id , $key_device_id);
                }

                $data['arr_device_id'] = $arr_device_id;

                $data['booking_id'] = $key_booking_id;

            }
        }else{
            $data = NULL;
        }

        $data_encode = json_encode($data);

        return response()->json(['status'=> 'true', 'order'=> $data_encode]);
    }

    public function equipment_detail(Request $request)
    {
        $data['booking_id'] = $request['booking_id'];

        $booking_device = BookingDevices::where('id', $request['booking_id'])
            ->first();

        $data['date_lend'] = $booking_device['created_at'];

        $decode_device = json_decode($booking_device['devices']);

        $arr_device = [];
        foreach($decode_device as $key_type => $item_type){
            foreach($item_type->devices_id as $key_device => $item_device){
                array_push($arr_device, $item_device);
            }
        }

        $data['devices'] = Device::whereIn('id', $arr_device)
            ->get();

        return view('pages.equipment.user.equipment_detail', $data);
    }

    public function equipment_detail_update(Request $request){
        dd($request);
        // BookingDevices::where('id', $request['booking_id'])
        //         ->update([
        //             'title' => $request['title'],
        //             'to' => $request['to'],
        //             'objective' => $request['objective'],
        //             'pattern' => $request['pattern'],
        //             'type'=> 'return',
        //             'status'=> 'waiting',
        //             'status_print' => 'not print',
        //             'updated_at' => date('Y-m-d H:i:s')
        //         ]);
    }


    public function equipment_user_return(Request $request){

        $booking_id = base64_decode($request->book_id);
        $device = base64_decode($request->device);

        if($device == 'NULL'){
            $booking_device = BookingDevices::where('id', $booking_id)
                ->first();

            $decode_deive = json_decode($booking_device['devices']);

            $arr_device_form_bookdevice = [];
            foreach($decode_deive as $key_type => $item_type){
                foreach($item_type->devices_id as $key_device => $item_device){
                    array_push($arr_device_form_bookdevice, $item_device);
                }
            }


            $arr_device = $arr_device_form_bookdevice;

        }else{
            $arr_device = explode(',' , $device);
        }

        $data['booking_devices'] = BookingDevices::where('id', $booking_id)
            ->select('id','user_id', 'title', 'to', 'objective', 'pattern')
            ->first();

        $data['device'] = Device::leftJoin('type_devices', 'devices.type_device', '=', 'type_devices.id')
            ->select('devices.id', 'device_no' , 'type_devices.id as type_id' , 'name_type')
            ->whereIn('devices.id', $arr_device)
            ->get();

        return view('pages.equipment.user.return', $data);
    }


    public function equipment_user_return_update(Request $request){

        $booking_devices = BookingDevices::where('id', $request['book_id'])
            ->first();


        $devices_list = json_decode($booking_devices['devices']);


        $arr_device_id = [];
        foreach ($devices_list as $key_type => $item_type){

            foreach($item_type->devices_id as $key_device_id =>$item_device_id){
                array_push( $arr_device_id, $item_device_id);
            }
        }


        $arr_diff_device_id = array_diff($arr_device_id,$request['device_id']);
        if(count($arr_diff_device_id) < 1 ){


            BookingDevices::where('id', $request['book_id'])
                ->update([
                    'title' => $request['title'],
                    'to' => $request['to'],
                    'objective' => $request['objective'],
                    'pattern' => $request['pattern'],
                    'type'=> 'return',
                    'status'=> 'waiting',
                    'status_print' => 'not print',
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

        }else{

            $device_not_return = Device::whereIn('id' , $arr_diff_device_id)
                ->get();


            // ส่วนของการแก้ไข BookingDevices ที่เป็นการคืน
            $arr_device = [];
            $data_array = [];
            foreach($device_not_return as $key_decive_not_return => $item_device_not_return){
                if(!in_array($item_device_not_return['type_device'], $data_array)){
                    $data_array['type_id'] = (string)$item_device_not_return['type_device'];
                    $data_array['devices_id'] = [];
                    $data_array['count_device'] = "0";

                    array_push($arr_device,$data_array);
                }

            }

            foreach($arr_device as $key_type => $item_type){
                foreach($device_not_return as $key_device_not_return => $item_device_not_return){
                    if($item_type['type_id'] == $item_device_not_return->type_device){
                        array_push($arr_device[$key_type]['devices_id'],$item_device_not_return->id);
                    }
                }

                $count_device = count($arr_device[$key_type]['devices_id']);
                $arr_device[$key_type]['count_device'] = (string)$count_device;
            }


            $devices_encode = json_encode($arr_device);
            BookingDevices::where('id', $request['book_id'])
                ->update([
                    'devices'=> $devices_encode,
                    'who_approve'=> 0,
                ]);


            // ส่วนของการสร้าง BookingDevices ที่เป็นการคืน
            $device_return = Device::whereIn('id' , $request['device_id'])
                ->get();

            $arr_device_return = [];
            $data_array_return = [];
            foreach($device_return as $key_decive_return => $item_device_return){
                // dd($item_device_return);
                if(!in_array($item_device_return['type_device'], $data_array_return)){

                    $data_array_return['type_id'] = (string)$item_device_return['type_device'];
                    $data_array_return['devices_id'] = [];
                    $data_array_return['count_device'] = "0";

                    array_push($arr_device_return,$data_array_return);
                }

            }

            foreach($arr_device_return as $key_type => $item_type){
                foreach($device_return as $key_device_return => $item_device_return){
                    if($item_type['type_id'] == $item_device_return->type_device){
                        array_push($arr_device_return[$key_type]['devices_id'],$item_device_return->id);
                    }
                }

                $count_device_return = count($arr_device_return[$key_type]['devices_id']);
                $arr_device_return[$key_type]['count_device'] = (string)$count_device_return;
            }


            $devices_return_encode = json_encode($arr_device_return);


            BookingDevices::insert([
                'user_id' => 1,
                'title' => $request['title'],
                'to' => $request['to'],
                'objective' => $request['objective'],
                'pattern' => $request['pattern'],
                'devices'=> $devices_return_encode,
                'type' => 'return',
                'status' => 'waiting',
                'status_print' => 'not print',
                'form_status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

        }

        return response()->json(['status'=> 'true']);

    }

    public function equipment_user_lend(Request $request){
        // dd(Auth::user());
        if ($request->booking_id != NULL){
            $data['bookking_deveces'] = BookingDevices::where('id', $request['booking_id'])
                ->get();
        }else{
            $data['bookking_deveces'] = NULL;
        }
        return view('pages.equipment.user.lend', $data);
    }

    public function equipment_user_lend_update(Request $request){

        $bookking_deveces = BookingDevices::where('id', $request['booking_id'])
            ->where('user_id', $request['user_id'])
            ->get();


        if ($bookking_deveces->isEmpty()) {
            $booking_id = BookingDevices::insertGetId(
                [
                    'user_id' => $request['user_id'],
                    'title' => $request['title'],
                    'to' => $request['to'],
                    'objective' => $request['objective'],
                    'pattern' => $request['pattern'],
                    'type' => 'lend',
                    'status' => 'waiting',
                    'status_print' => 'not print',
                    'form_status' => 0,
                    'created_at' => date('Y-m-d H:i:s')
                    // 'updated_at' => date('Y-m-d H:i:s')
                ]);
        } else {
            $booking_id = $request['booking_id'];
            BookingDevices::where('id', $request['booking_id'])->update(
                [
                    'user_id' => $request['user_id'],
                    'title' => $request['title'],
                    'to' => $request['to'],
                    'objective' => $request['objective'],
                    'pattern' => $request['pattern'],
                    'type' => 'lend',
                    'status' => 'waiting',
                    'status_print' => 'not print',
                    'form_status' => 0,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
        }

        return response()->json(['status'=> 'true', 'booking_id'=> $booking_id]);
    }

    public function equipment_user_list(Request $request){
        $booking_devices = BookingDevices::where('id', $request['booking_id'])
            ->first();

        // set who_lend ที่เคยยืม booking นั่น ๆ ให้เป็น 0 ** กรณีแก้ไข **
        if ($booking_devices['devices'] != NULL) {
            $devices_decode = json_decode($booking_devices['devices']);
            foreach ($devices_decode as $key => $item){
                foreach($item->devices_id as $device_id){
                    Device::where('id', $device_id)->update(['who_lend' => 0]);
                }
            }
        }



        // วนนับ ว่าแต่ละ type_devices มีจำนวนเท่าไร
        $data['booking_id'] = $request->booking_id;
        $data['type_devices'] = TypeDevices::select('id', 'name_type')
            ->get();

        foreach ($data['type_devices'] as $key => $item){
            // dd($item['id']);
            $count_form_deive = Device::select('display', 'type_device')
                ->where('display' , 1)
                ->where('who_lend' , 0)
                ->where('type_device', $item->id )
                ->count();

            $count_form_booking_device = BookingDevices::select('id','devices')
                ->where('type' , 'lend')
                ->where('type' , 'lend')
                ->where('status' , 'waiting')
                ->get();
            $count = 0;
            foreach($count_form_booking_device as $key_book_deive => $item_book_deive){
                if($item_book_deive['id'] == $request['booking_id']){
                    continue;
                }else{
                    $decode_device = json_decode($item_book_deive['devices']);
                    if($decode_device != NULL){
                        foreach($decode_device as $key_type => $item_type){
                            if($item_type->type_id == $item['id']){
                                $count += $item_type->count_device;
                            }
                        }
                    }
                }
            }


            $data['type_devices'][$key]['count_device'] = $count_form_deive - $count;
        };


        return view('pages.equipment.user.lend_list', $data);
    }


    public function equipment_user_list_update(Request $request){

        $arr_device = [];
        $id_update_status = [];

        // dd($request);

        // pack data ฟิล devices เป็น array
        foreach ($request['type_id'] as $key => $data) {
            if($data != 0){
                $arr_device_id = [];
                array_push($arr_device, ['type_id' => (string) $key,'devices_id' =>$arr_device_id,'count_device' =>$data]);
            }
        }

        // dd($arr_device);

        // อัพเดท field devices table booking_devices
        BookingDevices::where('user_id', Auth::user()->id)->where('id', $request->booking_id)->update([
            'devices' => json_encode($arr_device),
        ]);

        // เอา type_id มาวนหา device ที่ยังไม่ถูกยืม
        // foreach ($request['type_id'] as $key => $data) {
        //     $arr_device_id = [];
        //     for ($i=0 ; $i < $data ; $i++){
        //         $device = Device::where('display' , 1)
        //         ->where('type_device', $key)
        //         ->where('who_lend', 0)
        //         ->get();

        //         array_push($id_update_status, $device[$i]->id);
        //         array_push($arr_device_id, $device[$i]->id);
        //     }

        //     array_push($arr_device, ['type_id' => (string) $key,'devices_id' =>$arr_device_id]);
        // }


        // วนอัพเดท field who_lend table device ว่ามีใครเป็นคนยืม
        // foreach ($id_update_status as $key=>$item){
        //     Device::where('id', $item)->update(['who_lend' => 1]);
        // }

        return response()->json(['status'=> 'true', 'booking_id'=> $request->booking_id]);
    }

    public function equipment_user_check(Request $request){

        // dd($request);

        // get ข้อมูลของการยืมตาม booking_id
        $data['bookking_deveces'] = BookingDevices::select('id','user_id', 'title', 'to', 'objective', 'pattern', 'devices' ,'status','form_status','who_approve')
            ->where('id', $request->booking_id)
            ->get();


        // decode devices
        $arr_device = json_decode($data['bookking_deveces'][0]['devices']);


        // นับจำนวน device ทั้งหมดที่ยืม
        $count_deviceall = 0;
        foreach($arr_device as $key=> $item){
            $type_devices = TypeDevices::where('id', $item->type_id)
                ->select('name_type')
                ->get();
            $count_device = $item->count_device;

            $data['type_devices'][$key]['name'] = $type_devices[0]->name_type;
            $data['type_devices'][$key]['count'] = $count_device;

            $count_deviceall +=  $count_device;
        };
        $data['count_deviceall'] =  $count_deviceall;

        // dd($data);
        return view('pages.equipment.user.lend_check', $data);
    }

    public function equipment_user_check_update(Request $request){

        if($request->method == 'request_lend'){

            // update status ของ form_status
            BookingDevices::where('user_id', Auth::user()->id)
            ->where('id', $request->book_id)
            ->update([
                'form_status'=> 1,
            ]);
        }else{
            // update status ของ status (ยกเลิกการขอยืม)
            BookingDevices::where('id', $request->book_id)
            ->update([
                'status'=> 'cancelled',
            ]);
        }

        return response()->json(['status'=> 'true']);
    }


    public function equipment_manager_index(){

        $data['devices'] = Device::leftJoin('users', 'devices.who_lend', '=', 'users.id')
            ->leftJoin('type_devices', 'devices.type_device', '=', 'type_devices.id')
            ->select(
                'devices.id',
                'device_no',
                'devices.name as device_name',
                'img',
                'date_exp',
                'type_device',
                'display',
                'qrcode',
                'who_lend',
                'users.name as user_name',
                'no_type',
                'name_type',
                )
            ->paginate(5);
        $data['booking_devices_lend'] = BookingDevices::leftJoin('users', 'booking_devices.user_id', '=', 'users.id')
            ->select(
                'booking_devices.id',
                'user_id',
                'title',
                'to',
                'objective',
                'pattern',
                'devices',
                'booking_devices.type',
                'status',
                'status_print',
                'form_status',
                'who_approve',
                'booking_devices.created_at',
                'booking_devices.updated_at',
                'users.name as user_name',
                'group',
                )
            ->where('booking_devices.type', 'lend')
            ->where('form_status', 1)
            ->orderBy('status', 'desc')
            ->paginate(10);



        $data['booking_devices_return'] = BookingDevices::leftJoin('users', 'booking_devices.user_id', '=', 'users.id')
            ->select(
                'booking_devices.id',
                'user_id',
                'title',
                'to',
                'objective',
                'pattern',
                'devices',
                'booking_devices.type',
                'status',
                'status_print',
                'form_status',
                'who_approve',
                'booking_devices.created_at',
                'booking_devices.updated_at',
                'users.name as user_name',
                'group',
                )
            ->where('booking_devices.type', 'return')
            ->where('form_status', 1)
            ->orderBy('status', 'desc')
            ->paginate(10);



        foreach($data['booking_devices_lend'] as $key => $booking_device_lend){


            $data['booking_devices_lend'][$key]['devices'] = json_decode($booking_device_lend['devices']);

            $count = 0;
            $arr_type_device = [];
            foreach($booking_device_lend['devices'] as $device){
                $count += $device->count_device;
                array_push( $arr_type_device,$device->type_id);
            }

            $type_device = TypeDevices::whereIn('id', $arr_type_device)
                        ->select('id', 'name_type')
                        ->get();

            $name_type_all = '';
            foreach($type_device as $key_type => $item_type){

                if($key_type>0){
                    $name_type_all = $name_type_all.', '.$item_type['name_type'];
                }else{
                    $name_type_all = $item_type['name_type'];
                }
            }

            $data['booking_devices_lend'][$key]['name_type'] = $name_type_all;
            $data['booking_devices_lend'][$key]['count'] = $count;
        }

        foreach($data['booking_devices_return'] as $key => $booking_device_lend){
            $data['booking_devices_return'][$key]['devices'] = json_decode($booking_device_lend['devices']);

            $count = 0;
            $arr_type_device = [];
            foreach($booking_device_lend['devices'] as $device){
                $count += $device->count_device;
                array_push( $arr_type_device,$device->type_id);
            }

            $type_device = TypeDevices::whereIn('id', $arr_type_device)
                        ->select('id', 'name_type')
                        ->get();

            $name_type_all = '';
            foreach($type_device as $key_type => $item_type){

                if($key_type>0){
                    $name_type_all = $name_type_all.', '.$item_type['name_type'];
                }else{
                    $name_type_all = $item_type['name_type'];
                }
            }

            $data['booking_devices_return'][$key]['name_type'] = $name_type_all;

            $data['booking_devices_return'][$key]['count'] = $count;
        }

        // dd($data);

        return view('pages.equipment.approval.index', $data);
    }


    public function equipment_manager_adddevice(){

        $data['img'] = \QrCode::format('svg')
                //  ->merge('img/t.jpg', 0.1, true)
                 ->generate('A simple example of QR code!');

        $data['type_devices'] = TypeDevices::get();
        return view('pages.equipment.approval.add_device', $data);
    }

    public function equipment_manager_adddevice_update(Request $request){

        $no_device = TypeDevices::where('id', $request['type_device'])
            ->first();

        $count_device = count($request['no_device']);


        // img
        $image = $request['pic_device'];
        $extension = $request['pic_device']->getClientOriginalExtension();
        $device_picture = 'device_' . time() . '.' . $extension;
        $location = public_path('/img/device/') . $device_picture;
        Image::make($image)->save($location);





        for ( $i=0 ; $i < $count_device ; $i++){

            // ต่อ string เลขครุภัณฑ์
            $no_device_full = $no_device['no_type'] .'-' . $request['no_device'][$i];

            // $data_qr = base64_decode('No_device=' .$no_device_full);
            $data_qr = 'device_no=' .$no_device_full;


            // QR Code
            $image_qr = \QrCode::format('svg')
                     ->generate($data_qr);

            $name_qr = 'QR-'.$no_device_full . '.svg';
            $part_qr = "img/qr_device/";
            file_put_contents($part_qr. $name_qr, $image_qr);

            Device::insert(
                [
                    'device_no' => $no_device_full,
                    'name' => $request->name_device,
                    'type_device' => $request->type_device,
                    'brand' => $request->brand_device,
                    'price' => $request->price_device,
                    'date_buy' => $request->date_buy,
                    'date_exp' => $request->date_exp,
                    'date_warranty_exp' => $request->date_warranty_end,
                    'content' => $request->content,
                    'img' => $device_picture,
                    'qrcode' => $name_qr,
                    'display' => 1,
                    'who_lend' => 0,
                    'created_at' => date('Y-m-d H:i:s')
                ]
            );

        }


        return('true');
        // return view('pages.equipment.approval.add_device');
    }


    public function equipment_manager_detail(Request $request){

        $data['device'] = Device::where('id', $request['device_id'])
            ->first();
        // dd($data);
        return view('pages.equipment.approval.device_detail', $data);
    }

    public function equipment_manager_edit_device(Request $request){
        $data['type_devices'] = TypeDevices::get();

        $data['device'] = Device::leftJoin('type_devices', 'devices.type_device', '=', 'type_devices.id')
            ->select('devices.id', 'name', 'brand', 'price', 'date_buy', 'date_exp', 'date_warranty_exp', 'content', 'img', 'name_type')
            ->where('devices.id', $request['device_id'])
            ->first();
        return view('pages.equipment.approval.edit_device', $data);
    }

    public function equipment_manager_edit_device_update(Request $request){

        if(!is_null($request['pic_device'])){
            $image = $request['pic_device'];
            $extension = $request['pic_device']->getClientOriginalExtension();
            $device_picture = 'device_' . time() . '.' . $extension;
            $location = public_path('/img/device/') . $device_picture;
            Image::make($image)->save($location);
        }else{
            $device_picture = $request['data_img'];
        }

        Device::where('id', $request['device_id'])
        ->update(
            [
                'name' => $request->name_device,
                'brand' => $request->brand_device,
                'price' => $request->price_device,
                'date_buy' => $request->date_buy,
                'date_exp' => $request->date_exp,
                'date_warranty_exp' => $request->date_warranty_end,
                'content' => $request->content,
                'img' => $device_picture,
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );

        return('true');
    }




    public function equipment_manager_return(Request $request){

        $data['booking_device'] = BookingDevices::where('id', $request['booking_id'])
            ->select('id', 'user_id', 'title', 'to', 'objective', 'pattern', 'devices', 'status', 'who_approve')
            ->first();

        $data['user'] = User::where('id', $data['booking_device']['user_id'])
            ->select('name', 'office_name', 'group', 'position', 'manager_name', 'tel_internal')
            ->first();

        $device_decode = json_decode($data['booking_device']['devices']);

        foreach ($device_decode as $key_type_device => $item_device_type){
            foreach($item_device_type->devices_id as $key_device => $item_device){
                $arr_device_id[] = $item_device;
            }
        }

        $data['device'] = Device::whereIn('id', $arr_device_id)
            ->select('id','device_no', 'name')
            ->get();

        return view('pages.equipment.approval.app_return', $data);
    }

    public function equipment_manager_return_update(Request $request){
        // dd($request['book_id']);

        $booking_device = BookingDevices::where('id', $request['book_id'])
            ->first();

        // dd($booking_device['devices']);

        $device_decode = json_decode($booking_device['devices']);

        $arr_device = [];
        foreach($device_decode as $key_type => $item_type){
            foreach($item_type->devices_id as $key_device => $item_device){
                array_push($arr_device,$item_device);
            }
        }

        Device::whereIn('id', $arr_device)
            ->update(['who_lend' => 0]);

        BookingDevices::where('id', $request['book_id'])
            ->update([
                'status' => 'approved',
                'who_approve' => Auth::user()->id,
            ]);

        return response()->json(['status'=> 'true']);
    }

    public function equipment_manager_lend(Request $request){

        // get ข้อมูลของการยืมตาม booking_id
        $data['bookking_deveces'] = BookingDevices::select('id','user_id', 'title', 'to', 'objective', 'pattern', 'devices', 'status' ,'who_approve')
            ->where('id', $request['booking_id'])
            ->get();

        $data['user'] = User::where('id', $data['bookking_deveces'][0]['user_id'])
            ->select('name', 'office_name', 'group', 'position', 'manager_name', 'tel_internal')
            ->first();


        // decode devices
        $arr_device = json_decode($data['bookking_deveces'][0]['devices']);

        // dd($arr_device[0]->devices_id);

        // dd($arr_device);

        // นับจำนวน device ทั้งหมดที่ยืม
        $count_deviceall = 0;
        foreach($arr_device as $key=> $item){
            // dd($item->devices_id);

            $data['type_devices'][$key]['show_deives'] = Device::whereIn('id', $item->devices_id)
                ->select('id','device_no','type_device')
                ->get();

            $type_devices = TypeDevices::where('id', $item->type_id)
                ->select('name_type')
                ->get();

            $data['type_devices'][$key]['deives'] = Device::where('type_device', $item->type_id)
                ->where('who_lend' , 0)
                ->select('id','device_no','type_device')
                ->get();
            // dd($test[0]->device_no);
            // $data['type_devices'][$key]['deives']

            $count_device = $item->count_device;

            $data['type_devices'][$key]['name'] = $type_devices[0]->name_type;
            $data['type_devices'][$key]['count'] = $count_device;

            $count_deviceall +=  $count_device;
        };
        $data['count_deviceall'] =  $count_deviceall;


        // dd($data);

        return view('pages.equipment.approval.app_lend', $data);

    }

    public function equipment_manager_lend_update (Request $request){
        $device_app = Device::whereIn('id', $request['no_device'])
            ->select('id','type_device')
            ->orderBy('type_device', 'ASC')
            ->get();


        $arr_device = [];
        $data_array = [];

        foreach($device_app as $key_device_app => $item_device_app){

            if(!in_array($item_device_app['type_device'], $data_array)){

                $data_array['type_id'] = (string)$item_device_app['type_device'];
                $data_array['devices_id'] = [];
                $data_array['count_device'] = "0";

                array_push($arr_device,$data_array);


            }

        }

        foreach($arr_device as $key_type => $item_type){

            foreach($device_app as $key_device_lend => $item_device_lend){
                if($item_type['type_id'] == $item_device_lend->type_device){
                    array_push($arr_device[$key_type]['devices_id'],$item_device_lend->id);
                }
            }

            $count_device = count($arr_device[$key_type]['devices_id']);

            $arr_device[$key_type]['count_device'] = (string)$count_device;
        }


        $devices_encode = json_encode($arr_device);


        BookingDevices::where('id', $request['booking_id'])
            ->update([
                'devices'=> $devices_encode,
                'status'=> 'approved',
                'who_approve'=> Auth::user()->id,
            ]);

        // dd($request['no_device']);

        // วนอัพเดท field who_lend table device ว่ามีใครเป็นคนยืม
        foreach ($request['no_device'] as $key=>$item){
            Device::where('id', $item)->update(['who_lend' => 1]);
        }


        return response()->json(['status'=> 'true']);
    }

    public function equipment_manager_lend_cancel (Request $request){
        BookingDevices::where('id', $request['booking_id'])
        ->update([
            'status'=> 'cancelled',
            'who_approve'=> Auth::user()->id,
        ]);
        return response()->json(['status'=> 'true']);
    }
}

