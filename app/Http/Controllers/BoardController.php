<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BoardController extends Controller
{
    public function index (){
        return view('/pages/board/board');

    }
    public function myboard(){
        return view('/pages/board/myboard');

    }
    public function content_board($id){
        return view('/pages/board/contentboard');

    }
    public function createboard(){
        return view('/pages/board/createboard');

    }
    public function group_board($namegroup){
        return view('/pages/board/group');

    }
}
