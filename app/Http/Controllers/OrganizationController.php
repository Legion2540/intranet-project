<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function index(){
        return view('/pages/organization/index');

    }
    public function section($namesection){
        return view('/pages/organization/section');

    }
    public function edit_section(){
        return view('/pages/organization/editsection');

    }
}
