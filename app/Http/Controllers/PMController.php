<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PMController extends Controller
{
    public function pm_inbox(){
        return view('pages/privatemessage/inbox');
    }
    public function pm_chat(){
        return view('pages/privatemessage/chat');
    }
}
