<?php

namespace App\Http\Controllers\Booking\Car;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function booking_car_index(){
        return view('pages/booking/booking');

    }


    public function form_car1(){
        return view('pages/booking/user/form_booking/car_1');

    }
    public function form_car2(){
        return view('pages/booking/user/form_booking/car_2');

    }
    public function form_car3(){
        return view('pages/booking/user/form_booking/car_3');

    }

}
