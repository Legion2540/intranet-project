<?php

namespace App\Http\Controllers\Booking\Car;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QueueCarController extends Controller
{
    public function index_queueCar(){
        return view('/pages/booking/approval/manage_queue_car/index');

    }
    public function queue_approve_1(){
        return view('/pages/booking/approval/manage_queue_car/approve_car_1');

    }
    public function queue_approve_2(){
        return view('/pages/booking/approval/manage_queue_car/approve_car_2');

    }
    public function queue_approve_3(){
        return view('/pages/booking/approval/manage_queue_car/approve_car_3');

    }
    public function queue_approve_4(){
        return view('/pages/booking/approval/manage_queue_car/approve_car_4');

    }
    public function queue_approve_5(){
        return view('/pages/booking/approval/manage_queue_car/approve_car_5');

    }
    public function history(){
        return view('/pages/booking/approval/manage_queue_car/history_car');

    }

    // Add
    public function add_car(){
        return view('/pages/booking/approval/manage_queue_car/add_car');

    }
    public function add_driver(){
        return view('/pages/booking/approval/manage_queue_car/add_driver');

    }
    // Add




    // Edit
    public function edit_car(){
        return view('/pages/booking/approval/manage_queue_car/edit_car');

    }
    public function edit_drive(){
        return view('/pages/booking/approval/manage_queue_car/edit_driver');

    }
    // Edit






    public function request_fix(){
        return view('/pages/booking/approval/manage_queue_car/request_fix');

    }
    public function sendForm(){
        return view('/pages/booking/approval/manage_queue_car/sendForm');

    }
    public function return_car(){
        return view('/pages/booking/approval/manage_queue_car/returnCar');

    }
}
