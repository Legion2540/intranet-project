<?php

namespace App\Http\Controllers\Booking\Room;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    public function index_device(){
        return view('pages.equipment.myequipment');

    }



    // Type User
    public function user_return(){
        return view('pages.equipment.user.return');

    }
    public function user_lend(){
        return view('pages.equipment.user.lend');

    }
    public function user_list_device(){
        return view('pages.equipment.user.lend_list');

    }
    public function user_check(){
        return view('pages.equipment.user.lend_check');

    }
    // Type User



    public function index_manger_device(){
        return view('pages.equipment.approval.index');

    }
    public function manager_add_device(){
        return view('pages.equipment.approval.add_device');

    }
    public function manager_return(){
        return view('pages.equipment.approval.app_return');

    }
    public function manager_lend(){
        return view('pages.equipment.approval.app_lend');

    }

}
