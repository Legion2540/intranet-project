<?php

namespace App\Http\Controllers\Booking\Room;

use App\BookingRoom;
use App\Food;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WelfareController extends Controller
{
    public function index_welfare()
    {
        $data = [];
        // $room = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->where('manager_approved', 'approved')->where('welfare_approved',null)->get();
        // $his_room = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->whereIn('welfare_approved', ['approved','cancelled'])->get();
        $data['room'] = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->where('manager_approved', 'approved')->where('welfare_approved', null)->get();
        $data['his_room'] = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->whereIn('welfare_approved', ['approved', 'cancelled'])->get();
        // $food = Food::paginate(5);
        $food = Food::all();

        foreach ($food as $keyfood => $data_food) {

            if ($food[$keyfood]['period'] == 'อาหารกลางวัน') {
                $data['lunch'][] = $food[$keyfood];
            } else if ($food[$keyfood]['period'] == 'อาหารว่าง') {
                $data['snack'][] = $food[$keyfood];
            } else if ($food[$keyfood]['period'] == 'เครื่องดื่ม') {
                $data['drink'][] = $food[$keyfood];
            }
        }

        foreach ($data['room'] as $keyroom => $data_room) {

            $data['room'][$keyroom]['device_id'] = json_decode($data_room['device_id']);
            $data['room'][$keyroom]['food_detail'] = json_decode($data_room['food_detail']);
        }

        foreach ($data['his_room'] as $keyroom => $data_his_room) {

            $data['his_room'][$keyroom]['device_id'] = json_decode($data_his_room['device_id']);
            $data['his_room'][$keyroom]['food_detail'] = json_decode($data_his_room['food_detail']);
        }

        // dd($data);
        return view('pages/booking/approval/welfare/index', $data);
    }

    public function approved_welfare(Request $request)
    {
        // dd($request->all());
        $room = BookingRoom::where('id', $request['book_id'])->first();

        $room['device_id'] = json_decode($room['device_id']);
        $room['food_detail'] = json_decode($room['food_detail']);

        return view('pages/booking/approval/welfare/approve_room', compact('room'));
    }

    public function approved_welfare_approved(Request $request)
    {

        BookingRoom::where('id', $request['book_id'])->update([
            'welfare_approved' => 'approved',
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        return back();
    }
    public function approved_welfare_cancelled(Request $request)
    {

        BookingRoom::where('id', $request['book_id'])->update([
            'status_booking' => 'cancelled',
            'welfare_approved' => 'cancelled',
            'reason' => $request['reason'],
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return back();
    }






    public function add_welfare(Request $request)
    {
        $data['period'] = $request['period'];
        return view('pages/booking/approval/welfare/creatfood', $data);
    }
    public function add_welfate_update(Request $request)
    {

        // dd($request->all());

        $list = [];
        foreach ($request['name_food'] as $key => $food) {
            $list = array($request['name_food'][$key], $request['price_food'][$key], 'ready');
        }


        $period = '';
        if ($request['period'] == 'snack') {
            $period = 'อาหารว่าง';
        } elseif ($request['period'] == 'drink') {
            $period = 'เครื่องดื่ม';
        } elseif ($request['period'] == 'lunch') {
            $period = 'อาหารกลางวัน';
        }


        $old_food = Food::where('period', $period)->first();

        if ($old_food != null) {

            $test = Food::where('period', $period)->first();
            $list_food = json_decode($test['list_food']);
            $list_food = array_merge($list_food, [$list]);

            Food::where('period', $period)->update([
                'list_food' => json_encode($list_food),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            $food = new Food();
            $food['period'] = $period;
            $food['list_food'] = json_encode($list);
            $food['status_food'] = 'ready';
            $food['created_at'] = date('Y-m-d H:i:s');
            $food['updated_at'] = date('Y-m-d H:i:s');
            $food->save();
        }



        return redirect('/welfare');
    }


    public function status_food(Request $request)
    {
        Food::where('id', $request['food_id'])->update([
            'status_food' => $request['status']
        ]);


        return true;
    }






    public function edit_welfare(Request $request)
    {
        $food = Food::where('id', $request['food_id'])->first();
        $food['list_food'] = json_decode($food['list_food']);
        return view('pages/booking/approval/welfare/editfood', compact('food'));
    }

    public function edit_welfare_update(Request $request)
    {
        // dd($request->all());

        $snack = [];
        $drink = [];
        $lunch = [];

        foreach ($request['type_food'] as $key => $type) {
            // dd($type);
            if ($type == 'อาหารกลางวัน') {
                $snack[] = [
                    '0' => $request['name_food'][$key],
                    '1' => $request['price_menu'][$key],
                    '2' => 'ready',
                ];
            } elseif ($type == 'เครื่องดื่ม') {

                $drink[] = [
                    '0' => $request['name_food'][$key],
                    '1' => $request['price_menu'][$key],
                    '2' => 'ready',
                ];
            } elseif ($type == 'อาหารว่าง') {
                $lunch[] = [
                    '0' => $request['name_food'][$key],
                    '1' => $request['price_menu'][$key],
                    '2' => 'ready',
                ];
            }
        }


        foreach ($request['type_food'] as $key => $type) {
            $old_food = Food::where('period', $type)->first();
            if ($old_food == null) {
                if ($type == 'อาหารกลางวัน') {
                    $food = new Food();
                    $food['period'] = $type;
                    $food['list_food'] = json_encode($snack);
                    $food['status_food'] = 'ready';
                    $food['created_at'] = date('Y-m-d H:i:s');
                    $food['updated_at'] = date('Y-m-d H:i:s');
                    $food->save();

                } elseif ($type == 'เครื่องดื่ม') {
                    $food = new Food();
                    $food['period'] = $type;
                    $food['list_food'] = json_encode($drink);
                    $food['status_food'] = 'ready';
                    $food['created_at'] = date('Y-m-d H:i:s');
                    $food['updated_at'] = date('Y-m-d H:i:s');
                    $food->save();

                } elseif ($type == 'อาหารว่าง') {
                    $food = new Food();
                    $food['period'] = $type;
                    $food['list_food'] = json_encode($lunch);
                    $food['status_food'] = 'ready';
                    $food['created_at'] = date('Y-m-d H:i:s');
                    $food['updated_at'] = date('Y-m-d H:i:s');
                    $food->save();

                }
            } else {

                if ($old_food['period'] == 'อาหารกลางวัน') {
                    Food::where('period', $old_food['period'])->update([
                        'list_food' => json_encode($snack),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                } elseif ($old_food['period'] == 'เครื่องดื่ม') {
                    Food::where('period', $old_food['period'])->update([
                        'list_food' => json_encode($drink),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                } elseif ($old_food['period'] == 'อาหารว่าง') {
                    Food::where('period', $old_food['period'])->update([
                        'list_food' => json_encode($lunch),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }



        return redirect('/welfare');
    }
}
