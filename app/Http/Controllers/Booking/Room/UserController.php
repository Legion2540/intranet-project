<?php

namespace App\Http\Controllers\Booking\Room;

use App\BookingCar;
use App\BookingRoom;
use App\Device;
use App\Food;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use GuzzleHttp;

class UserController extends Controller
{
    public function booking_room_index()
    {

        $data['booking_room'] = BookingRoom::where('form_status', 1)->orderBy('created_at', 'desc')->get();
        $data['booking_car'] = BookingCar::all();


        foreach ($data['booking_room'] as $keyroom => $room) {
            $data['booking_room'][$keyroom]['device_id'] = json_decode($room['device_id']);

            $data['booking_room'][$keyroom]['food_detail'] = json_decode($room['food_detail']);
        }



        // dd($booking_room);
        // รายการตคำขอ Objective
        // room_id โชว์ห้องที่เลือก จากหน้าเลือกห้อง ก่อนเข้า กรอกข้อมูลหน้าที่ 1
        // วันที่เริ่มใช้ start_date.start_time
        // ผู้อนุมัติ manager_approved
        // คำขอเพิ่มเติม device_id

        // {
        // status_booking (waiting approved cancelled)
        // form_booking 1
        // }
        return view('pages/booking/booking', $data);
    }


    public function choose_room(Request $request)
    {
        if (isset($request['book_id'])) {
            BookingRoom::where('id', $request['book_id'])->delete();
        }


        $data = [];
        $room_get = Room::where('type_room','meeting')->orderByRaw("FIELD(status,'ready','not ready') ASC")->paginate(5);
        $data['room'] = $room_get;
        foreach ($room_get as $key => $room) {

            $data['room'][$key]['img_room'] = json_decode($room['img_room']);
            $data['room'][$key]['device_id'] = json_decode($room['device_id']);
        }



        // $id_device = json_decode($room_get[0]->device_id);
        // foreach ($id_device as $id) {
        //     // $dev = Device::where('id', $id)->select('name', 'brand', 'device_no', 'type_device', 'display', 'time_use', 'time_own', 'img', 'content', 'status', 'qrcode', 'owner')->first();
        //     $dev = Device::where('id', $id)->first();
        //     $data['room_get'][0]['device'] = $dev;
        // }
        // dd($data);




        return view('pages/booking/room', $data);
    }






    public function choose_room_update(Request $request)
    {

        $data = BookingRoom::insertGetId([
            'user_id' => 1,
            'type_booking' => 'room',
            'room_id' => $request['room_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);



        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }

        return response()->json(['status' => $status, 'book_id' => $data]);
    }



    // Form
    public function form_room1_view(Request $request)
    {

        $data['book_id'] = $request['book_id'];
        $room = BookingRoom::where('id', $request['book_id']);
        $device = Room::where('id', $room->first()['room_id'])->first();

        $data['device'] = $device;
        $data['device']['device_id'] = json_decode($device['device_id']);

        if ($room->count() > 0) {
            $data['room'] = $room->first();
            // $data['room']['device_id'] = json_decode($data['room']['device_id']);
            $data['room']['food_detail'] = json_decode($data['room']['food_detail']);

            // dd($data);

            return view('pages/booking/user/form_booking/room_1', $data);
        } else {
            // echo '<script type="text/javascript">';
            // echo ' alert("มีข้อมูลผิดพลาด")';
            // echo '</script>';
            // return back();



            return view('pages/booking/user/form_booking/room_1', $data);
        }
    }

    public function form_room1_update(Request $request)
    {

        // dd($request->all());
        // $data = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->update([
        $data = BookingRoom::where('id', $request['book_id'])->update([
            'user_id' => 1,
            'title' => $request['title'],
            'request_to' => $request['request_to'],
            'room_request' => $request['room_request'],
            'device_id' => json_encode($request['device_room']),
            'staff' => $request['staff_device'],
            'objective' => $request['objective'],
            'start_date' => $request['start_date'],
            'start_time' => $request['start_time'],
            'end_date' => $request['end_date'],
            'end_time' => $request['end_time'],
            'place' => $request['place'],
            'manager_id' => $request['mamager_id'],
            'status_booking' => 'waiting',
            'form_status' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $status = 'false';

        if ($data != null) {
            $status = 'true';
        }

        return $status;
    }


    public function form_room2_view(Request $request)
    {
        // dd($request->all());

        $data['food'] = Food::where('status_food', '=', 'ready')->get();
        // $data['food'] = Food::all();
        // $food_ready = [];
        foreach ($data['food'] as $key => $food) {
            // $jd_food = json_decode($food['list_food']);
            // foreach ($jd_food as $keyJD => $jd_list) {

            //     if ($jd_food[$keyJD][2] !== 'not ready') {
            //         $food_ready[] = $jd_list;
            //     }
            // }
            // $data['food'][$key]['list_food'] = $food_ready;
            $data['food'][$key]['list_food'] = json_decode($food['list_food']);
        }

        $data['book_id'] = $request['book_id'];

        $room = BookingRoom::where('id', $request['book_id'])->select('food_detail')->first();

        if ($room['food_detail'] != null) {
            $data['room']['food_detail'] = json_decode($room['food_detail']);
            return view('pages/booking/user/form_booking/room_2', $data);
        } else {


            return view('pages/booking/user/form_booking/room_2', $data);
        }



        // return view('pages/booking/user/form_booking/room_2', $data);
    }

    public function form_room2_update(Request $request)
    {
        // dd($request->all());

        $array = [];
        if (isset($request['period']) && isset($request['set_food']) && isset($request['time_serve']) && isset($request['count_food']) && isset($request['price'])) {

            foreach ($request['period'] as $keyperiod => $period) {

                if ($request['period'] !== 'เลือกรายการ' && $request['set_food'] !== 'เลือกเมนูอาหาร' && $request['time_serve'] !== '' && $request['count_food'] !== '') {
                    $array[] = ([
                        'period' => $request['period'][$keyperiod],
                        'set_food' => $request['set_food'][$keyperiod],
                        'time_serve' => $request['time_serve'][$keyperiod],
                        'count_food' => $request['count_food'][$keyperiod],
                        'price' => $request['price'][$keyperiod]
                    ]);
                }
            }


            $food_detail = ([
                "appertizer" => ($request->appertizer == 'on') ? 'on' : 'off',
                "total" => $request->total,
                "detail_food" => $array,
            ]);


            // $data = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->update([
            $data = BookingRoom::where('id', $request['book_id'])->update([
                'food_detail' => json_encode($food_detail)
            ]);
        } else {

            // $data = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->update([
            $data = BookingRoom::where('id', $request['book_id'])->update([
                'food_detail' => null
            ]);
        }



        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }

        return $status;
    }


    public function form_room3_view(Request $request)
    {
        $data['book_id'] = $request['book_id'];
        // $data['room'] = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->where('form_status', 0)->orWhere('status_booking', 'waiting')->first();
        $data['room'] = BookingRoom::where('id', $request['book_id'])->where('form_status', 0)->where('status_booking', ['waiting', 'approved'])->first();

        $data['room']['device_id'] = json_decode($data['room']['device_id']);
        $data['room']['food_detail'] = json_decode($data['room']['food_detail']);


        return view('pages/booking/user/form_booking/room_3', $data);
    }


    public function form_room3_update(Request $request)
    {


        $data = BookingRoom::where('id', $request['book_id'])->update([
            'form_status' => 1,
            'status_booking' => 'waiting'
        ]);

        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }


        return $status;
    }



    public function form_room4_view(Request $request)
    {
        $data['book_id'] = $request['book_id'];
        $data['room'] = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->first();

        $data['room']['device_id'] = json_decode($data['room']['device_id']);
        $data['room']['food_detail'] = json_decode($data['room']['food_detail']);

        // dd($data);

        return view('pages/booking/user/form_booking/room_4', $data);
    }

    public function form_room4_update(Request $request)
    {

        $data = BookingRoom::where('id', $request['book_id'])->update([
            'status_booking' => 'cancelled'
        ]);



        $status = 'unsucess';
        if ($data == 1) {
            $status = 'success';
        }

        return $status;
    }
    // Form





















    //Booking Welfare
    public function form_welfare1_view(Request $request)
    {

        $data['book_id'] = $request['book_id'];
        $room = BookingRoom::where('id', $request['book_id']);
        if ($room->count() > 0) {

            $data['room'] = $room->first();
            // $data['room']['device_id'] = json_decode($data['room']['device_id']);
            $data['room']['food_detail'] = json_decode($data['room']['food_detail']);



            return view('/pages/booking/user/form_booking/vdoConference_1', $data);
        } else {

            return view('/pages/booking/user/form_booking/vdoConference_1', $data);
        }
    }
    public function form_welfare1_update(Request $request)
    {
        $data = BookingRoom::insertGetId([
            'user_id' => 1,
            'type_booking' => 'conference',
            'title' => $request['title'],
            'request_to' => $request['request_to'],
            'room_request' => $request['room_request'],
            'device_id' => json_encode($request['device_room']),
            'staff' => $request['staff_device'],
            'objective' => $request['objective'],
            'start_date' => $request['start_date'],
            'start_time' => $request['start_time'],
            'end_date' => $request['end_date'],
            'end_time' => $request['end_time'],
            'place' => $request['place'],
            'manager_id' => $request['mamager_id'],
            'status_booking' => 'waiting',
            'form_status' => 0,
        ]);


        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }


        return response()->json(['status' => $status, 'book_id' => $data]);
    }




    public function form_welfare2_view(Request $request)
    {

        $data['food'] = Food::where('status_food', '=', 'ready')->get();
        foreach ($data['food'] as $key => $food) {
            $data['food'][$key]['list_food'] = json_decode($food['list_food']);
        }
        $data['book_id'] = $request->book_id;

        return view('pages/booking/user/form_booking/vdoConference_2', $data);
    }
    public function form_welfare2_update(Request $request)
    {


        $array = [];

        if (isset($request['period']) && isset($request['set_food']) && isset($request['time_serve']) && isset($request['count_food']) && isset($request['price'])) {

            foreach ($request['period'] as $keyperiod => $period) {
                $array[] = ([
                    'period' => $request['period'][$keyperiod],
                    'set_food' => $request['set_food'][$keyperiod],
                    'time_serve' => $request['time_serve'][$keyperiod],
                    'count_food' => $request['count_food'][$keyperiod],
                    'price' => $request['price'][$keyperiod]
                ]);
            }


            $food_detail = ([
                "appertizer" => ($request->appertizer == 'on') ? 'on' : 'off',
                "total" => $request->total,
                "detail_food" => $array,
            ]);




            $data = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->update([
                'food_detail' => json_encode($food_detail)
            ]);
        } else {
            $data = BookingRoom::where('id', $request['book_id'])->update([
                'food_detail' => null
            ]);
        }


        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }

        return $status;
    }



    public function form_welfare3_view(Request $request)
    {

        $data['book_id'] = $request['book_id'];

        // $data['room'] = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->where('form_status', 0)->orWhere('status_booking', 'waiting')->first();
        $data['room'] = BookingRoom::where('id', $request['book_id'])->where('form_status', 0)->where('status_booking', ['waiting', 'approved'])->first();
        $data['room']['device_id'] = json_decode($data['room']['device_id']);
        $data['room']['food_detail'] = json_decode($data['room']['food_detail']);

        return view('pages/booking/user/form_booking/vdoConference_3', $data);
    }
    public function form_welfare3_update(Request $request)
    {

        // dd($request->all());

        $data = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->update([
            'form_status' => 1,
            'status_booking' => 'waiting'
        ]);

        $status = 'false';
        if ($data != null) {
            $status = 'true';
        }


        return $status;
    }



    public function form_welfare4_view(Request $request)
    {
        $data['book_id'] = $request['book_id'];
        $data['room'] = BookingRoom::where('user_id', 1)->where('id', $request['book_id'])->first();

        $data['room']['device_id'] = json_decode($data['room']['device_id']);
        $data['room']['food_detail'] = json_decode($data['room']['food_detail']);



        return view('pages/booking/user/form_booking/vdoConference_4', $data);
    }
    public function form_welfare4_update(Request $request)
    {

        $data = BookingRoom::where('id', $request['book_id'])->update([
            'status_booking' => 'cancelled'
        ]);



        $status = 'unsucess';
        if ($data == 1) {
            $status = 'success';
        }

        return $status;
    }
    //Booking Welfare




    public function testapi(Request $request)
    {
    }
}
