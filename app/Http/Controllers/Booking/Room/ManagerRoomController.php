<?php

namespace App\Http\Controllers\Booking\Room;

use App\BookingRoom;
use App\Food;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use Intervention\Image\Facades\Image;


class ManagerRoomController extends Controller
{
    public function  index_building()
    {

        $data = [];

        $data['room'] = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->where('manager_approved', 'approved')->where('room_approved', null)->get();
        $data['his_room'] = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->whereIn('room_approved', ['approved', 'cancelled'])->get();
        // $food = Food::paginate(5);
        $food = Food::all();

        $data['list_room'] = Room::all();


        foreach ($food as $keyfood => $data_food) {

            if ($food[$keyfood]['type_food'] == 'อาหารกลางวัน') {
                $data['lunch'][] = $food[$keyfood];
            } else if ($food[$keyfood]['type_food'] == 'อาหารว่าง') {
                $data['snack'][] = $food[$keyfood];
            } else if ($food[$keyfood]['type_food'] == 'เครื่องดื่ม') {
                $data['drink'][] = $food[$keyfood];
            }
        }

        foreach ($data['room'] as $keyroom => $data_room) {

            $data['room'][$keyroom]['device_id'] = json_decode($data_room['device_id']);
            $data['room'][$keyroom]['food_detail'] = json_decode($data_room['food_detail']);
        }

        foreach ($data['his_room'] as $keyroom => $data_his_room) {

            $data['his_room'][$keyroom]['device_id'] = json_decode($data_his_room['device_id']);
            $data['his_room'][$keyroom]['food_detail'] = json_decode($data_his_room['food_detail']);
        }

        foreach ($data['list_room'] as $keylistroom => $data_list_room) {
            $data['list_room'][$keylistroom]['img_room'] = json_decode($data_list_room['img_room']);
            $data['list_room'][$keylistroom]['device_id'] = json_decode($data_list_room['device_id']);
        }



        return view('pages/booking/approval/building/index', $data);
    }

    public function approved_room(Request $request)
    {
        $room = BookingRoom::where('id', $request['book_id'])->first();

        $room['device_id'] = json_decode($room['device_id']);
        $room['food_detail'] = json_decode($room['food_detail']);

        return view('pages/booking/approval/building/approve_room', compact('room'));
    }


    public function approved_room_approved(Request $request)
    {
        // dd($request->all());

        BookingRoom::where('id', $request['book_id'])->update([
            'room_approved' => 'approved',
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        return back();
    }

    public function approved_room_cancelled(Request $request)
    {

        BookingRoom::where('id', $request['book_id'])->update([
            'status_booking' => 'cancelled',
            'room_approved' => 'cancelled',
            'reason' => $request['reason'],
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        return back();
    }


    public function add_room()
    {
        return view('pages/booking/approval/building/creatroom');
    }

    public function add_room_update(Request $request)
    {

        if (isset($request['img_room'])) {

            $image = $request['img_room'];
            $extension = $request['img_room']->getClientOriginalExtension();
            $user_picture = 'room_' . time() . '.' . $extension;
            $location = 'img/room/' . $user_picture;
            if (file_exists('img/room/')) {
                $path = public_path($location);
                Image::make($image->getRealPath())->save($path);
            } else {
                mkdir('img/room/');
                $path = public_path($location);
                Image::make($image->getRealPath())->save($path);
            }
            Room::create([
                'type_room' => $request['type_room'],
                'no_room' => $request['number_room'],
                'name_room' => $request['name_room'],
                'level' => $request['level'],
                'name_building' => $request['no_building'],
                'size' => $request['size_room'],
                'opacity' => $request['member_count'],
                'img_room' => json_encode([$user_picture]),
                'device_id' => json_encode($request['device']),
                'place' => $request['place'],
                'status' => 'ready',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ]);
        } else {
            Room::create([
                'type_room' => $request['type_room'],
                'no_room' => $request['number_room'],
                'name_room' => $request['name_room'],
                'level' => $request['level'],
                'name_building' => $request['no_building'],
                'size' => $request['size_room'],
                'opacity' => $request['member_count'],
                'img_room' => json_encode(['insert-photo-material@2x.png']),
                'device_id' => json_encode($request['device']),
                'place' => $request['place'],
                'status' => 'ready',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ]);
        }





        return redirect('/building');
    }





    public function detail_room(Request $request)
    {
        // dd($request->all());
        $data = [];
        $room = Room::where('id', $request['room_id'])->first();
        $history = BookingRoom::where('room_id', $request['room_id'])->get();

        $room['img_room'] = json_decode($room['img_room']);
        $room['device_id'] = json_decode($room['device_id']);

        $data['room'] = $room;
        $data['history'] = $history;

        // dd($data);

        return view('pages/booking/approval/building/editroom', $data);
    }


    public function edit_room(Request $request){

        // "room_id" => "1"
        // "no_room" => "b125"
        // "name_room" => "่James"
        // "level" => "30"
        // "name_building" => "hq"
        // "status" => "ready"
        // "type_room" => "meeting"
        // "size" => "เล็ก"
        // "member_count" => "10"
        // "place" => "รัชดา"

        if (isset($request['room_img'])) {

            $image = $request['room_img'];
            $extension = $request['room_img']->getClientOriginalExtension();
            $user_picture = 'room_' . time() . '.' . $extension;
            $location = 'img/room/' . $user_picture;
            if (file_exists('img/room/')) {
                $path = public_path($location);
                Image::make($image->getRealPath())->save($path);
            } else {
                mkdir('img/room/');
                $path = public_path($location);
                Image::make($image->getRealPath())->save($path);
            }
            Room::where('id',$request['room_id'])->update([
                'type_room' => $request['type_room'],
                'no_room' => $request['no_room'],
                'name_room' => $request['name_room'],
                'level' => $request['level'],
                'name_building' => $request['name_building'],
                'size' => $request['size'],
                'opacity' => $request['member_count'],
                'img_room' => json_encode([$user_picture]),
                'place' => $request['place'],
                'status' => (isset($request['status']) ? $request['status'] : 'not ready'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ]);


        } else {
            Room::where('id',$request['room_id'])->update([
                'type_room' => $request['type_room'],
                'no_room' => $request['no_room'],
                'name_room' => $request['name_room'],
                'level' => $request['level'],
                'name_building' => $request['name_building'],
                'size' => $request['size'],
                'opacity' => $request['member_count'],
                'place' => $request['place'],
                'status' => (isset($request['status']) ? $request['status'] : 'not ready'),
                'updated_at'  => date('Y-m-d H:i:s'),
            ]);
        }


        return 'true';
    }






    public function status_device(Request $request)
    {

        // dd($request->all());

        Room::where('id', $request['room_id'])->update([
            'device_id' => json_encode($request['device_id'])
        ]);

        return "true";
    }
}
