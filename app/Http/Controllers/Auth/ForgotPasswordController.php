<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('pages/forgotpassword');
    }

    public function forgotpassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);


        if ($validator->fails()) {
            return back()->with("message", "Your email have not exists");
        }

        $token = str_random(64);


        User::where('email', $request['email'])->update([
            'remember_token' => $token,
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        Mail::send('pages.resetpassword', ['token' => $token], function ($message) use ($request) {
            $message->to($request['email']);
            $message->subject('Reset Password Notification');
        });

        return view('pages.resetpassword');
    }
}
