<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function redirectTo()
    {
        if (Auth::user()->typeUser()) {
            return '/admin/dashboard';
        } else {
            return '/home';
        }
    }

    public function index(){
        return view('pages/login');
    }

    public function login(Request $request)
    {
        // dd($request->all());

        // $this->validate($request, [
        //     'email' => 'required',
        //     'password' => 'required',
        // ], [
        //     'email.required' => 'กรุณากรอกอีเมลผู้ใช้งาน',
        //     'password.required' => 'กรุณากรอกรหัสผ่า'
        // ]);

        $user = User::where('email', $request['email'])->first();
        // if (!empty($user)) {

            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                Auth::login($user, true);

                return redirect()->intended('/');

            } else {


                User::insert([
                    'email' => $request['email'],
                    'password' => Hash::make($request['password']),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $user = User::where('email', $request['email'])->first();

                Auth::login($user, true);


                // return redirect()->intended('profile/setting');
                return redirect('/profile/setting');
            }


        // } else {

        //     return redirect()->back()->with('message', "Error!! Username or Password Incorrect. \nPlease try again.");
        // }





        // $username = 'ibusiness@ditp.go.th';
        // $password = 'Subsange123';
        // $server = "10.8.99.17";
        // $username = $request['email'];
        // $password = $request['password'];
        // $server = "10.8.99.17";


        // $ds = ldap_connect($server) or die('Could not connect to LDAP server.');
        // $r = ldap_bind($ds);

        // $sr = ldap_search($ds, "ou=mail,dc=linux,dc=co,dc=th", "mail=" . $username);
        // $info = ldap_get_entries($ds, $sr);
        // if ($info["count"] > 0) {
        //     $dn = $info[0]['dn'];
        //     $cn = $info[0]['cn'][0];
        //     $sn = $info[0]['sn'][0];

        //     $ldap_bind = @ldap_bind($ds, $dn, $password);


        //     if ($ldap_bind) {


                // if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

                //     Auth::login($user, true);
                //     return redirect()->intended('/');

                // } else {
                //     User::insert([
                //         'email' => $request['email'],
                //         'password' => Hash::make($request['password']),
                //         'created_at' => date('Y-m-d H:i:s'),
                //         'updated_at' => date('Y-m-d H:i:s')
                //     ]);
                //     $user = User::where('email', $request['email'])->first();
                //     Auth::login($user, true);


                //     return redirect()->intended('profile/setting');
                // }


        //     } else {

        //         return redirect()->back()->with('message',"Error!! Username or Password Incorrect. \nPlease try again.");

        //     }
        // }


        // ldap_close($ds);
    }


    public function logout(Request $request)
    {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
