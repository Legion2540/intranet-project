<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function question_index(){
        return view('pages/question/index');
    }

    public function question_create(){
        return view('pages/question/admin/create_question');
    }
}
