<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(){

        return view('/pages/news/news');
    }

    public function type_news($type_news){

        return view('/pages/news/typenews');
    }

    public function content_news($type_news,$id){
        
        return view('/pages/news/contentnews');

    }
}
