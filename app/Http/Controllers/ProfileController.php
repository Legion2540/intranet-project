<?php

namespace App\Http\Controllers;

use App\BookingDevices;
use App\TypeDevices;
use App\BookingCar;
use App\BookingRoom;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

class ProfileController extends Controller
{

    public function board()
    {
        return view('pages/profile/user/myBoard');
    }


    public function blog()
    {
        return view('pages/profile/user/myblog');
    }


    public function media()
    {
        return view('pages/profile/user/mymedia');
    }


    public function booking()
    {
        $data['room'] = BookingRoom::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $data['car'] = BookingCar::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        // $data['device'] = BookingDevices::where('user_id',1)->orderBy('created_at', 'desc')->get();
        $data['devices'] = BookingDevices::leftJoin('users', 'booking_devices.who_approve', '=', 'users.id')
            ->select(
                'booking_devices.id',
                'user_id',
                'title',
                'to',
                'objective',
                'pattern',
                'devices',
                'booking_devices.type',
                'status',
                'status_print',
                'form_status',
                'who_approve',
                'booking_devices.created_at',
                'booking_devices.updated_at',
                'users.name as user_name'
                )
            ->where('user_id', Auth::user()->id)
            ->orderBy('booking_devices.created_at', 'desc')
            ->get();

        if(count($data['devices']) > 0){
            foreach ($data['devices'] as $key_booking_device => $item_booking_device) {
                $data['devices'][$key_booking_device]['booking_id'] = $item_booking_device['id'];

                if(is_null($item_booking_device['user_name'])){
                    $data['devices'][$key_booking_device]['who_app'] = '-';
                }else{
                    $data['devices'][$key_booking_device]['who_app'] = $item_booking_device['user_name'];
                }

                $devices_decode = json_decode($item_booking_device['devices']);

                if($devices_decode != NULL){
                    $count_device_all = 0;
                    $arr_type_device = [];
                    foreach ($devices_decode as $key_device=> $item_device){
                        array_push( $arr_type_device,$item_device->type_id);

                        $count_device_all += $item_device->count_device;
                    }

                    $type_device = TypeDevices::whereIn('id', $arr_type_device)
                        ->select('id', 'name_type')
                        ->get();

                    $name_type_all = '';
                    foreach($type_device as $key_type => $item_type){

                        if($key_type>0){
                            $name_type_all = $name_type_all.', '.$item_type['name_type'];
                        }else{
                            $name_type_all = $item_type['name_type'];
                        }
                    }

                    $data['devices'][$key_booking_device]['name_type'] = $name_type_all;
                    $data['devices'][$key_booking_device]['count_device'] = $count_device_all;
                
                }else{
                    $data['devices'][$key_booking_device]['name_type'] = '-';
                    $data['devices'][$key_booking_device]['count_device'] = '-';
                }
                
            }
        }else{
            $data['devices'] = NULL;
        }

        foreach ($data['room'] as $keyroom => $room) {
            $data['room'][$keyroom]['device_id'] = json_decode($room['device_id']);

            $data['room'][$keyroom]['food_detail'] = json_decode($room['food_detail']);
        }

        // dd($data);

        return view('pages.profile.user.booking_user', $data);
    }



    public function product()
    {
        return view('pages/profile/user/myproducts');
    }


    public function group()
    {
        return view('pages/profile/user/mygroup');

    }



    public function approved()
    {
        $data = [];
        // $data['approve_room'] = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->orderBy('created_at', 'desc')->get();
        $approve_room = BookingRoom::where('status_booking', 'waiting')->where('form_status', 1)->orderBy('created_at', 'desc')->get();
        // $data['approve_car'] = BookingCar::where('user_id', )->orderBy('created_at', 'desc')->get();
        foreach ($approve_room as $keyroom => $room) {
            // dd($room->datauser);
            $data['approve_room'][$keyroom] = $room;
            $data['approve_room'][$keyroom]['device_id'] = json_decode($room['device_id']);

            $data['approve_room'][$keyroom]['food_detail'] = json_decode($room['food_detail']);
        }
        // dd($approve_room);

        return view('pages/profile/approval/appr_booking', $data);
    }










    public function setting()
    {
        return view('pages/profile/setting_profile');
    }



    public function updateprofile(Request $request)
    {

        User::where('id', Auth::user()->id)->update([
            'email' => $request['email'],
            'name' => $request["name"],
            'nickname' => $request["nickname"],
            'birthday' => $request["birthday"],
            'address' => $request["address"],
            'introduce' => $request["introducing"],
            'office_name' => $request["office"],
            'group' => $request["group"],
            'position' => $request["position"],
            'manager_name' => $request["manager"],
            'tel_internal' => $request["tel_internal"],

        ]);

        return redirect()->back();
    }
}
