<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KnowledgeController extends Controller
{
    public function index(){
        return view('pages/knowledgeCenter/knowledge');

    }

    public function myfiles(){
        return view('pages/knowledgeCenter/myfiles');

    }

    public function instorage(){
        return view('pages/knowledgeCenter/instorage');

    }

    public function content_knowledge($id_know){
        return view('pages/knowledgeCenter/details/pages');

    }

    public function createKnowledge(){
        return view('pages/knowledgeCenter/createBlog');

    }



}
