<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormsController extends Controller
{
    public function forms_index(){
        return view('pages/forms/user/forms');
    }

    public function forms_1(){
        return view('pages/forms/user/forms_1');
    }
    public function forms_2(){
        return view('pages/forms/user/forms_2');
    }
    public function forms_3(){
        return view('pages/forms/user/forms_3');
    }
    public function forms_4(){
        return view('pages/forms/user/forms_4');
    }
    public function forms_5(){
        return view('pages/forms/user/forms_5');
    }
    public function forms_6(){
        return view('pages/forms/user/forms_6');
    }
}
