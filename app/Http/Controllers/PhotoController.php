<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function photo_index(){
        return view('pages.photo.photo');
    }



    public function photo_gallery(){
        return view('pages.photo.user.mygallery');
    }

    public function photo_video(){
        return view('pages.photo.user.myvideo');
    }

    public function photo_files(){
        return view('pages.photo.user.myfiles');
    }


    public function photo_up_photo(){
        return view('pages.photo.user.upload.photo');
    }
    public function photo_up_album(){
        return view('pages.photo.user.upload.album');
    }
}
