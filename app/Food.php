<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'food';

    protected $fillable = [
        'period',
        'list_food',
        'status_food',
        'created_at',
        'updated_at'
    ];

}
