<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    protected $cast = ['time_use' => 'array', 'time_own' => 'array'];

}
