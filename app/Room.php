<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';

    protected $cast = [
        'device_id' => 'array'
    ];

    protected $fillable = [
        'id',
        'type_room',
        'no_room',
        'name_room',
        'level',
        'name_building',
        'size',
        'opacity',
        'img_room',
        'device_id',
        'place',
        'status',
        'created_at',
        'updated_at'

    ];
}
