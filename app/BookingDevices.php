<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDevices extends Model
{
    protected $table = 'booking_devices';

    protected $fillable = [
        'id',
        'user_id',
        'room_id',
        'manager_id',
        'device_id',
        'staff',
        'group_name',
        'title',
        'food_detail',
        'place',
        'objective',
        'request_to',
        'room_request',
        'status_booking',
        'form_status',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'manager_approved',
        'welfare_approved',
        'device_approved',
        'room_approved',
        'member_count',
        'created_at',
        'updated_at'
    ];



    
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

}
