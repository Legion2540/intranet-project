<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeDevices extends Model
{
    protected $table = 'type_devices';
}
