<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRoom extends Model
{
    protected $table = 'booking_room';

    protected $fillable = [
        'id',
        'user_id',
        'room_id',
        'manager_id',
        'type_booking',
        'title',
        'device_id',
        'staff',
        'food_id',
        'place',
        'objective',
        'request_to',
        'room_request',
        'status_booking',
        'form_status',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'created_at',
        'updated_at'];

    protected $cast = [
        'created_at' => 'date',
        'updated_at' => 'date'
    ];

    public function datauser(){
        // return $this->hasMany('app\User','user_id',' id');
        return $this->belongsTo('app\User','user_id');
    }

    public function detialRoom(){
        return $this->belongsTo('app\Room','room_id');
    }
}
