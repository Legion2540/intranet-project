<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCar extends Model
{
    protected $table = 'booking_car';

    protected $fillable = [
        'user_id',
        'approval',
        'start_date',
        'end_date',
        'type_car',
        'count_member',
        'purpose',
        'location',
        'status_approved',
        'car_id',
        'driver_id',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'updated_at' => 'datetime',
    ];
}
