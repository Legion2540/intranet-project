<style>
    .dropright {
        position: relative;
    }

    .dropright .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }

    .dropright-admin {
        position: relative;
    }

    .dropright-admin .dropdown-menu {
        top: 0;
        left: 140%;
    }

    .dropright-admin .menu_manage_service {
        top: -60%;
        left: 25%;
    }

    .dropright-admin .menu_manage_statistic {
        top: 30%;
        left: 25%;
    }


    .navbar {
        height: 75px;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 180%);
    }

    /* ul liown-item,
    button.dropdown-toggle {
        color: #4f72e5 !important;
        font-family: ;
        font-size: 14px;
        font-weight: 500;
    } */

    .search_header input#search,
    .search_header input#search:focus,
    .search_header input#search:active {
        width: 137px;
        fill: none;
        background: transparent;
        border: none;
        border-bottom: solid 0.5px #ffffff;
        font-size: 18px;
        margin-bottom: 16px;
        color: #ffffff;
        font-family: Kanit-Regular;

    }

    input#search::placeholder {
        width: 37px;
        height: 26px;
        margin: 9px 76px 8.8px 38px;
        opacity: 0.26;
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #ffffff;
    }

    #search {
        position: absolute;
    }


    #search_img {
        position: relative;
        left: 113px;

    }

    .lineUp {
        width: 1px;
        height: 26px;
        margin: 0px 0px 3px 120px;
        border: solid 0.5px #ffffff;
    }

    .lineUp2 {
        width: 1px;
        height: 26px;
        margin: 1px 0px 6px 16px;
        border: solid 0.5px #ffffff;

    }

    #nav_nameProfile {
        color: #ffffff;
    }

    #nav_profile {
        width: 40px;
        height: 40px;
        margin: 1px 0 2.8px 9px;
        object-fit: cover;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    #nav_profile2 {
        width: 40px;
        height: 40px;
        /* margin: 1px 0 2.8px 9px; */
        object-fit: cover;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    ul li a {
        padding-bottom: 0px !important;
        font-weight: bolder;

    }

    .layout_padding {
        padding: 0px 210px 0px 212px;
    }

    /* .nav-link {
        margin-right: 9px !important;
    } */

    .dropdown_profile {
        cursor: pointer;

    }

    .dropdown_profile_text {
        font-size: 14px;
        font-weight: 500;
        color: #4f72e5 !important;
        text-decoration: none !important;
    }

    /* .dropdown_profile :hover,
    .dropdown_profile_text:hover {
        color: #ffffff !important;
        background-color: #4f72e5 !important;
        transition: .2s;
        text-decoration: none;
    } */

    .dropdown_profile:hover {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
        text-decoration: none;
        transition: .2s;
    }

    .dropdown_profile .row:hover,
    .dropdown_profile .row .col-12:hover,
    .dropdown_profile .col-12 .col-10:hover,
    .dropdown_profile .col-10 .dropdown_profile_text:hover {
        color: #ffffff !important;
        text-decoration: none;
    }



    ul li a.nav-link {
        color: #ffffff !important;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
    }

    .arrow-bold-right {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 4px solid #4f72e5;
        height: 2px;
    }

    /* .menu_admin{
        width: 100%;
    }
    .menu_admin .dropdown-item{
        padding:0 12px 0 0;
    } */

</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid text-start layout_padding">
        <a href="/"><img src="/img/logo/group-2.svg" class="mx-5" alt=""></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">หน้าหลัก</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="/news" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">ข่าวสาร</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/news/hot_news/">Hot News </a></li>
                        <li><a class="dropdown-item" href="/news/factsheet/">FactSheet</a></li>
                        <li><a class="dropdown-item" href="/news/it_news/">IT News</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        จอง / ขอ
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/reserve/car">ระบบจองรถ</a></li>

                        <div class="dropright">
                            <button class="btn btn-light dropdown-toggle" style="padding-right:0; padding-left:23px;"
                                data-toggle="dropdown">ระบบจองห้องประชุม</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/reserve/room">ห้องประชุม</a>
                                <a class="dropdown-item" href="#">อาหารว่าง</a>
                                <a class="dropdown-item" href="#">Conferrence</a>
                            </div>
                        </div>


                        <li><a class="dropdown-item" href="#">คำของบประมาณ</a></li>
                        <li><a class="dropdown-item" href="/equipment/manager">ระบบบริหารครุภัณฑ์</a></li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/knowleadgeCenter">Knowledge Center</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="/board" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">Blog</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">สถานการณ์การค้าผลกระทบ COVID-19</a></li>
                        <li><a class="dropdown-item" href="#">เกษตรและอาหาร</a></li>
                        <li><a class="dropdown-item" href="#">อุตสาหกรรม</a></li>
                        <li><a class="dropdown-item" href="#">ไลฟ์สไตล์/แฟชั่น</a></li>
                        <li><a class="dropdown-item" href="#">อัญมณีและตลาดเฉพาะ</a></li>
                        <li><a class="dropdown-item" href="#">สุขภาพและความงาม</a></li>
                        <li><a class="dropdown-item" href="#">ธุรกิจบริการ</a></li>
                    </ul>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="#">Board</a>

                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">ทำเนียบพนักงาน</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">คลัง</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Photo</a></li>
                        <li><a class="dropdown-item" href="#">VDO</a></li>
                        <li><a class="dropdown-item" href="#">File</a></li>
                    </ul>
                </li>

                <li class="nav-item ml-4">
                    <div class="mb-2 ml-4 search_header">
                        <input type="text" name="search" id="search" placeholder="ค้นหา">
                        <img id="search_img" src="/img/icon/search-material.png" />
                    </div>
                </li>


                <li class="nav-item ml-2">
                    <div class="lineUp"></div>
                </li>

                <li class="nav-item ml-3 mb-1">
                    <img src="/img/icon/notifications-material.png" alt="">
                </li>

                <li class="nav-item ml-3 mb-1">
                    <img src="/img/icon/mail.png" alt="">
                </li>

                <li class="nav-item d-flex mr-4">
                    <div class="lineUp2"></div>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center drop_profile ml-5">
                <li class="nav-item ">
                    <div class="dropdown">

                        <button class="btn dropdown-toggle px-0" type="button" id="user_normal"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <span id="nav_nameProfile">{{ Auth::user()->name }}</span>
                            <img id="nav_profile"
                                src="{{ Auth::user()->pic_profile != null ? '/img/profile/' . Auth::user()->pic_profile : '/img/profile/profile-default.jpg' }}"
                                alt="">
                        </button>


                        @if (Auth::user()->type == 'employee')
                        <ul class="dropdown-menu p-0" aria-labelledby="user_normal" style="width:196px; left: -14%;">

                            <li class="d-flex justify-content-center p-1" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex align-content-center p-0">
                                        <div class="col-3 p-0 d-flex align-itmes-start mr-2">
                                            <img id="nav_profile2"
                                                src="{{ Auth::user()->pic_profile != null ? '/img/profile/' . Auth::user()->pic_profile : '/img/profile/profile-default.jpg' }}"
                                                alt="">
                                        </div>
                                        <div class="col-9 px-2 d-flex">
                                            <div class="row">
                                                <span class="text-start px-1" style="font-size: 12px;
                                                font-weight: bold;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->name }}</span>
                                                <span class="px-1" style="font-size: 12px;
                                                font-weight: 300;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->email }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center p-0">
                                        <div style="  width: 170px;
                                    height: 1px;
                                    margin: 10px 0px 0px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>

                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/account-circle-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/setting" class="dropdown_profile_text">โปรไฟล์</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/comment-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/board" class="dropdown_profile_text">บอร์ดของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/recording.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/blog" class="dropdown_profile_text">บทความของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/image-collections-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/media" class="dropdown_profile_text">คลังของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/book-material-copy.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/booking" class="dropdown_profile_text">คำขอ / จอง</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/widgets-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/product" class="dropdown_profile_text">สินค้าของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/group-work-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/group" class="dropdown_profile_text">กลุ่ม</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/settings-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/setting" class="dropdown_profile_text">ตั้งค่า</a>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center p-0">
                                        <div style="  width: 170px;
                                    height: 1px;
                                    margin: 3px 0px 1px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>
                                </div>
                            </li>


                            <li class="d-flex justify-content-center pt-1 pb-2" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/logout.png" alt="">
                                        </div>
                                        <div class="col-10">
                                            <a href="/logout" style="
                                            font-size: 14px;
                                            text-decoration:none;
                                            font-weight: 500;
                                            color: #d43112;">ออกจากระบบ</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        @elseif(Auth::user()->type == 'approval')
                        <ul class="dropdown-menu p-0" aria-labelledby="approval" style="width: 196px; left: -14%;">

                            <li class="d-flex justify-content-center p-1" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex align-content-center p-0">
                                        <div class="col-3 p-0 d-flex align-itmes-start mr-2">
                                            <img id="nav_profile2"
                                                src="{{ Auth::user()->pic_profile != null ? '/img/profile/' . Auth::user()->pic_profile : '/img/profile/profile-default.jpg' }}"
                                                alt="">
                                            <img src="/img/icon/group-71-copy-3.png" alt=""
                                                style="position: absolute;top: 31px;left: 4px;">
                                        </div>
                                        <div class="col-9 px-2 d-flex">
                                            <div class="row">
                                                <span class="text-start px-1" style="font-size: 12px;
                                                font-weight: bold;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->name }}</span>
                                                <span class="px-1" style="font-size: 12px;
                                                font-weight: 300;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->email }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center p-0">
                                        <div style="  width: 170px;
                                    height: 1px;
                                    margin: 10px 0px 0px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/account-circle-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/" class="dropdown_profile_text">โปรไฟล์</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/comment-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/board" class="dropdown_profile_text">บอร์ดของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/recording.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/blog" class="dropdown_profile_text">บทความของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/twice-correct.png" alt="">
                                        </div>
                                        <div class="col-7 pr-0 pl-1">
                                            <a href="/profile/approved" class="dropdown_profile_text">คำขออนุมัติ</a>
                                        </div>
                                        <div class="col-3 px-0 text-start">
                                            <img src="/img/icon/group-71-copy-3.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/image-collections-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/media" class="dropdown_profile_text">คลังของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/book-material-copy.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/booking" class="dropdown_profile_text">คำขอ / จอง</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/widgets-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/product" class="dropdown_profile_text">สินค้าของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/group-work-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/group" class="dropdown_profile_text">กลุ่ม</a>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/settings-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/setting" class="dropdown_profile_text">ตั้งค่า</a>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center p-0">
                                        <div style="width: 170px;
                                    height: 1px;
                                    margin: 3px 0px 1px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>
                                </div>
                            </li>

                            <li class="d-flex justify-content-center dropright-admin" style="width: 189px; cursor: pointer;">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/poll-material.png" alt="">
                                        </div>
                                        <div class="col-6 d-flex align-items-center pr-0 pl-1 dropdown">
                                            <a href="#" class="dropdown-item dropdown-toggle dropdown_profile_text p-0"
                                                data-toggle="dropdown"
                                                aria-expanded="false">รายงานและสถิติ</a>

                                                @if (Auth::user()->role == 'queue_car')

                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานการใช้รถยนต์</a>
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานสถิติขอใช้รถยนต์</a>
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานข้อมูลการเติมเชื้อเพลิง</a>
                                                </div>

                                                @elseif(Auth::user()->role == 'manager_car')
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานการใช้รถยนต์</a>
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานสถิติขอใช้รถยนต์</a>
                                                    <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานข้อมูลการเติมเชื้อเพลิง</a>
                                                </div>

                                                @elseif(Auth::user()->role == 'building')
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item dropdown_profile_text px-2" href="/building">รายงานการใช้ห้องประชุม</a>
                                                        <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานสถิติขอใช้ห้องประชุม</a>
                                                    </div>

                                                @elseif(Auth::user()->role == 'welfare')
                                                    <div class="dropdown-menu">
                                                        <a href="/welfare" class="dropdown-item dropdown_profile_text px-2" >รายงานการใช้ห้องประชุม</a>
                                                        <a class="dropdown-item dropdown_profile_text px-2" href="#">รายงานสถิติขอใช้ห้องประชุม</a>
                                                    </div>
                                                @endif


                                        </div>

                                        <div class="col-4 d-flex align-items-center">
                                            <img class="mr-2" src="/img/icon/group-71-copy-3.png" alt="">
                                            <div class="arrow-bold-right"></div>
                                        </div>

                                    </div>
                                    <div class="col-12 text-center p-0">
                                        <div style="width: 170px;
                                    height: 1px;
                                    margin: 3px 0px 1px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>
                                </div>
                            </li>


                            <li class="d-flex justify-content-center pt-1 pb-2" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/logout.png" alt="">
                                        </div>
                                        <div class="col-10">
                                            <a href="/logout" style="
                                            font-size: 14px;
                                            text-decoration:none;
                                            font-weight: 500;
                                            color: #d43112;">ออกจากระบบ</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        @elseif (Auth::user()->type == 'admin')
                        <ul class="dropdown-menu p-0" aria-labelledby="admin" style="width: 196px; left: -14%;">

                            <li class="d-flex justify-content-center p-1" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex align-content-center p-0">
                                        <div class="col-3 p-0 d-flex align-itmes-start mr-2">
                                            <img id="nav_profile2"
                                                src="{{ Auth::user()->pic_profile != null ? '/img/profile/' . Auth::user()->pic_profile : '/img/profile/profile-default.jpg' }}"
                                                alt="">
                                            <img src="/img/icon/group-71-copy-2.png" alt=""
                                                style="position: absolute;top: 31px;left: 4px;">
                                        </div>
                                        <div class="col-9 px-2 d-flex">
                                            <div class="row">
                                                <span class="text-start px-1" style="font-size: 12px;
                                                font-weight: bold;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->name }}</span>
                                                <span class="px-1" style="font-size: 12px;
                                                font-weight: 300;
                                                color: #4f72e5;
                                                width:90%;">{{ Auth::user()->email }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center p-0">
                                        <div style="  width: 170px;
                                    height: 1px;
                                    margin: 10px 0px 0px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>

                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/account-circle-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/" class="dropdown_profile_text">โปรไฟล์</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/comment-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/board" class="dropdown_profile_text">บอร์ดของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/recording.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/blog" class="dropdown_profile_text">บทความของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/image-collections-blue.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/media" class="dropdown_profile_text">คลังของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/book-material-copy.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/booking" class="dropdown_profile_text">คำขอ / จอง</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/widgets-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/product" class="dropdown_profile_text">สินค้าของฉัน</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/group-work-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/group" class="dropdown_profile_text">กลุ่ม</a>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li class="dropdown_profile d-flex justify-content-center" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/settings-material.png" alt="">
                                        </div>
                                        <div class="col-10 pr-0 pl-1">
                                            <a href="/profile/setting" class="dropdown_profile_text">ตั้งค่า</a>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center p-0">
                                        <div style="  width: 170px;
                                    height: 1px;
                                    margin: 3px 0px 1px 2px;
                                    object-fit: contain;
                                    border: solid 0.5px #ceced4;"></div>
                                    </div>
                                </div>
                            </li>

                            <li class="d-flex justify-content-center dropright-admin"
                                style="width: 189px; cursor: pointer;">
                                <div class="row w-100 ">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/settings-applications-material.png" alt="">
                                        </div>


                                        <div class="col-7 d-flex align-items-center pr-0 pl-1 dropdown">

                                            <a class="dropdown-toggle dropdown_profile_text justify-content-between"
                                                data-toggle="dropdown" aria-expanded="false">งานบริหารจัดการ</a>

                                            <div class="dropdown-menu menu_admin">
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">บริหารจัดการผู้ใช้</a>


                                                {{-- manage service --}}
                                                <a class="dropdown-item dropdown-toggle dropdown_profile_text px-2"
                                                    href="#" data-toggle="dropdown" aria-expanded="false">
                                                    <div
                                                        class="col-12 p-0 d-flex align-items-center justify-content-between">
                                                        <span>บริหารจัดการหมวดหมู่</span>
                                                        <div class="arrow-bold-right d-flex align-content-center"></div>
                                                    </div>
                                                </a>

                                                <div class="dropdown-menu menu_manage_service">
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่ากลุ่มบุคลากร</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าสิทธิ์การเข้าถึง</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าหมวดหมู่ห้องสนทนา</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าหมวดหมู่กระดานซื้อขาย</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าหมวดหมู่งบประมาณ</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าหมวดหมู่บล็อก</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">ตั้งค่าหมวดหมู่ Knowledge
                                                        Center</a>
                                                </div>
                                                {{-- manage service --}}




                                                {{-- statistic --}}
                                                <a class="dropdown-item dropdown-toggle dropdown_profile_text px-2"
                                                    href="#" data-toggle="dropdown" aria-expanded="false">
                                                    <div
                                                        class="col-12 p-0 d-flex align-items-center justify-content-between">
                                                        <span>รายงานและสถิติ</span>
                                                        <div class="arrow-bold-right d-flex align-content-center"></div>
                                                    </div>
                                                </a>

                                                <div class="dropdown-menu menu_manage_statistic">
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิติการใช้งานเว็บไซต์</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิติรถยนต์</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิติห้องประชุม</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิติงบประมาณ</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิติงานคุรุภัณฑ์</a>
                                                    <a class="dropdown-item dropdown_profile_text" href="#">รายงานสถิิติคลังความรู้</a>

                                                </div>
                                                {{-- statistic --}}


                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบบริหารรถ</a>
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบบริหารห้องประชุม</a>
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบกรองคำหยาบ/ลิงค์</a>
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบแบบสอบถาม</a>
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบประกาศ</a>
                                                <a class="dropdown-item dropdown_profile_text px-2"
                                                    href="#">ระบบจดหมายข่าว</a>
                                                <a class="dropdown-item dropdown_profile_text px-2" href="#">ระบบ
                                                    FAQ</a>
















                                            </div>




                                        </div>




                                        <div class="col-3 d-flex align-items-center pl-0">
                                            <img class="mr-2" src="/img/icon/group-71-copy-2.png" alt="">
                                            <div class="arrow-bold-right"></div>
                                        </div>
                                    </div>


                                    <div class="col-12 text-center p-0">
                                        <div
                                            style="width: 170px; height: 1px; margin: 3px 0px 1px 2px; object-fit: contain; border: solid 0.5px #ceced4;">
                                        </div>
                                    </div>
                                </div>


                                {{-- <ul class="dropdown-menu" id="manage"
                                        style="display: none; width:183px;line-height:1">
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">บริหารจัดการผู้ใช้</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" id="manage_category" href="#">
                                                <div class="d-flex">
                                                    <div
                                                        class="col-12 px-1 d-flex align-items-center justify-content-between">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">บริหารจัดการหมวดหมู่</span>
                                                        <div class="arrow-bold-right"></div>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" id="report_stat" href="#">
                                                <div class="d-flex">
                                                    <div
                                                        class="col-12 px-1 d-flex align-items-center justify-content-between">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานและสถิติ</span>
                                                        <div class="arrow-bold-right"></div>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบบริหารรถ</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบบริหารห้องประชุม</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบแบบฟอร์ม</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบกรองคำหยาบ/ลิงค์</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบแบบสอบถาม</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบประกาศ</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบจดหมายข่าว</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ระบบ
                                                            FAQ</span>
                                                    </div>
                                                </div>
                                            </a></li>

                                    </ul>

                                    <ul class="dropdown-menu" id="manage_category"
                                        style="display: none; width:218px;line-height:1">
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่ากลุ่มบุคลากร</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าสิทธิ์การเข้าถึง</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าหมวดหมู่ห้องสนทนา</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าหมวดหมู่กระดานซื้อขาย</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าหมวดหมู่งบประมาณ</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าหมวดหมู่บล็อก</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">ตั้งค่าหมวดหมู่
                                                            Knowledge Center
                                                        </span>
                                                    </div>
                                                </div>
                                            </a></li>








                                    </ul>

                                    <ul class="dropdown-menu" id="report_stat"
                                        style="display: none; width:189px;line-height:1">
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิติการใช้งานเว็บไซต์</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิิติรถยนต์</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิติห้องประชุม</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิติงบประมาณ</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิิติงานคุรุภัณฑ์</span>
                                                    </div>
                                                </div>
                                            </a></li>
                                        <li><a class="dropdown-item py-1 px-2" href="#">
                                                <div class="d-flex">
                                                    <div class="col-12 px-1">
                                                        <span
                                                            style="color:#4f72e5; font-weight:bold; font-size:14px;">รายงานสถิิติคลังความรู้</span>
                                                    </div>
                                                </div>
                                            </a></li>

                                    </ul> --}}


                            </li>


                            <li class="d-flex justify-content-center pt-1 pb-2" style="width: 189px">
                                <div class="row w-100">
                                    <div class="col-12 d-flex px-1">
                                        <div class="col-2 px-0 text-center">
                                            <img src="/img/icon/logout.png" alt="">
                                        </div>
                                        <div class="col-10">
                                            <a href="/logout" style="
                                            font-size: 14px;
                                            text-decoration:none;
                                            font-weight: 500;
                                            color: #d43112;">ออกจากระบบ</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>


                        @endif

            </ul>



            {{-- </div>
        </li>
        </ul> --}}






        </div>
    </div>
</nav>




<script>
    $(document).ready(function () {
        $('.dropright button').on("click", function (e) {
            e.stopPropagation();
            e.preventDefault();

            if (!$(this).next('div').hasClass('show')) {
                $(this).next('div').addClass('show');
            } else {
                $(this).next('div').removeClass('show');
            }
        });

        $('.dropright-admin a').on("click", function (e) {
            e.stopPropagation();
            e.preventDefault();

            if (!$(this).next('div').hasClass('show')) {
                $(this).next('div').addClass('show');
            } else {
                $(this).next('div').removeClass('show');
            }
        });


    });

</script>
