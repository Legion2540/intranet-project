<div class="shadow border-0 text-16" style="padding: 24px 15px 24px 15px;color:black;background-color:#ffffff">
    <div class="col-12 p-1" style="width: 208px;height: 32px;border-radius: 8px;background-blend-mode: multiply;background-image: linear-gradient(to bottom, #c5d0f5, #c5d0f5);">
        <div class="col-12 px-1">
            <span class="text-head-listforms">
                <img class="mr-2" src="/img/icon/add-circle-material.png" alt="">
                แบบฟอร์มคำขอ
            </span>
        </div>
    </div>
    <div class="col-12 pl-2 pr-1 my-2 d-flex">
        <div class="col-1 p-0"><img class="mr-2" src="/img/icon/form-black.png" alt=""></div>
        <div class="col-11 p-0 ml-2">
            <span class="text-list-forms">แบบฟอร์มขอมีบัตรข้าราชการ</span>
        </div>
    </div>
    <div class="col-12 pl-2 pr-1 my-2 d-flex">
        <div class="col-1 p-0"><img class="mr-2" src="/img/icon/form-black.png" alt=""></div>
        <div class="col-11 p-0 ml-2">
            <span class="text-list-forms">แบบมอบหมายงานการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
    </div>
    <div class="col-12 pl-2 pr-1 my-2 d-flex">
        <div class="col-1 p-0"><img class="mr-2" src="/img/icon/form-black.png" alt=""></div>
        <div class="col-11 p-0 ml-2">
            <span class="text-list-forms">แบบบันทึกผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
    </div>
    <div class="col-12 pl-2 pr-1 my-2 d-flex">
        <div class="col-1 p-0"><img class="mr-2" src="/img/icon/form-black.png" alt=""></div>
        <div class="col-11 p-0 ml-2">
            <span class="text-list-forms">แบบประเมินผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
    </div>
    <div class="col-12 pl-2 pr-1 my-2 d-flex">
        <div class="col-1 p-0"><img class="mr-2" src="/img/icon/form-black.png" alt=""></div>
        <div class="col-11 p-0 ml-2">
            <span class="text-list-forms">แบบรายงานการประเมินผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
    </div>
</div>
