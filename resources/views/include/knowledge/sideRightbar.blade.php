<style>
    .img_profile_knowledge {
        width: 73px;
        height: 73px;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    #postall {
        font-family: Kanit-ExtraLight;
        font-size: 18px;
        font-weight: bold;
        line-height: 0.83;
        color: #ffffff;
    }

    #countpost {
        font-size: 26px;
        font-weight: bold;
        line-height: 0.58;
        color: #ffffff;
    }

    .underline_box {
        width: 100%;
        height: 1px;
        opacity: 0.32;
        border: solid 1px #ffffff;
    }

    .text_files_right,
    .text_files_right:hover {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: 500;
        color: #ffffff;
        text-decoration: none;

    }

    .active_css {
        border-radius: 7px;
        box-shadow: inset 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        background-blend-mode: multiply;
        background-image: linear-gradient(to bottom, #d8d8d8, #d8d8d8);
    }

    .create_files_btn,.create_files_btn:hover {
        font-family: Kanit-ExtraLight;
        font-size: 20px;
        font-weight: 500;
        color: #4f72e5;
        text-decoration: none;
    }

</style>


<div class="col-12 py-3 px-4 d-flex align-items-center">
    <div class="col-4 px-0">
        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
            class="img_profile_knowledge">
    </div>
    <div class="col-8 px-0">
        <div class="col-12 text-start">
            <span id="postall">โพสต์ทั้งหมด :</span>
        </div>
        <div class="col-12 text-start mt-1">
            <span id="countpost">2 files</span>
        </div>
    </div>
</div>
<div class="underline_box"></div>

<div class="col-12 mt-3">
    <div class="col-12 d-flex myfiles py-1">
        <div class="col-2 mr-2">
            <img src="/img/icon/assignment-material.png" alt="">
        </div>
        <div class="col-10 text-start px-0">
            <a href="/knowleadgeCenter/myfiles" class="text_files_right">เอกสารของฉัน</a>
        </div>
    </div>

    <div class="col-12 mt-3 d-flex instorage py-1">
        <div class="col-2 mr-2">
            <img src="/img/icon/flag-material-white.png" alt="" class="mr-2">
        </div>
        <div class="col-10 text-start px-0">
            <a href="/knowleadgeCenter/instorage" class="text_files_right">เอกสารที่บันทึกในคลัง</a>
        </div>
    </div>

    <div class="col-12 mt-4 d-flex align-items-center"
        style="height:40px;border-radius: 3px;box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.28);background-color: #ffffff;">
        <div class="col-2 px-0">
            <img src="/img/icon/recording.png" alt="">
        </div>
        <div class="col-10 text-start px-0">
            <a href="/knowleadgeCenter/creatKnowledge" class="create_files_btn">โพสต์บทความ/เอกสาร</a>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        var pathname = window.location.pathname;
        if (pathname === "/knowleadgeCenter/myfiles") {
            $('.myfiles').addClass('active_css');
            return false;
        } else if (pathname === "/knowleadgeCenter/instorage") {
            $('.instorage').addClass('active_css');
            return false;
        }
        $('.myfiles,.instorage').removeClass('active_css');

    });

</script>
