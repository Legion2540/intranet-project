<style>
    .head_list_lastest {
        font-size: 16px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .no_content_list {
        font-family: Kanit-ExtraLight;
        font-size: 13px;
        text-align: center;
        color: #8a8c8e;
    }

    .content_list {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .underline {
        border-bottom: solid 0.5px #979797;
    }

</style>
<div class="col-12 text-start px-0">
    <span class="head_list_lastest">รายการที่เข้าดูล่าสุด</span>
</div>



@for($i=1; $i<=2; $i++)
<div class="row px-3 underline">
    <div class="col-12 d-flex align-items-center px-0 mt-2 mb-1">
        <div class="col-1 px-0"><span style="color:#4f72e5; font-size: 18px; font-weight: normal;">-</span></div>
        <div class="col-11 text-center px-0"><span class="content_list">ขั้นตอนการส่งออก :
                กล้องถ่ายรูปและชิ้นส่วน</span></div>
    </div>
</div>
@endfor




<div class="col-12 mt-5 mb-5" style="display:none;">
    <span class="no_content_list">ยังไม่มีรายการล่าสุด</span>
</div>
