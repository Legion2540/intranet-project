<style>
    .side_board {
        width: 373px;
    }

    #img_profile_board {
        width: 73px;
        height: 73px;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    .title_all_my_board {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #ffffff;
    }

    .count_all_my_board {
        font-size: 26px;
        font-family: Kanit-Regular;
        color: #ffffff;
    }

    .menu_side_board a {
        text-decoration: none;
    }

    .dash {
        margin: 0px 7px 0 7px;
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #ffffff;
    }

    .dash_market {
        margin: 10px 0px 10px 0px;
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #8a8c8e;
    }

    .my_board_title {
        font-size: 20px;
        color: #ffffff;
    }

    .text_new_member {
        font-family: Kanit-Regular;
        font-size: 22px;
        color: #4f72e5;
    }

    .create_board,
    .create_board:hover {
        width: 323px;
        height: 45px;
        border-radius: 3px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.28);
        background-color: #ffffff;
        text-decoration: none;
    }

    .text_group {
        font-family: Kanit-Regular;
        font-size: 22px;
        color: #ffffff;
    }

    .create_group {
        background-color: transparent;
        border: none;
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #ffffff;
    }

    .no_group {
        width: 317px;
        height: 60px;
        border-radius: 8px;
        background-color: rgba(234, 234, 234, 0.44);
    }

    .text_no_group {
        font-size: 13px;
        color: #ffffff;
    }

    .img_member {
        width: 73px;
        height: 72.8px;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 1px #ffffff;
        position: relative;
    }

    .new_member {
        width: 19px;
        height: 14px;
        font-family: Kanit-Regular;
        font-size: 8px;
        color: #ffffff;
        text-align: center;
        vertical-align: middle;
        border-radius: 3px;
        background-color: #ee2a27;
        position: absolute;
        top: 1px;
        transform: translateX(25px);

    }

    .name_member {
        font-size: 15px;
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .position_member {
        font-size: 12px;
        color: #4a4a4a;
    }

    .birthday_member {
        width: 40px;
        height: 40px;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    .name_birthday_member {
        font-family: Kanit-Regular;
        font-size: 17px;
        color: #4a4a4a;
    }

    .birthday {
        font-size: 13px;
        color: #4a4a4a;
    }

    .title_marketplace {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .name_product {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .name_sale {
        font-size: 17px;
        color: #4a4a4a;
    }

    .count_comment_producy {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .price_product {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4f72e5;
    }

    .text-create-group {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .name-front-input {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    #name_group_create {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #add_member {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .type_member {
        line-height: 1;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_member_more {
        width: 100%;
        height: 30px;
        border-radius: 2px;
        border: none;
        color: #ffffff;
        background-color: #4f72e5;
    }

    .warning_add_member {
        font-size: 12px;
        color: #4a4a4a;
    }

    .cancel_add_member {
        width: 114px;
        height: 31px;
        border-radius: 8px;
        border: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    .submit_add_member {
        width: 114px;
        height: 31px;
        border-radius: 8px;
        border: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 100%, #314d7b 57%);
    }

</style>


<div class="side_board" style="background-color:#4f72e5">
    <div class="col-12 d-flex align-items-center px-4 py-3">
        <div class="col-4">
            <img id="img_profile_board" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
        </div>
        <div class="col-8">
            <div class="col-12 px-0">
                <span class="title_all_my_board">บอร์ดของฉันทั้งหมด :</span>
            </div>
            <div class="col-12 d-flex align-items-center px-0">
                <span class="count_all_my_board">3</span>
                <span class="count_all_my_board">บอร์ด</span>
            </div>
        </div>
    </div>



    <div class="dash"></div>




    <div class="col-12 menu_side_board">
        <a href="/board/myboard">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/forum-material-copy-5.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">บอร์ดของฉัน</span>
                </div>
            </div>
        </a>


        <a href="#">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/forum-material-copy-5.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">บอร์ดที่ตอบ</span>
                </div>
            </div>
        </a>


        <a href="#">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/forum-material-copy-5.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">บอร์ดที่ถูกใจ</span>
                </div>
            </div>
        </a>



    </div>


    <div class="col-12 d-flex justify-content-center p-3">

        <a href="/board/createboard" class="create_board d-flex align-items-center justify-content-center">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="col-5 text-end p-0">
                    <img src="/img/icon/border-color-material-copy.png" alt="" class="mr-2">
                </div>

                <div class="col-7 text-start">
                    <span>สร้างบอร์ด</span>
                </div>
            </div>
        </a>
    </div>


    <div class="dash"></div>


    <div class="col-12 d-flex align-items-center px-4 pt-2 pb-2">
        <div class="col-6 px-0 d-flex align-items-center">
            <img src="/img/icon/visibility-material.png" alt="" class="mr-2">
            <span class="text_group">กลุ่ม</span>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end p-0">
            <img src="/img/icon/visibility-material.png" alt="" class="mr-2">
            <button class="create_group" type="button" data-bs-toggle="modal"
                data-bs-target="#exampleModal">สร้างกลุ่มใหม่</button>
        </div>
    </div>

    <div class="col-12 d-flex align-items-center  px-4 pt-1 pb-3">
        <div class="col-12 no_group d-flex align-items-center justify-content-center p-0">
            <span class="text_no_group">ยังไม่มีกลุ่ม</span>
        </div>
    </div>

</div>






<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 717px;">
        <div class="modal-content">
            <div class="modal-body">

                <div class="col-12">
                    <span class="text-create-group">สร้างกลุ่ม</span>
                </div>
                <div class="col-12 p-3">
                    <div class="col-12 p-0 d-flex align-items-center">
                        <div class="col-2">
                            <span class="name-front-input">ชื่อกลุ่ม</span>
                        </div>
                        <div class="col-10">
                            <input type="text" name="name_group_create" id="name_group_create">
                        </div>
                    </div>

                    <div class="col-12 p-0 d-flex align-items-center mt-3">
                        <div class="col-2">
                            <span class="name-front-input">เพิ่มสมาชิก</span>
                        </div>
                        <div class="col-6 pr-0">
                            <input type="text" name="add_member" id="add_member">
                        </div>
                        <div class="col-4">
                            <select class="form-select type_member" aria-label="Default select example">
                                <option selected>รูปแบบสมาชิก</option>
                                <option value="member">สมาชิกกลุ่ม</option>
                                <option value="admin">ผู้ดูแล</option>
                                <option value="owner">เจ้าของลุ่ม</option>

                            </select>
                        </div>
                    </div>


                    <div class="col-12 p-0 d-flex align-items-center mt-3">
                        <div class="col-2"></div>
                        <div class="col-10 d-flex align-items-center justify-content-center">
                            <button class="add_member_more">+ เพิ่มสมาชิก</button>
                        </div>
                    </div>
                    <div class="col-12 p-0 mt-3 d-flex align-items-center">
                        <div class="col-2"></div>
                        <div class="col-10 d-flex align-items-center justify-content-start">
                            <span class="warning_add_member">ระบุอีเมลสมาชิกที่ต้องการเพ่ิมในกลุ่ม</span>

                        </div>
                    </div>



                    <div class="col-12 p-0 d-flex align-items-center mt-3">
                        <div class="col-6 d-flex justify-content-end">
                            <button type="button" class="cancel_add_member">ยกเลิก</button>
                        </div>
                        <div class="col-6 ">
                            <button type="button" class="submit_add_member">สร้างกลุ่ม</button>
                        </div>

                    </div>

                </div>

            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
        </div>
    </div>
</div>






<div class="side_board p-3 mt-4" style="background-color:#e1e1e1">
    <div class="col-12 p-0">
        <span class="text_new_member">สมาชิกใหม่</span>
    </div>

    <div class="row d-flex">
        @for($i=0; $i<6; $i++) <div class="col-4 pl-0">
            <div class="col-12 p-0 d-flex align-items-center justify-content-center">
                <img class="img_member" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                <span class="new_member">ใหม่</span>
            </div>
            <div class="col-12 p-0 mt-2 d-flex align-items-center justify-content-center">
                <span class="name_member">Darisa</span>
            </div>
            <div class="col-12 p-0 text-center" style="line-height: 1;">
                <span class="position_member">ประชาสัมพันธ์และสื่อสารองค์กร</span>
            </div>
    </div>
    @endfor



    <div class="col-12 mt-3">
        <span class="text_new_member">สุขสันต์วันเกิด</span>
    </div>

    @for($i=0; $i<4; $i++) <div class="col-12 mt-1 px-3">
        <div class="col-12 p-2 d-flex align-items-center" style="border-radius: 3px; background-color: #ffffff;">
            <div class="col-2">
                <img class="birthday_member" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    alt="">
            </div>
            <div class="col-10">

                <div class="col-12 p-0">
                    <span class="name_birthday_member">Prakhan kordumronh</span>
                </div>
                <div class="col-12 d-flex align-items-center p-0">
                    <img src="/img/icon/cake-material.png" alt="" class="mr-2">
                    <span class="birthday">25 NOVEMBER</span>
                </div>

            </div>

        </div>
</div>
@endfor

</div>
</div>


<div class="side_board p-2 mt-4">
    <div class="col-12 d-flex align-items-center p-0 mb-2">
        <div class="col-6 p-0">
            <span class="title_marketplace">กระดานซื้อ-ขาย</span>
        </div>
        <div class="col-6 p-0 d-flex justify-content-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    @for($i=0; $i<5; $i++) <div class="col-12 d-flex align-items-center p-0">
        <div class="col-3 p-0">
            <img src="/img/icon/news10.png" alt="" style="object-fit: contain;width:81px;height:81px;">
        </div>
        <div class="col-9 p-0">
            <div class="col-12 pr-0">
                <span class="name_product">Asus ROG Strix G15 GL542LI-HN05</span>
            </div>
            <div class="col-12 pr-0">
                <span class="name_sale">Prakhan kordumrong</span>
            </div>
            <div class="col-12 d-flex align-items-center pr-0">
                <div class="col-6 d-flex align-items-center pl-0">
                    <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                    <span class="count_comment_producy">8</span>

                </div>
                <div class="col-6 pr-0 text-end">
                    <span class="price_product">21,900</span>
                </div>
            </div>
        </div>
</div>

<div class="dash_market"></div>

@endfor

</div>
