<div class="px-0 mt-3" style="width: 373px;">
    <div class="col-12 px-0">
        <span class="text-head-card">Popular</span>
    </div>
    @for ($i = 0; $i < 5; $i++) 
        <div class="col-12 mt-2 px-0 d-flex">
            <div class="col-3 px-0">
                <img class="img-pupular" src="/img/icon/rectangle.png" alt="" style="image-rendering: pixelated;">
            </div>
            <div class="col-9 pr-0">
                <div class="col-12 px-0">Asus ROG Strix G15 GL542LI-HN052T i7-10750H ประกันศูนย์</div>
                <div class="col-12 px-0 d-flex">
                    <div class="col-6 px-0">
                        <img src="/img/icon/comment-blue.png" alt="">
                        <span>8</span>
                    </div>
                    <div class="col-6 px-0 text-price d-flex justify-content-end">21,600</div>
                </div>
            </div>
        </div>
        <hr class="card">
    @endfor
</div>

<div class="px-0 mt-3" style="width: 373px;">
    <div class="col-12 px-0">
        <span class="text-head-card">New Arrival</span>
    </div>
    @for ($i = 0; $i < 5; $i++) 
        <div class="col-12 mt-2 px-0 d-flex">
            <div class="col-3 px-0">
                <img class="img-pupular" src="/img/icon/rectangle.png" alt="" style="image-rendering: pixelated;">
            </div>
            <div class="col-9 pr-0">
                <div class="col-12 px-0">Surface Pro 5 i5-7300U RAM 4GB SSD 128GB คีย์บอร์ด ปากกา ยกกล่อง</div>
                <div class="col-12 px-0 d-flex">
                    <div class="col-6 px-0">
                        <img src="/img/icon/comment-blue.png" alt="">
                        <span>8</span>
                    </div>
                    <div class="col-6 px-0 text-price d-flex justify-content-end">36,000</div>
                </div>
            </div>
        </div>
        <hr class="card">
    @endfor
</div>
