<div class="px-0" style="width: 373px;box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);background-color: #4f72e5;">
    <div class="col-12 d-flex" style="padding:21px 45px 0px 58px;">
        <div class="col-4 px-0">
            <img id="photo_profile" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg">
        </div>
        <div class="col-8 px-0 py-2">
            <span class="p-0 m-0 text-start" style="font-size: 18px;color:#ffffff;">สินค้าของฉันทั้งหมด :</span><br>
            <span class="p-0 m-0" style="font-size: 26px;color:#ffffff;line-height: 30px;">1 รายการ</span>
        </div>
    </div>
    <hr>
    <div class="col-12 px-0 pb-4">
        <div class="col-12 d-flex card-profile">
            <div class="col-1">
                <img src="/img/icon/favorite-material.png" alt="">
            </div>
            <div class="col-11">
                <a href="#" class="menu-list">สินค้าที่ถูกใจ</a>
            </div>
        </div>
        <div class="col-12 mt-2 d-flex card-profile">
            <div class="col-1">
                <img src="/img/icon/stock-material.png" alt="">
            </div>
            <div class="col-11">
                <a href="#" class="menu-list">สินค้าของฉัน</a>
            </div>
        </div>
        <div class="col-12 mt-2 d-flex card-profile">
            <div class="col-1">
                <img src="/img/icon/stock-close-material.png" alt="">
            </div>
            <div class="col-11">
                <a href="#" class="menu-list">สินค้าที่ปิดการขายแล้ว</a>
            </div>
        </div>
        <div class="col-12 mt-4 px-4 d-flex">
            <button class="form-control btn-sell">
                <img src="/img/icon/stock-close-material.png" alt="">
                <span class="pl-2">สร้างประกาศขาย</span>
            </button>
        </div>
    </div>
</div>