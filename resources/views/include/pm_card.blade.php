<div class="shadow">
    <div class="px-0 pt-3 col-12">
        <div class="col-12 d-flex align-items-center">
            <div class="px-0 col-9">
                <span class="text-head">
                    ทำเนียบผู้บริหาร
                </span>
            </div>
            <div class="px-0 col-3">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="row mx-0 mt-2 d-flex">
            @for ($i = 0; $i < 3; $i++)
                <div class="col-4 px-0 position-relative" style="text-align: center">
                    <img class="img-card" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    <span class="text-profile-name">Darisa</span>
                    <p class="px-1 text-profile-position" style="font-size: 10px;line-height: normal;">ประชาสัมพันธ์และสื่อสารองค์กร</p>
                    <div class="statusnew-position-absolute bg-danger">ใหม่</div>
                    <div class="status-position-absolute bg-success"></div>
                </div>
            @endfor
            @for ($i = 0; $i < 6; $i++)
                <div class="col-4 px-0 position-relative" style="text-align: center">
                    <img class="img-card" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    <span class="text-profile-name">Darisa</span>
                    <p class="px-1 text-profile-position" style="font-size: 10px;line-height: normal;">ประชาสัมพันธ์และสื่อสารองค์กร</p>
                </div>
            @endfor
        </div>
    </div>
    <div class="px-0 col-12">
        <div class="col-12 d-flex align-items-center">
            <div class="px-0 col-9">
                <span class="text-head">
                    สมาชิกใหม่
                </span>
            </div>
            <div class="px-0 col-3">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="row mx-0 mt-2 d-flex">
            @for ($i = 0; $i < 3; $i++)
                <div class="col-4 px-0 position-relative" style="text-align: center">
                    <img class="img-card" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    <span class="text-profile-name">Darisa</span>
                    <p class="px-1 text-profile-position" style="font-size: 10px;line-height: normal;">ประชาสัมพันธ์และสื่อสารองค์กร</p>
                    <div class="statusnew-position-absolute bg-danger">ใหม่</div>
                    <div class="status-position-absolute bg-success"></div>
                </div>
            @endfor
            @for ($i = 0; $i < 3; $i++)
                <div class="col-4 px-0 position-relative" style="text-align: center">
                    <img class="img-card" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    <span class="text-profile-name">Darisa</span>
                    <p class="px-1 text-profile-position" style="font-size: 10px;line-height: normal;">ประชาสัมพันธ์และสื่อสารองค์กร</p>
                </div>
            @endfor
        </div>
    </div>
    <div class="px-0 col-12">
        <div class="col-12 d-flex align-items-center">
            <div class="px-0 col-12">
                <span class="text-head">
                    สุขสันต์วันเกิด
                </span>
            </div>
        </div>
        <div class="row mx-0 px-3 pb-3 d-flex">
            @for ($i = 0; $i < 4; $i++)
                <div class="col-12 my-1 items-birdday d-flex">
                    <div class="col-3 px-0 py-2">
                        <img class="img-items-birdday" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    </div>
                    <div class="col-9 px-0 py-2">
                        <span class="text-bridday-name">Prakhan kordumrong</span>
                        <div class="d-flex align-items-baseline">
                            <img src="/img/icon/cake-material.png">
                            <p class="text-birdday-date">15 JANUARY</p>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>