
<div class="shadow border-0 text-light" style="background-color: #4f72e5;margin-bottom: 22px;">
    <div class="row py-4 px-5 m-0 ">
        <div class="col-4 pr-0">
            <img id="photo_profile" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg">
        </div>
        <div class="col-8 pl-4 py-1">
            <span class="p-0 m-0 text-start text-key-volte">คะแนนโหวต:</span><br>
            <span class="mt-4 text-value-volte">0 points</span>
        </div>
    </div>
    <hr class="mt-0 mb-0">
    <div class="row py-4 px-5 m-0">
        <div class="px-4 pb-3">
            <a class="p-0 pupulacard-alink d-flex" href="/photo/user/gallery">
                <div class="col-2 px-0">
                    <img src="/img/icon/photo-library-material-copy.png"/>
                </div>
                <div class="col-10 px-0 text-left">
                    <span class="pl-2">คลังภาพของฉัน</span>
                </div>
            </a>
        </div>
        <div class="px-4 pb-3">
            <a class="p-0 pupulacard-alink d-flex" href="/photo/user/video">
                <div class="col-2 px-0">
                    <img src="/img/icon/attach-video-material-white.png"/>
                </div>
                <div class="col-10 px-0 text-left">
                    <span class="pl-2">คลังวีดีโอของฉัน</span>
                </div>
            </a>
        </div>
        <div class="px-4">
            <a class="p-0 pupulacard-alink d-flex" href="/photo/user/files">
                <div class="col-2 px-0">
                    <img src="/img/icon/attach-file-material-white.png"/>
                </div>
                <div class="col-10 px-0 text-left">
                    <span class="pl-2">คลังไฟล์ของฉัน</span>
                </div>
            </a>
        </div>
    </div>
    <div class="row ms-3 mx-3 pb-3">
        <div class="btn-group" style="width: 400px">
            <button type="button" class="btn btn-upload" style="width: 300px">
                <img src="/img/icon/files-download-materail-blue.png" />
                <span class="ml-2">อัพโหลด</span>
            </button>
            <button type="button" class="btn btn-upload-dropdown dropdown-toggle dropdown-toggle-split"
                data-bs-toggle="dropdown" aria-expanded="false" style="width: 31px">
                <span class="visually-hidden">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">1</a></li>
                <li><a class="dropdown-item" href="#">2</a></li>
                <li><a class="dropdown-item" href="#">3</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="border-0 text-primary" style="background-color: #ffffff;margin-bottom: 22px;box-shadow: 2px 3px 10px 5px rgba(0, 0, 0, 0.1);">
    <div class="row ms-0 mx-4 pt-3">
        <div class="col-6 ps-1 px-1 d-flex align-items-center">
            <span class="text-head-popula">อัลบั้มรูปยอดนิยม</span>
        </div>

        <div class="col-6 ps-1 px-1 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row ms-0 mx-4 pt-3">
        <table class="table">
            <tbody>
                @for ($i = 0; $i < 3; $i++)
                    <tr>
                        <td class="p-1 w-25">
                            <img src="/img/icon/rectangle-copy-32.png" class="photo-pupular"
                                style="background-color: rgb(71, 71, 71); width:79px;height:79px;object-fit: cover;image-rendering: pixelated;" />
                        </td>
                        <td class="p-1 pl-3 w-75">
                            <span class="text-event-pupula">ประมวลภาพกิจกรรมงานเลี้ยงปีใหม่ส่งท้ายปี 2563</span>
                            <p class="mb-0 text-10">999 Download</p>
                        </td>
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>

{{-- pupular video --}}
<div class="border-0 text-primary" style="background-color: #ffffff;margin-bottom: 22px;box-shadow: 2px 3px 10px 5px rgba(0, 0, 0, 0.1);">
    <div class="row ms-0 mx-4 pt-3">
        <div class="col-6 ps-1 px-1 d-flex align-items-center">
            <span class="text-head-popula">วีดีโอยอดนิยม</span>
        </div>

        <div class="col-6 ps-1 px-1 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row ms-0 mx-4 pt-1">
        <table class="table table-borderless mb-1">
            <tbody>
                @for ($i = 0; $i < 2; $i++)
                    <tr>
                        <td class="p-0 px-1">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=vQWlgd7hV4A"
                                    allowfullscreen></iframe>
                            </div>
                            <div class="px-1 py-2 text-10">
                                <p class="text-event-pupula mb-0">งานปีใหม่ 2564</p>
                                <span class="text-body ">Draisa Patcharachot</span>
                                <p class="text-body ">18 มกราคม 2564</p>
                            </div>
                        </td>
                        <td class="p-0 px-1">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=vQWlgd7hV4A"
                                    allowfullscreen></iframe>
                            </div>
                            <div class="px-1 py-2 text-10">
                                <p class="text-event-pupula mb-0">กิจกรรมพบปะตัวแทนจากต่างประเทศ ณ กรุงไทเป</p>
                                <span class="text-body ">Draisa Patcharachot</span>
                                <p class="text-body ">18 มกราคม 2564</p>
                            </div>
                        </td>
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>

{{-- pupular Documents --}}
<div class="border-0 text-primary" style="background-color: #ffffff;margin-bottom: 22px;box-shadow: 2px 3px 10px 5px rgba(0, 0, 0, 0.1);">
    <div class="row ms-0 mx-4 pt-3">
        <div class="col-6 ps-1 px-1 d-flex align-items-center">
            <span class="text-head-popula">เอกสารยอดนิยม</span>
        </div>

        <div class="col-6 ps-1 px-1 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row ms-0 mx-4 pt-1 pb-2">
        <table class="table">
            <tbody>
                <tr>
                    <td class="p-1" style="width: 50px;">
                        <img src="/img/type_files/group-20-copy-9@2x.png" class="photo-documents" style="width: 40px;height : auto;"/>
                    </td>
                    <td class="p-1" style="width: auto;">
                        <span class="textover-documents">ประกาศวันหยุดตามประเพณี 2564</span>
                        <p class="text-10 mb-0">321 Download</p>
                    </td>
                </tr>
                <tr>
                    <td class="p-1" style="width: 50px;">
                        <img src="/img/type_files/group-20-copy-7@2x.png" class="photo-documents" style="width: 40px;height : auto;"/>
                    </td>
                    <td class="p-1" style="width: auto;">
                        <span class="textover-documents">ประกาศวันหยุดตามประเพณี 2564</span>
                        <p class="text-10 mb-0">321 Download</p>
                    </td>
                </tr>
                <tr>
                    <td class="p-1" style="width: 50px;">
                        <img src="/img/type_files/group-20-copy-11@2x.png" class="photo-documents" style="width: 40px;height : auto;"/>
                    </td>
                    <td class="p-1" style="width: auto;">
                        <span class="textover-documents">ประกาศวันหยุดตามประเพณี 2564</span>
                        <p class="text-10 mb-0">321 Download</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
