<style>
    .side_board {
        width: 373px;
    }

    #img_profile_board {
        width: 73px;
        height: 73px;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    .title_all_my_board {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #ffffff;
    }

    .count_all_my_board {
        font-size: 26px;
        font-family: Kanit-Regular;
        color: #ffffff;
    }

    .menu_side_board a {
        text-decoration: none;
    }

    .dash {
        margin: 0px 7px 0 7px;
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #ffffff;
    }

    .dash_market {
        margin: 10px 0px 10px 0px;
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #8a8c8e;
    }

    .my_board_title {
        font-size: 20px;
        color: #ffffff;
    }

    .text_new_member {
        font-family: Kanit-Regular;
        font-size: 22px;
        color: #4f72e5;
    }

    .create_board,
    .create_board:hover {
        width: 323px;
        height: 45px;
        border-radius: 3px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.28);
        background-color: #ffffff;
        text-decoration: none;
    }

    .text_group {
        font-family: Kanit-Regular;
        font-size: 22px;
        color: #ffffff;
    }

    .create_group {
        background-color: transparent;
        border: none;
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #ffffff;
    }

    .no_group {
        width: 317px;
        height: 60px;
        border-radius: 8px;
        background-color: rgba(234, 234, 234, 0.44);
    }

    .text_no_group {
        font-size: 13px;
        color: #ffffff;
    }

    .img_member {
        width: 73px;
        height: 72.8px;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 1px #ffffff;
        position: relative;
    }

    .new_member {
        width: 19px;
        height: 14px;
        font-family: Kanit-Regular;
        font-size: 8px;
        color: #ffffff;
        text-align: center;
        vertical-align: middle;
        border-radius: 3px;
        background-color: #ee2a27;
        position: absolute;
        top: 1px;
        transform: translateX(25px);

    }

    .name_member {
        font-size: 18px;
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .count_object {
        font-size: 16px;
        color: #4f72e5;
    }

    .birthday_member {
        width: 40px;
        height: 40px;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    .name_birthday_member {
        font-family: Kanit-Regular;
        font-size: 17px;
        color: #4a4a4a;
    }

    .birthday {
        font-size: 13px;
        color: #4a4a4a;
    }

    .title_marketplace {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .name_product {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .name_sale {
        font-size: 17px;
        color: #4a4a4a;
    }

    .count_comment_producy {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .price_product {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4f72e5;
    }

    .text-create-group {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .name-front-input {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    #name_group_create {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #add_member {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .type_member {
        line-height: 1;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_member_more {
        width: 100%;
        height: 30px;
        border-radius: 2px;
        border: none;
        color: #ffffff;
        background-color: #4f72e5;
    }

    .warning_add_member {
        font-size: 12px;
        color: #4a4a4a;
    }

    .cancel_add_member {
        width: 114px;
        height: 31px;
        border-radius: 8px;
        border: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    .submit_add_member {
        width: 114px;
        height: 31px;
        border-radius: 8px;
        border: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 100%, #314d7b 57%);
    }

    .GURU {
        width: 26px;
        height: 12px;
        /* margin: 1px 0 0 0; */
        border-radius: 3px;
        background-color: #0066b2;
        font-size: 7px;
        font-weight: bold;
        text-align: center;
        color: #ffffff;
    }

    .GURU-card {
        width: 38px;
        height: 17px;
        /* margin: 1px 0 0 0; */
        border-radius: 3px;
        background-color: #0066b2;
        font-size: 7px;
        font-weight: bold;
        text-align: center;
        color: #ffffff;
    }

    .GURU-card span{
        color: #ffffff;
        font-size: 12px;
        font-family: Kanit-Regular;
    }

    .fa-arrow-left{
        color:#ffffff;
    }

    .text-back{
        color: #ffffff;
        font-size: 20px;
    }

    .text-name-card{
        color: #ffffff;
        font-size: 26px;
        font-family: Kanit-Regular;
        line-height: 28px;
    }

    .text-key-card{
        color: #ffffff;
        font-size: 14px;
    }

    .text-val-card{
        font-size: 26px;
        color: #ffffff;
        font-family: Kanit-Regular;
    }


</style>


@if(Request::path() != '/blog/other')
<div class="side_board" style="background-color:#4f72e5">
    <div class="col-12 d-flex align-items-center px-4 py-3">
        <div class="col-4">
            <img id="img_profile_board" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
        </div>
        <div class="col-8">
            <div class="col-12 px-0">
                <span class="title_all_my_board">คะแนนโหวต :</span>
            </div>
            <div class="col-12 d-flex align-items-center px-0">
                <span class="count_all_my_board">0 &nbsp;</span>
                <span class="count_all_my_board">points</span>
            </div>
        </div>
    </div>



    <div class="dash"></div>



    <div class="col-12 menu_side_board">
        <a href="/blog/myblog">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/assignment-material.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">บล็อคของฉัน</span>
                </div>
            </div>
        </a>


        <a href="#">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/invalid-name-star-white.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">บทความที่โหวต</span>
                </div>
            </div>
        </a>


        <a href="/blog/myblog/1">
            <div class="col-12 d-flex align-items-center justify-content-center mt-2 mb-3">
                <div class="col-2 text-center px-0">
                    <img src="/img/icon/forum-material-copy-5.png" alt="">
                </div>
                <div class="col-10 text-start px-0">
                    <span class="my_board_title">การพูดคุย</span>
                </div>
            </div>
        </a>



    </div>


    <div class="col-12 d-flex justify-content-center p-3">

        <a href="/board/createboard" class="create_board d-flex align-items-center justify-content-center">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="col-5 text-end p-0">
                    <img src="/img/icon/border-color-material-copy.png" alt="" class="mr-2">
                </div>

                <div class="col-7 text-start">
                    <span>สร้างบอร์ด</span>
                </div>
            </div>
        </a>
    </div>

</div>





@elseif(Request::path() == '/blog/other')

<div class="side_board" style="background-color:#4f72e5">
    <a href="#">
        <div class="col-12 text-center py-3" style="border-bottom: solid 1px #ffffff;">
            <i class="fas fa-arrow-left"></i>
            <span class="text-back ml-2">กลับไปบล็อกของฉัน</span>
        </div>
    </a>


    <div class="col-12 d-flex align-items-center px-4 py-3" style="border-bottom: solid 1px #ffffff;">
        <div class="col-4">
            <img id="img_profile_board" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
        </div>
        <div class="col-8">
            <div class="col-12 px-0">
                <span class="text-name-card">Prakhan kordumrong</span>
            </div>
            <div class="col-12 GURU-card px-0 mt-2">
                <span>GURU</span>
            </div>
        </div>
    </div>
    <div class="col-12 px-0 d-flex">
        <div class="col-4 text-center py-4">
            <span class="text-key-card">คะแนนโหวต</span>
            <span class="text-val-card">127</span>
        </div>
        <div class="col-4 text-center py-4" style="border-left: solid 1px #ffffff;border-right: solid 1px #ffffff;">
            <span class="text-key-card">จำนวนบทความ</span>
            <span class="text-val-card">172</span>
        </div>
        <div class="col-4 text-center py-4">
            <span class="text-key-card">ยอดเข้าชม</span>
            <span class="text-val-card">2,647</span>
        </div>
    </div>
</div>

@endif






@if(Request::path() != 'blog/myblog' && Request::path() != 'blog/createblog')
<div class="side_board p-2 mt-4">
    <div class="col-12 d-flex align-items-center p-0 mb-2">
        <div class="col-6 p-0">
            <span class="title_marketplace">Highlight</span>
        </div>
        {{-- <div class="col-6 p-0 d-flex justify-content-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div> --}}
    </div>

    @for($i=0; $i<5; $i++) <div class="col-12 d-flex align-items-start p-0">
        <div class="col-3 p-0">
            <img src="/img/icon/news10.png" alt="" style="object-fit: contain;width:81px;height:81px;">
        </div>
        <div class="col-9 p-0">
            <div class="col-12 pr-0">
                <span class="name_product">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries
                    ในเนเธอร์แลนด์สูงกว่า 50%</span>
            </div>

            {{-- <div class="col-12 pr-0">
                <span class="name_sale">Prakhan kordumrong</span>
            </div>
            <div class="col-12 d-flex align-items-center pr-0">
                <div class="col-6 d-flex align-items-center pl-0">
                    <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                    <span class="count_comment_producy">8</span>

                </div>
                <div class="col-6 pr-0 text-end">
                    <span class="price_product">21,900</span>
                </div>
            </div> --}}

        </div>
</div>

<div class="dash_market"></div>

@endfor

</div>


<div class="side_board p-2 mt-4">
    <div class="col-12 d-flex align-items-center p-0 mb-2">
        <div class="col-6 p-0">
            <span class="title_marketplace">ใหม่ล่าสุด</span>
        </div>
        {{-- <div class="col-6 p-0 d-flex justify-content-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div> --}}
    </div>

    @for($i=0; $i<5; $i++) <div class="col-12 d-flex align-items-start p-0">
        <div class="col-3 p-0">
            <img src="/img/icon/news10.png" alt="" style="object-fit: contain;width:81px;height:81px;">
        </div>
        <div class="col-9 p-0">
            <div class="col-12 pr-0">
                <span class="name_product">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries
                    ในเนเธอร์แลนด์สูงกว่า 50%</span>
            </div>

            {{-- <div class="col-12 pr-0">
                <span class="name_sale">Prakhan kordumrong</span>
            </div>
            <div class="col-12 d-flex align-items-center pr-0">
                <div class="col-6 d-flex align-items-center pl-0">
                    <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                    <span class="count_comment_producy">8</span>

                </div>
                <div class="col-6 pr-0 text-end">
                    <span class="price_product">21,900</span>
                </div>
            </div> --}}

        </div>
</div>

<div class="dash_market"></div>

@endfor

</div>






<div class="side_board p-3 mt-4" style="background-color:#e1e1e1">
    <div class="col-12 d-flex align-items-center p-0 mb-2">
        <div class="col-6 p-0">
            <span class="text_new_member">นักเขียนยอดนิยม</span>
        </div>
        <div class="col-6 p-0 d-flex justify-content-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    <div class="row d-flex">
        @for($i=0; $i<3; $i++) <div class="col-12 d-flex align-items-start">
            <div class="col-3 d-flex align-items-center justify-content-center">
                <img class="img_member" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>

            <div class="col-9 mt-2">
                <div class="col-12 p-0 d-flex align-items-center">
                    <span class="name_member">Prakhan kordumrong</span>
                    <span class="guru ml-2">GURU</span>
                </div>
                <div class="col-12 p-0 d-flex aling-items-center" style="line-height: 2">
                    <div class="col-5 d-flex align-items-center justify-content-start pl-0">
                        <img src="/img/icon/grade-material.png" alt="" class="mr-2">
                        <span class="count_object">127 &nbsp;</span>
                        <span class="count_object">p.</span>
                    </div>

                    <div class="col-5 d-flex align-items-center justify-content-end pr-0">
                        <img src="/img/icon/assignment-material-copy-2.png" alt="" class="mr-2">
                        <span class="count_object">ดูบล็อค</span>
                    </div>

                </div>
            </div>

    </div>


    <div class="dash_market"></div>


    @endfor





</div>
</div>


<div class="side_board p-3 mt-4">
    <div class="col-12 d-flex align-items-center p-0 mb-2">
        <span class="text_new_member">Tag</span>
    </div>
    <div class="col-12 d-flex align-items-start" style="width: 100%;overflow: hidden">
        <span class="tag_blog"></span>
    </div>
</div>



@endif







<script>
    $(document).ready(function () {
        var tag = ['ส่งออก', 'นำเข้า ญี่ปุ่น', 'เสื้อผ้า', 'ราคาถูก', 'ผลไม้ไทย', 'อเมริกา', 'ไส้กรอกเยอรมัน',
            'เครื่องยนต์', 'เครื่องจักร', 'ร้านอาหารไทย', 'คลีนฟู้ด', 'ออแกนิกส์', 'รองเท้า', 'โรงงาน',
            'จีน', 'ผลิตภัณฑ์', 'ผ้ามัดย้อม', 'กระเป๋าสาน', 'ขั้นตอน', 'เยอรมัน'
        ];
        // var style = ["font-size:", "color:"];

        var size = ['14px', '20px', '25px'];

        var color = "#4a4a4a";

        var tags = Object.keys(tag);
        var sizes = Object.keys(size);

        var random_text = tags[Math.floor(Math.random() * tags.length)]
        var random_css = size[Math.floor(Math.random() * sizes.length)]

        $.each(tag, function (key, value) {
            $('.tag_blog').parent().append('<span class="tag_blog" style="font-size:' + random_css +
                '">' + value + '</span>')
            // $('.tag_blog').text(value);
            // $('.tag_blog').css('font-size', random_css);


        });



    });

</script>
