<style>
    .side_board {
        width: 358px;
    }

    .member-text {
        font-family: Kanit-Regular;
        font-size: 24px;
        color: #4f72e5;
    }

    .count-all-member {
        font-size: 14px;
        color: #4a4a4a;
    }

    .add-member {
        width: 100px;
        height: 26px;
        color: #ffffff;
        border-radius: 4px;
        align-items: center;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
    }

    .profile-pic-member {
        width: 73px;
        height: 72.8px;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 3px #ffffff;
        position: relative;
    }

    .status-member-sidebar {
        width: 19px;
        height: 19px;
        position: absolute;
        left: 32px;
        bottom: 36px;
    }

    .name-member-sidebar {
        font-size: 15px;
        line-height: 3;
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .readmore_member {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }

    .readmember-sidebar {
        border-top: solid 0.5px rgba(151, 151, 151, 0.58);
    }

    .title-menu-sidebar {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .image-cover-album {
        width: 79px;
        height: 79px;
        object-fit: cover;
    }

    .name-album-sidebar {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .count-download-album-sidebar {
        font-size: 10px;
        color: #4a4a4a;
    }

    .name-clip-sidebar {
        font-size: 14px;
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .uploader-sidebar,
    .date-uploade-sidebar {
        font-size: 10px;
        color: #4a4a4a;
    }

    .name-files-sidebar {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .count-download-files {
        font-size: 10px;
        color: #4a4a4a;
    }

</style>


<div class="side_board p-2" style="background-color:#e1e1e1">
    <div class="col-12 d-flex align-items-center px-1">
        <div class="col-3 px-0">
            <span class="member-text">สมาชิก</span>
        </div>
        <div class="col-4 px-0">
            <span class="count-all-membee">สมาชิกทั้งหมด 31</span>
        </div>
        <div class="col-5 px-0 text-end">
            <button type="button" class="add-member">+ เพิ่มสมาชิก</button>
        </div>
    </div>
    <div class="row d-flex align-items-center px-3">
        @for($i=0; $i<6; $i++) <div class="col-4 mt-3">
            <div class="col-12 p-0 text-center">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    class="profile-pic-member">
                <img src="/img/icon/error-outline-material.png" alt="" class="status-member-sidebar">

                <span class="name-member-sidebar">Darisa</span>
            </div>
    </div>
    @endfor
</div>




<div class="readmember-sidebar mt-4 mb-1"></div>

<div class="col-12 d-flex align-items-center justify-content-center ">
    <span class="arrow-down"></span>
    <span class="readmore_member">ดูเพิ่มเติม</span>
</div>


</div>


<div class="side_board p-2 mt-3" style="background-color:#ffffff;">
    <div class="col-12 d-flex align-items-center">
        <div class="col-6 px-0">
            <span class="title-menu-sidebar">อัลบั้มรูป</span>
        </div>
        <div class="col-6 text-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    @for($k=0; $k<3; $k++) <div class="col-12 d-flex align-items-center pb-2">
        <div class="col-3 px-0">
            <img src="/img/icon/rectangle-copy-3.png" alt="" class="image-cover-album">
        </div>
        <div class="col-9">
            <div class="col-12">
                <span class="name-album-sidebar">ประมวลภาพกิจกรรมงานเลี้ยงปีใหม่ ส่งท้ายปี 2563</span>
            </div>
            <div class="col-12">
                <span class="count-download-album-sidebar">321 Downloads</span>
            </div>
        </div>
</div>

@if($k != 2)
<div class="readmember-sidebar my-2"></div>
@endif
@endfor


</div>



<div class="side_board py-3 px-2 mt-3" style="background-color:#ffffff;">
    <div class="col-12 d-flex align-items-center">
        <div class="col-6 px-0">
            <span class="title-menu-sidebar">วิดีโอ</span>
        </div>
        <div class="col-6 text-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    @for($j=0; $j<2; $j++) <div class="col-12 d-flex align-items-between flex-wrap">
        <div class="col-6 px-0">
            <div class="col-12 p-0">

                <video width="150" height="120" poster="/img/icon/rectangle-copy-3.png" controls>
                    {{-- <source src="video.mp4" type="video/mp4">
                    <source src="video.ogg" type="video/ogg">
                    <source src="video.webm" type="video/webm">
                    <object data="video.mp4" width="470" height="255">
                    <embed src="video.swf" width="470" height="255"> --}}
                    </object>
                </video>

            </div>
            <div class="col-12 px-0 mb-2">
                <span class="name-clip-sidebar">งานปีใหม่ 2564</span>
            </div>
            <div class="col-12 px-0" style="line-height:1;">
                <span class="uploader-sidebar">Darisa Patcharachot</span><br>
                <span class="date-uploade-sidebar">18 มกราคม 2564</span>
            </div>
        </div>

        <div class="col-6 px-0">
            <div class="col-12 p-0">

                <video width="150" height="120" poster="/img/icon/rectangle-copy-3.png" controls>
                    {{-- <source src="video.mp4" type="video/mp4">
                    <source src="video.ogg" type="video/ogg">
                    <source src="video.webm" type="video/webm">
                    <object data="video.mp4" width="470" height="255">
                    <embed src="video.swf" width="470" height="255"> --}}
                    </object>
                </video>

            </div>
            <div class="col-12 px-0 mb-2">
                <span class="name-clip-sidebar">งานปีใหม่ 2564</span>
            </div>
            <div class="col-12 px-0" style="line-height:1;">
                <span class="uploader-sidebar">Darisa Patcharachot</span><br>
                <span class="date-uploade-sidebar">18 มกราคม 2564</span>
            </div>
        </div>
</div>
@endfor

</div>



<div class="side_board px-2 py-3 mt-3" style="background-color:#ffffff;">
    <div class="col-12 d-flex align-items-center">
        <div class="col-6 px-0">
            <span class="title-menu-sidebar">เอกสาร</span>
        </div>
        <div class="col-6 text-end">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    <div class="col-12 d-flex align-items-center p-2">
        <div class="col-1">
            <img src="/img/type_files/group-20-copy-9.png" alt="">
        </div>
        <div class="col-11" style="line-height:1;">
            <div class="col-12">
                <span class="name-files-sidebar">ประกาศวันหยุดตามประเพณีปี 2564</span>
            </div>
            <div class="col-12">
                <span class="count-download-files">321 Downloads</span>
            </div>
        </div>
    </div>
    <div class="readmember-sidebar my-1"></div>

    <div class="col-12 d-flex align-items-center p-2">
        <div class="col-1">
            <img src="/img/type_files/group-20-copy-7.png" alt="">
        </div>
        <div class="col-11" style="line-height:1;">
            <div class="col-12">
                <span class="name-files-sidebar">แบบฟอร์มลาพักร้อน</span>
            </div>
            <div class="col-12">
                <span class="count-download-files">267 Downloads</span>
            </div>
        </div>
    </div>
    <div class="readmember-sidebar my-1"></div>

    <div class="col-12 d-flex align-items-center p-2">
        <div class="col-1">
            <img src="/img/type_files/group-20-copy-6.png" alt="">
        </div>
        <div class="col-11" style="line-height:1;">
            <div class="col-12">
                <span class="name-files-sidebar">แบบรายงานประเมินประจำปี</span>
            </div>
            <div class="col-12">
                <span class="count-download-files">219 Downloads</span>
            </div>
        </div>
    </div>
    <div class="readmember-sidebar my-1"></div>

</div>
