<div class="container mt-4 calendar_room" style="background-color:white;max-width:1370px;">
    <div class="row p-3">
        <nav>
            <div class="nav nav-tabs border-0" id="nav-tab" role="tablist">
                <button class="nav-link  mr-2 change_booking_btn" id="rooms" data-bs-toggle="tab"
                    data-bs-target="#calendar_room" typ e="button" role="tab" aria-controls="calendar_room"
                    aria-selected="false">ปฎิทินการใช้ห้องประชุม</button>
            </div>
        </nav>

        <div class="tab-content p-2" id="nav-tabContent" style="background-color:#ffffff;">
            <div class="tab-pane fade show active m-0 show_calendar" id="calendar_room" role="tabpanel"
                aria-labelledby="calendar_room">
                <div class="col-12 d-flex align-content-center p-0">
                    <div class="col-4 d-flex align-content-center pr-0">
                        <div class="d-flex align-items-center">
                            <span class="arrow-booking left"></span>
                        </div>
                        <div class="px-2 w-auto ">
                            <span
                                style="font-family: Kanit-Regular; font-size: 25px; font-weight: bold; color: #4a4a4a;">December
                                2021</span>
                        </div>
                        <div class="d-flex align-items-center">
                            <span class="arrow-booking right"></span>
                        </div>

                        <img class="ml-3" src="/img/icon/date-range-material-copy-4.png"
                            style="object-fit:contain; width:20px" alt="">
                    </div>
                    <div class="col-8 d-flex align-content-center pl-0 justify-content-end pr-0">

                        <div class="d-flex align-items-center">
                            <input type="search" class="p-2 mr-1" name="calendar_room"
                                style="width: 320px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png"
                                style="position: absolute; left: 295px; top: 12px;" alt="">
                        </div>
                        <div class="d-flex align-items-center">
                            {{-- Filter Calendar img --}}
                            <span class="filter_calendar_room" data-bs-placement="bottom" data-bs-toggle="popover"
                                data-bs-title="Filters">
                                <img src="/img/icon/tune-material-copy-3.png" class="mx-2" alt="">
                            </span>

                            <select class="form-select form-select-1x filter_month" aria-label="Month">
                                <option selected>Month</option>
                                <option value="day">Day</option>
                                <option value="week">Week</option>
                                <option value="month">Month</option>
                            </select>

                            <select class="form-select form-select-1x filter_province" aria-label="เมืองทอง">
                                <option selected>เมืองทอง</option>
                                <option value="1">รัชดา</option>
                                <option value="2">ทองหล่อ</option>
                                <option value="3">สีลม</option>
                            </select>

                            <div>
                                <a href="/reserve/vdoconference/vdoConference1"><span class="btn_rent_room shadow">+ VDO Conference</span></a>
                                <a href="/reserve/room/choose"><span class="btn_rent_room shadow">+ จองห้องประชุม</span></a>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="room_calendar" class="mt-4"></div>
            </div>
        </div>



    </div>
</div>
