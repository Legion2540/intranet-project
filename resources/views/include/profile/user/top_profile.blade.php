<style>
    #detial_user {
        width: fit-content;
        height: 23px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.83;
        letter-spacing: normal;
        text-align: center;
        color: #4a4a4a;
    }

    #email_user {
        width: fit-content;
        height: 23px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.83;
        letter-spacing: normal;
        text-align: center;
        color: #4a4a4a;
    }

    .profile_nav {
        width: fit-content;
        height: 26px;
        /* margin: 0 24px 54px 14px; */
        object-fit: contain;
        /* background-color: #999999; */
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #999999;
    }

    .profile_nav:focus,
    .profile_nav:link {
        width: fit-content;
        height: 26px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #4f72e5;
        background-color: ##ededed;
    }

    .profile {
        width: 120px;
        height: 120px;
        object-fit: contain;
        border: solid 3px #ffffff;
        border-radius: 50%;
    }

</style>


<div class="col-12 d-flex justify-content-center mt-4">
    <img src="/img/icon/group-4.png" style="position: absolute;left: 400px;
    top: 20px;" alt="">
    <img class="profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
</div>
<div class="col-12 mt-3">
    <span>Wanchai Nagtang</span><br>
    <span>เจ้าหน้าที่การพัฒนาเว็บไซต์ & ดังที่สุด</span>
</div>

<div class="col-12">
    <span id="detial_user">สำนักประชาสัมพันธ์และสื่อสารองค์กร กลุ่มงานสื่อประชาสัมพันธ์</span><br>
    <span id="email_user">อีเมล : darisa.p@ditp.co.th เบอร์ภายใน : 0904</span>
</div>

<div class="col-12 nav d-flex justify-content-center mt-4 mb-4 nav_bar_profile">
    <a class="nav-link profile_nav" href="/profile/board"><b>บอร์ดของฉัน</b></a>
    <a class="nav-link profile_nav" href="/profile/blog"><b>บทความของฉัน</b></a>
    <a class="nav-link profile_nav" href="/profile/media"><b>คลังของฉัน</b></a>
    <a class="nav-link active profile_nav" href="/profile/booking" aria-current="page"><b>คำของ / จอง</b></a>
    <a class="nav-link profile_nav" href="/profile/products"><b>สินค้าของฉัน</b></a>
    <a class="nav-link profile_nav" href="/profile/group"><b>กลุ่มของฉัน</b></a>
</div>
