<style>
    .card-organize {
        width: 237px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .group-work,
    .group-work-span {
        text-decoration: none !important;
        font-size: 14px !important;
        font-weight: 500 !important;
        color: #4a4a4a !important;
        border: none !important;
    }

    .group-work:hover,
    .group-work:active,
    .group-work-span:hover,
    .group-work-span:active,
        {
        text-decoration: none !important;
        font-family: Kanit-Regular !important;
        font-size: 14px !important;
        color: #4f72e5 !important;
        border-radius: 1px !important;
        background-blend-mode: multiply !important;
        background-image: linear-gradient(to bottom, #c5d0f5, #c5d0f5) !important;
    }

</style>


<div class="card-organize p-2">

    <a href="/organization" class="group-work" style="text-decoration: none;">
        <div class="col-12 d-flex align-items-center mb-1 p-2" style="line-height:1;">
            <div class="col-2">
                <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
            </div>
            <div class="col-10 pr-0">
                <span class="group-work-span">ผู้บริหารระดับสูง</span>
            </div>
        </div>
    </a>



    <a href="/organization/section/middledepartment" class="group-work" style="text-decoration: none;">
        <div class="col-12 d-flex align-items-center mb-1 p-2" style="line-height:1;">
            <div class="col-2">
                <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
            </div>
            <div class="col-10 pr-0">
                <span class="group-work-span">สำนักบริหารกลาง 0901 (สบก.)</span>
            </div>
        </div>
    </a>


    <a href="#" style="text-decoration: none;">
        <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
            <div class="col-2">
                <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
            </div>
            <div class="col-10 pr-0">
                <span class="group-work-span">สถาบันพัฒนาผู้ประกอบการการค้ายุคใหม่ (สพม.) 0902</span>
            </div>
        </div>



        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักส่งเสริมการค้าสินค้าไลฟ์สไตล์ (สพม.) 0902</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ 1 (สพต. 1) 0909</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักส่งเสริมการค้าสินค้าเกษตรและอุตสาหกรรม (สกอ.) 0906</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักประชาสัมพันธ์และสื่อสารองค์กร (สปส.) 0904</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักพัฒนาและส่งเสริมธุรกิจบริการ (สพบ.) 0907</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักส่งเสริมนวัตกรรมและสร้างมูลค่าเพิ่มเพื่อการค้า (สนม.) 0908</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักพัฒนาตลาดและธุรกิจไทย ในต่างประเทศ 2 (สพต.2) 0910</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักสารสนเทศและการบริการการค้าระหว่างประเทศ(สสบ.) 0911</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">สำนักตลาดพาณิชย์ดิจิทัล(สตพ.) 0912</span>
                </div>
            </div>
        </a>


        <a href="#" style="text-decoration: none;">
            <div class="col-12 d-flex align-items-center mb-1 p-2 group-work" style="line-height:1;">
                <div class="col-2">
                    <img src="/img/icon/logo-man-black.png" alt="" class="logo-group">
                </div>
                <div class="col-10 pr-0">
                    <span class="group-work-span">กลุ่มตรวจสอบภายใน 0913</span>
                </div>
            </div>
        </a>
</div>




<script>
    $(document).ready(function () {
        $('.group-work').hover(function () {
            var img = $(this).find('img');
            img.attr('src', '/img/icon/account-circle-material.png').fadeIn(500);
        }, function () {
            var img = $(this).find('img');
            img.attr('src', '/img/icon/logo-man-black.png').fadeIn(500);
        });
    });

</script>
