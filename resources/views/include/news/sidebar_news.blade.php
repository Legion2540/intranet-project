<style>
    .side_news {
        width: 335px;
        /* margin-left: 10px; */
    }

    .most_popular {
        font-size: 18px;
        font-weight: bold;
        color: #4f72e5;
    }

    .tiltle_side_news {
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .dash {
        margin: 10px 0 10px 0;
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #8a8c8e;
    }

    .rank_news {
        font-size: 16px;
        font-weight: bold;
        color: #4f72e5;
    }

    .count_viewer {
        font-size: 12px;
        font-weight: 500;
        color: #4f72e5;
    }

</style>


<div class="side_news">
    <div class="col-12 px-0">
        <img src="/img/icon/news2.png" alt="" style="width:100%;height:100%; object-fit:contain;">
    </div>



    <div class="col-12 mt-4 mb-2 px-0">
        <span class="most_popular">MOST POPULAR</span>
    </div>
    <div class="col-12 px-0">
        <img src="/img/icon/news3.png" alt="" style="width:100%;height:100%;object-fit:contain;">
    </div>
    <div class="col-12 px-0">
        <span class="tiltle_side_news">Cloud Kitchen ผู้ปฏิวัติอุตสาหกรรมร้านอาหารยุคใหม่เมินทำเลทอง
            บนเดิมพัน</span>
    </div>



    <div class="dash"></div>


    <div class="col-12 px-0">
        <div class="col-12 d-flex py-2">
            <div class="col-4 px-0">
                <img src="/img/icon/news4.png" alt="" style="width: 98px; height: 97.3px; object-fit: contain;">
            </div>
            <div class="col-8 pr-0">
                <div class="col-12 d-flex justify-content-between align-items-center px-0">
                    <div class="col-6 px-0">
                        <span class="rank_news">#2</span>
                    </div>
                    <div class="col-6 px-0 text-end">
                        <img src="/img/icon/visibility-material-bule.png" alt="">
                        <span class="count_viewer">287</span>
                    </div>
                </div>
                <div class="col-12 px-0">
                    <span class="tiltle_side_news">ผู้ใช้ Clubhouse โปรดระวัง อาจถูกแบนหากถ่ายทอดเสียงไปช่องทางอื่น</span>
                </div>
            </div>
        </div>
    </div>


    <div class="dash"></div>


    <div class="col-12 px-0">
        <div class="col-12 d-flex py-2">
            <div class="col-4 px-0">
                <img src="/img/icon/news4.png" alt="" style="width: 98px; height: 97.3px; object-fit: contain;">
            </div>
            <div class="col-8 pr-0">
                <div class="col-12 d-flex justify-content-between align-items-center px-0">
                    <div class="col-6 px-0">
                        <span class="rank_news">#3</span>
                    </div>
                    <div class="col-6 px-0 text-end">
                        <img src="/img/icon/visibility-material-bule.png" alt="">
                        <span class="count_viewer">137</span>
                    </div>
                </div>
                <div class="col-12 px-0">
                    <span class="tiltle_side_news">ทิพย์ ส่งประกันภัยคุ้มครองฉีดวัคซีนป้อนกันโควิด-19 เริ่มต้นเพียง 70 บาท</span>
                </div>
            </div>
        </div>
    </div>


    <div class="dash"></div>


</div>
