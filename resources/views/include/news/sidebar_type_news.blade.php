<style>
    .side_news {
        width: 335px;
        /* margin-left: 10px; */
    }

    .tiltle_side_type_news {
        font-family: Kanit-Regular;
        font-size: 18px;
        font-weight: bold;
        color: #4f72e5;
    }

    .content_side_type_news {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .dash {
        height: 1.2px;
        object-fit: contain;
        border: solid 1px #8a8c8e;
    }

    .rank_news {
        font-size: 16px;
        font-weight: bold;
        color: #4f72e5;
    }

    .count_viewer {
        font-size: 12px;
        font-weight: 500;
        color: #4f72e5;
    }

</style>


<div class="side_news">
    <div class="col-12 px-0">
        <img src="/img/icon/news2.png" alt="" style="width:100%;height:100%; object-fit:contain;">
    </div>


    <div class="col-12 p-0">
            <div class="col-12 d-flex px-0 mt-3">
                <div class="col-1 px-0">
                    <img src="/img/icon/description-material.png" alt="">
                </div>
                <div class="col-11 px-0">
                    <span class="tiltle_side_type_news">ข่าวล่าสุด</span>
                </div>
            </div>


            @for($i=0; $i<4; $i++) <div class="col-12 px-0">
                <div class="col-12 d-flex py-2">
                    <div class="col-4 px-0">
                        <img src="/img/icon/news4.png" alt="" style="width: 80px; height: 80px; object-fit: contain;">
                    </div>
                    <div class="col-8 pr-0">

                        <span class="content_side_type_news">ผู้ใช้ Clubhouse โปรดระวัง
                            อาจถูกแบนหากถ่ายทอดเสียงไปช่องทางอื่น</span>

                    </div>
                </div>
        </div>


        <div class="dash"></div>
        @endfor
    </div>




    <div class="col-12 p-0">
        <div class="col-12 d-flex align-items-center px-0 mt-3">
            <div class="col-6 px-0">
                <span class="tiltle_side_type_news">HOT NEWS</span>
            </div>
            <div class="col-6 text-end px-0">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>


        @for($i=0; $i<3; $i++) <div class="col-12 px-0">
            <div class="col-12 d-flex py-2">
                <div class="col-4 px-0">
                    <img src="/img/icon/news4.png" alt="" style="width: 80px; height: 80px; object-fit: contain;">
                </div>
                <div class="col-8 pr-0">

                    <span class="content_side_type_news">ผู้ใช้ Clubhouse โปรดระวัง
                        อาจถูกแบนหากถ่ายทอดเสียงไปช่องทางอื่น</span>

                </div>
            </div>
        </div>


        <div class="dash"></div>
        @endfor
</div>



    <div class="col-12 p-0">
            <div class="col-12 d-flex align-items-center px-0 mt-3">
                <div class="col-6 px-0">
                    <span class="tiltle_side_type_news">FACT SHEET</span>
                </div>
                <div class="col-6 text-end px-0">
                    <img src="/img/icon/group-14-copy-2.png" alt="">
                </div>
            </div>


            @for($i=0; $i<3; $i++) <div class="col-12 px-0">
                <div class="col-12 d-flex py-2">
                    <div class="col-4 px-0">
                        <img src="/img/icon/news4.png" alt="" style="width: 80px; height: 80px; object-fit: contain;">
                    </div>
                    <div class="col-8 pr-0">

                        <span class="content_side_type_news">ผู้ใช้ Clubhouse โปรดระวัง
                            อาจถูกแบนหากถ่ายทอดเสียงไปช่องทางอื่น</span>

                    </div>
                </div>
        </div>


        <div class="dash"></div>
        @endfor
    </div>




    <div class="col-12 p-0">
            <div class="col-12 d-flex align-items-center px-0 mt-3">
                <div class="col-6 px-0">
                    <span class="tiltle_side_type_news">IT NEWS</span>
                </div>
                <div class="col-6 text-end px-0">
                    <img src="/img/icon/group-14-copy-2.png" alt="">
                </div>
            </div>


            @for($i=0; $i<3; $i++) <div class="col-12 px-0">
                <div class="col-12 d-flex py-2">
                    <div class="col-4 px-0">
                        <img src="/img/icon/news4.png" alt="" style="width: 80px; height: 80px; object-fit: contain;">
                    </div>
                    <div class="col-8 pr-0">

                        <span class="content_side_type_news">ผู้ใช้ Clubhouse โปรดระวัง
                            อาจถูกแบนหากถ่ายทอดเสียงไปช่องทางอื่น</span>

                    </div>
                </div>
        </div>


        <div class="dash"></div>
        @endfor
</div>







</div>
