<style>
    .nav_footer {
        height: 168px;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 119%);

    }

    .text_footer {
        margin: 0 0px 0px 0px;
        font-size: 16px;
        font-weight: bold;
        font-style: normal;
        color: #ffffff;

    }

</style>


<div class="container-fluid ">
    <div class="row justify-content-start align-content-center nav_footer pt-5 padding_footer">

        <div class="col-1 mr-4"></div>

        <div class="col-7 justify-content-center align-items-center p-0 ">
            <div class="row p-0">
                <div class="col-12 p-0" style="line-height: 0;">
                    <span class="text_footer">Department of International Trade Promotion, Ministry of Commerce,
                        Thailand.</span>
                </div>
                <div class="col-12 p-0 w-25" >
                    <span class="text_footer">Copyright © 2021, Department of International Trade Promotion, Ministry of
                        Commerce, Thailand. All rights reserved.
                    </span>

                </div>
            </div>
        </div>


        <div class="col-3 justify-content-start" style="line-height: 2">
            <img src="/img/icon/group-236.png" alt="">
        </div>

        <div class="col-1"></div>
    </div>
</div>
