@extends('layout/board')
@section('style')
<style>
    .head_content_board {
        font-family: Kanit-Regular;
        font-size: 28px;
        color: #4a4a4a;
    }

    .date_content_board {
        font-size: 14px;
        color: #4a4a4a;
    }

    .like_content_board {
        font-family: Kanit-Regular;
        font-size: 19px;
        color: #4a4a4a;
    }

    .content_board {
        font-size: 18px;
        color: #4a4a4a;
    }

    .head_tag {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .tag {
        font-size: 16px;
        color: #4f72e5;
    }

    .name_post_board {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .position_post_board {
        font-size: 16px;
        color: #4a4a4a;
    }

    .ask_q_board {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .count_comment_board {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .comment_board {
        border-radius: 8px;
        border: solid 0.5px #8a8c8e;
    }

    .user_comment {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .date_time {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .conment_user {
        font-size: 16px;
        color: #4a4a4a;
    }

</style>
@endsection


@section('content')
<div class="col-12 p-0" style="width: 948px; height: 456px;">
    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
        style="object-fit:cover;width: 100%; height: 100%;  border: solid 1px #979797;">
</div>

<div class="col-12 p-0 d-flex align-items-start mt-2">
    <div class="col-1 pt-2">
        <img src="/img/icon/comment-blue@2x.png" alt="">
    </div>
    <div class="col-11 px-0">
        <span class="head_content_board">ขอแจ้งสมาชิกทุกท่านร่วมทำบุญในกิจกรรม บริจาคเงินสมทบทุนโครงการ
            “DITP ปันน้ำใจให้แมวจร”</span>
    </div>
</div>
<div class="col-12 d-flex justify-content-between align-items-center">
    <div class="col-6" style="padding-left:65px;">
        <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
            style="width: 18; height: 14px; object-fit:contain;">
        <span class="date_content_board">16 มีนาคม 2564</span>

    </div>
    <div class="col-6 text-end">
        <img src="/img/icon/like.png" alt="" class="mr-2">
        <span class="like_content_board">33</span>
    </div>
</div>


<div class="row px-3">
    <div class="col-12 p-0">
        <p class="content_board">มีสัตว์จำนวนมากที่กำลังลำบากเนื่องจากการระบาดรอบใหม่ของโควิด-19
            หลายคนที่ได้รับผลกระทบจากการระบาดพยายามหาอาหารและเลี้ยงดูสัตว์เลี้ยงของตนเอง
            ในขณะเดียวกันความต้องการอาหารของสัตว์ที่อยู่ตามท้องถนนก็มีเพิ่มขึ้นต่อเนื่อง
            ศูนย์ช่วยเหลือสัตว์เลี้ยงหลายแห่งพยายามที่จะหาอาหารและสิ่งของต่างๆ
            เพื่อรองรับกับความต้องการที่มากขึ้นในขณะนี้
            เนื่องจากมีจำนวนการบริจาคลดลง
            การให้ความช่วยเหลือนี้เป็นอีกหนึ่งความมุ่งมั่นที่ไทยยูเนี่ยนและกลุ่มธุรกิจอาหารสัตว์เลี้ยงจะสนับสนุนชุมชนต่างๆ
            ของเรา นั่นก็คือ ผู้คนและสัตว์เลี้ยงของพวกเขา รวมถึงคนที่ให้ความช่วยเหลือสัตว์ป่วยและอดอยากตามท้องถนน</p>
        <p class="content_board">สำนักประชาสัมพันธ์และสื่อสารองค์กร จึงขอเป็นสื่อกลางในการร่วมบริจาคครั้งนี้
            โดยท่านสามารถนำเงินมาชำระได้ที่
            สำนักโดยตรง ณ อาคาร สำนักประชาสัมพันธ์ ชั้น 2 หรือโอนชำระผ่านบัญชีธนาคาร ชื่อบัญชี "มูลนิธิรักษ์แมว
            ปันน้ำใจให้แมวจร" Prompt Pay พร้อมเพย์/เลขผู้เสียภาษี 0993000388143 ได้ตั้งแต่วันนี้จนถึง 30 เมษายน 2574</p>
    </div>
    <div class="col-12 p-0 text-center my-4">
        <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
            style="width: 609px; height: 406px; object-fit: contain;">
    </div>



    <div class="col-12 p-0 mb-3 mt-4">
        <span class="head_tag">Tag :&nbsp;</span>
        <span class="tag">COVID19, ทำบุญ, กิจกรรมดีๆ, แมว, กิจกรรมเพื่อแมวจร</span>
    </div>


    <div class="col-12 p-0">
        <span style="  font-size: 14px; color: #4a4a4a;">โพสต์โดย : </span>
    </div>

    <div class="col-12 p-0 d-flex align-items-center mt-2">
        <div class="col-1 px-0">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                style="width: 62px; height: 61.8px; border: solid 2px #ffffff; border-radius: 50%;">
        </div>
        <div class="col-11">
            <div class="col-12">
                <span class="name_post_board">Darisa Patcharachot</span>
            </div>
            <div class="col-12">
                <span class="position_post_board">สำนักประชาสัมพันธ์และสื่อสารองค์กร</span>
            </div>
        </div>
    </div>


    <div class="col-12 mt-4"
        style="padding: 25px 33px 37px 36px; border-radius: 2px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); background-color: #ffffff;">
        <div class="col-12 d-flex align-items-center px-0">
            <div class="col-6 px-0">
                <span class="ask_q_board">คุณรู้สึกอย่างไรกับบาทความนี้</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-end">
                <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                <span class="count_comment_board">1</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-center mt-3 pr-0">
            <div class="col-1 p-0 mr-2">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    style="border-radius: 50%; width: 60px; height: 60px; border: solid 1px #d0d0d0;">
            </div>
            <div class="col-11 p-0 d-flex align-items-center">
                <textarea name="comment_board" class="comment_board" cols="90" rows="2"
                    style="resize: none;"></textarea>
            </div>
        </div>
    </div>




    <div class="col-12 px-4 py-3 mt-3"
        style="padding: 25px 33px 37px 36px; border-radius: 2px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); background-color: #ffffff;">

        <div class="col-12 px-3 py-2 d-flex align-items-center" style="border-radius: 8px; border: solid 0.5px #8a8c8e;">
            <div class="col-1 p-0 mr-2">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    style="border-radius: 50%; width: 60px; height: 60px; border: solid 1px #d0d0d0;">
            </div>
            <div class="col-11">
                <div class="col-12 px-0 d-flex justify-content-between">
                    <span class="user_comment">Chiraphon Thangthai</span>
                <img src="/img/icon/more-horiz-material-copy-7.png" alt="" style="object-fit: contain;">

                </div>
                <div class="col-12 px-0 d-flex">
                    <span class="date_time">4 ม.ค. 2564</span>
                    <span class="date_time">09:47</span>
                </div>
                <div class="col-12 px-0">
                    <span class="conment_user">น่าสนใจมากเลยครับ
                        ผมจะประสานงานกับทีมในสำนักให้ช่วยกันกระจายข่าวครับ</span>
                </div>
            </div>
        </div>


    </div>


</div>



@endsection


@section('script')
<script>

</script>
@endsection
