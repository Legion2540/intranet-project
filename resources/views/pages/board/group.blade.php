@extends('/layout/board_group')
@section('style')
<style>
    .title_photo {
        width: 1400px;
        height: 360px;
        padding: 15px 12px 13px 35px;
        border-radius: 2px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
    }

    .title-group {
        color: #ffffff;
        font-size: 41px;
        font-family: Kanit-Regular;
        position: absolute;
        bottom: 13px;
        left: 43px;
        text-shadow: 0 2px 4px rgba(0, 0, 0, 0.63);
    }

    .backward,
    .backward:hover {
        font-family: Kanit-Regular;
        font-size: 22px;
        color: #ffffff;
        text-decoration: underline #ffffff;
        position: absolute;
        top: 15px;
        left: 35px;
    }

    .upload-img {
        position: absolute;
        right: 12px;
        bottom: 15px
    }


    .card-post {
        width: 1005px;
        min-height: fit-content;
        border-radius: 1px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .text-head-post {
        color: #4a4a4a;
        font-size: 22px;
        font-family: Kanit-Regular;
    }

    .img-profile-create-post {
        width: 60px;
        height: 60px;
        border: solid 1px #d0d0d0;
        border-radius: 50%;
    }

    .img-profile {
        width: 60px;
        height: 60px;
        border: solid 1px #d0d0d0;
        border-radius: 50%;
        position: absolute;
    }

    .textarea-detel {
        width: 100%;
        height: 90px;
        border-radius: 8px;
        resize: none;
        border: solid 0.5px #999999;
    }

    .text-post {
        width: 763px;
        height: 103px;
        font-size: 22px;
        color: #4a4a4a;
        font-weight: 500;
    }

    .select-group {
        width: 250px;
        font-size: 14px;
        height: 25px;
        padding: 0;
        border: 0;
        font-family: Kanit-Regular;
    }

    .btn-post {
        width: 180px;
        height: 35px;
        border-radius: 8px;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.28);
        background-color: #4f72e5;
        color: #ffffff;
        font-size: 18px;
    }

    .list-post {
        width: 891px;
        height: fit-content;
        border-radius: 2px;
        padding: 35px;
        margin: 23px 0;
        border: solid 1px #999999;
    }

    .img-role {
        position: absolute;
        bottom: -17%;
        left: 30%;
    }

    .img-role-comment {
        position: absolute;
        top: 48px;
        left: 20px;
    }

    .text-name {
        color: #4a4a4a;
        font-size: 22px;
        font-family: Kanit-Regular;
    }

    .text-name-comment {
        color: #4a4a4a;
        font-size: 22px;
        font-family: Kanit-Regular;
    }

    .text-comment {
        color: #4a4a4a;
        font-size: 16px;
    }

    .name_file {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
        display: inline-block;
        width: 100%;
        white-space: nowrap;
        overflow: hidden !important;
        text-overflow: ellipsis;
    }

    .size_file {
        font-size: 10px;
        color: #4a4a4a;
    }

    .arrow-down {
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-top: 5px solid black;
        margin-right: 8px;
    }

</style>
@endsection
@section('title_image')
<div class="col-12 p-0">
    <a href="#" class="backward">
        < Sharing Board</a> <img src="/img/icon/rectangle-copy-3@3x.png" alt=""
            style="width: 100%; height:100%; object-fit:cover;">

            <img src="/img/icon/icon-camera.png" class="upload-img" alt="">
            <span class="title-group">กลุ่มงานสื่อประชาสัมพันธ์</span>
</div>



@endsection



@section('content')
<div class="col-12 px-0">
    <div class="col-12 card-post">
        <div class="col-12" style="padding: 24px 24px 10px 24px;">
            <span class="text-head-post">สร้างโพสต์</span>
        </div>
        <div class="col-12 d-flex px-3 pb-4">
            <div class="col-1 px-0 text-center">
                <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="" class="img-profile-create-post">
            </div>
            <div class="col-11 px-0">
                <div class="col-12">
                    <textarea class="textarea-detel"></textarea>
                </div>
                <div class="col-12 d-flex">
                    <div class="col-6 d-flex align-items-center">
                        <span style="color:#4a4a4a;font-size:14px;">การแสดงผล :</span>
                        <select class="form-select pl-2 select-group" aria-label="Default select example">
                            <option selected>สำนักประชาสัมพันธ์และสื่อสารองค์กร</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-6 px-0 text-end">
                        <img src="/img/icon/attach-file-material-copy.png" alt="">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="ml-3">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="ml-3">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="ml-3">
                        <button class="btn-post ml-3">โพสต์
                            <img src="/img/icon/keyboard-arrow-right-material.png" alt="" class="ml-2">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 card-post mt-4" style="padding: 45px 57px">
        <div class="col-12 list-post">
            <div class="col-12 px-0 d-flex">
                <div class="col-1 px-0">
                    <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="img-profile">
                    <img src="/img/icon/error-outline-material.png" alt="" class="img-role">
                </div>
                <div class="col-10">
                    <span class="text-name">Darisa Patcharachot</span><br>
                    <span class="text-time">4 ม.ค. 2564 09:00</span>
                </div>
                <div class="col-1 text-end">
                    <img src="/img/icon/more-horiz-material-copy-10.png" alt="">
                </div>
            </div>
            <div class="col-12 px-0 mt-3">
                <span class="text-post">สวัสดีค่ะ รบกวนทุกท่านอัพเดตชื่อผู้บังคับบัญชาที่เมนูตั้งค่าประวัติส่วนตัว
                    เพื่อแสดงผลในหน้า Organization Charge หากต้องการความช่วยเหลือกรุณาโทร 9123</span>
            </div>
            <div class="col-12 mt-3 d-flex align-items-center">
                <img src="/img/icon/forum-material.png" alt="" class="mr-2" style="object-fit: contain;">
                <span>2</span>
                <img src="/img/icon/like.png" alt="" class="ml-5 mr-2" style="object-fit: contain;">
                <span>10</span>
            </div>

            <div class="col-12 mt-2">
                <div class="col-12 d-flex"
                    style="width: 791px;border-radius: 4px;background-color: #ededed;padding: 13px 20px 20px 25px;">
                    <div class="col-1 px-0">
                        <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="img-profile">
                        <img src="/img/icon/error-outline-material.png" alt="" class="img-role-comment">
                    </div>
                    <div class="col-10">
                        <div class="col-12">
                            <span class="text-name-comment">Chiraphon Thangthai</span>
                        </div>
                        <div class="col-12">
                            <span class="text-time">4 ม.ค. 2564 09:47</span>
                        </div>
                        <div class="col-12 mt-2 pr-0">
                            <span class="text-comment">ดำเนินการเรียบร้อยแล้วครับ</span>
                        </div>
                    </div>
                    <div class="col-1 d-flex align-items-start pt-2">
                        <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="col-12 d-flex"
                    style="width: 791px;border-radius: 4px;background-color: #ededed;padding: 13px 20px 20px 25px;">
                    <div class="col-1 px-0">
                        <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="img-profile">

                        {{-- <img src="/img/icon/error-outline-material.png" alt="" class="img-role-comment"> --}}

                    </div>
                    <div class="col-10">
                        <div class="col-12">
                            <span class="text-name-comment">Prajongporn Tanmanee</span>
                        </div>
                        <div class="col-12">
                            <span class="text-time">4 ม.ค. 2564 09:47</span>
                        </div>
                        <div class="col-12 mt-2 pr-0">
                            <span class="text-comment">ขอบคุณสำหรับการแนะนำค่ะ ดำเนินการเรียบร้อยแล้วค่ะ</span>
                        </div>
                    </div>
                    <div class="col-1 d-flex align-items-start pt-2">
                        <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="col-12 d-flex"
                    style="width: 791px;border-radius: 4px;background-color: #ededed;padding: 13px 20px 20px 25px;">
                    <div class="col-1 px-0">
                        <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="img-profile">

                        <img src="/img/icon/error-outline-material.png" alt="" class="img-role-comment">

                    </div>
                    <div class="col-10">
                        <div class="col-12">
                            <span class="text-name-comment">Darisa Patcharachot</span>
                        </div>
                        <div class="col-12">
                            <span class="text-time">4 ม.ค. 2564 09:47</span>
                        </div>
                        <div class="col-12 mt-2 pr-0">
                            <span class="text-comment">ยินดีค่ะ หากท่านใดที่ยังไม่สามารถทำได้ สามารถแจ้งมาได้ที่เบอร์
                                0904 จะมีเจ้าหน้าที่ไปช่วยดูแลนะคะ
                                หรือสามารถศึกษาด้วยตัวเองได้ตามเอกสารแนบนี้ค่ะ ขอบคุณค่ะ</span>
                        </div>
                        <div class="col-12 d-flex align-items-center mt-2">
                            <div class="col-5 d-flex align-items-center p-2"
                                style="border-radius: 4px;border: solid 1px #dadada;background-color: #eaeaea;">
                                <div class="col-3 d-flex align-items-center pl-2">
                                    <img src="/img/type_files/group-20-copy-9.png" alt="">
                                </div>
                                <div class="col-8 px-0" style="line-height:1;">
                                    <div class="col-12 px-0">
                                        <span class="name_file">คู่มือการตั้งค่าใช้งานระบบ INTRANET</span>
                                    </div>
                                    <div class="col-12 px-0">
                                        <span class="size_file">57 KB</span>
                                    </div>
                                </div>
                                <div class="col-1 px-0">
                                    <img src="/img/icon/file-download-material-black.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1 d-flex align-items-start pt-2">
                        <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                    </div>
                </div>
            </div>


        </div>

        <div class="col-12 list-post">
            <div class="col-12 px-0 d-flex">
                <div class="col-1 px-0">
                    <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="img-profile">
                    <img src="/img/icon/error-outline-material.png" alt="" class="img-role">
                </div>
                <div class="col-10">
                    <span class="text-name">Darisa Patcharachot</span><br>
                    <span class="text-time">4 ม.ค. 2564 09:00</span>
                </div>
                <div class="col-1 text-end">
                    <img src="/img/icon/more-horiz-material-copy-10.png" alt="">
                </div>
            </div>
            <div class="col-12 px-0 mt-3">
                <span class="text-post">สวัสดีค่ะ รบกวนทุกท่านอัพเดตชื่อผู้บังคับบัญชาที่เมนูตั้งค่าประวัติส่วนตัว
                    เพื่อแสดงผลในหน้า Organization Charge หากต้องการความช่วยเหลือกรุณาโทร 9123</span>
            </div>
            <div class="col-12 px-0 mt-3">
                <img src="/img/icon/news8@3x.png" alt=""
                    style="object-fit:cover;width: 806px;height: 436px;border: solid 1px #979797;">
            </div>
            <div class="col-12 mt-3 d-flex align-items-center">
                <img src="/img/icon/forum-material.png" alt="" class="mr-2" style="object-fit: contain;">
                <span>2</span>
                <img src="/img/icon/like.png" alt="" class="ml-5 mr-2" style="object-fit: contain;">
                <span>10</span>
            </div>


        </div>

        <div class="col-12 list-post">
            <div class="col-12 px-0 d-flex">
                <div class="col-1 px-0">
                    <img src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="img-profile">
                    <img src="/img/icon/error-outline-material.png" alt="" class="img-role">
                </div>
                <div class="col-10">
                    <span class="text-name">Darisa Patcharachot</span><br>
                    <span class="text-time">4 ม.ค. 2564 09:00</span>
                </div>
                <div class="col-1 text-end">
                    <img src="/img/icon/more-horiz-material-copy-10.png" alt="">
                </div>
            </div>
            <div class="col-12 px-0 mt-3">
                <span class="text-post">ระบบอินทราเน็ตเปิดให้ใช้งานพื้นที่การทำงานแบบกลุ่มแล้ว ทุกท่านสามารถค้นหาไฟล์ภาพ
                    อัลบั้มรูป หรือวีดีโอ ที่คลังกลุ่มตรงเมนูด้านขวามือค่ะ</span>
            </div>
            <div class="col-12 mt-3 d-flex align-items-center">
                <img src="/img/icon/forum-material.png" alt="" class="mr-2" style="object-fit: contain;">
                <span>2</span>
                <img src="/img/icon/like.png" alt="" class="ml-5 mr-2" style="object-fit: contain;">
                <span>10</span>
            </div>


        </div>



        <div class="col-12 d-flex align-items-center justify-content-center readMore">
            <span class="arrow-down"></span>
            <span style="cursor: pointer; font-family:Kanit-Regular;  font-size: 12px;">ดูเพิ่มเติม</span>
        </div>
    </div>




</div>
@endsection

@section('script')
<script>

</script>
@endsection
