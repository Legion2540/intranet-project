@extends('layout/board')
@section('style')
<style>
    .board {
        font-family: Kanit-Regular;
        font-size: 28px;
        color: #4f72e5;
    }

    #search_board {
        width: 464px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .hot_topic,
    .new_topic {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }


    .title_hot_topic,
    .title_new_topic,
    .title_room_topic {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .img_own_hot_topic,
    .img_own_new_topic {
        width: 38px;
        height: 38px;
        border: solid 1px #ffffff;
        border-radius: 50%;
    }

    .own_hot_board,
    .own_new_board {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }


    .date_hot_board,
    .date_new_board {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }

    .count_c_l_hot_board,
    .count_c_l_new_board {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .room {
        width: 169;
        height: 169;
    }

    .name_room {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }

    .text_count_room {
        font-size: 11px;
        color: #4a4a4a;
    }

    .count_board_room {
        font-family: Kanit-Regular;
        font-size: 11px;
        color: #4a4a4a;
    }

    /* Footer */
    .count_myboard_pages {
        font-family: Kanit-Regular;
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .next_page_myboard,
    .next_page_myboard:hover {
        color: #ffffff;
        width: 183px !important;
        height: 35px;
        border-radius: 3px;
        background-color: #4f72e5;
        text-decoration: none;
    }

    .text_pages_myboard {
        font-family: Kanit-Regular;
        font-size: 12px;
        font-weight: bold;
        color: #4a4a4a;
    }

    input[name="page_type_myboard"] {
        width: 34px;
        height: 23px;
        border-radius: 4px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .page_prev {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;
        margin-right: 1px;
    }

    .page_next {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;

    }

    .arrow_my_board {
        border: solid #ffffff;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
        height: 1px !important;
        width: 1px !important;
    }

    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }
    .my-board a,.my-board a:hover{
        text-decoration: none;
    }

</style>
@endsection


@section('content')
<div class="col-12 d-flex align-items-center justify-content-between p-0">
    <div class="col-6 p-0 d-flex align-items-center">
        <img src="/img/icon/comment-blue@2x.png" alt="" class="mr-3">
        <span class="board">บอร์ดของฉัน</span>
    </div>
    <div class="col-6 p-0 d-flex align-items-center">
        <input type="search" name="search_board" id="search_board" placeholder="ค้นหาบอร์ด"
            style="position: relative;padding: 5px 8px 5px 43px;">
        <img src="/img/icon/search-material-copy@2x.png" alt=""
            style="position: absolute; transform: translate(10px, -1px);">
    </div>
</div>




<div class="col-12 px-0" style="min-height: 1617px; max-height: 1617px; overflow:hidden;">
    <div class="col-12 px-4 py-4 mt-3 my-board" style="background-color:#eaeaea; ">
        {{-- <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 pl-0">
            <span class="hot_topic">Hot Topic</span>
        </div>
        <div class="col-6 text-end pr-0">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div> --}}

        @for($i=0; $i<3; $i++) <a href="/board/myboard/{{ $i }}">
            <div class="col-12 d-flex align-items-center p-2 mt-3" style="background-color: #ffffff;">
                <div class="col-3">
                    <img src="" alt="" style="width:185px; height:110px; objecy-fit:cover; background-color:grey;">
                </div>
                <div class="col-9 px-2 d-flex align-content-around flex-wrap" style="height:110px;">
                    <div class="col-12 d-flex p-0">
                        <div class="col-10 px-0">
                            <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                            <span class="title_hot_topic">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
                        </div>
                        <div class="col-2 text-end pr-0">
                            <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 d-flex px-0">
                        <div class="col-1 px-0 d-flex align-items-center">
                            <img class="img_own_hot_topic"
                                src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                        </div>
                        <div class="col-7 p-0" style="line-height: 1.5;">
                            <div class="col-12">
                                <span class="own_hot_board">Somdet Susomboon</span>
                            </div>
                            <div class="col-12">
                                <span class="date_hot_board">4 มี.ค. 2564 09:40</span>
                            </div>
                        </div>
                        <div class="col-4 d-flex align-items-start justify-content-end">
                            <div class="col-6">
                                <img src="/img/icon/forum-material.png" alt="" class="mr-2">
                                <span class="count_c_l_hot_board">33</span>
                            </div>
                            <div class="col-6">
                                <img src="/img/icon/like.png" alt="" class="mr-2">
                                <span class="count_c_l_hot_board">33</span>
                            </div>
                        </div>


                    </div>
                </div>


            </div>
            </a>
            @endfor
    </div>



</div>


{{-- Footer Type News --}}
<div class="col-12 p-0 d-flex align-items-center p-0 my-5">
    <div class="col-12 d-flex justify-content-between align-items-center">
        <div class="col-4 text-start p-0">
            <span class="count_myboard_pages">แสดงผล <b>10</b> จากทั้งหมด <b>150</b></span>
        </div>
        <div class="col-4 d-flex align-items-center justify-content-center">
            <a href="#" class="d-flex justify-content-center align-items-center next_page_myboard">
                <img src="/img/icon/group-14.png" alt="">
            </a>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center pr-0">
            <span class="text_pages_myboard mr-2">หน้่า</span>
            <input type="text" name="page_type_myboard" class="text_pages_myboard mr-2 text-center" value="1">
            <span class="text_pages_myboard mr-2">จาก 15</span>

            <button class="d-flex align-items-center page_prev pl-2" type="button">
                <span class="arrow_my_board left"></span>
            </button>

            <button class="d-flex align-items-center page_next" type="button">
                <span class="arrow_my_board right"></span>
            </button>


        </div>
    </div>
</div>



@endsection


@section('script')
<script>

</script>
@endsection
