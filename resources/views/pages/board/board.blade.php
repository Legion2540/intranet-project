@extends('layout/board')
@section('style')
<style>
    .board {
        font-family: Kanit-Regular;
        font-size: 28px;
        color: #4f72e5;
    }

    #search_board {
        width: 464px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .hot_topic,
    .new_topic {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }


    .title_hot_topic,
    .title_new_topic,
    .title_room_topic {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .img_own_hot_topic,
    .img_own_new_topic {
        width: 38px;
        height: 38px;
        border: solid 1px #ffffff;
        border-radius: 50%;
    }

    .own_hot_board,
    .own_new_board {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }


    .date_hot_board,
    .date_new_board {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }

    .count_c_l_hot_board,
    .count_c_l_new_board {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .room {
        width: 169;
        height: 169;
    }

    .name_room {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }

    .text_count_room {
        font-size: 11px;
        color: #4a4a4a;
    }

    .count_board_room {
        font-family: Kanit-Regular;
        font-size: 11px;
        color: #4a4a4a;
    }

</style>
@endsection


@section('content')
<div class="col-12 d-flex align-items-center justify-content-between p-0">
    <div class="col-6 p-0 d-flex align-items-center">
        <img src="/img/icon/comment-blue@2x.png" alt="" class="mr-2">
        <span class="board">Board</span>
    </div>
    <div class="col-6 p-0 d-flex align-items-center">
        <input type="search" name="search_board" id="search_board" placeholder="ค้นหาบอร์ด"
            style="position: relative;padding: 5px 8px 5px 43px;">
        <img src="/img/icon/search-material-copy@2x.png" alt=""
            style="position: absolute; transform: translate(10px, -1px);">
    </div>
</div>





<div class="col-12 px-4 py-4 mt-3" style="background-color:#eaeaea">
    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 pl-0">
            <span class="hot_topic">Hot Topic</span>
        </div>
        <div class="col-6 text-end pr-0">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    @for($i=0; $i<3; $i++) <div class="col-12 d-flex align-items-center p-2 mt-3" style="background-color: #ffffff;">
        <div class="col-3">
            <img src="" alt="" style="width:185px; height:110px; objecy-fit:cover; background-color:grey;">
        </div>
        <div class="col-9 px-2 d-flex align-content-around flex-wrap" style="height:110px;">
            <div class="col-12 d-flex p-0">
                <div class="col-10 px-0">
                    <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                    <span class="title_hot_topic">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
                </div>
                <div class="col-2 text-end pr-0">
                    <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                </div>
            </div>
            <div class="col-12 d-flex px-0">
                <div class="col-1 px-0 d-flex align-items-center">
                    <img class="img_own_hot_topic"
                        src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                </div>
                <div class="col-7 p-0" style="line-height: 1.5;">
                    <div class="col-12">
                        <span class="own_hot_board">Somdet Susomboon</span>
                    </div>
                    <div class="col-12">
                        <span class="date_hot_board">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
                <div class="col-4 d-flex align-items-start justify-content-end">
                    <div class="col-6">
                        <img src="/img/icon/forum-material.png" alt="" class="mr-2">
                        <span class="count_c_l_hot_board">33</span>
                    </div>
                    <div class="col-6">
                        <img src="/img/icon/like.png" alt="" class="mr-2">
                        <span class="count_c_l_hot_board">33</span>
                    </div>
                </div>


            </div>
        </div>


</div>
@endfor
</div>



<div class="col-12 px-4 py-4 mt-3" style="background-color:#eaeaea">
    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 pl-0">
            <span class="new_topic">New Topic</span>
        </div>
        <div class="col-6 text-end pr-0">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    @for($i=0; $i<3; $i++)
    <div class="col-12 d-flex align-items-center p-2 mt-3" style="background-color: #ffffff;">
        <div class="col-3">
            <img src="" alt="" style="width:185px; height:110px; objecy-fit:cover; background-color:grey;">
        </div>
        <div class="col-9 px-2 d-flex align-content-around flex-wrap" style="height:110px;">
            <div class="col-12 d-flex p-0">
                <div class="col-10 px-0">
                    <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                    <span class="title_new_topic">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
                </div>
                <div class="col-2 text-end pr-0">
                    <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                </div>
            </div>
            <div class="col-12 d-flex px-0">
                <div class="col-1 px-0 d-flex align-items-center">
                    <img class="img_own_new_topic"
                        src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                </div>
                <div class="col-7 p-0" style="line-height: 1.5;">
                    <div class="col-12">
                        <span class="own_new_board">Somdet Susomboon</span>
                    </div>
                    <div class="col-12">
                        <span class="date_new_board">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
                <div class="col-4 d-flex align-items-start justify-content-end">
                    <div class="col-6">
                        <img src="/img/icon/forum-material.png" alt="" class="mr-2">
                        <span class="count_c_l_new_board">33</span>
                    </div>
                    <div class="col-6">
                        <img src="/img/icon/like.png" alt="" class="mr-2">
                        <span class="count_c_l_new_board">33</span>
                    </div>
                </div>


            </div>
        </div>


</div>
@endfor
</div>



<div class="col-12 px-0 py-4 mt-3 room_chat" style="background-color:#eaeaea">
    <div class="col-12 d-flex align-items-center px-4">
        <div class="col-6 pl-0">
            <span class="title_room_topic">ห้องส้นทนา</span>
        </div>
        <div class="col-6 text-end pr-0">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>


        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide">
                    <div class="room pb-2" style="border-radius: 2px; background-color: #96cfee;">
                        <div class="col-12 d-flex align-items-center justify-content-center pt-3">
                            <img src="/img/icon/group-8-copy.png" alt=""
                                style="width:65px; height:65px; object-fit: contain;">
                        </div>
                        <div class="col-12 mt-3 px-2">
                            <span class="name_room">สำนักตลาดพาณิชย์ดิจิทัล</span>
                        </div>
                        <div class="col-12">
                            <img src="" alt="" class="mr-2">
                            <span class="text_count_room">จำนวนกระทู้ทั้งหมด &nbsp; <span
                                    class="count_board_room">15</span></span>
                        </div>
                    </div>
                </div>


                <div class="swiper-slide">
                    <div class="room pb-2" style="border-radius: 2px; background-color: #a4b6f0;">
                        <div class="col-12 d-flex align-items-center justify-content-center pt-3">
                            <img src="/img/icon/group-18.png" alt=""
                                style="width:65px; height:65px; object-fit: contain;">
                        </div>
                        <div class="col-12 mt-3 px-2">
                            <span class="name_room">สำนักตลาดพาณิชย์ดิจิทัล</span>
                        </div>
                        <div class="col-12">
                            <img src="" alt="" class="mr-2">
                            <span class="text_count_room">จำนวนกระทู้ทั้งหมด &nbsp; <span
                                    class="count_board_room">15</span></span>
                        </div>
                    </div>
                </div>


                <div class="swiper-slide">
                    <div class="room pb-2" style="border-radius: 2px; background-color: #e9bdee;">
                        <div class="col-12 d-flex align-items-center justify-content-center pt-3">
                            <img src="/img/icon/group-3.png" alt=""
                                style="width:65px; height:65px; object-fit: contain;">
                        </div>
                        <div class="col-12 mt-3 px-2">
                            <span class="name_room">สำนักตลาดพาณิชย์ดิจิทัล</span>
                        </div>
                        <div class="col-12">
                            <img src="" alt="" class="mr-2">
                            <span class="text_count_room">จำนวนกระทู้ทั้งหมด &nbsp; <span
                                    class="count_board_room">15</span></span>
                        </div>
                    </div>
                </div>


                <div class="swiper-slide">
                    <div class="room pb-2" style="border-radius: 2px; background-color: #a0e6b0;">
                        <div class="col-12 d-flex align-items-center justify-content-center pt-3">
                            <img src="/img/icon/group-13.png" alt=""
                                style="width:65px; height:65px; object-fit: contain;">
                        </div>
                        <div class="col-12 mt-3 px-2">
                            <span class="name_room">สำนักตลาดพาณิชย์ดิจิทัล</span>
                        </div>
                        <div class="col-12">
                            <img src="" alt="" class="mr-2">
                            <span class="text_count_room">จำนวนกระทู้ทั้งหมด &nbsp; <span
                                    class="count_board_room">15</span></span>
                        </div>
                    </div>
                </div>


                <div class="swiper-slide">
                    <div class="room pb-2" style="border-radius: 2px; background-color: #e9cb8d;">
                        <div class="col-12 d-flex align-items-center justify-content-center pt-3">
                            <img src="/img/icon/group-3.png" alt=""
                                style="width:65px; height:65px; object-fit: contain;">
                        </div>
                        <div class="col-12 mt-3 px-2">
                            <span class="name_room">สำนักตลาดพาณิชย์ดิจิทัล</span>
                        </div>
                        <div class="col-12">
                            <img src="" alt="" class="mr-2">
                            <span class="text_count_room">จำนวนกระทู้ทั้งหมด &nbsp; <span
                                    class="count_board_room">15</span></span>
                        </div>
                    </div>
                </div>




            </div>

        </div>

        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>


</div>
@endsection


@section('script')
<script>
    var swiper = new Swiper(".room_chat .swiper-container", {
        // loop: true,
        slidesPerView: 5,
        spaceBetween: 10,
        navigation: {
            nextEl: ".room_chat .swiper-button-next",
            prevEl: ".room_chat .swiper-button-prev",
        },
    });

</script>
@endsection
