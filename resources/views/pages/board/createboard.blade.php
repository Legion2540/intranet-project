@extends('layout/board')
@section('style')
<style>
    .title_create_board {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .label-input {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    #title_board,
    #tag_board,
    #link_add {
        width: 95%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding-left: 18px;
        background-color: #ffffff;
    }

    #room_chat_board,
    #album_board {
        width: 437px;
        height: 30px;
        line-height: 1;
        border-radius: 8px;
        padding-left: 18px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .upload_title_board,
    .upload_file_title_board {
        width: 95px;
        height: 32px;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
        border-radius: 8px;
        border: none;
        color: #ffffff;
        text-align: center;
        font-size: 14px;
        cursor: pointer;
    }

    #upload_title,
    #upload_file_title {
        display: none;
    }

    .extension_title_board {
        font-size: 14px;
        color: #ee2a27;
        margin-left: 15px;
    }

    #link_add {
        width: 437px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .cancel_form_board,.cancel_form_board:hover {
        width: 112px;
        height: 30px;
        margin: 0 17px 0 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        border: none;
        text-decoration: none;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        padding-top: 3px;
    }

    .submit_form_board {
        width: 112px;
        height: 30px;
        border: none;
        color: #ffffff;
        margin: 0 0 0 17px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
    }

</style>

@section('content')
<div class="col-12 px-3 py-2" style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 d-flex align-items-center">
        <img src="/img/icon/border-color-material-copy.png" alt="" class="mr-3">
        <span class="title_create_board">สร้างบอร์ด</span>
    </div>

    <div class="col-12 mt-4 p-0 ">
        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">ชื่อหัวเรื่อง</span>
            </div>
            <div class="col-10 p-0">
                <input type="text" name="title_board" id="title_board" placeholder="ระบุชื่อบทความ หัวเรื่อง...">
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">ห้องสนทนา</span>
            </div>
            <div class="col-10 p-0">
                <select class="form-select" id="room_chat_board">
                    <option selected>ระบุหมวดหมู่บทความ</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">ภาพปก</span>
            </div>
            <div class="col-10 p-0">
                <button type="button" class="upload_title_board">Browse File</button>
                <input type="file" name="upload_title" id="upload_title">
                <span class="extension_title_board">(.jpg , .png)</span>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-start mb-3">
            <div class="col-2">
                <span class="label-input">ข้อความ</span>
            </div>
            <div class="col-10 p-0">
                <textarea name="content" id="create_boead"></textarea>
            </div>
        </div>


        <div class="col-12 p-0 d-flex align-items-start mb-3">
            <div class="col-2">
                <span class="label-input">แนบรูป</span>
            </div>
            <div class="col-10 p-0">
                <input type="file" name="img_blog[]" id="input-fas" multiple>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">แนบอัลบั้ม</span>
            </div>
            <div class="col-10 p-0">
                <select class="form-select" id="album_board">
                    <option selected>เลือกอัลบั้ม</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">แนบไฟล์</span>
            </div>
            <div class="col-10 p-0">
                <button type="button" class="upload_file_title_board">Browse File</button>
                <input type="file" name="upload_file_title" id="upload_file_title">
                <span class="extension_title_board">(.pdf, .mp4, .docx, .xls)</span>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">แนบลิงค์</span>
            </div>
            <div class="col-10 p-0">
                <input type="text" name="link_add" id="link_add" placeholder="ระบุ URL">
                <span class="extension_title_board">(.pdf, .mp4, .docx, .xls)</span>
            </div>
        </div>

        <div class="col-12 p-0 d-flex align-items-center mb-3">
            <div class="col-2">
                <span class="label-input">Tag</span>
            </div>
            <div class="col-10 p-0">
                <input type="text" name="tag_board" id="tag_board" placeholder="ระบุแท็ก">
            </div>
        </div>


        <div class="col-12 p-0 d-flex align-items-center justify-content-center mb-4" style="margin-top: 70px;">

            <a href="/board" class="cancel_form_board">ยกเลิก</a>
            <button class="submit_form_board">สร้าง</button>

        </div>



    </div>

</div>
@endsection


@section('script')
<script src="/js/ck_editor/editor.js"></script>
<script>
    $(document).ready(function () {
        $('.upload_title_board').on('click', function () {
            $('#upload_title').click();
        });

        $('.upload_file_title_board').on('click', function () {
            $('#upload_file_title').click();
        });


        $("#input-fas").fileinput({
            uploadUrl: "/site/test-upload",
            enableResumableUpload: true,
            initialPreviewAsData: true,
            theme: 'fas',
            deleteUrl: '/site/file-delete',
            fileActionSettings: {
                showZoom: function (config) {
                    if (config.type === 'pdf' || config.type === 'image') {
                        return true;
                    }
                    return false;
                }
            }
        });


    });

</script>
@endsection
