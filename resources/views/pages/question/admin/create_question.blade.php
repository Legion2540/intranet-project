@extends('layout.question')
@section('style')
<style>
    .forms-question {
        padding: 22px 67px 60px 30px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .card-question {
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
        padding: 22px 67px 60px 30px;
        border-left: 4px solid #4f72e5;
    }

    .text-head-forms {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .text-14 {
        font-size: 14px;
    }

    .input-namequestion {
        width: 792px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-question {
        width: 792px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-multiple, .input-checkboxes {
        width: 758px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_answer,
    .input_shortanswer {
        width: 776px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_paragraph {
        width: 776px;
        height: 90px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .text-key-question {
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .text-del-question {
        font-size: 16px;
        color: #d5001a;
        font-family: Kanit-Regular;
    }


    .textarea-other {
        width: 792px;
        height: 85px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .select-typequestion {
        width: 396px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_answer {
        width: 396px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_checkboxes,
    .add_dropdown {
        border: 0;
        font-size: 16px;
        font-family: Kanit-Regular;
        text-decoration: underline;
        background: none;
        color: #4f72e5;
    }


    .add_multiple {
        border: 0;
        font-size: 16px;
        font-family: Kanit-Regular;
        text-decoration: underline;
        background: none;
        color: #4f72e5;
    }

    .btn_addquestion {
        width: 100%;
        height: 47px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        border: 0;
        background-color: #4f72e5;
        font-size: 20px;
        color: #ffffff;
    }

    .btn-post {
        width: 140px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }


    .btn-cancel {
        width: 112px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

</style>
@endsection
@section('content_question')
<div class="forms-question">
    <div class="col-12 px-0 d-flex align-items-center">
        <img src="/img/icon/border-color-material-copy.png" alt="" width="23px" height="23px">
        <span class="text-head-forms ml-3">สร้างแบบสอบถาม</span>
    </div>
    <div class="col-12 px-0 pt-4 d-flex">
        <div class="col-1 px-0 d-flex align-items-center">
            <span class="text-key-question">ชื่อแบบสอบถาม</span>
        </div>
        <div class="col-10 pl-3">
            <input type="text" class="input-namequestion form-control">
        </div>
    </div>
    <div class="col-12 px-0 pt-4 d-flex">
        <div class="col-1 px-0 d-flex">
            <span class="text-key-question">คำอธิบาย</span>
        </div>
        <div class="col-10 pl-3">
            <textarea class="textarea-other"></textarea>
        </div>
    </div>
</div>


<div class="clone_question_results">
    <div class="card-question card_ques_1 mt-3" id="1">
        <div class="col-12 px-0 pt-3 d-flex">
            <div class="col-1 px-0 d-flex align-items-center">
                <span class="text-key-question">คำถาม</span>
            </div>
            <div class="col-9 pl-3">
                <input type="text" class="input-question form-control" id="val_question_1" name="val_question_[]">
            </div>
            <div class="col-2 px-0 text-end">
                <button style="border:0;background-color: #ffffff;" onclick="delete_question(this)">
                    <img src="/img/icon/bin-red.png" alt="">
                    <span class="text-del-question pl-1">ลบคำถาม</span>
                </button>
            </div>
        </div>
        <div class="col-12 px-0 pt-3 d-flex">
            <div class="col-1 px-0 d-flex align-items-center">
                <span class="text-key-question">ประเภทคำตอบ</span>
            </div>
            <div class="col-10 pl-3">
                <select class="form-select select-typequestion px-3 py-1 text-14" aria-label="Default select example"
                    id="select_typequestion_1" onchange="control_question(this)" name="select_typequestion_[]">
                    <option selected value="0">กรุณาเลือกรูปแบบคำถาม</option>
                    <option value="1">Multiple choice</option>
                    <option value="2">Checkboxes</option>
                    <option value="3">Dropdown</option>
                    <option value="4">Short answer</option>
                    <option value="5">Paragraph</option>
                </select>
            </div>
        </div>

        {{--  --}}
        <div class="col-12 px-0 pt-3 d-flex align-items-start control_input" id="control_input_1"
            style="display:none !important">
            <div class="col-1 px-0 mt-3 d-flex align-items-center">
                <span class="text-key-question">ตัวเลือกคำตอบ</span>
            </div>
            <div class="col-10 pl-4 question_multiple" id="question_multiple_1" data-member="test">
                <div class="form-check multiple_choice mt-3 d-flex align-items-center" id="multiple_choice_1_1">
                    <input class="form-check-input" type="radio" id="flexRadioDefault1">
                    <input type="text" class="input-multiple form-control ml-2" name="input_multiple_[]">
                </div>
                <div class="clone_results_multiple" id="clone_results_multiple_1"></div>
                <div class="form-check mt-3 d-flex align-items-center">
                    <button class="add_multiple" id="add_multiple_1" onclick="clone_multiple(this)">+
                        เพิ่มตัวเลือกคำตอบ</button>
                </div>
            </div>
            <div class="col-10 pl-4 question_checkboxes" id="question_checkboxes_1">
                <div class="form-check checkboxes_choice mt-3 d-flex align-items-center" id="checkboxes_choice_1_1">
                    <input class="form-check-input" type="checkbox" id="flexCheckDefault">
                    <input type="text" class="input-checkboxes form-control ml-2" name="input_checkboxes_[]">
                </div>
                <div class="clone_results_checkbox" id="clone_results_checkbox_1"></div>
                <div class="form-check mt-3 d-flex align-items-center">
                    <button class="add_checkboxes" id="add_checkboxes_1" onclick="clone_checkboxes(this)">+
                        เพิ่มตัวเลือกคำตอบ</button>
                </div>
            </div>
            <div class="col-10 pl-4 question_dropdown" id="question_dropdown_1">
                <div class="form-dropdown dropdown_choice mt-3 d-flex align-items-center" id="dropdown_choice_1_1">
                    <input type="text" class="input_answer form-control" id="input_answer_1" name="input_dropdown_[]">
                </div>
                <div class="clone_results_dropdown" id="clone_results_dropdown_1"></div>
                <div class="form-dropdown mt-3 d-flex align-items-center">
                    <button class="add_dropdown" id="add_dropdown_1" onclick="clone_dropdown(this)">+
                        เพิ่มตัวเลือกคำตอบ</button>
                </div>
            </div>
            <div class="col-10 pl-4 question_shortanswer" id="question_shortanswer_1">
                <div class="form-shortanswer shortanswer_choice mt-3 d-flex align-items-center" id="shortanswer_1_1">
                    <input type="text" class="input_shortanswer form-control" id="input_shortanswer_1" name="input_shortanswer">
                </div>
            </div>
            <div class="col-10 pl-4 question_paragraph" id="question_paragraph_1">
                <div class="form-paragraph paragraph_choice mt-3 d-flex align-items-center" id="paragraph_1_1">
                    <textarea class="input_paragraph" id="input_paragraph_1" name="input_paragraph"></textarea>
                </div>
            </div>
        </div>
    </div>

</div>

<button class="btn_addquestion mt-3" onclick="clone_question(this)">+ เพิ่มคำถาม</button>

<div class="col-12 mt-5 d-flex justify-content-center">
    <button class="btn-cancel">ยกเลิก</button>
    <button class="btn-post ml-3" onclick="submit(this)">โพสต์แบบสอบถาม</button>
</div>

@endsection


@section('script')
<script>
    var count_ml = 2;
    var count_ch = 2;
    var count_drop = 2;
    $(document).ready(function () {
        window.count_question = 2;
        count_ml = 2;
        count_ch = 2;
        count_drop = 2;
    })

    function clone_multiple(_this) {
        id_question = $(_this).parents('.card-question').attr('id');

        $('.multiple_choice').clone().last().appendTo('#clone_results_multiple_' + id_question).attr("id",
            "multiple_choice_" + id_question + '_' + count_ml);
        $(_this).parents('.question_multiple').find('.multiple_choice input[type="text"]').last().val('');
        count_ml = count_ml + 1;
    }

    function clone_checkboxes(_this) {
        id_question = $(_this).parents('.card-question').attr('id');

        $('.checkboxes_choice').clone().last().appendTo('#clone_results_checkbox_' + id_question).attr("id",
            "checkboxes_choice_" + id_question + '_' + count_ch);
        $(_this).parents('.question_checkboxes').find('.checkboxes_choice input[type="text"]').last().val('');
        count_ch = count_ch + 1;
    }

    function clone_dropdown(_this) {
        id_question = $(_this).parents('.card-question').attr('id');

        $('.dropdown_choice').clone().last().appendTo('#clone_results_dropdown_' + id_question).attr("id",
            "dropdown_choice_" + id_question + '_' + count_drop);

        $(_this).parents('.question_dropdown').find('.dropdown_choice input[type="text"]').last().val('');

        count_drop = count_drop + 1;
    }

    function clone_question(_this) {
        var parents_qustion = $(_this).parents('.question_class');

        $('.card-question').last().clone().appendTo('.clone_question_results');
        parents_qustion.find('.card-question').last().attr("id", count_question);
        parents_qustion.find('.card-question').last().removeClass( "card_ques_"+(count_question-1) ).addClass( "card_ques_"+ count_question );

        parents_qustion.find('.input-question').last().attr("id", 'val_question_' + count_question);

        parents_qustion.find('.clone_results_multiple').last().attr("id", 'clone_results_multiple_' + count_question);
        parents_qustion.find('.clone_results_checkbox').last().attr("id", 'clone_results_checkbox_' + count_question);
        parents_qustion.find('.clone_results_dropdown').last().attr("id", 'clone_results_dropdown_' + count_question);

        parents_qustion.find('.select-typequestion').last().attr("id", 'select_typequestion_' + count_question);

        parents_qustion.find('.question_multiple').last().attr("id", 'question_multiple_' + count_question);
        parents_qustion.find('.question_checkboxes').last().attr("id", 'question_checkboxes_' + count_question);

        parents_qustion.find('.question_dropdown').last().attr("id", 'question_dropdown_' + count_question);
        parents_qustion.find('.input_answer').last().attr("id", 'input_answer_' + count_question);

        parents_qustion.find('.question_shortanswer').last().attr("id", 'question_shortanswer_' + count_question);
        parents_qustion.find('.input_shortanswer').last().attr("id", 'input_shortanswer_' + count_question);

        parents_qustion.find('.question_paragraph').last().attr("id", 'question_paragraph_' + count_question);
        parents_qustion.find('.input_paragraph').last().attr("id", 'input_paragraph_' + count_question);

        parents_qustion.find('.control_input').last().attr("id", 'control_input_' + count_question);

        reset_question(count_question);

        count_question = count_question + 1

    }

    function control_question(_this) {

        var id_question = $(_this).parents('.card-question').attr('id');

        $('#control_input_' + id_question).show();

        var value_select = $('#select_typequestion_' + id_question).val();

        if (value_select == 1) {
            $('#question_multiple_' + id_question).show();
            $('#question_checkboxes_' + id_question).hide();
            $('#question_dropdown_' + id_question).hide();
            $('#question_shortanswer_' + id_question).hide();
            $('#question_paragraph_' + id_question).hide();
        } else if (value_select == 2) {
            $('#question_multiple_' + id_question).hide();
            $('#question_checkboxes_' + id_question).show();
            $('#question_dropdown_' + id_question).hide();
            $('#question_shortanswer_' + id_question).hide();
            $('#question_paragraph_' + id_question).hide();
        } else if (value_select == 3) {
            $('#question_multiple_' + id_question).hide();
            $('#question_checkboxes_' + id_question).hide();
            $('#question_dropdown_' + id_question).show();
            $('#question_shortanswer_' + id_question).hide();
            $('#question_paragraph_' + id_question).hide();
        } else if (value_select == 4) {
            $('#question_multiple_' + id_question).hide();
            $('#question_checkboxes_' + id_question).hide();
            $('#question_dropdown_' + id_question).hide();
            $('#question_shortanswer_' + id_question).show();
            $('#question_paragraph_' + id_question).hide();
        } else if (value_select == 5) {
            $('#question_multiple_' + id_question).hide();
            $('#question_checkboxes_' + id_question).hide();
            $('#question_dropdown_' + id_question).hide();
            $('#question_shortanswer_' + id_question).hide();
            $('#question_paragraph_' + id_question).show();
        } else {
            alert("Error value_select");
        }
    }

    function delete_question(_this) {
        var count_question = $('.card-question').length;
        if (count_question > 1) {
            $(_this).parents('.card-question').remove();
        } else if (count_question == 1) {
            alert("ไม่สามารถลบคำถามได้");
        } else {
            alert("Error :" + count_question);
        }
    }

    function reset_question(id_question) {
        $('#control_input_' + id_question).attr("style", "display: none !important");

        $('#control_input_' + id_question).find('.multiple_choice input[type="text"]').val('');
        $('#control_input_' + id_question).find('.multiple_choice').not(':first').remove();

        $('#control_input_' + id_question).find('.checkboxes_choice input[type="text"]').val('');
        $('#control_input_' + id_question).find('.checkboxes_choice').not(':first').remove();

        $('#control_input_' + id_question).find('.dropdown_choice input[type="text"]').val('');
        $('#control_input_' + id_question).find('.dropdown_choice').not(':first').remove();

        $('#control_input_' + id_question).find('.shortanswer_choice input[type="text"]').val('');

        $('#control_input_' + id_question).find('.paragraph_choice textarea').val('');

        $('#control_input_' + id_question).parents('.card-question').find('#val_question_' + id_question).val('');
    }

    function submit(_this){
        var parents_qustion = $(_this).parents('.question_class');
        var card_arr = [];

        var id_card = parents_qustion.find('.card-question').map(function(){
            return $(this).attr('id');  
        }).get();

        var question = parents_qustion.find('input[name="val_question_[]"]').map(function(){
            return $(this).val();  
        }).get();

        var type_answer = parents_qustion.find('select[name="select_typequestion_[]"]').map(function(){
            return $(this).val();  
        }).get();

        $.each(id_card, function (key, value){
            var answer_arr = [];
            var answer = 0;

            if (type_answer[key] == 1){
                answer = $('.card_ques_' + (key+1)).find('input[name="input_multiple_[]"]').map(function(){
                    answer_arr.push($(this).val());
                }).get();
            }else if (type_answer[key] == 2){
                answer = $('.card_ques_' + (key+1)).find('input[name="input_checkboxes_[]"]').map(function(){
                    answer_arr.push($(this).val());
                }).get();
            }else if (type_answer[key] == 3){
                answer = $('.card_ques_' + (key+1)).find('input[name="input_dropdown_[]"]').map(function(){
                    answer_arr.push($(this).val());
                }).get();
            }else if (type_answer[key] == 4){
                answer = $('.card_ques_' + (key+1)).find('input[name="input_shortanswer"]');
                answer_arr.push(answer.val());
            }else if (type_answer[key] == 5){
                answer = $('.card_ques_' + (key+1)).find('textarea[name="input_paragraph"]');
                answer_arr.push(answer.val());
            }

            card_arr.push({"input_question" : question[key], "type_answer" : type_answer[key], "answer" : answer_arr}) ;     
        });
    }

</script>
@endsection
