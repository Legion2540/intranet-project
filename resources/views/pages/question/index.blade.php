@extends('layout.question')
@section('style')
<style>
    .forms-question {
        padding: 22px 67px 60px 30px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .text-head-forms {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .input-cearch {
        width: 420px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-family: Kanit-ExtraLight;
    }

    .position-icon-ceach {
        position: absolute;
        top: 25%;
        right: 3%;
    }

    .hr-head-question {
        border-top: 1px dashed #8c8b8b;
        background-color: white;
        margin-left: 50px;
    }

    .hr-list-question {
        border-top: 1px #8c8b8b;
        margin-left: 50px;
    }

    .text-title {
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .text-date {
        font-size: 10px;
        color: #4a4a4a;
        font-family: Kanit-ExtraLight;
    }

    .btn-create-question {
        width: 204px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #314d7b 99%);
        font-family: Kanit-ExtraLight;
    }

</style>
@endsection
@section('content_question')
<div class="forms-question">
    <div class="col-12 px-0 d-flex">
        <div class="col-4 d-flex align-items-center">
            <div class="col-1 px-0">
                <img src="/img/icon/description-material@3x.png" style="width: 23px; height:auto;">
            </div>
            <div class="col-11 px-0">
                <span class="text-head-forms">แบบสอบถาม</span>
            </div>
        </div>
        <div class="col-8 px-0 d-flex align-items-center justify-content-end">
            <div class="position-relative">
                <input class="input-cearch form-control" type="text" placeholder=" ค้นหารายการ">
                <img class="position-icon-ceach" src="/img/icon/search-material-bule.png" alt="">
            </div>
            <div class="mx-3">
                <img src="/img/icon/tune-material-copy-3.png" alt="">
            </div>
            <button class="btn-create-question py-0">+ สร้างแบบสอบถาม</button>
        </div>
    </div>

    <hr class="hr-head-question">
    @for ($i = 0; $i < 7; $i++) 
        <div class="col-12 pl-5 pr-0 d-flex">
            <div class="col-10 px-0">
                <img src="/img/icon/forum-material.png" alt="">
                <span class="text-title ml-3">แบบประเมินการทำงานที่บ้านในสถานการณ์โควิด 19</span>
            </div>
            <div class="col-2 px-4 text-end">
                <span class="text-date">19 ม.ค. 2563 09:00</span>
            </div>
        </div>
        <hr class="hr-list-question">
        <div class="col-12 pl-5 pr-0 d-flex">
            <div class="col-10 px-0">
                <img src="/img/icon/forum-material.png" alt="">
                <span class="text-title ml-3">แบบสอบถามความเสี่ยงในการเดินทางไปจุดเสี่ยงในช่วงเทศกาลปีใหม่</span>
            </div>
            <div class="col-2 px-4 text-end">
                <span class="text-date">12 ม.ค. 2563 09:00</span>
            </div>
        </div>
        <hr class="hr-list-question">
    @endfor
</div>
@endsection
