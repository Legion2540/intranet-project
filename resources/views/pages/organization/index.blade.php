@extends('layout/organization')
@section('style')
<style>
    .search_organize {
        position: relative;
        width: 294px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding-left: 35px;
    }

    .title-pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    #magnifier {
        width: 20px;
        height: 20px;
        position: absolute;
        left: 212px;
        z-index: 1;
        top: 6px;
    }

    .card {
        width: 306px;
        height: 105px;
        border-radius: 100px 10px 10px 100px;
        border: none;
        background-color: rgba(225, 225, 225, 0.5);
    }

    .image_emplyee {
        width: 80px;
        height: 80px;
        margin: 3px 56px 4px 0;
        object-fit: contain;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 3px #ffffff;
    }

    .tag-boss {
        width: 197px;
        height: 20px;
        border-radius: 100px 10px 10px 100px;
        box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.11);
        background-color: #4f72e5;
    }

    .boss {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #ffffff;
    }

    .employee {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4f72e5;
    }

    .name_employee {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;

    }

    .contact {
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

</style>
@endsection





@section('content')
<div class="col-12 px-4 py-3"
    style="background-color:#ffffff; width:1134px; box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">
    <div class="col-12 d-flex align-items-center p-0">
        <div class="col-6 d-flex align-items-center p-0">
            <img src="/img/icon/logo-man-blue.png" alt="" class='mr-2'>
            <span class="title-pages">ผู้บริหารระดับสูง</span>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end p-0">
            <img src="/img/icon/search-material-copy@2x.png" alt="" id="magnifier">
            <input type="text" class="search_organize mr-4" name="search_organize"
                placeholder="ค้นหาเจ้าหน้าที่, เบอร์ภายใน">

            <img src="/img/icon/tune-material-copy-3.png" alt="">
        </div>
    </div>

    {{-- head --}}
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="card d-flex align-items-center">
            <div class="col-12 d-flex align-items-center justify-content-start">
                <div class="col-4 px-0">
                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="image_emplyee">
                </div>

                {{-- <div class="col-8 px-0 text-end">
                    <div class="col-12 tag-boss d-flex align-items-center justify-content-end">
                        <span class="boss">อธิบดี</span>
                    </div>
                    <div class="col-12 px-0">
                        <span class="name_employee">นายสมเด็จ สุสมบูรณ์</span>
                    </div>
                    <div class="col-12 px-0 mt-2" style="line-height: 1">
                        <span class="contact">0-2507-5749</span><br>
                        <span class="contact">somdet@ditp.go.th</span>
                    </div>
                </div> --}}

                <div class="col-8 px-0 text-end">
                    <div class="tag-boss d-flex align-items-center justify-content-end mb-1 px-2"
                        style="position: absolute;bottom: 59px;left: 10px;">
                        <span class="boss">อธิบดี</span>
                    </div>
                    <div class="col-12 px-0 mt-2">
                        <span class="name_employee">นายสมเด็จ สุสมบูรณ์</span>
                    </div>
                    <div class="col-12 px-0 mt-2" style="line-height: 1">
                        <span class="contact">0-2507-5749</span><br>
                        <span class="contact">somdet@ditp.go.th</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row d-flex align-items-center justify-content-between px-3 mb-3">

        @for($i=0; $i<10; $i++)
        <div class="card d-flex align-items-center mt-4 mx-4">
            <div class="col-12 d-flex align-items-center justify-content-start">
                <div class="col-4 px-0">
                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="image_emplyee">
                </div>

                {{-- <div class="col-8 px-0 text-end">
                    <div class="col-12 tag-boss d-flex align-items-center justify-content-end">
                        <span class="boss">อธิบดี</span>
                    </div>
                    <div class="col-12 px-0">
                        <span class="name_employee">นายสมเด็จ สุสมบูรณ์</span>
                    </div>
                    <div class="col-12 px-0 mt-2" style="line-height: 1">
                        <span class="contact">0-2507-5749</span><br>
                        <span class="contact">somdet@ditp.go.th</span>
                    </div>


                </div> --}}
                <div class="col-8 px-0 text-end">
                    <div class="col-12 d-flex align-items-center justify-content-end px-0">
                        <span class="employee">รองอธิบดี</span>
                    </div>
                    <div class="col-12 px-0 mt-2">
                        <span class="name_employee">นายวิทยากร มณีเนตร</span>
                    </div>
                    <div class="col-12 px-0 mt-2" style="line-height: 1">
                        <span class="contact">0-2507-8073</span><br>
                        <span class="contact">witt@ditp.go.th</span>
                    </div>
                </div>

            </div>


        </div>
        @endfor

    </div>




</div>
@endsection





@section('script')
<script>

</script>
@endsection
