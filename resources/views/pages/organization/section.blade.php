@extends('layout/organization')
@section('style')
<style>
    .search_organize {
        position: relative;
        width: 294px;
        height: 34px;
        font-size: 13px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding-left: 35px;
        padding-top: 2px;
        padding-bottom: 5px;
    }

    .search_organize::placeholder,
    .search_organize_admin::placeholder {
        font-size: 13px;
    }

    .search_organize_admin {
        position: relative;
        width: 189px;
        height: 31px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding-left: 35px;
        padding-top: 2px;
        padding-bottom: 5px;
    }

    .title-pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .organize_chart,
    .organize_chart:hover {
        width: 114px;
        height: 30px;
        font-size: 14px;
        border-radius: 8px;
        border: none;
        text-decoration: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
    }

    #magnifier {
        width: 18px;
        height: 18px;
        position: absolute;
        left: 118px;
        z-index: 1;
        top: 7px;
    }

    #magnifier_admin {
        width: 18px;
        height: 18px;
        position: absolute;
        left: 111px;
        z-index: 1;
        top: 6px;
    }

    .card {
        width: 306px;
        height: 105px;
        border-radius: 100px 10px 10px 100px;
        border: none;
        background-color: rgba(225, 225, 225, 0.5);
    }

    .card-employee {
        width: 202px;
        height: 74px;
        border-radius: 2px;
        border: none;
        background-color: rgba(225, 225, 225, 0.5);
        cursor: pointer;
    }

    .image_boss {
        width: 80px;
        height: 80px;
        margin: 3px 56px 4px 0;
        object-fit: contain;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 3px #ffffff;
    }

    .image_emplyee {
        width: 53px;
        height: 53px;
        object-fit: contain;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
        border: solid 3px #ffffff;
    }

    .tag-boss {
        width: 197px;
        height: 20px;
        border-radius: 100px 10px 10px 100px;
        box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.11);
        background-color: #4f72e5;
    }

    .boss {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #ffffff;
    }

    .employee {
        font-family: Kanit-Regular;
        font-size: 10px;
        color: #4f72e5;
    }

    .name_employee {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;

    }

    .contact {
        font-size: 10px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .th-group-section {
        width: 203px;
        min-height: 31px;
        max-height: 50px;
        border-radius: 2px;
        /* padding: 0 5px; */
        padding: 8px 9px 4px 13px;

        background-color: rgba(79, 114, 229, 0.27);

        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
        /* margin: 0 15px; */
        text-align: center;
        vertical-align: middle;
    }

</style>

<style>
    .detail-employee-popover {
        width: 338px;
        height: 544px;
        border-radius: 8px;
        box-shadow: 0 1px 13px 1px rgba(0, 0, 0, 0.11);
        background-color: #ffffff;
        display: none;
    }

    .popover {
        width: 338px;
        height: fit-content;
    }

    .detail-employee-name {
        font-family: Kanit-Regular;
        font-size: 18px;

        color: #4f72e5;
    }

    .detail-employee-position,
    .detail-employee-title-form {
        font-family: Kanit-Regular;
        font-size: 12px;

        color: #4a4a4a;
    }

    .detail-employee-content-form {
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .detail-employee-profile-btn,
    .detail-employee-profile-btn:hover {
        font-family: Kanit-ExtraLight;
        width: 183px;
        height: 23px;
        border-radius: 3px;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
        background-color: #4f72e5;
    }

</style>
@endsection





@section('content')
<div class="col-12 px-4 py-3"
    style="background-color:#ffffff; width:1134px; box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">
    <div class="col-12 d-flex align-items-center p-0">
        <div class="col-7 d-flex align-items-center p-0">
            <img src="/img/icon/logo-man-blue.png" alt="" class='mr-2'>
            <span class="title-pages">สำนักส่งเสริมการค้าสินค้าเกษตรและอุตสาหกรรม (สกอ.)</span>
        </div>
        <div class="col-5 d-flex align-items-center justify-content-end p-0">
            <img id="magnifier_admin" src="/img/icon/search-material-copy@2x.png" alt="">
            <input type="text" class="search_organize_admin mr-3" name="search_organize"
                placeholder="ค้นหาเจ้าหน้าที่, เบอร์ภายใน">

            <img src="/img/icon/tune-material-copy-3.png" alt="" style="width: 17px;height: 15px;" class="mr-3">

            <a href="/organization/editsection" class="organize_chart d-flex align-items-center px-2"><img src="/img/icon/tag-name.png" alt=""
                    style="width: 13px;height: 12px;object-fit: contain">แก้ไขผังองค์กร</a>

        </div>
    </div>

    {{-- head --}}
    <div class="col-12 d-flex align-items-center justify-content-center mt-4">
        <div class="card d-flex align-items-center">
            <div class="col-12 d-flex align-items-center justify-content-start">
                <div class="col-4 px-0">
                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                        class="image_boss">
                </div>
                <div class="col-8 px-0 text-end">
                    <div class="tag-boss d-flex align-items-center justify-content-end mb-1 px-2"
                        style="position: absolute;bottom: 59px;left: 10px;">
                        <span class="boss">อธิบดี</span>
                    </div>
                    <div class="col-12 px-0 mt-2">
                        <span class="name_employee">นายสมเด็จ สุสมบูรณ์</span>
                    </div>
                    <div class="col-12 px-0 mt-2" style="line-height: 1">
                        <span class="contact">0-2507-5749</span><br>
                        <span class="contact">somdet@ditp.go.th</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- <div class="d-flex align-items-center justify-content-center">
        <table class="section text-center mt-3">
            <thead>
                <tr>
                    <th class="th-group-section">กลุ่มงานแผนและอำนวยการ</th>
                    <th class="th-group-section">กลุ่มงานส่งเสริมภาพลักษณ์สินค้าเกษตร อุตสาหกรรมและฮาลาล</th>
                    <th class="th-group-section">กลุ่มงานสินค้าเกษตรและอาหาร</th>
                    <th class="th-group-section">กลุ่มงานสินค้าผลไม้และ Thai Select</th>
                    <th class="th-group-section">กลุ่มงานสินค้าอุตสาหกรรม</th>
                </tr>
            </thead>
            <tbody>

                <tr>

                @for($i=0;$i<5; $i++)
                    <td>
                        <div class="card-employee d-flex align-items-center mt-4">
                            <div class="col-12 d-flex align-items-center justify-content-start">
                                <div class="col-4 px-0">
                                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                        class="image_emplyee">
                                </div>
                                <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                                    <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                        <span class="employee">รองอธิบดี</span>
                                    </div>
                                    <div class="col-12 px-0">
                                        <span class="name_employee">นายวิทยากร มณีเนตร</span>
                                    </div>
                                    <div class="col-12 px-0" style="line-height: 1">
                                        <span class="contact">0-2507-8073</span><br>
                                        <span class="contact">witt@ditp.go.th</span>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </td>
                @endfor

                </tr>
            </tbody>
        </table>
    </div> --}}



    {{-- <div class="row d-flex align-items-center px-3 mb-4 mt-3">
        <div class="col-12 d-flex align-items-start justify-content-between">

            <div>
                <div class="col-12 th-group-section d-flex align-items-center justify-content-center">
                    <span class="">กลุ่มงานแผนและอำนวยการ</span>
                </div>


                <div class="card-employee d-flex align-items-center mt-4">
                    <div class="col-12 d-flex align-items-center justify-content-start">
                        <div class="col-4 px-0">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="image_emplyee">
                        </div>
                        <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                            <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                <span class="employee">รองอธิบดี</span>
                            </div>
                            <div class="col-12 px-0">
                                <span class="name_employee">นายวิทยากร มณีเนตร</span>
                            </div>
                            <div class="col-12 px-0" style="line-height: 1">
                                <span class="contact">0-2507-8073</span><br>
                                <span class="contact">witt@ditp.go.th</span>
                            </div>
                        </div>

                    </div>


                </div>


            </div>





            <div>
                <div class="col-12 th-group-section d-flex align-items-center justify-content-center">
                    <span class="">กลุ่มงานส่งเสริมภาพลักษณ์สินค้าเกษตร อุตสาหกรรมและฮาลาล</span>
                </div>

                <div class="card-employee d-flex align-items-center mt-4">
                    <div class="col-12 d-flex align-items-center justify-content-start">
                        <div class="col-4 px-0">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="image_emplyee">
                        </div>
                        <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                            <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                <span class="employee">รองอธิบดี</span>
                            </div>
                            <div class="col-12 px-0">
                                <span class="name_employee">นายวิทยากร มณีเนตร</span>
                            </div>
                            <div class="col-12 px-0" style="line-height: 1">
                                <span class="contact">0-2507-8073</span><br>
                                <span class="contact">witt@ditp.go.th</span>
                            </div>
                        </div>

                    </div>


                </div>

            </div>

            <div>
                <div class="col-12 th-group-section d-flex align-items-center justify-content-center">
                    <span class="">กลุ่มงานสินค้าเกษตรและอาหาร</span>
                </div>

                <div class="card-employee d-flex align-items-center mt-4">
                    <div class="col-12 d-flex align-items-center justify-content-start">
                        <div class="col-4 px-0">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="image_emplyee">
                        </div>
                        <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                            <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                <span class="employee">รองอธิบดี</span>
                            </div>
                            <div class="col-12 px-0">
                                <span class="name_employee">นายวิทยากร มณีเนตร</span>
                            </div>
                            <div class="col-12 px-0" style="line-height: 1">
                                <span class="contact">0-2507-8073</span><br>
                                <span class="contact">witt@ditp.go.th</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div>
                <div class="col-12 th-group-section d-flex align-items-center justify-content-center">
                    <span class="">กลุ่มงานสินค้าผลไม้และ Thai Select</span>
                </div>

                <div class="card-employee d-flex align-items-center mt-4">
                    <div class="col-12 d-flex align-items-center justify-content-start">
                        <div class="col-4 px-0">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="image_emplyee">
                        </div>
                        <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                            <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                <span class="employee">รองอธิบดี</span>
                            </div>
                            <div class="col-12 px-0">
                                <span class="name_employee">นายวิทยากร มณีเนตร</span>
                            </div>
                            <div class="col-12 px-0" style="line-height: 1">
                                <span class="contact">0-2507-8073</span><br>
                                <span class="contact">witt@ditp.go.th</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="col-12 th-group-section d-flex align-items-center justify-content-center">
                    <span class="">กลุ่มงานสินค้าอุตสาหกรรม</span>
                </div>

                <div class="card-employee d-flex align-items-center mt-4">
                    <div class="col-12 d-flex align-items-center justify-content-start">
                        <div class="col-4 px-0">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="image_emplyee">
                        </div>
                        <div class="col-8 px-0 text-end" style="line-height: 1.3;">
                            <div class="col-12 d-flex align-items-center justify-content-end px-0">
                                <span class="employee">รองอธิบดี</span>
                            </div>
                            <div class="col-12 px-0">
                                <span class="name_employee">นายวิทยากร มณีเนตร</span>
                            </div>
                            <div class="col-12 px-0" style="line-height: 1">
                                <span class="contact">0-2507-8073</span><br>
                                <span class="contact">witt@ditp.go.th</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> --}}



    <div class="col-12 d-flex align-items-center justify-content-between mt-4"
        style="overflow-x: auto;overflow-y: hidden;white-space: nowrap;">

        @for($i=0;$i<5; $i++) <div class="col-12 px-0" style="min-width: 204px; max-width: 204px;">
            <div class="th-group-section">
                <span>กลุ่มงานแผนและอำนวยการ</span>
            </div>

            @for($k=0;$k<5; $k++) <div class="card-employee d-flex align-items-center mt-4">
                <div class="col-12 d-flex align-items-center justify-content-center">

                    <div class="col-4 px-0">
                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="image_emplyee">
                    </div>

                    <div class="col-8 px-0 text-end" style="line-height: 1">
                        <div class="col-12 d-flex align-items-center justify-content-end px-0">
                            <span class="employee">รองอธิบดี</span>
                        </div>
                        <div class="col-12 px-0">
                            <span class="name_employee">นายวิทยากร มณีเนตร</span>
                        </div>
                        <div class="col-12 px-0" style="line-height: 1">
                            <span class="contact">0-2507-8073</span><br>
                            <span class="contact">witt@ditp.go.th</span>
                        </div>
                    </div>
                </div>




    </div>
    @endfor
</div>
@endfor

</div>
</div>




<div class="detail-employee-popover">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="" style="width: 100px;height: 100px;
        border-radius:50%;
        object-fit: contain;
        border: solid 3px #ffffff;
        position: absolute;
        top: -25px;">
    </div>
    <div class="col-12 p-0" style="margin-top: 80px;">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <span class="detail-employee-name">สรวิส นิ่มสอาด</span>
        </div>
        <div class="col-12 px-0 text-center">
            <span class="detail-employee-position">เจ้าหน้าที่วิเคราะห์การค้าสินค้าเกษตรและอุตสาหกรรม</span>
        </div>

        {{-- Detial Job --}}
        <div class="col-12 d-flex align-items-center mt-3 px-1">
            <img src="/img/icon/work-material.png" alt="" class="mr-2">
            <span class="detail-employee-title-form">เกี่ยวกับตำแหน่ง</span>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">สำนัก/หน่วยงาน</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">สำนักส่งเสริมการค้าสินค้าเกษตรและอุตสาหกรรม (สกอ.)</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">กลุ่มงาน</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">กลุ่มงานส่งเสริมภาพลักษณ์สินค้าเกษตร
                    อุตสาหกรรมและฮาลาล</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">อีเมล</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">sorawis@ditp.go.th</span>
                <img src="/img/icon/attach-file-material-bule.png" alt="">
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">เบอร์ภายใน</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">0906</span>
            </div>
        </div>


        {{-- Detial Profile --}}
        <div class="col-12 d-flex align-items-center mt-3 px-1">
            <img src="/img/icon/account-box-material.png" alt="" class="mr-2">
            <span class="detail-employee-title-form">แนะนำตัว</span>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">ชื่อเล่น</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">บอล</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">วันเกิด</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">9 มิถุนายน</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1;">
            <div class="col-4 px-0">
                <span class="detail-employee-title-form">จาก</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">อำเภอเมือง, จังหวัดเลย</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-start justify-content-between mt-2 px-1" style="line-height: 1.3;">
            <div class="col-5 px-0">
                <span class="detail-employee-title-form">ข้อความแนะนำตัว</span>
            </div>
            <div class="col-7 px-0">
                <span class="detail-employee-content-form">สวัสดีครับทุกคน :)
                    หากต้องการสอบถามเกี่ยวกับสินค้าเกษตรหรือข้อมูลฮาลาล สามารถติดต่อผมได้ครับ </span>
            </div>
        </div>

        <div class="col-12 d-flex align-items-center justify-content-center mt-3 px-1">
            <a href="#" class="detail-employee-profile-btn">ดูโปรไฟล์</a>
        </div>

    </div>
</div>



@endsection





@section('script')
<script>
    $(document).ready(function () {
        // container: '.card-employee',
        $('.card-employee').popover({
            html: true,
            container: 'body',
            sanitize: false,
            trigger: 'click',
            content: function () {
                return $('.detail-employee-popover').html();
            }
        }).on('click', function (e) {
            $(this).popover('show');
        });
        $('.card-employee').on('click', function (e) {
            e.stopPropagation();
        });
    });

</script>
@endsection
