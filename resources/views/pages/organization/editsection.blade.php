@extends('layout/organization')
@section('style')
<style>
    .spec-div-first {
        background-color: #ffffff;
        width: 1134px;
        box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);
        padding: 13px 156px 35px 28px;
    }

    .spec-div {
        background-color: #ffffff;
        width: 1134px;
        box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);
        border-left: 5px solid #4f72e5;
    }

    .btn-add-group {
        background-color: #4f72e5;
        width: 1134px;
        box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);
    }

    .add-group {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #ffffff;
        background-color: transparent;
        border: none;
    }

    .title-pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .spec-input {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .title-form {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;


    }

    .title-edit-text {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #d5001a;
    }

    .text-in-btn-add {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #ffffff;
    }

    .email_employee {
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .cacnel-form,
    .cacnel-form:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
        text-decoration: none;
    }

    .submit-form {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        color: #ffffff;
        border: none;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);

    }

</style>

@endsection





@section('content')

<div class="col-12 spec-div-first mb-3">
    <div class="col-12 d-flex align-items-center p-0">
        <img src="/img/icon/logo-man-blue.png" alt="" class='mr-2'>
        <span class="title-pages">แก้ไขผังองค์กร</span>
    </div>
    <div class="col-12 mt-4 p-0 d-flex align-items-center">
        <div class="col-2 p-0">
            <span class="title-form">ชื่อสำนัก/หน่วยงาน</span>
        </div>
        <div class="col-10">
            <input type="text" class="spec-input px-2">
        </div>
    </div>

    <div class="col-12 mt-4 p-0 d-flex align-items-center">
        <div class="col-2 p-0">
            <span class="title-form">ผู้อำนวยการ</span><br>
            <span style="font-size: 11px;color: #4a4a4a;">ใส่ได้ 1 อีเมล</span>
        </div>

        <div class="col-10">
            <input type="text" class="spec-input px-2">
        </div>
    </div>
</div>


<div class="space-group">
    <div class="col-12 spec-div py-3 pl-3 d-flex mt-4">
        <div class="col-11">
            <div class="col-12 p-0 d-flex align-items-center">
                <div class="col-2 p-0">
                    <span class="title-form">ชื่อกลุ่มงาน</span>
                </div>
                <div class="col-10">
                    <input type="text" name="name_group[]" class="spec-input px-2">
                </div>
            </div>
            <div class="col-12 mt-4 p-0 d-flex align-items-center">
                <div class="col-2 p-0">
                    <span class="title-form">รองผู้อำนวยการ</span>
                </div>
                <div class="col-10">
                    <input type="text" name="email_deputy[]" class="spec-input px-2">
                </div>
            </div>
            <div class="col-12 mt-4 p-0 d-flex align-items-start">
                <div class="col-2 p-0">
                    <span class="title-form">เจ้าหน้าที่</span>
                </div>

                <div class="col-10">
                    <textarea name="email_employee" id="email_employee" name="email_employee[]" class="email_employee"
                        cols="95" rows="3" style="resize: none"></textarea>
                </div>
            </div>
        </div>
        <div class="col-1 p-0 d-flex align-items-start">
            <div class="col-12 p-0 d-flex align-items-center justify-content-between title-edit"
                style="cursor:pointer;">
                <img src="/img/icon/delete-material-copy-25.png" alt="">
                <span class="title-edit-text">ลบกลุ่มงาน</span>
            </div>
        </div>
    </div>

</div>










<div class="col-12 btn-add-group py-3 pl-3 mt-4 d-flex justify-content-center">
    <button type="button" class="add-group">+ เพิ่มกลุ่มงานใหม่</button>
</div>


<div class="col-12 py-3 pl-3 mt-5 d-flex justify-content-center">
    <a href="#" class="cacnel-form mr-3 d-flex align-items-center justify-content-center">ยกเลิก</a>
    <button class="submit-form" type="button">บันทึก</button>
</div>





@endsection





@section('script')
<script>
    $(document).ready(function () {
        $('.btn-add-group').on('click', function () {
            $('.spec-div').last().clone().appendTo('.space-group').each(function () {
                $(this).find('input').val('');
                $(this).find('textarea').val('');
            });
        });

        $('.submit-form').on('click', function () {

            var name_group = $("input[name='name_group[]']").map(function () {
                return $(this).val();
            }).get();

            var email_deputy = $("input[name='email_deputy[]']").map(function () {
                return $(this).val();
            }).get();
            var email_employee = $("input[name='email_employee[]']").map(function () {
                return $(this).val();
            }).get();


            var new_data = new FormData();
            new_data.append('name_group', name_group);
            new_data.append('email_deputy', email_deputy);
            new_data.append('email_employee', email_employee);
        });




        $('.space-group').on('click', '.title-edit', function () {
            if ($('.spec-div').length > 1) {
                $(this).parents('.spec-div').fadeOut('fast', function () {
                    $(this).remove();
                });
            }else{
                alert('ไม่สามารถลบการ์ดที่จำนวนเหลือแค่ 1 ได้');
            }
        });







    });

</script>
@endsection
