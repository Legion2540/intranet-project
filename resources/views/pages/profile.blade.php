@extends('layout.profile')
@section('style')
    <style>

    </style>
@endsection

@section('content')



{{-- Approval --}}
@if(Auth::user()->type == 'approval')
    @include('include/profile/approval/appr_top_profile')
    @include('include/profile/approval/appr_booking')

    {{-- @elseif(Auth::user()->type == 'employee')
        @include('include/profile/user/top_profile')
    @endif --}}


    @if (Request::path() == 'profile/board')
        @include('include/profile/user/myBoard')

    @elseif(Request::path() == "profile/blog")
        @include('include/profile/user/myblog')

    @elseif(Request::path() == "profile/media")
        @include('include/profile/user/mymedia')

    @elseif(Request::path() == "profile/booking")
        @include('include/profile/user/booking_user')

    @elseif(Request::path() == "profile/products")
        @include('include/profile/user/myproducts')

    @elseif(Request::path() == "profile/group")
        @include('include/profile/user/mygroup')

    @elseif (Request::path() == 'profile/approved')

        @include('include/profile/approval/appr_booking')
    @endif




@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#booking_car_profile').DataTable();
            $('#booking_room_profile').DataTable();
        });
    </script>

@endsection
