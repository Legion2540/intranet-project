@extends('layout.faq')

@section('contentfaqs')

<style>
    .text-headfaq {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .text-head {
        color: #4f72e5;
        font-size: 18px;
        font-family: Kanit-Regular;
    }

    .select-list {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .drop_detial_room-arrow {
        border: solid blue;
        border-width: 0 1px 1px 0;
        display: inline-block;
        width: 7px;
        height: 7px;
        transition: .2s ease;
    }

    .drop_detial_room-right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .drop_detial_room-down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

    .list_detial-equipment ul {
        list-style: none;
        padding-left: 15px;
        margin: 5px;
    }

    .list_detial-equipment ul li::before {
        content: "\2022";
        color: blue;
        font-weight: bold;
        display: inline-block;
        width: 1em;
    }

    .textarea-detel {
        width: 390px;
        height: 131px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .input-name {
        width: 390px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .btn-rq {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        padding: 0;
        color: #ffffff;
        font-size: 14px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
    }

</style>

<div class="card rounded-0 border-0" style="min-height:650px;padding: 19px 30px 19px 23px;">
    <div class="col-12 d-flex px-0 align-items-center">
        <img class="mr-3" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
        <span class="text-headfaq">คำถามที่พบบ่อย FAQs</span>
    </div>
    <div class="col-12 mt-2 d-flex">
        <div class="col-7 px-2">
            <div class="col-12 d-flex">
                <div class="col-3 px-0">
                    <span>เลือกหมวดคำถาม</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            @for ($i = 0; $i < 6; $i++) <div class="col-12 px-0 drop_detial_equipment">
                <hr class="mx-3">
                <div class="col-12 mt-2 pl-4 d-flex">
                    <div class="col-1">
                        <img src="/img/icon/settings-material.png" alt="">
                    </div>
                    <div class="col-10 px-0">
                        <span>จะเริ่มต้นธุรกิจส่งออกต้องเตรียมอะไรบ้าง</span>
                    </div>
                    <div class="col-1 px-0">
                        <span class="drop_detial_room-arrow drop_detial_room-right"></span>
                    </div>
                </div>
                <div class="col-12 mt-2 pl-5 d-flex">
                    <div class="col-12 list_detial-equipment border" style="display:none;">
                        <ul>
                            <li>เซ็นเซอร์ APS-C CMOS ความละเอียด 24.1 ล้านพิกเซล พร้อมหน่วยประมวลผลภาพ DIGIC 8</li>
                            <li>เทคโนโลยี Dual Pixel CMOS AF</li>
                            <li>ออโต้โฟกัส 143 จุด</li>
                            <li>ภาพนิ่งและภาพเคลื่อนไหว (One-Shot & Servo AF)</li>
                            <li>บันทึกวิดีโอความละเอียดสูงระดับ 4K</li>
                        </ul>
                    </div>
                </div>
        </div>
        @endfor
    </div>
    <div class="col-5 p-2" style="border-radius: 10px;background-color: #e8e8e8;height: fit-content;">
        <div class="col-12">
            <span class="text-head">ส่งคำถาม</span>
        </div>
        <div class="col-12 mt-3">
            <span>คำถาม</span>
            <input type="text" class="form-control">
        </div>
        <div class="col-12 mt-3">
            <span>รายละเอียด</span>
            <textarea class="textarea-detel" id="exampleFormControlInput1"></textarea>
        </div>
        <div class="col-12 mt-3">
            <span>ชื่อ-สกุล</span>
            <input type="text" class="input-name">
        </div>
        <div class="col-12 mt-3">
            <span>อีเมล</span>
            <input type="text" class="input-name">
        </div>
        <div class="col-12 my-3 d-flex justify-content-end">
            <button class="form-control btn-rq">ส่งคำถาม</button>
        </div>
    </div>
</div>
</div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.drop_detial_equipment').on('click', function () {
            $(this).find('.drop_detial_room-right').toggleClass('drop_detial_room-down');
            $(this).find('.list_detial-equipment').toggle();

        });
    });

</script>
@endsection
