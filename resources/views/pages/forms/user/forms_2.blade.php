@extends('layout.forms')
@section('contentforms')
<style>

    .btn-addadmin{
        width: 73px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color:#4f72e5;
        background-color: #ffffff;
        font-family: Kanit-Regular;
    }

    .input-name{
        width: 387px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-position{
        width: 407px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-other{
        width: 418px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select-office{
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select-list{
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .date{
        width: 193px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        position: relative;
    }

    #inputcalendar1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    .text-head{
        font-size: 14px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-area{
        width: 1049px;
        height: 90px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        resize: none;
    }

    .form2_ul_li li{
        margin: 5px;
    }
    /* ul li{
        margin: 5px;
    } */

</style>
<div class="card rounded-0 border-0" style="min-height:650px;padding: 19px 30px 19px 23px;">
    <div class="col-12 d-flex px-0">
        <div class="col-10 px-0 d-flex align-items-center">
            <img class="mr-2" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
            <span class="text-headform ml-2">
                แบบมอบหมายงานการทดลองปฏิบัติหน้าที่ราชการ
            </span>
        </div>
        <div class="col-2 px-0 d-flex align-items-center justify-content-end">
            <span style="font-size: 14px;color:#4f72e5">(แบบฟอร์ม 1)</span>
        </div>
    </div>
    <hr class="ml-3" style="border-top: 1px dashed #8c8b8b;background-color: white;">
    <div class="mx-4 text-14">
        <div class="col-12 px-0">
            <span class="text-head">1. ผู้ทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-name form_1">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-position form_1">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>ฝ่าย/งาน/กลุ่มงาน</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>สำนัก/กอง/ศูนย์</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>กรม/สำนักงาน</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-office" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-7 px-0">
                    <span>ระยะเวลาทดลองปฏิบัติหน้าที่ราชการ ตั้งแต่วันที่</span>
                </div>
                <div class="col-5 px-0">
                    <input class="form-control date" type="date" value="2011-08-19" id="inputcalendar1">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ถึงวันที่</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control date" type="date" value="2011-08-19" id="inputcalendar2">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head">2. ผู้ดูแลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12 px-0 mt-2 d-flex add_admin">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-name form_1">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-position form_1">
                </div>
            </div>
        </div>
        <div class="p-0 results"></div>
        <div class="col-12 mt-2 pl-4  div_add_admin">
            <button type="button" class="btn-addadmin" onclick="clone()">+ ผู้ดูแล</button>
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head">3. ผลการปฏิบัติงาน</span>
        </div>
        <div class="form-group col-12 px-0 mt-2">
            <label for="comment">รายละเอียดของงาน</label>
            <textarea class="form-control text-area" rows="5" id="comment"></textarea>
        </div>
        <div class="form-group col-12 px-0 mt-2">
            <label for="comment">ความรู้ ความสามารถ ทักษะ สมรรถนะ และพฤติกรรมที่จำเป็นสำหรับการปฏิบัติงาน</label>
            <textarea class="form-control text-area" rows="5" id="comment"></textarea>
        </div>
        <div class="form-group col-12 px-0 mt-2">
            <label for="comment">อื่นๆถ้ามี</label>
            <textarea class="form-control text-area" rows="5" id="comment"></textarea>
        </div>
        <div class="form-group col-12 px-0 mt-2">
            <label for="comment">ตัวชี้วัดความสำเร็จของงาน</label>
            <textarea class="form-control text-area" rows="5" id="comment"></textarea>
        </div>
        <div class="form-group col-12 px-0 mt-2">
            <label for="comment">ผลผลิตของงานที่คาดหวัง </label>
            <textarea class="form-control text-area" rows="5" id="comment"></textarea>
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head">4. การพัฒนาผู้ทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12">
            <span>ผู้ทดลองปฏิบัติหน้าที่ราชการจะต้องเข้าร่วมในการปฐมนิเทศ และอบรมหลักสูตรต่าง ๆ ภายในระยะเวลาทดลองปฏิบัติหน้าที่ราชการ ดังนี้</span>
            <ul class="form2_ul_li" style="list-style-type:none;">
                <li>4.1 การปฐมนิเทศเพื่อให้มีความรู้เกี่ยวกับส่วนราชการ</li>
                <li>4.2 การเรียนรู้ด้วยตนเองเพื่อให้มีความรู้เกี่ยวกับกฎหมาย กฎ ระเบียบแบบแผนของทางราชการ</li>
                <li class="mb-0">4.3 การอบรมสัมมนาร่วมกันเพื่อปลูกฝังการประพฤติปฏิบัติตนเป็นข้าราชการที่ดี</li>
                <li class="d-flex align-items-center my-0">
                    <div>
                        <span>4.4 การอบรมอื่น ๆ ที่ส่วนราชการกำหนด (ถ้ามี)</span>
                    </div>
                    <div class="ml-2">
                        <input type="text" class="form-control input-other form_1">
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head">5. การประเมินผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12">
            <ul class="form2_ul_li" style="list-style-type:none;">
                <li>5.1 ประเมินผลการทดลองปฏิบัติหน้าที่ราชการ โดยคณะกรรมการประเมินผลการทดลองปฏิบัติหน้าที่ราชการ</li>
                <li class="mb-0">5.2 รายละเอียดการประเมิน ประกอบด้วย ๒ ส่วน คือ</li>
                <li class="align-items-center my-0 ml-4 pl-1">
                    <div>
                        <span>ส่วนที่ 1 ผลสัมฤทธิ์ของการทดลองปฏิบัติหน้าที่ราชการ</span>
                        <ul class="form2_ul_li" style="list-style-type:none;">
                            <li>ความสามารถในการเรียนรู้งาน</li>
                            <li>ความสามารถในการปรับใช้ความรู้กับงานในหน้าที่</li>
                            <li class="mb-0">ความสำเร็จของงานที่ได้มอบหมาย</li>
                            <li class="d-flex align-items-center my-0">
                                <div>
                                    <span>อื่นๆ (ถ้ามี)</span>
                                </div>
                                <div class="ml-2">
                                    <input type="text" class="form-control input-other form_1">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="mt-2">
                        <span>ส่วนที่ 2 พฤติกรรมของผู้ทดลองปฏิบัติหน้าที่ราชการ</span>
                        <ul class="form2_ul_li" style="list-style-type:none;">
                            <li>ความประพฤติ</li>
                            <li>ความมีคุณธรรม จริยธรรม</li>
                            <li class="mb-0">การรักษาวินัย</li>
                            <li class="d-flex align-items-center my-0">
                                <div>
                                    <span>อื่นๆ (ถ้ามี)</span>
                                </div>
                                <div class="ml-2">
                                    <input type="text" class="form-control input-other form_1" >
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>5.3 ข้อมูลที่ใช้ประกอบการประเมินผลการทดลองปฏิบัติหน้าที่ราชการ ประกอบด้วย</li>
                <li>
                    <div class="ml-4">
                        <ul class="form2_ul_li" style="list-style-type:none;">
                            <li>บันทึกผลการทดลองปฏิบัติหน้าที่ราชการของผู้ดูแลการทดลองปฏิบัติหน้าที่ราชการ</li>
                            <li>รายงานผลการประเมินผลการทดลองปฏิบัติหน้าที่ราชการของผู้บังคับบัญชา</li>
                            <li>รายงานผลการพัฒนาตามที่ สำนักงาน ก.พ. กำหนด</li>
                        </ul>
                    </div>
                </li>
                <li class="mb-0">5.4 สัดส่วนของคะแนนและมาตรฐานการประเมิน</li>
                <li class="align-items-center my-0 ml-4 pl-1">
                    <div class="ml-4 pl-3">
                        <span>สัดส่วนคะแนนการทดลองปฏิบัติหน้าที่ราชการ</span>
                        <ul class="form2_ul_li" style="list-style-type:none;">
                            <li>ส่วนที่ 1 คะแนนผลสัมฤทธิ์ของการทดลองปฏิบัติหน้าที่ราชการ ร้อยละ 50</li>
                            <li>ส่วนที่ 2 คะแนนพฤติกรรมของผู้ทดลองปฏิบัติหน้าที่ราชการ ร้อยละ 50</li>
                        </ul>
                        <span>มาตรฐานการประเมิน แต่ละส่วนต้องได้คะแนนไม่ต่ำกว่า ร้อยละ 60</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-12 d-flex check justify-content-center mt-5 mb-3">
            <button type="submit" class="btn-edit-form shadow" onclick="edit()" style="display: none" >แก้ไข</button>
            <button type="submit" class="btn-check-form shadow" onclick="check()">ตรวจสอบ</button>
            <button type="submit" class="btn-home-form shadow" style="display: none" onclick="comback_home()">กลับหน้าหลัก</button>
            <button type="submit" class="btn-print-form shadow" style="display: none" ><img src="/img/icon/print-material-white.png" style="margin-right:10px; " alt="">พิมพ์</button>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

    $(document).ready(function() {
        ///
    });


    function check() {
        $('.div_add_admin').closest(".div_add_admin").hide();

        $('.btn-check-form').html('บันทึก');
        $('.check .btn-edit-form').css('display','block');

        if ($('.btn-check-form').hasClass('afterreadonly')) {
            $('.form_1').removeAttr('readonly').css('background-color', '#ffffff');
            Swal.fire({
                icon: 'success',
                title: 'บันทึกสำเร็จ',
                showConfirmButton: false,
                timer: 1700
            });

             // btn control
            $('.check .btn-edit-form').css('display','none');
            $('.check .btn-print-form').css('display','block');
            $('.check .btn-check-form').css('display','none');
            $('.check .btn-home-form').css('display','block');

        }else{
            $('.btn-check-form').addClass('afterreadonly');
        }

        $('.form_1').attr('readonly', true).css('background-color', '#f3f3f3');
        $('select').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('input[type="date"]').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('textarea').attr('disabled', 'true').css('background-color', '#f3f3f3');
    }

    function clone() {
        $('.add_admin').last().clone().appendTo('.results').find('.input-name, .input-position').val('');
    }


    function show_display(){
        $('.form_1').attr('readonly', false).css('background-color', '#ffffff');
        $('select').attr('disabled', false).css('background-color', '#ffffff');
        $('input[type="date"]').attr('disabled', false).css('background-color', '#ffffff');
        $('textarea').attr('disabled', false).css('background-color', '#ffffff');

        // btn control
        $('.check .btn-edit-form').css('display','none');
        $('.check .btn-print-form').css('display','none');
        $('.check .btn-check-form').css('display','block');
    }

    function edit(){
        show_display()
        $('.div_add_admin').closest(".div_add_admin").show();
    }

    function comback_home(){
        window.location.href='/'
    }

</script>
@endsection
