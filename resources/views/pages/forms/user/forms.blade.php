@extends('layout.uploadimage')
@section('contentuploadphoto')
<style>
    .tag_office {
        font-size: 10px;
        font-weight: 500;
        color: #4f72e5;
        text-decoration: underline;
        font-family: Kanit-Regular;
    }

    .text-12{
        font-size: 12px;
        font-weight: bold;
    }

    .text-headform {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .text_content {
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .text-headlistforms{
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .by_user {
        font-size: 10px;
        font-weight: 500;
        color: #4a4a4a;
        font-family: Kanit-ExtraLight;
    }

    .input-cearch {
        width: 420px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-family: Kanit-ExtraLight;
    }
    .position-icon-ceach {
        position: absolute;
        top: 25%;
        right: 3%;
    }


</style>
<div class="card rounded-0 mb-4 border-0" style="min-height: 650px;padding: 31px;">
    <div class="col-12 d-flex px-0">
        <div class="col-6 px-0 d-flex align-items-center">
            <img class="mr-2" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
            <span class="text-headform ml-2">แบบฟอร์ม</span>
        </div>
        <div class="col-6 px-0 pr-5 d-flex align-items-center justify-content-end">
            <div class="position-relative">
                <input class="input-cearch form-control" type="text" placeholder=" ค้นหารายการ">
                <img class="position-icon-ceach" src="/img/icon/search-material-bule.png" alt="">
            </div>
        </div>
    </div>
    <hr class="ml-3" style="border-top: 1px dashed #8c8b8b;background-color: white;">
    <div class="col-12 pr-0">
        <div class="col-12 px-0 d-flex">
            <div class="col-6 px-0">
                <span class="text-headlistforms">E - Form</span>
            </div>
            <div class="col-6 px-0 d-flex justify-content-end align-items-center">
                <img src="/img/icon/tune-material-copy-4@2x.png" alt="" width="16px" height="16px">
                <img src="/img/icon/group-14-copy-2.png" alt="" class="ml-3">
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#7992e4">
                    <img src="/img/icon/description-material-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6">
                        <span class="text_content">แบบฟอร์มขอมีบัตรข้าราชการ
                        </span>
                    </div>
                    <div class="col-3 d-flex">
                        {{-- <img src="/img/type_files/group-20-copy-16.png" alt="" class="mr-2"> --}}
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#7992e4">
                    <img src="/img/icon/description-material-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6">
                        <span class="text_content">แบบมอบหมายงานการทดลองปฏิบัติหน้าที่ราชการ
                        </span>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#7992e4">
                    <img src="/img/icon/description-material-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6">
                        <span class="text_content">แบบบันทึกผลการทดลองปฏิบัติหน้าที่ราชการ
                        </span>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/comment-bule.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#7992e4">
                    <img src="/img/icon/description-material-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6">
                        <span class="text_content">แบบประเมินผลการทดลองปฏิบัติหน้าที่ราชการ
                        </span>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/comment-bule.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#7992e4">
                    <img src="/img/icon/description-material-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6">
                        <span class="text_content">แบบรายงานการประเมินผลการทดลองปฏิบัติหน้าที่ราชการ
                        </span>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 px-0 d-flex mt-4">
            <div class="col-6 px-0">
                <span class="text-headlistforms">แบบฟอร์มตามสำนัก</span>
            </div>
            <div class="col-6 px-0 d-flex justify-content-end align-items-center">
                <img src="/img/icon/tune-material-copy-4@2x.png" alt="" width="16px" height="16px">
                <img src="/img/icon/group-14-copy-2.png" alt="" class="ml-3">
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#96cfee">
                    <img src="/img/icon/group-8-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6 p-0">
                        <div class="col-12 text-start">
                            <a href="#" class="tag_office">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>
                        <div class="col-12 text-start d-flex align-items-start">
                            <span class="text_content">แบบฟอร์ม
                            </span>
                        </div>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#96cfee">
                    <img src="/img/icon/group-8-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6 p-0">
                        <div class="col-12 text-start">
                            <a href="#" class="tag_office">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>
                        <div class="col-12 text-start d-flex align-items-start">
                            <span class="text_content">แบบฟอร์ม
                            </span>
                        </div>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#a4b6f0">
                    <img src="/img/icon/group-18.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6 p-0">
                        <div class="col-12 text-start">
                            <a href="#" class="tag_office">กลุ่มตรวจสอบภายใน</a>
                        </div>
                        <div class="col-12 text-start d-flex align-items-start">
                            <span class="text_content">แบบฟอร์ม
                            </span>
                        </div>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                        <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/comment-bule.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#e9bdee">
                    <img src="/img/icon/group-8-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6 p-0">
                        <div class="col-12 text-start">
                            <a href="#" class="tag_office">สำนักส่งเสริมการค้าสินค้าไลฟ์สไตล์</a>
                        </div>
                        <div class="col-12 text-start d-flex align-items-start">
                            <span class="text_content">แบบฟอร์ม
                            </span>
                        </div>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/comment-bule.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-12 d-flex p-0">
                <div class="d-flex align-items-center justify-content-center"
                    style="width:70px;height:70px; background-color:#96cfee">
                    <img src="/img/icon/group-8-copy.png" style="object-fit: contain" alt="">
                </div>
                <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
                    <div class="col-6 p-0">
                        <div class="col-12 text-start">
                            <a href="#" class="tag_office">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>
                        <div class="col-12 text-start d-flex align-items-start">
                            <span class="text_content">แบบฟอร์ม
                            </span>
                        </div>
                    </div>
                    <div class="col-3 d-flex">
                        <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                        <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                    </div>
                    <div class="col-2">
                        <div class="col-12">
                            <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span class="by_user">19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                    <div class="col-1 more_knowledge">
                        <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                        <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="more ml-1">
                        {{-- <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1"> --}}

                        <ul class="dropdown-menu" style="display: none">
                            <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                    style="background-color:#4f72e5;">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                        </div>

                                    </div>
                                </a></li>
                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span
                                                style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            <li><a class="dropdown-item py-1 px-2" href="# ">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                        </div>
                                        <div class="col-10 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li><a class="dropdown-item py-1 px-2" href="#">
                                    <div class="d-flex">
                                        <div class="col-2 px-1">
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                        </div>
                                        <div class="col-2 px-1">
                                            <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.more').on('click', function () {
            $(this).parents('.more_knowledge').find('.dropdown-menu').toggle();
        });
    });

</script>
@endsection
