@extends('layout.forms')
@section('contentforms')
<style>

    .input-col-6 {
        width: 400px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-col-4 {
        width: 225px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-age {
        width: 53px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-county {
        width: 346px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-symbol {
        width: 127px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-district {
        width: 251px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-city {
        width: 180px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-identity {
        width: 25px;
        height: 30px;
        margin-right: 4px;
        border-radius: 8px;
        padding: 7px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-position {
        width: 224px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-division {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-bureau {
        width: 397px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-organization {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-ministry {
        width: 354px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-degree {
        width: 396px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-etc {
        width: 198px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-other {
        width: 366px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .inputcalendar {
        width: 237px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        position: relative;
    }

    #inputcalendar1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 80%;
    }

    #inputcalendar2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 80%;
    }

    .input-numberHome {
        width: 102px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-lane {
        width: 235px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-roat {
        width: 253px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-nocard {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .checkbox {
        width: 10px;
        height: 10px;
        border-radius: 0px !important;
        background-color: #ffffff;
    }

    .file-upload-content {
        width: 20px;
        height: 20px;
    }

    .btn-upload-photo {
        width: 90px;
        height: 26px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 14px;
        padding: 4px 18px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    div.options>label>input,
    div.category>label>input,
    div.change>label>input,
    div.newcard>label>input,
    div.case>label>input {
        visibility: hidden;
    }

    div.options>label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;
        width: 83px;
    }

    div.category>label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;
        width: 178px;
    }

    div.change>label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;

    }

    div.case>label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;
        width: 200px;
    }

    div.newcard>label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;
        width: 200px;
    }

    div.options>label>img,
    div.category>label>img,
    div.change>label>img,
    div.newcard>label>img,
    div.case>label>img {
        display: inline-block;
        height: 13px;
        width: 13px;
        border-radius: 3px;
        background: none;
    }

    div.options>label>input:checked+img,
    div.category>label>input:checked+img,
    div.change>label>input:checked+img,
    div.newcard>label>input:checked+img,
    div.case>label>input:checked+img {
        background: url(http://cdn1.iconfinder.com/data/icons/onebit/PNG/onebit_34.png);
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 13px 13px;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

</style>
<div class="card rounded-0 border-0" style="min-height:650px;padding: 19px 30px 19px 23px;">
    {{-- <div class="card shadow-sm rounded-0 border-0" style="width: 1143px;padding: 19px 30px 19px 23px;"> --}}
    <div class="col-12 d-flex px-0">
        <div class="col-6 px-0 d-flex align-items-center">
            <img class="mr-2" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
            <span class="text-headform">แบบฟอร์มขอมีบัตรข้าราชการ</span>
        </div>
        <div class="col-6 px-0 d-flex align-items-center justify-content-end">
            <div class="position-relative">
                <input class="input-cearch form-control" type="text" placeholder=" ค้นหารายการ">
                <img class="position-icon-ceach" src="/img/icon/search-material-bule.png" alt="">
            </div>
            <div class="mx-2">
                <img src="/img/icon/tune-material-copy-3@2x.png" alt="" width="23px" height="23px">
            </div>
            <button class="btn btn-create-form py-0">+ สร้างแบบฟอร์ม</button>
        </div>
    </div>
    <hr class="ml-3" style="border-top: 1px dashed #8c8b8b;background-color: white;">
    <div class="mx-3 text-14">
        <div class="col-12 px-0 d-flex mb-2">
            <div class="col-6 px-0 d-flex">
                <div class="col-2 px-0 d-flex align-items-center">
                    <span>เขียนที่</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control input-col-6 form_governmentcard" type="text">
                </div>
            </div>
            <div class="col-6 px-0 d-flex ml-4">
                <div class="col-2 px-0 d-flex align-items-center">
                    <span>วันที่</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control inputcalendar test_date" id="inputcalendar1" data-date-format="DD M YYYY" type="date"
                        id="birthday" name="birthday">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mb-2 d-flex">
            <div class="col-6 px-0 d-flex">
                <div class="col-2 px-0 d-flex align-items-center">
                    <span>คำนำหน้า</span>
                </div>
                <div class="col-10 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio_prefix" value="2" />
                            <img />
                            <span class="ml-1">นาย</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_prefix" value="2" />
                            <img />
                            <span class="ml-1">นาง</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_prefix" value="2" />
                            <img />
                            <span class="ml-1">นางสาว</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 d-flex mb-2">
            <div class="col-6 px-0 d-flex">
                <div class="col-2 px-0 d-flex align-items-center">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control input-col-6 form_governmentcard" type="text">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-6 p-0 d-flex">
                <div class="col-8 p-0 d-flex">
                    <div class="col-3 p-0 d-flex align-items-center">
                        <span>เกิดวันที่</span>
                    </div>
                    <div class="col-9 p-0">
                        <input class="form-control inputcalendar" id="inputcalendar2" type="date">
                    </div>
                </div>
                <div class="col-4 p-0 ml-5 d-flex">
                    <div class="col-2 p-0 d-flex align-items-center">
                        <span>อายุ</span>
                    </div>
                    <div class="col-10 p-0 pl-2 d-flex align-items-center">
                        <input class="form-control input-age form_governmentcard" type="number">
                        <span class="ml-2">ปี</span>
                    </div>
                </div>
            </div>
            <div class="col-6 p-0 pl-4 d-flex">
                <div class="col-6 p-0 d-flex">
                    <div class="col-4 p-0 d-flex align-items-center">
                        <span>สัญชาติ</span>
                    </div>
                    <div class="col-8 p-0 p-1">
                        <input type="text" class="form-control input-symbol form_governmentcard">
                    </div>
                </div>
                <div class="col-6 p-0 d-flex">
                    <div class="col-4 p-0 d-flex align-items-center">
                        <span>หมู่โลหิต</span>
                    </div>
                    <div class="col-8 p-0 p-1">
                        <input type="text" class="form-control input-symbol form_governmentcard">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-4 p-0 d-flex">
                <div class="col-6 p-0 d-flex align-items-center">
                    <span>ที่อยู่ตามทะเบียนบ้านเลขที่</span>
                </div>
                <div class="col-6 p-0 pl-2">
                    <input type="text" class="form-control input-numberHome form_governmentcard">
                </div>
            </div>
            <div class="col-4 p-0 d-flex">
                <div class="col-3 p-0 d-flex align-items-center">
                    <span>ตรอก/ซอย</span>
                </div>
                <div class="col-9 p-0 pl-2">
                    <input type="text" class="form-control input-lane form_governmentcard">
                </div>
            </div>
            <div class="col-4 p-0 d-flex">
                <div class="col-3 p-0 d-flex align-items-center justify-content-center">
                    <span>ถนน</span>
                </div>
                <div class="col-9 p-0 pl-2">
                    <input type="text" class="form-control input-roat form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-5 p-0 d-flex align-items-center">
                <div class="p-0" style="width: 88px">
                    <span class="p-0">ตำบล/แขวง</span>
                </div>
                <div classs="p-0">
                    <input type="text" class="form-control input-county form_governmentcard">
                </div>
            </div>
            <div class="col-4 d-flex ml-2 align-items-center">
                <div class="col-3 p-0">
                    <span>อำเภอ/เขต</span>
                </div>
                <div class="col-9 p-0">
                    <input type="text" class="form-control input-district form_governmentcard">
                </div>
            </div>
            <div class="col-3 d-flex align-items-center">
                <div class="col-3 p-0">
                    <span>จังหวัด</span>
                </div>
                <div class="col-9 p-0">
                    <input type="text" class="form-control input-city form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-5 p-0 d-flex align-items-center">
                <div class="p-0" style="width: 88px">
                    <span class="p-0">รหัสไปรษณีย์</span>
                </div>
                <div classs="p-0">
                    <input type="text" class="form-control input-county form_governmentcard">
                </div>
            </div>
            <div class="col-7 d-flex ml-2 align-items-center">
                <div class="p-0" style="width: 79px">
                    <span>โทรศัพท์</span>
                </div>
                <div class="p-0">
                    <input type="text" class="form-control input-district form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-10 d-flex p-0 mb-2 align-items-center">
            <div class="col-3 p-0">
                <span>หมายเลขประจำตัวประชาชน</span>
            </div>
            <div class="col-9 p-0">
                <fieldset class="d-flex align-items-center">
                    <input type="text" class="form-control input-identity" maxlength="1">
                    <span class="mr-1 field">-</span>
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <span class="mr-1 field">-</span>
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <span class="mr-1 field">-</span>
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <input type="text" class="form-control input-identity " maxlength="1">
                    <span class="mr-1 field">-</span>
                    <input type="text" class="form-control input-identity " maxlength="1">
                </fieldset>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-4 p-0 d-flex">
                <div class="col-6 p-0 d-flex align-items-center">
                    <span>ที่อยู่ปัจจุบันที่ติดต่อได้</span>
                </div>
                <div class="col-6 p-0 pl-2">
                    <input type="text" class="form-control input-numberHome form_governmentcard">
                </div>
            </div>
            <div class="col-4 p-0 d-flex">
                <div class="col-3 p-0 d-flex align-items-center">
                    <span>ตรอก/ซอย</span>
                </div>
                <div class="col-9 p-0 pl-2">
                    <input type="text" class="form-control input-lane form_governmentcard">
                </div>
            </div>
            <div class="col-4 p-0 d-flex">
                <div class="col-3 p-0 d-flex align-items-center justify-content-center">
                    <span>ถนน</span>
                </div>
                <div class="col-9 p-0 pl-2">
                    <input type="text" class="form-control input-roat form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-5 p-0 d-flex align-items-center">
                <div class="p-0" style="width: 88px">
                    <span class="p-0">ตำบล/แขวง</span>
                </div>
                <div classs="p-0">
                    <input type="text" class="form-control input-county form_governmentcard">
                </div>
            </div>
            <div class="col-4 d-flex ml-2 align-items-center">
                <div class="col-3 p-0">
                    <span>อำเภอ/เขต</span>
                </div>
                <div class="col-9 p-0">
                    <input type="text" class="form-control input-district form_governmentcard">
                </div>
            </div>
            <div class="col-3 d-flex align-items-center">
                <div class="col-3 p-0">
                    <span>จังหวัด</span>
                </div>
                <div class="col-9 p-0">
                    <input type="text" class="form-control input-city form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-12 p-0 d-flex align-items-center">
                <div class="p-0" style="width: 88px">
                    <span class="p-0">รหัสไปรษณีย์</span>
                </div>
                <div classs="p-0">
                    <input type="text" class="form-control input-county form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-2 p-0">
                <span>เจ้าหน้าที่ของรัฐประเภท</span>
            </div>
            <div class="col-10 p-0 category d-flex align-items-center">
                <label title="item3">
                    <input type="radio" name="radio_category" value="2" />
                    <img />
                    <span class="ml-1">ข้าราชการ</span>
                </label>
                <label title="item3">
                    <input type="radio" name="radio_category" value="2" />
                    <img />
                    <span class="ml-1">ลูกจ้างประจำ</span>
                </label>
                <label title="item3">
                    <input type="radio" name="radio_category" value="2" />
                    <img />
                    <span class="ml-1">พนักงานราชการ</span>
                </label>
                <label title="item3">
                    <input type="radio" name="radio_category" value="2" />
                    <img />
                    <span class="ml-1">ข้าราชการบำเหน็จบำนาญ</span>
                </label>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-6 d-flex p-0 align-items-center">
                <div class="col-6 p-0">
                    <span>รับราชการ/ปฏิบัติ/เคยสังกัดแผนก/งาน</span>
                </div>
                <div class="col-6 p-0">
                    <input type="text" class="form-control input-position form_governmentcard">
                </div>
            </div>
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 p-0 ml-3">
                    <span>ฝ่าย/ส่วน</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-division form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-6 px-0 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>กอง/สำนัก</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-bureau form_governmentcard">
                </div>
            </div>
            <div class="col-6 d-flex align-items-center">
                <div class="col-4 p-0 ml-3">
                    <span>กรม/เทศบาล/องค์การ</span>
                </div>
                <div class="col-8 px-0">
                    <input type="text" class="form-control input-organization form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-6 px-0 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>กระทรวง/ทบวง</span>
                </div>
                <div class="col-9 px-0">
                    <input type="text" class="form-control input-ministry form_governmentcard">
                </div>
            </div>
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 p-0 ml-3">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-division form_governmentcard">
                </div>
            </div>
        </div>
        <div class="col-12 mb-2 px-0 d-flex align-items-center">
            <div class="col-1 px-0">
                <span>ระดับ/ยศ</span>
            </div>
            <div class="col-11 px-0">
                <input type="text" class="form-control input-degree form_governmentcard">
            </div>
        </div>
        <div class="col-12 d-flex p-0 mb-2">
            <div class="col-3 px-0">
                <span>มีความประสงค์ขอมีบัตรประจำตัวของรัฐ</span>
            </div>
            <div class="col-9 px-0 d-flex">
                <div class="col-1">
                    <span>กรณี</span>
                </div>
                <div class="col-11 p-0">
                    <div class="col-12 px-0 mb-2 case d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio_case" value="2" onchange="radio_firstcard()" />
                            <img />
                            <span class="ml-1">1. ขอมีบัตรครั้งแรก</span>
                        </label>
                    </div>
                    <div class="col-12 px-0 d-flex">
                        <div class="col-4 p-0 case d-flex ">
                            <label title="item3">
                                <input type="radio" name="radio_case" value="2" onchange="radio_newcard()" />
                                <img />
                                <span class="ml-1">2. ขอมีบัตรครั้งใหม่ เนื่องจาก</span>
                            </label>
                        </div>
                        <div class="col-8 p-0  d-flex newcard">
                            <label title="item3">
                                <input type="radio" name="radio_newcard" value="2" />
                                <img />
                                <span class="ml-1">บัตรหมดอายุ</span>
                            </label>
                            <label title="item3">
                                <input type="radio" name="radio_newcard" value="2" />
                                <img />
                                <span class="ml-1">บัตรหายหรือถูกทำลาย</span>
                            </label>
                        </div>

                    </div>
                    <div class="col-12 pl-5 mb-3 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span>หมายเลขบัตรเดิม</span>
                        </div>
                        <div class="col-4 px-0">
                            <input type="text" class="form-control form_governmentcard input-nocard">
                        </div>
                        <div class="col-5 px-0 ml-3">
                            <span>(ถ้าทราบ)</span>
                        </div>
                    </div>
                    <div class="col-12 px-0 d-flex">
                        <div class="col-3 p-0 case d-flex mb-2">
                            <label title="item3">
                                <input type="radio" name="radio_case" value="2" onchange="radio_change()" />
                                <img />
                                <span class="ml-1">3.ขอเปลี่ยนบัตร เนื่องจาก</span>
                            </label>
                        </div>
                        <div class="col-9 p-0 d-flex change mb-2">
                            <label title="item3">
                                <input type="radio" name="radio_change" value="2" />
                                <img />
                                <span class="ml-1">เปลี่ยนตำแหน่ง/เลื่อนยศ</span>
                            </label>
                            <label title="item3">
                                <input type="radio" name="radio_change" value="2" />
                                <img />
                                <span class="ml-1">เปลี่ยนชื่อ</span>
                            </label>
                            <label title="item3">
                                <input type="radio" name="radio_change" value="2" />
                                <img />
                                <span class="ml-1">เปลี่ยนนามสกุล</span>
                            </label>
                            <label title="item3">
                                <input type="radio" name="radio_change" value="2" />
                                <img />
                                <span class="ml-1">ชำรุด</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 change d-flex align-items-center" style="margin-left:164px; ">
                        <label title="item3">
                            <input type="radio" name="radio_change" value="2" />
                            <img />
                            <span class="ml-1">อื่นๆ</span>
                        </label>
                        <input type="text" class="form-control input-etc form_governmentcard ml-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex p-0 mt-3 mb-2">
            <div class="col-3 pr-0 d-flex">
                <div class="col-2 px-0">
                    <span>รูปถ่าย</span>
                </div>
                <div class="col-10">
                    <div class="vertical-center dropzone" id="dropzone"
                        style="border-style: dashed;width: 143px;height: 168px;color:#4f72e5">
                        <div class="col-12 mt-4 d-flex justify-content-center">
                            <img src="/img/icon/insert-photo-material.png" alt="" width="30px" height="30px">
                        </div>
                        <div class="col-12 d-flex justify-content-center mt-2">
                            <span class="d-flex justify-content-center"
                                style="font-size: 12px;text-align: center;">Choose file from your device or drag image
                                here.</span>
                        </div>
                        <div class="col-12 d-flex justify-content-center">
                            <span style="font-size: 12px;color:red">(jpg., jpeg.)</span>
                        </div>
                    </div>
                    <div class="vertical-center avatar" style="width: 143px;height: 168px;display:none">
                        <img class="profile-pic w-100 h-100" src=""
                            style="object-fit: contain;image-rendering: pixelated;" />
                    </div>
                    <div class="mt-3 mr-4 d-flex justify-content-center align-items-center">
                        <div class="col-9 px-0 d-flex justify-content-end">
                            <input class="img-upload-input" type='file' hidden />
                            <button type="submit" id="createalbum" class="btn-upload-photo shadow">อัพโหลด</button>
                        </div>
                        <div class="col-3 px-0 pl-2">
                            <img src="/img/icon/check-material.png" alt="" class="upload-img">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9 px-0">
                <div class="col-12 px-0 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span>หลักฐานอื่นๆ (ถ้ามี)</span>
                    </div>
                    <div class="col-6 pr-0">
                        <input type="text" class="form-control input-other form_governmentcard">
                    </div>
                    <div class="col-4 d-flex">
                        <input class="file-upload-input" type='file' onchange="uploadfile()" hidden />
                        <button class="file-upload-btn mr-2" type="button"
                            onclick="$('.file-upload-input').trigger( 'click' )">Browse File</button>
                        <img src="/img/icon/check-material.png" alt="" class="file-upload-content">
                        <span class="typefile" style="color: #ee2a27;">(pdf., docx., excel.)</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex check justify-content-center mt-5 mb-3">
            <button type="submit" class="btn-edit-form shadow" onclick="edit()" style="display: none">แก้ไข</button>
            <button type="submit" class="btn-check-form shadow" onclick="check()">ตรวจสอบ</button>
            <button type="submit" class="btn-home-form shadow" style="display: none"
                onclick="comback_home()">กลับหน้าหลัก</button>
            <button type="submit" class="btn-print-form shadow" style="display: none"><img
                    src="/img/icon/print-material-white.png" style="margin-right:10px; " alt="">พิมพ์</button>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.file-upload-content').toggle();
        $('.upload-img').toggle();
        // $('.avatar').toggle();


        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.dropzone').remove();
                    $('.profile-pic').attr('src', e.target.result);
                    $('.avatar').show();
                    $('.upload-img').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".dropzone").on('dragenter', function (ev) {
            // Entering drop area. Highlight area
            $(".dropzone").addClass("highlightDropArea");
        });

        $(".dropzone").on('dragleave', function (ev) {
            // Going out of drop area. Remove Highlight
            $(".dropzone").removeClass("highlightDropArea");
        });

        $(".dropzone").on('drop', function (ev) {
            ev.preventDefault();
            ev.stopPropagation();
            readURL(ev.originalEvent.dataTransfer);
        });

        $(".dropzone").on('dragover', function (ev) {
            ev.preventDefault();
        });

        $(".img-upload-input").on('change', function () {
            readURL(this);
        });

        $(".btn-upload-photo").on('click', function () {
            $(".img-upload-input").click();
        });

        // radio hidden
        $('div.change label img,div.newcard label img').css('background-color', '#f3f3f3');
        $('.input-nocard').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.input-etc').attr('readonly', true).css('background-color', '#f3f3f3');

        $('input[name="radio_newcard"]').attr('disabled', 'true');
        $('input[name="radio_change"]').attr('disabled', 'true');

        // บัตรประชาชน
        $(".input-identity").keyup(function () {
            if (this.value.length == this.maxLength) {
                if ($(this).next().hasClass("field")) {
                    $(this).next().next('.input-identity').focus();
                } else {
                    $(this).next('.input-identity').focus();
                }
            }
        });

        $('.input-identity').on('keypress keyup', function (e) {
            // console.log(e.keyCode);
            if (e.keyCode === 8) {
                $(this).prev().focus().val('');
                if ($(this).prev().hasClass('field')) {
                    $(this).prev().prev().focus().val('');
                }
            }
        })
    });


    function uploadfile() {
        $('.file-upload-content').show();
        $('.typefile').toggle();
    }

    function check() {
        $('.form_governmentcard').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.input-identity').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('input[type="date"]').attr('disabled', 'true').css('background-color', '#f3f3f3');

        $('input[type="radio"]').attr('disabled', 'true');
        $('div.options label img,div.change label img,div.category label img,div.case label img').css(
            'background-color', '#f3f3f3');

        $('.btn-check-form').html('บันทึก');
        $('.check .btn-edit-form').css('display', 'block');

        $('.file-upload-content').hide();
        $('.upload-img').hide();


        $('.file-upload-btn').css('display', 'none');
        $('.btn-upload-photo').css('display', 'none');
        $('.typefile').css('display', 'none');
        if ($('.btn-check-form').hasClass('afterreadonly')) {
            Swal.fire({
                icon: 'success',
                title: 'บันทึกสำเร็จ',
                showConfirmButton: false,
                timer: 1700
            });

            // btn control
            $('.check .btn-edit-form').css('display', 'none');
            $('.check .btn-print-form').css('display', 'block');
            $('.check .btn-check-form').css('display', 'none');
            $('.check .btn-home-form').css('display', 'block');

            $('.file-upload-content').hide();
            $('.upload-img').hide();

        } else {
            $('.btn-check-form').addClass('afterreadonly');
        }
    }

    function radio_firstcard() {
        $('input[name="radio_change"]').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('input[name="radio_newcard"]').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('div.change label img,div.newcard label img').css('background-color', '#f3f3f3');

        $('.input-etc').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.input-nocard').attr('readonly', true).css('background-color', '#f3f3f3');

        $('input[name="radio_newcard"]').attr('checked', false);
        $('input[name="radio_change"]').attr('checked', false);
        $('.input-nocard').val('');
        $('.input-etc').val('');
    }

    function radio_newcard() {
        $('div.newcard label img').css('background-color', '#ffffff');
        $('input[name="radio_newcard"]').attr('disabled', false);
        $('.input-nocard').attr('readonly', false).css('background-color', '#ffffff');

        // dis change
        $('input[name="radio_change"]').attr('disabled', 'true');
        $('div.change label img').css('background-color', '#f3f3f3');
        $('.input-etc').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.input-etc').val('');
        $('input[name="radio_change"]').attr('checked', false);
    }

    function radio_change() {
        $('div.change label img').css('background-color', '#ffffff');
        $('input[name="radio_change"]').attr('disabled', false);
        $('.input-etc').attr('readonly', false).css('background-color', '#ffffff');

        // dis newcard
        $('input[name="radio_newcard"]').attr('disabled', 'true');
        $('div.newcard label img').css('background-color', '#f3f3f3');
        $('.input-nocard').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.input-nocard').val('');
        $('input[name="radio_newcard"]').attr('checked', false);
    }

    function edit() {
        show_display()

        $('.btn-upload-photo').css('display', 'block');
        $('.file-upload-btn').css('display', 'block');
        $('.typefile').css('display', 'block');
    }

    function show_display() {
        $('.form_governmentcard').attr('readonly', false).css('background-color', '#ffffff');
        $('input[type="date"]').attr('disabled', false).css('background-color', '#ffffff');
        $('.input-identity').attr('disabled', false).css('background-color', '#ffffff');

        $('input[type="radio"]').attr('disabled', false);
        $('div.options label img,div.change label img,div.category label img,div.case label img').css(
            'background-color', '#ffffff');

        // btn control
        $('.check .btn-print-form').css('display', 'none');
        $('.check .btn-edit-form').css('display', 'none');
        $('.check .btn-check-form').css('display', 'block');
    }

    function comback_home() {
        window.location.href = '/'
    }

    // Filter value identity
    (function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));
    $(".input-identity").inputFilter(function (value) {
        return /^-?\d*$/.test(value);
    });

</script>
@endsection
