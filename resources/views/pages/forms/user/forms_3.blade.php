@extends('layout.forms')
@section('contentforms')
<style>

    .btn-addlist {
        width: 108px;
        height: 30px;
        border-radius: 8px;
        color: #4f72e5;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-family: Kanit-Regular;
    }

    .input-name {
        width: 387px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-time {
        width: 58px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-position {
        width: 407px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text-head {
        font-size: 14px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .date {
        width: 193px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        position: relative;
    }

    #inputcalendar1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar3::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar4::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    .select-list {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .textarea-indicators {
        width: 184px;
        height: 69px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .input-results_of_test {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-other {
        width: 480px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-improve{
        width: 423px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }


    .hr-head{
        padding: 0.5em 0;
        border-top: solid 2px #4f72e5;
        border-bottom: solid 2px #4f72e5;
        font-family: Kanit-Regular;

    }
    .hr-bottom{
        padding: 0.5em 0;
        border-bottom: solid 2px #4f72e5;
    }

    .hr-row {
        padding: 0.5em 0;
        border-bottom: solid 1px #bfbfbf;
    }

    .hr-row-end{
        padding: 3px;
    }

    div.options > label > input {
	visibility: hidden;
    }

    div.options > label {
        display: block;
        padding: 0 0 0 0;
        height: 20px;
        width: 150px;
    }

    div.options > label > img {
        display: inline-block;
        height:13px;
        width:13px;
        border-radius: 3px;
        background: none;
    }

    div.options > label > input:checked +img {
        background: url(http://cdn1.iconfinder.com/data/icons/onebit/PNG/onebit_34.png);
        background-repeat: no-repeat;
        background-position:center center;
        background-size:13px 13px;
    }

    div.options{
        font-family: Kanit-ExtraLight;  
    }


}

</style>
<div class="card rounded-0 border-0" style="min-height:650px;padding: 19px 30px 19px 23px;">
    <div class="col-12 d-flex px-0">
        <div class="col-8 px-0 d-flex align-items-center">
            <img class="mr-2" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
            <span class="text-headform ml-2">แบบบันทึกผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-4 px-0 d-flex align-items-center justify-content-end">
            <span style="font-size: 14px;color:#4f72e5">สำหรับผู้ดูแลการทดลองปฏิบัติหน้าที่ราชการ (แบบฟอร์ม 2)</span>
        </div>
    </div>
    <hr class="ml-3" style="border-top: 1px dashed #8c8b8b;background-color: white;">

    <div class="mx-4 text-14">
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-name form_2">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-position form_2">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3">
            <span style="font-size: 14px;color:#4a4a4a;">เป็นผู้ดูแลหน้าที่ปฏิบัติงานของ</span>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-name form_2">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-position form_2">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>ฝ่าย/งาน/กลุ่มงาน</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>กอง/สำนัก/ศูนย์</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>กรม</span>
                </div>
                <div class="col-10 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>จังหวัด</span>
                </div>
                <div class="col-10 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-7 px-0">
                    <span>ระยะเวลาทดลองปฏิบัติหน้าที่ราชการ ตั้งแต่วันที่</span>
                </div>
                <div class="col-5 px-0">
                    <input class="form-control date form_2" type="date" value="2011-08-19" id="inputcalendar1">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ถึงวันที่</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control date form_2" type="date" value="2011-08-19" id="inputcalendar2">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-4 d-flex px-0 align-items-center">
                <div class="col-9 px-0">
                    <span>ขอรายงานผลการทดลองปฏิบัติงาน ครั้งที่</span>
                </div>
                <div class="col-3 px-0">
                    <input type="text" class="form-control input-time form_2 pl-3" name="" id="">
                </div>
            </div>
            <div class="col-4 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>ตั้งแต่วันที่</span>
                </div>
                <div class="col-9 px-0">
                    <input class="form-control date form_2" type="date" value="2011-08-19" id="inputcalendar3">
                </div>
            </div>
            <div class="col-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ถึงวันที่</span>
                </div>
                <div class="col-10 px-0 ml-2">
                    <input class="form-control date form_2" type="date" value="2011-08-19" id="inputcalendar4">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head">1. ผลสัมฤทธิ์ของการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12 px-0 mt-2">
            <span>ความสำเร็จของงานที่ได้รับมอบหมาย</span>
        </div>
        <div class="col-12 px-0 mt-2 hr-head">
            <div class="col-12 px-4 d-flex">
                <div class="col-4">
                    <span>ตัวชี้วัดความสำเร็จ</span>
                </div>
                <div class="col-4">
                    <span>ผลผลิตของงาน</span>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    <span>ระดับความสำเร็จ</span>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3 clone_add_list">
            <div class="col-12 hr-row px-0 mb-3 d-flex">
                <div class="col-3 px-0 d-flex">
                    <div class="col-1">
                        <span class="span_id">1</span>
                    </div>
                    <div class="col-11">
                        <textarea class="form-control textarea-indicators" rows="5" id="comment"></textarea>
                    </div>
                </div>
                <div class="col-5 px-0">
                    <div class="d-flex align-items-center">
                        <div class="col-3 px-0 pl-3">
                            <span>ผลที่คาดหวัง</span>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control input-results_of_test form_2">
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="col-3 px-0 pl-3">
                            <span>ผลที่เกิดขึ้นจริง</span>
                        </div>
                        <div class="col-9 mt-2">
                            <input type="text" class="form-control input-results_of_test form_2" >
                        </div>
                    </div>
                </div>
                <div class="col-4 px-0 radio_addlist align-items-center">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio_list_1" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_list_1" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_list_1" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_list_1" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio_list_1" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                    <div class="pl-2">
                        <span style="font-size:12px;color:#ee2a27;">5 = สูงกว่าความคาดหวังมาก</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-0 clone_results_addlist"></div>
        <div class="col-12 px-0 mt-3">
            <button type="submit" class="btn-addlist ml-5" onclick="addlist()">+ เพิ่มรายการ</button>
        </div>
        <div class="col-12 px-0 mt-3 hr-head align-items-center">
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>ความสามารถในการเรียนรู้งาน</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio1_1" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_1" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_1" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_1" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_1" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>ความสามารถในการปรับใช้ความรู้กับงานในหน้าที่</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio1_2" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_2" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_2" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_2" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_2" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 hr-row-end mt-2 d-flex align-items-center">
                <div class="col-8 d-flex align-items-center pl-5">
                    <span>อื่นๆ (ถ้ามี)</span>
                    <input type="text" class="form-control ml-3 input-other form_2">
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio1_3" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_3" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_3" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_3" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio1_3" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3 d-flex align-items-center">
            <span>จุดเด่น</span>
            <input type="text" class="form-control ml-3 input-other form_2">
        </div>
        <div class="col-12 px-0 mt-2 d-flex align-items-center">
            <span>สิ่งที่ควรปรับปรุง</span>
            <input type="text" class="form-control ml-3 input-improve form_2">
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head"><b>2. พฤติกรรมของผู้ทดลองปฏิบัติหน้าที่ราชการ</b></span>
        </div>
        <div class="col-12 hr-bottom px-0 mt-2">
            <div class="col-12 hr-head d-flex px-4">
                <div class="col-8 pl-4">
                    <span>รายการ</span>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    <span>ระดับความสำเร็จ</span>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>ความประพฤติ</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio2_1" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_1" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_1" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_1" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_1" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>ความมีคุณธรรม จริยธรรม</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio2_2" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_2" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_2" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_2" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_2" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>การรักษาวินัย</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio2_3" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_3" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_3" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_3" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_3" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 hr-row-end mt-2 d-flex align-items-center">
                <div class="col-8 d-flex align-items-center pl-5">
                    <span>อื่นๆ (ถ้ามี)</span>
                    <input type="text" class="form-control ml-3 input-other form_2">
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio2_4" value="2" />
                            <img />
                            <span class="ml-1">1</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_4" value="2" />
                            <img />
                            <span class="ml-1">2</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_4" value="2" />
                            <img />
                            <span class="ml-1">3</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_4" value="2" />
                            <img />
                            <span class="ml-1">4</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio2_4" value="2" />
                            <img />
                            <span class="ml-1">5</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3 d-flex align-items-center">
            <span>จุดเด่น</span>
            <input type="text" class="form-control ml-3 input-other form_2">
        </div>
        <div class="col-12 px-0 mt-2 d-flex align-items-center">
            <span>สิ่งที่ควรปรับปรุง</span>
            <input type="text" class="form-control ml-3 input-improve form_2">
        </div>
        <div class="col-12 px-0 mt-3">
            <span class="text-head"><b>3. การพัฒนาผู้ทดลองปฏิบัติหน้าที่ราชการ</b></span>
        </div>
        <div class="col-12 hr-bottom px-0 mt-2">
            <div class="col-12 hr-head d-flex px-4">
                <div class="col-8 pl-4">
                    <span>ตัวชี้วัดความสำเร็จ</span>
                </div>
                <div class="col-4 d-flex justify-content-center">
                    <span>การดำเนินการ</span>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>การปฐมนิเทศ</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio3_1" value="2" />
                            <img />
                            <span class="ml-1">ดำเนินการแล้ว</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio3_1" value="2" />
                            <img />
                            <span class="ml-1">ยังไม่ได้ดำเนินการ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>การเรียนรู้ด้วยตนเอง</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio3_2" value="2" />
                            <img />
                            <span class="ml-1">ดำเนินการแล้ว</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio3_2" value="2" />
                            <img />
                            <span class="ml-1">ยังไม่ได้ดำเนินการ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 hr-row d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>การอบรมสัมมนาร่วมกัน</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio3_3" value="2" />
                            <img />
                            <span class="ml-1">ดำเนินการแล้ว</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio3_3" value="2" />
                            <img />
                            <span class="ml-1">ยังไม่ได้ดำเนินการ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mt-2 pt-1 d-flex align-items-center">
                <div class="col-8 pl-5">
                    <span>การอบรมอื่น ๆ ตามที่ส่วนราชการกำหนด (ถ้ามี)</span>
                </div>
                <div class="col-4 px-0">
                    <div class="options d-flex align-items-center">
                        <label title="item3">
                            <input type="radio" name="radio3_4" value="2" />
                            <img />
                            <span class="ml-1">ดำเนินการแล้ว</span>
                        </label>
                        <label title="item3">
                            <input type="radio" name="radio3_4" value="2" />
                            <img />
                            <span class="ml-1">ยังไม่ได้ดำเนินการ</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex check justify-content-center mt-5 mb-3">
            <button type="submit" class="btn-edit-form shadow" onclick="edit()" style="display: none" >แก้ไข</button>
            <button type="submit" class="btn-check-form shadow" onclick="check()">ตรวจสอบ</button>
            <button type="submit" class="btn-home-form shadow" style="display: none" onclick="comback_home()">กลับหน้าหลัก</button>
            <button type="submit" class="btn-print-form shadow" style="display: none" ><img src="/img/icon/print-material-white.png" style="margin-right:10px; " alt="">พิมพ์</button>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        ///
    });


    function check() {
        $('.btn-addlist').css('display','none');

        $('.btn-check-form').html('บันทึก');
        $('.check .btn-edit-form').css('display','block');
        if ($('.btn-check-form').hasClass('afterreadonly')) {
            Swal.fire({
                icon: 'success',
                title: 'บันทึกสำเร็จ',
                showConfirmButton: false,
                timer: 1700
            });

             // btn control
            $('.check .btn-edit-form').css('display','none');
            $('.check .btn-print-form').css('display','block');
            $('.check .btn-check-form').css('display','none');
            $('.check .btn-home-form').css('display','block');
        } else {
            $('.btn-check-form').addClass('afterreadonly');
        }

        $('.form_2').attr('readonly', true).css('background-color', '#f3f3f3');
        $('input[type="radio"]').attr('disabled', 'true');
        $('div.options label img').css('background-color', '#f3f3f3');
        $('select').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('textarea').attr('disabled', 'true').css('background-color', '#f3f3f3');
    }


    var count = 1
    function addlist() {
        count = count + 1;
        $('.clone_add_list').last().clone().appendTo('.clone_results_addlist').find('input, textarea').val('');
        $('.clone_add_list').last().find('input').attr('name','radio_list_'+count);

        if ($('.span_id').length > 1) {
            var count_span = $('.span_id').length;
            $('.span_id').last().text($('.span_id').length);
            $('#comment').attr('data-id', count_span);
            
        }

    }

    function show_display(){
        $('.form_2').attr('readonly', false).css('background-color', '#ffffff');
        $('select').attr('disabled', false).css('background-color', '#ffffff');
        $('input[type="date"]').attr('disabled', false).css('background-color', '#ffffff');
        $('textarea').attr('disabled', false).css('background-color', '#ffffff');
        $('input[type="radio"]').attr('disabled', false);
        $('div.options label img').css('background-color', '#ffffff');

        // btn control
        $('.check .btn-edit-form').css('display','none');
        $('.check .btn-print-form').css('display','none');
        $('.check .btn-check-form').css('display','block');
    }

    function edit(){
        show_display()
        $('.btn-addlist').css('display','block');
    }

    function comback_home(){
        window.location.href='/'
    }

</script>
@endsection
