@extends('layout.forms')
@section('contentforms')
<style>
    .btn-check-form {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 14px;
        padding: 4px 18px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }


    .input-name {
        width: 387px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-time {
        width: 58px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input-timeperiod{
        width: 60px;
        height: 30px;
        border-radius: 8px;
        margin-left:10px;
        border: solid 0.5px #ceced4;
    }

    .input-position {
        width: 407px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text-head {
        font-size: 14px;
        color: #4f72e5;
        font-weight: bold;
    }

    .date {
        width: 193px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        position: relative;
    }

    #inputcalendar1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar3::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    #inputcalendar4::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 75%;
    }

    .select-list {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .hr-row {
        padding: 0.5em 0;
        border-bottom: solid 1px #bfbfbf;
    }


</style>
<div class="card rounded-0 border-0" style="min-height:650px;padding: 19px 30px 19px 23px;">
    <div class="col-12 d-flex px-0">
        <div class="col-7 px-0 d-flex align-items-center">
            <img class="mr-2" src="/img/icon/description-material@2x.png" alt="" width="20spx" height="23px">
            <span class="text-headform ml-2">แบบประเมินผลการทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-5 px-0 d-flex align-items-center justify-content-end">
            <span style="font-size: 14px;color:#4f72e5">สำหรับประธานกรรมการ (แบบฟอร์ม 5)</span>
        </div>
    </div>
    <hr class="ml-3" style="border-top: 1px dashed #8c8b8b;background-color: white;">

    <div class="mx-4 text-14">
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-3 d-flex px-0 align-items-center">
                <div class="col-3 px-0">
                    <span>คำสั่งที่</span>
                </div>
                <div class="col-3 pr-0">
                    <input type="text" class="form-control input-time form_5" name="" id="">
                </div>
                <div class="col-1 pr-0 ml-1" >
                   <span>/</span>
                </div>
                <div class="col-4 pr-0" >
                    <input type="text" class="form-control input-time form_5" name="" id="">
                </div>
            </div>
            <div class="col-3 px-0 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>ตั้งแต่วันที่</span>
                </div>
                <div class="col-9 px-0">
                    <input class="form-control date form_5" type="date" value="2011-08-19" id="inputcalendar1">
                </div>
            </div>
            <div class="col-6 ml-5 px-0 d-flex align-items-center">
                <div class="col-1 px-0">
                    <span>ถึงวันที่</span>
                </div>
                <div class="col-11 px-0 ml-4">
                    <input class="form-control date form_5" type="date" value="2011-08-19" id="inputcalendar2">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-3">
            <span style="font-size: 14px;color:#4a4a4a;">รายละเอียดผู้ทดลองปฏิบัติหน้าที่ราชการ</span>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ชื่อ-สกุล</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-name form_5">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ตำแหน่ง</span>
                </div>
                <div class="col-10 px-0">
                    <input type="text" class="form-control input-position form_5">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>ฝ่าย/งาน/กลุ่มงาน</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-3 px-0">
                    <span>กอง/สำนัก/ศูนย์</span>
                </div>
                <div class="col-9 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>กรม</span>
                </div>
                <div class="col-10 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>จังหวัด</span>
                </div>
                <div class="col-10 px-0">
                    <select class="form-select p-1 select-list" aria-label="Default select example">
                        <option selected></option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex">
            <div class="col-6 px-0 pr-5 d-flex align-items-center">
                <div class="col-7 px-0">
                    <span>ระยะเวลาทดลองปฏิบัติหน้าที่ราชการ ตั้งแต่วันที่</span>
                </div>
                <div class="col-5 px-0">
                    <input class="form-control date form_5" type="date" value="2011-08-19" id="inputcalendar3">
                </div>
            </div>
            <div class="col-6 px-0 pl-4 d-flex align-items-center">
                <div class="col-2 px-0">
                    <span>ถึงวันที่</span>
                </div>
                <div class="col-10 px-0">
                    <input class="form-control date form_5" type="date" value="2011-08-19" id="inputcalendar4">
                </div>
            </div>
        </div>
        <div class="col-12 px-0 mt-2 d-flex align-items-center">
            <span>เป็นระยะเวลา</span>
            <input type="text" class="form-control input-timeperiod form_5">
            <span class="ml-3">เดือน/ปี</span>
        </div>
        <div class="col-12 d-flex check justify-content-center mt-5 mb-3">
            <button type="submit" class="btn-edit-form shadow" onclick="edit()" style="display: none" >แก้ไข</button>
            <button type="submit" class="btn-check-form shadow" onclick="check()">ตรวจสอบ</button>
            <button type="submit" class="btn-home-form shadow" style="display: none" onclick="comback_home()">กลับหน้าหลัก</button>
            <button type="submit" class="btn-print-form shadow" style="display: none" ><img src="/img/icon/print-material-white.png" style="margin-right:10px; " alt="">พิมพ์</button>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        ///
    });

    function check() {
        $('.btn-addlist').closest(".btn-addlist").remove();

        $('.btn-check-form').html('บันทึก');
        $('.check .btn-edit-form').css('display','block');
        if ($('.btn-check-form').hasClass('afterreadonly')) {
            Swal.fire({
                icon: 'success',
                title: 'บันทึกสำเร็จ',
                showConfirmButton: false,
                timer: 1700
            });

             // btn control
            $('.check .btn-edit-form').css('display','none');
            $('.check .btn-print-form').css('display','block');
            $('.check .btn-check-form').css('display','none');
            $('.check .btn-home-form').css('display','block');
        } else {
            $('.btn-check-form').addClass('afterreadonly');
        }

        $('.form_5').attr('readonly', true).css('background-color', '#f3f3f3');
        $('select').attr('disabled', 'true').css('background-color', '#f3f3f3');
    }

    function show_display(){
        $('.form_5').attr('readonly', false).css('background-color', '#ffffff');
        $('select').attr('disabled', false).css('background-color', '#ffffff');
        $('input[type="date"]').attr('disabled', false).css('background-color', '#ffffff');

        // btn control
        $('.check .btn-edit-form').css('display','none');
        $('.check .btn-print-form').css('display','none');
        $('.check .btn-check-form').css('display','block');
    }

    function edit(){
        show_display()
    }

    function comback_home(){
        window.location.href='/'
    }

</script>
@endsection
