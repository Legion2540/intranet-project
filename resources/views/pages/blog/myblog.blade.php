@extends('layout.blog')
@section('style')
<style>
    .head_content {
        font-family: Kanit-Regular;
        font-size: 24px;
        font-weight: bold;
        color: #4f72e5;
    }

    #search_myblog {
        width: 295px;
        height: 34px;
        padding: 5px 45px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        position: relative;
    }

    .head_other {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .type_other {
        font-size: 14px;
        font-weight: 600;
        line-height: 2.29;
        color: #4f72e5;
        text-decoration: underline;
    }

    .date_other,
    .time_other,
    .three_number_other {
        font-family: Kanit-Regular;
        font-size: 12px;
        line-height: 2.29;
        color: #4a4a4a;
    }

    .img_writter_other {
        width: 30px;
        height: 30px;
        object-fit: contain;
        border: solid 2px #ffffff;
        border-radius: 50%;

    }

    .name_writter_other {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .position_writter_other {
        font-size: 12px;
        color: #4a4a4a;
    }





    .btn-next {
        background-color: #4f72e5;
        color: white;
        width: 140px;
        height: 30px;
        margin: 0px;
        padding: 0px;
    }

    .btn-back {
        background-color: #ffffff;
        color: #4f72e5;
        border-color: #4f72e5;
        width: 140px;
        height: 30px;
        margin: 0px;
        padding: 0px;
    }

    .next-back .btn-next {
        background-color: #4f72e5;
        width: 23px;
        height: 23px;
        font-size: 14px;
        border-radius: 0px;
        margin: 0px;
        padding: 0px;
    }

    .text-countresult {
        font-size: 12px;
        color: #4a4a4a;
    }

    .text-fortiness{
        font-size: 12px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

</style>
@endsection

@section('content')
<div class="col-12 p-0">
    <div class="col-12 p-0 d-flex align-items-center">
        <div class="col-6 pl-2">
            <span class="head_content">บล็อกของฉัน</span>
        </div>
        <div class="col-6 d-flex justify-content-evenly">
            <input type="search" name="search_myblog" id="search_myblog" placeholder="ค้นหาข้อมูล บล็อก">
            <img src="/img/icon/search-material-copy@2x.png" alt=""
                style="position: absolute;object-fit:contain; left: 11%; top: 15%;">
            <img src="/img/icon/widgets-material.png" alt="" style="object-fit: contain;">
            <img src="/img/icon/widgets-material.png" alt="" style="object-fit: contain;">
        </div>
    </div>

    <div class="row px-4 d-flex justify-content-between" style="min-height:500px;max-height:1500px;overflow:hidden;">

        @for($i=0; $i < 3; $i++)
        <div class="col-4 p-0 mt-3">
            <div class="col-12 p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 px-2 pt-2 pb-3">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor

    </div>

    {{-- Pagination --}}

    <div class="col-12 mt-4">
        <div class="row next-bar px-3">
            <div class="col-4 px-1 d-flex justify-content-start align-items-center">
                <span class="text-countresult">แสดงผล 7 จากทั้งหมด 7</span>
            </div>
            <div class="col-4 d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-back mr-2">&larr; ย้อนกลับ</button>
                <button type="button" class="btn btn-next">หน้าถัดไป &rarr;</button>
            </div>
            <div class="col-4 pr-4 next-back">
                <div class="row text-fortiness d-flex justify-content-end align-items-center">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>หน้า</span>
                    </div>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                        style="height: 23px; width:34px;font-size:11px;margin-right: 10px;">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>จาก 15</span>
                    </div>
                    <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                        <button type="button" class="btn btn-next">&lt;</button>
                        <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div>
@endsection
@section('script')
<script>

</script>
@endsection
