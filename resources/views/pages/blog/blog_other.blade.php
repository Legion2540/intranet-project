@extends('layout.blog')
@section('style')
<style>
    .text-title {
        font-size: 22px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-headcard {
        color: #4a4a4a;
        font-size: 16px;
        font-family: Kanit-Regular;
    }

    .text-tag {
        color: #4f72e5;
        font-size: 12px;
        font-family: Kanit-Regular;
        text-decoration: underline;
    }

    .text-date {
        color: #4a4a4a;
        font-size: 12px;
        font-family: Kanit-Regular;

    }

    .text-time {
        color: #4a4a4a;
        font-size: 12px;
        font-family: Kanit-Regular;

    }

    .text-event {
        color: #4a4a4a;
        font-size: 22px;
        font-family: Kanit-Regular;
    }

    .input-search {
        width: 295px;
        height: 34px;
        padding-left: 50px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .img-search {
        position: absolute;
        top: 22%;
        left: 4%;
        width: 20px;
        height: 20px;
    }

    .body-card {
        border-radius: 2px;
        background-color: #eaeaea;
    }

    .GURU {
        width: 26px;
        height: 12px;
        border-radius: 3px;
        background-color: #0066b2;
        font-size: 7px;
        font-weight: bold;
        color: #ffffff;
        padding: 2px;
    }

    .position_writter_other {
        font-size: 12px;
        color: #4a4a4a;
    }

    .name_writter_other {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .img_writter_other {
        width: 26px;
        height: 26px;
        object-fit: contain;
        border: solid 2px #ffffff;
        border-radius: 50%;

    }

    .btn-next {
        background-color: #4f72e5;
        color: white;
        width: 140px;
        height: 30px;
        margin: 0px;
        padding: 0px;
    }

    .btn-back {
        background-color: #ffffff;
        color: #4f72e5;
        border-color: #4f72e5;
        width: 140px;
        height: 30px;
        margin: 0px;
        padding: 0px;
    }

    .btn-list{
        width: 33px;
        height: 33px;
        border: 0;
        background-color: #f4f4f4;
    }
    .btn-col{
        width: 33px;
        height: 33px;
        border: 0;
        background-color: #f4f4f4;
    }
    .btn-list:hover{
        width: 33px;
        height: 33px;
        padding: 7px 7.2px 6px 5.8px;
        border-radius: 5px;
        border: solid 0.5px #ceced4;
        background-color: #4f72e5;
    }
    .btn-col:hover{
        width: 33px;
        height: 33px;
        padding: 7px 7.2px 6px 5.8px;
        border-radius: 5px;
        border: solid 0.5px #ceced4;
        background-color: #4f72e5;
    }

    .next-back .btn-next {
        background-color: #4f72e5;
        width: 23px;
        height: 23px;
        font-size: 14px;
        border-radius: 0px;
        margin: 0px;
        padding: 0px;
    }

    .text-countresult {
        font-size: 12px;
        color: #4a4a4a;
    }

    .text-fortiness{
        font-size: 12px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .card-content .nav-tabs {
        border-bottom: 0px;
    }
    .card-content .nav-link{
        border-radius:5px; 
        width: 33px;
        height: 33px;
    }

    .card-content .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #495057;
        background-color: #4f72e5;
        border-color: #dee2e6 #dee2e6 #fff;
        border-radius:5px; 
    }

</style>
@endsection
@section('content')
<div class="col-12 px-0 card-content">
    <nav>
        <div class="col-12 px-0 d-flex">
            <div class="col-6 px-0">
                <span class="text-title">บล็อกของ Prakhan kordumrong </span>
            </div>
            <div class="col-6 px-0 d-flex align-items-center justify-content-end">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <div class="position-relative">
                        <input type="text" class="input-search" placeholder="ค้นหาข้อมูล บล็อก">
                        <img src="/img/icon/search-material-copy@2x.png" alt="" class="img-search">
                    </div>
                    <button class="nav-link active mx-2 d-flex justify-content-center" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab"  aria-selected="true">
                        <img src="/img/icon/widgets-material.png" alt="">
                    </button>
                    <button class="nav-link d-flex justify-content-center" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab"  aria-selected="false">
                        <img src="/img/icon/widgets-material.png" alt="">
                    </button>
                </div>
            </div>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active"  id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="row px-2 mt-2 d-flex">
                @for ($i = 0; $i < 12; $i++) 
                    <div class="col-4 px-0">
                        <div class="col-12" style="padding: 11px;">
                            <img src="/img/profile/176198235_168911881677682_7343545927848232125_n.jpeg" alt="" style="width: 100%;border-radius: 2px;"
                                height="173px" style="object-fit: cover;">
                            <div class="col-12 px-0 py-2 body-card">
                                <div class="col-12">
                                    <span class="text-headcard">โอกาสส่งออกสินค้าไทยในช่วงวิกฤต COVID-19 ประเทศในโซนอเมริกา
                                        ลาตินอเมริกา ยุโรป CIS</span>
                                </div>
                                <div class="col-12">
                                    <span class="text-tag">เกษตรและอาหาร</span>
                                </div>
                                <div class="col-12 d-flex justify-content-between">
                                    <div class="col-6 p-0 d-flex justify-content-start">
                                        <span class="text-date mr-2">5 ม.ค. 2564</span>
                                        <span class="text-time">11 : 00</span>
                                    </div>
                                    <div class="col-6 p-0 d-flex justify-content-evenly">
                                        <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                                        <span class="three_number">1</span>
                                        <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                                        <span class="three_number">2</span>
                                        <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                                        <span class="three_number">25</span>
                                    </div>
                                </div>
                                <div class="col-12 d-flex align-content-center mt-2">
                                    <div class="col-1 d-flex justify-content-center align-items-center">
                                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                            class="img_writter_other">
                                    </div>
                                    <div class="col-11 px-2" style="line-height: 1">
                                        <div class="col-12 p-0 d-flex align-items-center">
                                            <span class="name_writter_other">Nissana Thaveepanit</span>
                                            <span class="GURU ml-2">GURU</span>
                                        </div>
                                        <div class="col-12 p-0">
                                            <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    
        <div class="tab-pane fade"  id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="col-12 px-0 mt-3">
                @for ($j = 0; $j < 12; $j++)
                    <div class="col-12 mt-2 px-0 d-flex" style="border-radius: 2px;background-color: #eaeaea;">
                        <div class="px-0" style="width: 264px; height: 130px;">
                            <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="" style="width: 264px; height: 130px;border-radius: 2px;">
                        </div>
                        <div class="col-5 pl-4 pr-1 py-3" style="border-right: 1px solid #ceced4">
                            <div class="col-12">
                                <span class="text-headcard">กัมพูชา เก็บค่าธรรมเนียมการตรวจ COVID-19 สำหรับชาวต่างชาติที่เดินทางเข้าประเทศ</span>
                            </div>
                            <div class="col-12">
                                <span class="text-tag">สถานการณ์การค้าผลกระทบ COVID-19</span>
                            </div>
                            <div class="col-12 d-flex">
                                <span class="mr-3 text-date">25 ธ.ค. 2563</span>
                                <span class="text-time">09:00</span>
                            </div>
                        </div>
                        <div class="col-4 p-3">
                            <div class="col-12 px-0 py-3 d-flex align-items-center">
                                <div class="col-4 d-flex align-items-center">
                                    <img src="/img/icon/grade-material@3x.png" alt="" style="object-fit:contain;width: 20px;height: 20px;">
                                    <span class="text-event ml-2">22</span>
                                </div>
                                <div class="col-4 d-flex align-items-center justify-content-center">
                                    <img src="/img/icon/comment-blue@3x.png" alt="" style="object-fit:contain;width: 20px;height: 20px;">
                                    <span class="text-event ml-2">2</span>
                                </div>
                                <div class="col-4 d-flex align-items-center">
                                    <img src="/img/icon/visibility-material-bule@3x.png" alt="" style="object-fit:contain;width: 27px;height: 27px;">
                                    <span class="text-event ml-2">33</span>
                                </div>
                            </div>
                            <div class="col-12 px-0 d-flex align-content-center mt-2">
                                <div class="col-1 d-flex justify-content-center align-items-center">
                                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="" class="img_writter_other">
                                </div>
                                <div class="col-11 px-2" style="line-height: 1">
                                    <div class="col-12 p-0 d-flex align-items-center">
                                        <span class="name_writter_other">Nissana Thaveepanit</span>
                                        <span class="GURU ml-2">GURU</span>
                                    </div>
                                    <div class="col-12 p-0">
                                        <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>

    <div class="col-12 mt-4">
        <div class="row next-bar">
            <div class="col-4 px-1 d-flex justify-content-start align-items-center">
                <span class="text-countresult">แสดงผล 7 จากทั้งหมด 7</span>
            </div>
            <div class="col-4 d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-back mr-2">&larr; ย้อนกลับ</button>
                <button type="button" class="btn btn-next">หน้าถัดไป &rarr;</button>
            </div>
            <div class="col-4 pr-4 next-back">
                <div class="row text-fortiness d-flex justify-content-end align-items-center">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>หน้า</span>
                    </div>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                        style="height: 23px; width:34px;font-size:11px;margin-right: 10px;">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>จาก 15</span>
                    </div>
                    <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                        <button type="button" class="btn btn-next">&lt;</button>
                        <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

</script>
@endsection
