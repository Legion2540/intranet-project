@extends('layout.blog')
@section('style')
<style>
    .title-img {
        width: 947px;
        height: 442px;
    }

    .title-blog {
        font-family: Kanit-Regular;
        font-size: 32px;
        color: #4a4a4a;
    }

    .tag-blog {
        font-size: 16px;
        font-weight: 600;
        line-height: 1;
        color: #4f72e5;
        text-decoration: underline;
    }

    .date-blog {
        font-size: 14px;
        color: #4a4a4a;
    }

    .count-view {
        font-family: Kanit-Regular;
        font-size: 19px;
        color: #4a4a4a;
    }

    .album_blog,
    .name_album {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .count_img_album {
        font-family: Kanit-Regular;
        font-size: 12px;
        color: #4a4a4a;
    }



    /* Swiper */
    .mycontent .swiper-container {
        width: 100%;
        height: 100%;
    }

    .mycontent .swiper-slide {
        display: flex;
        justify-content: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .mycontent .swiper-slide img {
        display: block;
        /* width: 218px;
        height: 180px; */
        width: 180px;
        height: 150px;
        object-fit: cover;
    }

    .mycontent .swiper-button-next,
    .mycontent .swiper-button-next::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size: 14px !important;
        right: 1px;
        top: 39%;
    }

    .mycontent .swiper-button-prev,
    .mycontent .swiper-button-prev::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size: 14px !important;
        right: 1px;
        top: 39%;
    }




    /* VDO */
    .preview_vdo {
        width: 235px;
        height: 148px;
    }

    .name_vdo {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .date_vdo {
        font-size: 14px;
        color: #4a4a4a;
    }

    .text-dots {
        /* display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis; */
    }

    .name_files_blog {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .size_file_blog {
        font-size: 10px;
        line-height: 1;
        color: #4a4a4a;
    }

    .tag-head-content {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .tag-content {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4f72e5;
    }

    /* Writer */
    .name_writer {
        font-family: Kanit-Regular;
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .guru {
        width: 26px;
        height: 12px;
        border-radius: 3px;
        background-color: #0066b2;
        font-size: 7px;
        color: #ffffff;
        padding: 2px;

    }

    .img_writer {
        width: 62px;
        height: 61.8px;
        border: solid 2px #ffffff;
        border-radius: 50%;
        border: solid 2px #ffffff;
    }

    .position_writer {
        font-size: 16px;
        color: #4a4a4a;
    }

    .count-vote {
        font-family: Kanit-Regular;
        font-size: 22px;
        font-weight: 500;
        color: #ffffff;
    }

    .for_thisBlog {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #ffffff;
    }

    .vote {
        font-family: Kanit-Regular;
        background-color: #ffffff;
        width: 118px;
        height: 33px;
        font-size: 18px;
        text-align: center;
        color: #4f72e5;
        border: none;
        border-radius: 4px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
    }

    .head-comment {
        font-size: 26px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .img_comment {
        width: 60px;
        height: 60px;
        border: solid 2px #ffffff;
        border-radius: 50%;
        border: solid 1px #d0d0d0;
    }

    /* Comment */
    .count_commet {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .comment {
        padding: 14.9px 23.6px 14.1px 14px;
        border-radius: 8px;
        border: solid 0.5px #8a8c8e;
        resize: none;
    }

    .name_commentor {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .date_comment {
        font-size: 14px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .content_comment {
        font-size: 16px;
        font-weight: 500;
        color: #4a4a4a;
    }

</style>
@endsection

@section('content')
<div class="col-12">
    <div class="title-img">
        <img src="/img/icon/rectangle-copy-3@3x.png" alt="" style="width: 100%; height:100%;">
    </div>

    <div class="col-12">
        <span class="title-blog">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries ในเนเธอร์แลนด์สูงกว่า
            50%</span>
    </div>
    <div class="col-12">
        <a href="#" class="tag-blog">สถานการณ์การค้าผลกระทบ COVID-19</a>
    </div>

    <div class="col-12 d-flex justify-content-between mt-3">
        <div>
            <img src="/img/icon/schedule-material.png" alt="">
            <span class="date-blog">11 มีนาคม 2564</span>
        </div>
        <div>
            <img src="/img/icon/visibility-material-bule.png" alt="">
            <span class="count-view">25</span>
        </div>
    </div>

    {{-- Make do new again when start backend --}}
    <div class="col-12 mt-3">
        <p>ผลการแพร่ระบาดของโควิด-19 กำลังทำให้เกิดภาวะการณ์ว่างงานเพิ่มขึ้นในทุกประเทศทั่วโลก รวมถึง "สหราชอาณาจักร"
            หรืออังกฤษ ซึ่งมีการคาดการณ์ว่าอาจจะมีประชาชนว่างงานถึง 2 ล้านคน ทำให้อัตราการขยายตัวทางเศรษฐกิจ (จีดีพี)
            ลดลง 35% ในไตรมาส 2</p>
        <p>สำนักงานส่งเสริมการค้าในต่างประเทศ ณ กรุงลอนดอน สหราชอาณาจักร เปิดเผยรายงานของ The Office for Budget
            Responsibility (OBR) ระบุว่ามาตรการล็อกดาวน์ และมาตรการควบคุมการรักษาระยะห่างทางสังคม (Social Distancing)
            เพื่อลดการแพร่ระบาดของเชื้อโควิด-19 ส่งผลกระทบต่อบางธุรกิจทำให้มีการผลิตลดลงในช่วงไตรมาส 2 ของปี
            โดยภาคธุรกิจที่ได้รับผลกระทบอย่างมาก คือการศึกษา ที่พักและบริการด้านอาหาร ก่อสร้าง การให้บริการอื่นๆ
            และอุตสาหกรรมการผลิต</p>
        <p>โดยเฉพาะอย่างยิ่งธุรกิจที่พักและบริการด้านอาหาร ถือว่าเป็นธุรกิจที่ได้รับผลกระทบสูงสุดและยากจะฟื้นตัว
            หลังจากรัฐบาลสหราชอาณาจักรประกาศมาตรการรักษาระยะห่างทางสังคมในเดือนมีนาคม 2563
            ส่งผลให้มีร้านอาหารจำนวนมากต้องปิด เลิกจ้างพนักงาน และสูญเสียวัตถุดิบไปอย่างมหาศาล
            ทั้งนี้หากสถานการณ์คลี่คลายทำให้แต่ละร้านต้องใช้เงินทุนกลับมาฟื้นฟูกิจการ
            และซื้อวัตถุดิบปรุงอาหารอีกครั้งไม่ต่ำกว่า 3-6 แสนบาท</p>
        <p>ด้วยเหตุนี้สมาคมร้านอาหารสหราชอาณาจักร และกลุ่มผู้ประกอบการร้านอาหาร ประเภทสาขา หรือ Chain restaurant กว่า 14
            บริษัท เช่น Burger King, Dishoom, Gordon Ramsay Group, Hawksmoor, Leon, Nardo's, Tortilla และ Wahaca
            ได้มีหนังสือถึงรัฐบาลเพื่อขอให้มีมาตรการช่วยเหลือ ทั้งการขอให้เจ้าของที่ดินผ่อนผันการชำระค่าเช่าออกไป 9
            เดือน หากไม่มีการดำเนินมาตรการช่วยเหลืออาจทำให้กลุ่มธุรกิจบริการนี้ต้องปิดตัวลงมากกว่า 50%</p>
        <p>ทั้งนี้รัฐบาลสหราชอาณาจักรได้ตอบสนองข้อเรียกร้องของผู้ประกอบการ โดยการยกเว้นภาษีธุรกิจให้เป็นเวลา 1 ปี
            สำหรับธุรกิจประเภทค้าปลีก ร้านอาหาร โรงละคร โรงหนัง ผับ และบาร์ มีขนาดเล็กไม่เกิน 2 ล้านบาท
            และให้เงินกู้ชั่วคราวกับธุรกิจขนาดเล็ก วงเงินประมาณ 50 ล้านบาทต่อราย
            พร้อมทั้งช่วยจ่ายเงินค่าจ้างให้กับชาวอังกฤษที่ต้องหยุดงานจากโควิดคิดเป็น 80% ของเงินเดือนแต่ไม่เกิน 97,000
            บาท เป็นเวลาอย่างน้อย 3 เดือน</p>
    </div>

    <div class="col-12 text-center mt-3">
        <img src="/img/icon/rectangle-copy-32.png" alt="" style="width: 609px; height: 406px; border:1px solid grey">
    </div>
    <div class="col-12 mt-3">
        <p>นอกจากมาตรการช่วยเหลือจากภาครัฐแล้ว ผู้ประกอบการร้านอาหารแบบสาขาได้ปรับตัว โดยหันไปเปิดบริการแบบเดลิเวอรี่
            (Delivery) และแบบนำกลับ (Take away) เพื่อให้สอดรับกับการใช้มาตรการล็อกดาวน์
            ซึ่งอาจจะมีการขยายระยะเวลาต่อไปจากเดือนพฤษภาคม 2563</p>
        <p>อย่างไรก็ตามอีกด้านหนึ่งข้อมูลจากศูนย์วิจัย Mintel และ Encovia Intelligence ระบุว่า
            พฤติกรรมของผู้บริโภคสหราชอาณาจักรหลังโควิด-19 ได้ปรับเปลี่ยนมาอยู่บ้านลดความเสี่ยงจากการแพร่เชื้อ
            ปรุงอาหารรับประทานเอง ทำให้ยอดจำหน่ายสินค้าของชำ (Grocery) ช่องทางออนไลน์ขยายตัวเพิ่มขึ้น 33% มูลค่า 6.5
            แสนล้านบาท จากปี 2562 และคาดว่าจะมีโอกาสขยายตัวเพิ่มขึ้นถึง 41% เป็น 7 แสนล้านบาทในอีก 4 ปีข้างหน้า หรือในปี
            2567 โดยเฉพาะกลุ่มผู้สูงวัยซึ่งมีแนวโน้มเพิ่มขึ้นในอนาคต</p>
        <p>ขณะเดียวกันผลการสำรวจยังพบอีกว่าผู้บริโภคให้ความสำคัญกับสุขภาพมากขึ้น
            ทำให้สินค้ากลุ่มออร์แกนิกและสินค้าสุขภาพในตลาดออนไลน์ขยายตัวเพิ่มขึ้นตามไปด้วย</p>

    </div>
    {{-- Make do new again when start backend --}}



    <div class="col-12 py-3 mt-4 mycontent"
        style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
        <div class="col-12 d-flex align-items-center justify-content-between">
            <div>
                <span class="album_blog">อัลบั้มภาพ</span>
            </div>
            <div>
                <span class="name_album mr-2">ประมวลธุรกิจเดลิเวอรี่อาหาร</span>
                <span class="count_img_album">10 ภาพ</span>
            </div>
        </div>

        <div class="row px-3 mt-3">
            <div class="swiper-container">
                <div class="swiper-wrapper mb-5">
                    <div class="swiper-slide">
                        <img src="/img/icon/news3@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news4@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news5@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news6@2x.png" alt="">
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

    </div>





    <div class="col-12 py-3 mt-4" style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
        <div class="col-12 d-flex align-items-center justify-content-between">
            <div>
                <span class="album_blog">วิดีโอ</span>
            </div>
            <div>
                <span class="count_img_album">3 คลิป</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-center">
            @for($i=0; $i < 3; $i++) <div class="col-4">
                <div class="preview_vdo">
                    <img src="/img/icon/news6.png" alt="" style="width:100%;height:100%;object-fit:contain;">
                </div>
                <div class="col-12 d-flex align-content-around flex-wrap px-2">
                    <div>
                        <span class="name_vdo">รายงานสถานการณ์ /
                            โอกาสทางการค้าเนเธอแลนด์</span>
                    </div>
                    <div>
                        <span class="date_vdo">18 มกราคม 2564</span>
                    </div>
                </div>
        </div>
        @endfor

    </div>
</div>


<div class="col-12 py-3 mt-4" style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 d-flex align-items-center justify-content-between">
        <div>
            <span class="album_blog">ไฟล์แนบ</span>
        </div>
        <div>
            <span class="count_img_album">2 ไฟล์</span>
        </div>
    </div>
    <div class="row px-4 d-flex align-items-center mt-3">

        @for($c=0; $c < 3; $c++) <div class="col-3 d-flex align-items-center mr-2 mb-2"
            style="width: 238px;height: 52px;border-radius: 4px;background-color: #eaeaea;">
            <div class="col-2 d-flex align-items-center p-0">
                <img src="/img/type_files/group-20-copy-9.png" alt="">
            </div>
            <div class="col-8 p-0">
                <div class="col-12 p-0" style="line-height: 1;">
                    <span class="name_files_blog three_dots">การล็อกดาวน์ช่วยกระasdasdasdasdasdads</span>
                    <span class="size_file_blog">57 KBB</span>
                </div>
            </div>
            <div class="col-2 d-flex align-items-center justify-content-center p-0">
                <img src="/img/icon/file-download-material-black.png" alt=""
                    style="object-fit:contain;cursor: pointer;">
            </div>
    </div>
    @endfor

</div>
</div>




<div class="col-12 d-flex align-items-center my-4 p-0">
    <span class="tag-head-content">Tag : &nbsp;</span>
    <span class="tag-content">COVID19, Food, Delivery, โควิด 19, อังกฤษ , สหราชอาณาจักร , UK, อาหาร, สุขภาพ</span>
</div>


<div class="col-12 mt-3 p-0">
    <div class="col-12 p-0">
        <span style="font-size: 14px;color: #4a4a4a;">บทความโดย : </span>
    </div>
    <div class="col-12 d-flex align-items-center p-0">
        <div class="col-1 px-0">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" class="img_writer" alt="">
        </div>
        <div class="col-6">
            <div class="col-12 p-0 d-flex align-items-center">
                <span class="name_writer mr-2">Nissana Thaveepanit</span>
                <span class="guru">GURU</span>
            </div>
            <div class="col-12 p-0">
                <span class="position_writer">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
            </div>
        </div>
        <div class="col-5 d-flex align-items-center p-2" style="background-color:#4f72e5; border-radius:4px;">
            <div class="col-2 d-flex align-items-center">
                <img src="/img/icon/grade-golde-material.png" alt="" class="mr-2">
                <span class="count-vote">1</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-center">
                <span class="for_thisBlog">โหวตให้บทความนี้</span>
            </div>
            <div class="col-4 d-flex align-items-center">
                <button class="vote">Vote!</button>
            </div>
        </div>
    </div>
</div>



<div class="col-12 mt-3 p-3" style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 d-flex align-items-center justify-content-between p-0">
        <div class="px-3">
            <span class="head-comment">คุณรู้สึกอย่างไรกับบทความนี้</span>
        </div>
        <div class="d-flex align-items-center px-3">
            <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
            <span class="count_commet">2</span>
        </div>
    </div>
    <div class="col-12 d-flex align-items-center mt-2">
        <div class="col-1 px-0">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" class="img_comment" alt="">
        </div>
        <div class="col-11">
            <textarea name="comment" class="comment" cols="90" rows="1"></textarea>
        </div>
    </div>
</div>


<div class="col-12 mt-3 p-3" style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">

    <div class="col-12 d-flex align-items-start mt-3 p-3" style="border-radius: 8px;border: solid 0.5px #8a8c8e;">
        <div class="col-1 px-0 ">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" class="img_comment" alt="">
        </div>
        <div class="col-10 px-2">
            <div class="col-12 p-0">
                <span class="name_commentor">Chiraphon Thangthai</span>
            </div>
            <div class="col-12 p-0" style="line-height: 1">
                <span class="date_comment">4 ม.ค. 2564 09:47</span>
            </div>
            <div class="col-12 p-0" style="line-height: 2">
                <span class="content_comment">บทความเป็นประโยชน์มากเลยครับ ชอแชร์เป็นข้อมูลนะครับ</span>
            </div>
        </div>
        <div class="col-1 text-end">
            <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
        </div>
    </div>



    <div class="col-12 d-flex align-items-start mt-3 p-3" style="border-radius: 8px;border: solid 0.5px #8a8c8e;">
        <div class="col-1 px-0 ">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" class="img_comment" alt="">
        </div>
        <div class="col-10 px-2">
            <div class="col-12 p-0">
                <span class="name_commentor">Chiraphon Thangthai</span>
            </div>
            <div class="col-12 p-0" style="line-height: 1">
                <span class="date_comment">4 ม.ค. 2564 09:47</span>
            </div>
            <div class="col-12 p-0" style="line-height: 2">
                <span class="content_comment">บทความเป็นประโยชน์มากเลยครับ ชอแชร์เป็นข้อมูลนะครับ</span>
            </div>
        </div>
        <div class="col-1 text-end">
            <img src="/img/icon/more-horiz-material-copy-7.png" alt="" style="cursor: pointer;">
        </div>
    </div>


</div>






</div>
@endsection

@section('script')
<script>
    var swiper = new Swiper(".mycontent .swiper-container", {
        slidesPerView: 4,
        loop: true,
        pagination: {
            el: ".mycontent .swiper-pagination",
            dynamicBullets: true,
        },
        navigation: {
            nextEl: ".mycontent .swiper-button-next",
            prevEl: ".mycontent .swiper-button-prev",
        },
    });

</script>
@endsection
