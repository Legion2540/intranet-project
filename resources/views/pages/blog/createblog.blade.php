@extends('layout.blog')
@section('style')
<style>
    .head-text {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .title-input {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    #input-text {
        width: 100%;
        height: 30px;
        padding: 5px 20px 5px 20px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #input-url {
        width: 437px;
        height: 30px;
        padding: 5px 20px 5px 20px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #input-text::placeholder,
    #input-url::placeholder {
        font-size: 14px;
        color: #8a8c8e;
    }

    .type_blog,
    .select_album {
        width: 437px;
        height: 30px;
        line-height: 1.2;
        font-size: 14px;
        color: #8a8c8e;
        padding: 5px 5px 4px 18.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .browse {
        width: 95px;
        height: 32px;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
        color: #ffffff;
        font-size: 14px;
        border-radius: 4px;
        border: none;
    }

    .extension_img {
        font-size: 14px;
        color: #ee2a27;
    }

    .notice_img {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4f72e5;
    }

    .cancelBlog,
    .cancelBlog:hover {
        width: 112px;
        height: 30px;
        color: #ffffff;
        border: none;
        text-decoration: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    .createBlog {
        width: 112px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 178%);
    }






    .dropphoto {
        width: 100%;
        height: 189px;
        border: dashed 2px #4f72e5;
        background-color: #ffffff;
    }

    .detel-photo {
        width: 285px;
        position: absolute;
        top: 12%;
        left: 30%;
    }

    .avatar {
        position: absolute;
        top: 2%;
        left: 0%;
        padding: 26.4px 21px 24.4px 20px;
    }

    .areadrop {
        width: 123px;
        height: 123px;
        margin: 0 7px 0 7px;
    }

    .img-del {
        position: absolute;
        top: 5%;
        right: 5%;
        cursor: pointer;
    }

    .profile-pic {
        object-fit: cover;
        width: 123px;
        height: 123px;
        image-rendering: pixelated;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
    }

</style>
@endsection

@section('content')
<div class="col-12 px-3 pt-2 pb-3" style="background-color:#ffffff;">
    <div class="col-12 d-flex align-items-center">
        <img src="/img/icon/border-color-material-copy.png" alt="" class="mr-3">
        <span class="head-text">เขียนบล็อค</span>
    </div>
    <div class="col-12 mt-3 p-0">

        <div class="col-12 d-flex align-items-center p-0">
            <div class="col-2">
                <span class="title-input">ชื่อหัวเรื่อง</span>
            </div>
            <div class="col-10">
                <input type="text" name="input-text" id="input-text" placeholder="ระบุชื่อบทความ หัวเรื่อง...">
            </div>
        </div>

        <div class="col-12 d-flex align-items-center p-0 mt-3">
            <div class="col-2">
                <span class="title-input">หมวดหมู่</span>
            </div>
            <div class="col-10">
                <select class="form-select type_blog" name="type_blog" aria-label="Default select example">
                    <option selected>ระบุหมดหมู่บทความ</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div>

        <div class="col-12 d-flex align-items-center p-0 mt-3">
            <div class="col-2">
                <span class="title-input">ภาพปก</span>
            </div>
            <div class="col-10">
                <input type="file" name="img_cover" id="img_cover" style="display: none;">
                <button class="browse mr-3">Browse File</button>
                <span class="extension_img">(.jpg, .png)</span>
            </div>
        </div>

        <div class="col-12 d-flex align-items-start p-0 mt-3">
            <div class="col-2">
                <span class="title-input">ข้อความ</span>
            </div>
            <div class="col-10">
                <textarea name="content_blog" id="content_blog" cols="80" rows="3" style="resize: none;"></textarea>
            </div>
        </div>


        <div class="col-12 d-flex align-items-start p-0 mt-3">
            <div class="col-2">
                <span class="title-input">ภาพปก</span>
            </div>
            <div class="col-10 d-flex align-items-center justify-content-center px-4 py-3">
                <div class="dropphoto position-relative">
                    <input type="file" class="img-upload-input custom-file-input" id="customFile" name="filename"
                        accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple
                        style="width: 100%;height: 100%;">
                    <div class="detel-photo text-center">
                        <div class="col-12">
                            <img src="/img/icon/insert-photo-material.png" alt="" width="60px" height="auto">
                        </div>
                        <div class="col-12">
                            <span style="font-size:18px;color:#4f72e5;text-align:center">Choose file from your device or
                                drag image here.</span>
                        </div>
                        <div class="col-12">
                            <span class="d-flex justify-content-center" style="font-size:12px;color:#ee2a27;">(bmp, gif,
                                jpg, jpeg, png)</span>
                        </div>
                    </div>
                    <div class="d-flex avatar" style="z-index: 2;">
                    </div>
                </div>




            </div>
        </div>


        <div class="col-12 d-flex align-items-center p-0 mt-3">
            <div class="col-2">
                <span class="title-input">แนบอัลบั้ม</span>
            </div>
            <div class="col-10">
                <select class="form-select select_album" name="id_album" aria-label="Default select example">
                    <option selected>เลือกอัลบั้ม</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div>




        <div class="col-12 d-flex align-items-center p-0 mt-3">
            <div class="col-2">
                <span class="title-input">แนบไฟล์</span>
            </div>
            <div class="col-10">
                <input type="file" name="file_blog[]" id="file_blog" style="display: none;">
                <button class="browse mr-3">Browse File</button>
                <span class="extension_img">(.pdf ,.mp4, .docx, .xls)</span>
            </div>
        </div>



        <div class="col-12 d-flex align-items-center mt-3 p-0">
            <div class="col-2">
                <span class="title-input">แนบลิงค์</span>
            </div>
            <div class="col-10">
                <input type="text" class="mr-2" name="input-url" id="input-url" placeholder="ระบุ URL">
                <span class="extension_img">ระบุ URL ของคลิปวิิดีโอจาก Youtube</span>
            </div>
        </div>

        <div class="col-12 d-flex align-items-center mt-3 p-0">
            <div class="col-2">
                <span class="title-input">Tag</span>
            </div>
            <div class="col-10">
                <input type="text" class="mr-2" name="tag_blog" id="input-text" placeholder="ระบุแท็ก">
            </div>
        </div>
    </div>


    <div class="col-12 d-flex align-items-center justify-content-center mt-5 mb-4">
        <a href="{{ URL::previous() }}" class="mr-3"><button class="cancelBlog">ยกเลิก</button></a>
        <button class="createBlog">สร้าง</button>
    </div>

</div>
@endsection

@section('script')
<script>
    var countimg = 0;
    $(document).ready(function () {

        // upload img
        var store_img = [];
        countimg = 0;

        var readURL = function (input) {
            countimg += input.files.length;


            if (input.files.length <= 5 && countimg <= 5) {

                $.each(input.files, function (key, img_input) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        store_img.push(e.target.result);
                        $('.avatar').append('<div class="areadrop position-relative">' +
                            '<img class="profile-pic" />' +
                            '<img class="img-del" src="/img/icon/close-material.png" onclick="remove_img(this)"/>' +
                            '</div>');

                        $('.detel-photo').hide();

                        $.each(store_img, function (key, value) {
                            $('.profile-pic').last().attr('src', value);
                        });
                    }
                    reader.readAsDataURL(input.files[key]);
                });
            } else {
                countimg -= input.files.length;
                alert('Maximum 5 file');
            }


        }

        $(".img-upload-input").on('change', function () {
            readURL(this);
        });
    });

    function remove_img(_this) {
        $(_this).parents('.areadrop').remove();
        countimg -= 1;
        if (countimg == 0) {
            $('.detel-photo').show();
        }

    }

</script>
@endsection
