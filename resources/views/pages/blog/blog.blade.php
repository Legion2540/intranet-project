@extends('layout.blog')
@section('style')
<style>
    .Head_content {
        font-family: Kanit-Regular;
        font-size: 20px;
        font-weight: bold;
        color: #4f72e5;
    }

    .title_blog {
        font-family: Kanit-Regular;
        font-size: 22px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .date,
    .time,
    .three_number {
        font-family: Kanit-Regular;
        font-size: 14px;
        line-height: 2.29;
        color: #4a4a4a;
    }

    .position_writter {
        font-size: 14px;
        color: #4a4a4a;
    }

    .type_blog {
        font-size: 14px;
        font-weight: 600;
        line-height: 2.29;
        color: #4f72e5;
        text-decoration: underline;
    }

    .type_blog_other {
        font-size: 12px;
        font-weight: 600;
        line-height: 2.29;
        color: #4f72e5;
        text-decoration: underline;
    }

    .img_writter {
        width: 50px;
        height: 50px;
        object-fit: contain;
        border: solid 2px #ffffff;
        border-radius: 50%;

    }

    .img_writter_other {
        width: 26px;
        height: 26px;
        object-fit: contain;
        border: solid 2px #ffffff;
        border-radius: 50%;

    }

    .name_writter {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .GURU {
        width: 26px;
        height: 12px;
        /* margin: 1px 0 0 0; */
        border-radius: 3px;
        background-color: #0066b2;
        font-size: 7px;
        font-weight: bold;
        color: #ffffff;
    }

    .head_other {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .type_other {
        font-size: 14px;
        font-weight: 600;
        line-height: 2.29;
        color: #4f72e5;
        text-decoration: underline;
    }

    .date_other,
    .time_other,
    .three_number_other {
        font-family: Kanit-Regular;
        font-size: 12px;
        line-height: 2.29;
        color: #4a4a4a;
    }

    .name_writter_other {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .position_writter_other {
        font-size: 12px;
        color: #4a4a4a;
    }

    #search_blog {
        width: 100%;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #9b9b9b;
        background-color: #ffffff;
        padding: 5px 45px;
        position: relative;
    }

    #search_blog,
    #search_blog::placeholder {
        font-size: 15px;
        font-weight: 500;
        color: #8a8c8e;
    }


</style>
@endsection

@section('content')
<div class="col-12 px-4 py-3" style="background-color:#ffffff; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 p-0">
        <span class="Head_content">Most Popular</span>
    </div>
    <div class="col-12 p-0 mt-2 d-flex align-items-start" style="background-color:#eaeaea">
        <div class="col-5 p-0" style="width: 401px;height: 299px;">
            <img src="/img/icon/news6@3x.png" alt="" style="width:100%;height:100%; object-fit:cover;">
        </div>
        <div class="col-7 pl-4 pr-2 d-flex align-content-around flex-wrap" style="height:300px;">
            <div class="col-12 p-0">
                <div class="col-12 p-0">
                    <span class="title_blog">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries
                        ในเนเธอร์แลนด์สูงกว่า 50%</span>
                </div>
                <div class="col-12 p-0">
                    <a href="#" class="type_blog">สถานการณ์การค้าผลกระทบ COVID-19</a>
                </div>
            </div>

            <div class="col-12 p-0">
                <div class="col-12 d-flex justify-content-between p-0">
                    <div class="col-6 p-0 d-flex justify-content-start">
                        <span class="date mr-2">5 ม.ค. 2564</span>
                        <span class="time">11 : 00</span>
                    </div>
                    <div class="col-6 p-0 d-flex justify-content-evenly">
                        <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                        <span class="three_number">1</span>
                        <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                        <span class="three_number">2</span>
                        <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                        <span class="three_number">25</span>
                    </div>
                </div>

                <div class="col-12 d-flex align-content-center justify-content-between p-0 mt-2">
                    <div class="col-2 p-0">
                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="img_writter">
                    </div>
                    <div class="col-10 p-0">
                        <div class="col-12 p-0 d-flex align-items-center">
                            <span class="name_writter">Nissana Thaveepanit</span>
                            <span class="GURU ml-2">GURU</span>
                        </div>
                        <div class="col-12 p-0">
                            <span class="position_writter">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="col-12 mt-3 px-4 pt-3 pb-4" style="background-color:#ffffff; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 p-0">
        <span class="Head_content">Highlight</span>
    </div>
    <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">

        @for($i=0; $i<3; $i++)
        <div class="p-0" style="background-color: #eaeaea; width:300px;">
            <div class="col-12 p-0">
                <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
            </div>
            <div class="col-12 p-2">
                <div class="col-12 p-0">
                    <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                </div>
                <div class="col-12 p-0">
                    <a href="#" class="type_other">เกษตรและอาหาร</a>
                </div>
                <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                    <div class="col-6 p-0">
                        <span class="date">31 ธ.ค. 2563</span>
                        <span class="time">09:00</span>
                    </div>
                    <div class="col-6 p-0 d-flex justify-content-evenly">
                        <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                        <span class="three_number_other">1</span>
                        <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                        <span class="three_number_other">2</span>
                        <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                        <span class="three_number_other">25</span>
                    </div>
                </div>
                <div class="col-12 d-flex align-content-center p-0 mt-2">
                    <div class="col-1 d-flex justify-content-center align-items-center">
                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            class="img_writter_other">
                    </div>
                    <div class="col-11 px-2" style="line-height: 1">
                        <div class="col-12 p-0 d-flex align-items-center">
                            <span class="name_writter_other">Nissana Thaveepanit</span>
                            <span class="GURU ml-2">GURU</span>
                        </div>
                        <div class="col-12 p-0">
                            <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                        </div>
                    </div>
                </div>

            </div>
    </div>

    @endfor

</div>
</div>



<div class="col-12 mt-3 px-4 pt-3 pb-4" style="background-color:#ffffff; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
    <div class="col-12 p-0">
        <input type="search" name="search_blog" id="search_blog" placeholder="ค้นหาข้อมูล บล็อก หรือผู้เขียน">
        <img src="/img/icon/search-material-copy@2x.png" alt=""
            style="position: absolute;object-fit:contain; left: 1%; top: 15%;">
    </div>

    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">สถานการณ์การค้าผลกระทบ COVID-19</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>

    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">เกษตรและอาหาร</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>


    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">อุตสาหกรรม</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>


    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">ไลฟ์สไตล์/แฟชั่น</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>


    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">อัญมณีและตลาดเฉพาะ</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++)
            <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>


    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">สุขภาพและความงาม</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>

    <div class="dash_market"></div>


    <div class="col-12 p-0 mb-5">
        <div class="col-12 p-0 d-flex align-items-center mt-4">
            <div class="col-6 p-0">
                <span class="Head_content">ธุรกิจบริการ</span>
            </div>
            <div class="col-6 p-0 text-end">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>
        <div class="col-12 p-0 mt-2 d-flex align-itmes-center justify-content-between">
            @for($i=0; $i<3; $i++) <div class="p-0" style="background-color: #eaeaea; width:300px;">
                <div class="col-12 p-0">
                    <img src="/img/icon/news7@2x.png" alt="" style="width:100%;height:100%;object-fit:cover;">
                </div>
                <div class="col-12 p-2">
                    <div class="col-12 p-0">
                        <span class="head_other">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564 (สคต.เซี่ยเหมิน)</span>
                    </div>
                    <div class="col-12 p-0">
                        <a href="#" class="type_other">เกษตรและอาหาร</a>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center justify-content-between">
                        <div class="col-6 p-0">
                            <span class="date">31 ธ.ค. 2563</span>
                            <span class="time">09:00</span>
                        </div>
                        <div class="col-6 p-0 d-flex justify-content-evenly">
                            <img src="/img/icon/grade-material.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">1</span>
                            <img src="/img/icon/comment-blue.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="" style="object-fit:contain;">
                            <span class="three_number_other">25</span>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-content-center p-0 mt-2">
                        <div class="col-1 d-flex justify-content-center align-items-center">
                            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                class="img_writter_other">
                        </div>
                        <div class="col-11 px-2" style="line-height: 1">
                            <div class="col-12 p-0 d-flex align-items-center">
                                <span class="name_writter_other">Nissana Thaveepanit</span>
                                <span class="GURU ml-2">GURU</span>
                            </div>
                            <div class="col-12 p-0">
                                <span class="position_writter_other">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        @endfor

        </div>
    </div>









</div>






@endsection





@section('script')
<script>

</script>
@endsection
