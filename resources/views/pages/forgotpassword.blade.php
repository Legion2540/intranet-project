@extends('layout.login')

@section('style')
<style>
    .container {
        transform: translate(0px, 218px);
    }

    .card-login {
        width: 1080px;
        background-color: #fff;
        border-radius: 4px;
    }

    .card-login .logo-web:first-child {
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        border-radius: 4px;
    }

    .input-side {
        background-color: #eff3fe;
    }

    .text-page {
        font-family: Kanit-Regular;
        font-size: 30px;
        color: #4f72e5;
    }

    .text-page-desc {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #a3a3a3;
    }

    .login-btn {
        width: 345px;
        height: 45px;
        margin: 26px 0 0;
        padding: 10px 150px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-image: linear-gradient(to left, #4f72e5 107%, #314d7b -89%);
    }

    .text-email,
    .text-password {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4869cb;
    }

    .form-control {
        font-family: Kanit-Regular;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #fff;
    }

    .forgot_password {
        color: #4f72e5;
        font-family: Kanit-Regular;
        cursor: pointer;
    }

    .title-pages {
        margin-top: 140px !important;
        margin-bottom: 30px !important;
        line-height: 30px;
    }

    .button-page {
        margin-top: 20px !important;
        margin-bottom: 80px !important;
    }

    input {
        color: black !important;
        background-color: #ffffff !important;
    }

    button[type="submit"],
    button[type="submit"]:active {
        background-image: linear-gradient(to left, #4f72e5 107%, #314d7b -89%);
        color: #ffffff;
    }

</style>
@endsection

@section('content')
<div class="container d-flex justify-content-center p-0">
    <div class="card-login">
        <div class="col-12 p-2 d-flex">
            <div class="col-7 logo-web">
                <img src="img/icon/Group7.png" alt="">
            </div>
            <div class="col-5 input-side">

                @if(Session::has('message'))
                <div class="alert alert-danger">
                    {{Session::get('message')}}
                </div>
                @endif


                <form action="/forgotpassword" method="post">
                    @csrf

                    <div class="col-12 mt-5 mb-3 title-pages">
                        <div class="col-12 p-0">
                            <span class="text-page">Forget password</span>
                        </div>
                        <div class="col-12 p-0">
                            <span class="text-page-desc">Send a link to your email to reset your password</span>
                        </div>
                    </div>

                    <div class="col-12 mt-5">
                        <label class="text-email">Email</label>
                        {{-- <input type="email" class="form-control" pattern=".+@globex\.com" name="email" --}}
                        <input type="email" class="form-control" name="email"
                            placeholder="Enter your Email" required>
                    </div>

                    <div class="col-12 button-page">
                        <button type="submit" class="form-control">Send reset link</button>
                        {{-- <a href="/resetpassword">Send reset link</a> --}}
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>













@endsection

@section('script')
<script type="text/javascript">
</script>
@endsection
