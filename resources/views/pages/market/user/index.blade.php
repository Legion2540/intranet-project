@extends('layout.market')
@section('contentmarket')
<style>

    .text-12{
        font-size: 12px;
    }

    .text-headbord {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .text-menu {
        font-size: 12px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-price{
        font-size: 20px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-comment{
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .select-list {
        width: 300px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .btn-menu {
        width: 95px;
        height: 95px;
        border: 0;
        background-color: #4f72e5;
    }

    .menu-market {
        width: 967px;
        height: 216px;
        box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);
        background-color: #eaeaea;
    }

    .select-date {
        width: 125px;
        height: 27px;
        border-radius: 4px;
        font-size: 12px;
        color: #4f72e5;
        background-color: #ffffff;
    }

    .select-price {
        width: 125px;
        height: 27px;
        border-radius: 4px;
        font-size: 12px;
        color: #4f72e5;
        background-color: #ffffff;
    }

    .card-body{
        padding: 10px;
    }

    .card-text{
        font-size: 14px;
        color: #4a4a4a;
    }

    .position-hard{
        position: absolute;
        top: 110px;
        right: 10px;
        width: 37px;
        height: 37px;
    }

</style>
<div class="px-0" style="min-height:650px;">
    <div class="col-12 px-0 menu-market">
        <div class="col-12 d-flex" style="padding: 15px 20px 15px 20px;">
            <div class="col-6">
                <span class="text-headbord">กระดานซื้อ - ขาย</span>
            </div>
            <div class="col-6 pr-0 d-flex justify-content-end">
                <div class="mr-2">
                    <select class="form-select p-0 pl-2 select-list" aria-label="Default select example"
                        style="font-size: 16px">
                        <option value="1">ทุกหมวดหมู่</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <div class="mr-2">
                    <img src="/img/icon/favorite-bule-material.png" alt="">
                </div>
                <div>
                    <img src="/img/icon/tune-material-copy-3.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-12 d-flex justify-content-between" style="padding: 0px 20px;">
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/device-1.png" alt="">
                </button>
                <br>
                <span class="text-menu">มือถือ/แทปเลต</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/fashion-1.png" alt="">
                </button>
                <br>
                <span class="text-menu">เสื้อผ้ามือสอง</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/toys-1.png" alt="">
                </button>
                <br>
                <span class="text-menu">แม่และเด็ก</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/desktop-1.png" alt="">
                </button>
                <br>
                <span class="text-menu">คอมพิวเตอร์</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/ring.png" alt="">
                </button>
                <br>
                <span class="text-menu">เครื่องประดับ</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/photo-camera.png" alt="">
                </button>
                <br>
                <span class="text-menu">กล้อง</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/electrical-appliances.png" alt="">
                </button>
                <br>
                <span class="text-menu">เครื่องใช้ไฟฟ้า</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/box.png" alt="">
                </button>
                <br>
                <span class="text-menu">ของสะสม/ของเล่น</span>
            </div>
            <div class="text-center" style="width: 95px;">
                <button class="btn-menu">
                    <img src="/img/icon/furnitures.png" alt="">
                </button>
                <br>
                <span class="text-menu">เฟอร์นิเจอร์</span>
            </div>
        </div>
    </div>

    <div class="col-12 px-0 mt-2">
        <div class="col-12 px-0 d-flex">
            <div class="col-6 px-0">
                <span style="color: #4f72e5;font-size:20px;font-family: Kanit-Regular;">สินค้าทั้งหมด</span>
            </div>
            <div class="col-6 px-0 text-12 d-flex">
                <div class="col-7 d-flex justify-content-end align-items-center">
                    <span class="mr-2">เรียงจาก ราคา</span>
                    <select class="form-select p-1 select-price" aria-label="Default select example">
                        <option value="1">น้อยสุด - มากสุด</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <div class="col-5 px-0 d-flex justify-content-end align-items-center">
                    <span class="mr-2">วันประกาศ</span>
                    <select class="form-select p-1 select-date" aria-label="Default select example">
                        <option value="1">ใหม่สุด - เก่าสุด</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-12 px-0 mt-2">
            @for ($a = 0; $a < 3; $a++) 
                <div class="col-12 mt-2 px-0 d-flex">
                    @for ($i = 0; $i < 4; $i++)
                        <div class="col-3 px-0 position-relative d-flex justify-content-center">
                            <div class="card" style="width: 232px;box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.08);">
                                <img src="/img/icon/rectangle-copy-33.png" class="card-img-top" alt="..." width="232px" height="155px">
                                <img src="/img/icon/group-43-copy.png" alt="" class="position-hard">
                                <div class="card-body">
                                <p class="card-text">ipad 2 wifi 16 GB เครื่องสีดำ </p>
                                <div class="col-12 px-0 d-flex">
                                    <div class="col-6 px-0">
                                        <span class="text-price">3,200</span>
                                    </div>
                                    <div class="col-6 px-0 text-end">
                                        <img src="/img/icon/comment-blue.png" alt="">
                                        <span class="ml-1 text-comment">1</span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            @endfor
    </div>

    <div class="col-12 mt-5">
        <div class="row next-bar">
            <div class="col-4 px-0 d-flex justify-content-start align-items-center">
                <span>แสดงผล 7 จากทั้งหมด 7</span>
            </div>
            <div class="col-4 d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-back mr-2">&larr; ย้อนกลับ</button>
                <button type="button" class="btn btn-next">หน้าถัดไป &rarr;</button>
            </div>
            <div class="col-4 pr-4 next-back">
                <div class="row d-flex justify-content-end align-items-center">
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                        style="height: 23px; width:34px;font-size:11px;margin-right: 10px;">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>จาก 15</span>
                    </div>
                    <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                        <button type="button" class="btn btn-next">&lt;</button>
                        <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
