@extends('layout.market')
@section('contentmarket')
<style>
    .btn-call,
    .btn-call:hover {
        width: 95px;
        height: 23px;
        color: #ffffff;
        border-radius: 7px;
        font-size: 14px;
        padding: 0;
        background-color: #4f72e5;
    }

    .btn-inbox,
    .btn-inbox:hover {
        width: 95px;
        height: 23px;
        color: #ffffff;
        border-radius: 7px;
        font-size: 14px;
        padding: 0;
        background-color: #2aa93d;
    }

    .card-comment {
        width: 948px;
        height: 181px;
        border-radius: 2px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .textarea-comment {
        width: 764px;
        height: 56px;
        border-radius: 8px;
        border: solid 0.5px #8a8c8e;
        resize: none;
    }

    .text-head-comment {
        font-size: 26px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-detel {
        color: #4a4a4a;
        font-size: 24px;
        font-family: Kanit-Regular;
    }

    .text-price-detel {
        font-size: 32px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-head-detel {
        color: #4a4a4a;
        font-size: 18px;
        font-family: Kanit-Regular;
    }

    .text-name-profile {
        color: #4a4a4a;
        font-size: 20px;
        font-family: Kanit-Regular;
    }

    .text-context-profile {
        color: #4a4a4a;
        font-size: 14px;
        font-family: Kanit-ExtraLight;
    }

    .photo-profile-product {
        width: 62px;
        height: 62px;
        border: solid 2px #ffffff;
        object-fit: contain;
        border-radius: 50%;
    }

    .gallery-container {
        width: 100%;
        display: flex;
        justify-content: space-between;
    }


    .gallery-main {
        width: 800px;
        height: 426px;
        box-shadow: 0 50px 75px 50px rgba(#1e1e1e, 0.18);
        /* border: 1px solid #e9e9e9; */

        .swiper-button {

            &-prev,
            &-next {
                position: absolute;
                bottom: 0;
                right: 0;
                top: auto;
                left: auto;
                height: 75px;
                width: 40px;
                color: #e9e9e9;
                background: #1e1e1e;

                &::after {
                    font-size: 12px;
                }
            }

            &-prev {
                right: 40px;
                border-right: 1px solid #e9e9e9;
            }
        }
    }

    .gallery-main img {
        width: 707px;
        height: 426px;
    }


    .gallery-thumbs {
        order: 1;
        width: 100px;
        height: 426px;
        margin-right: 15px;
        padding-left: 15px;

        .swiper-slide {
            img {
                transition: 0.3s;
            }

            &-active {
                opacity: 0.9;
            }

            &-thumb-active {
                opacity: 1;

                img {
                    margin-left: -15px;
                }
            }
        }
    }



    .swiper-product .swiper-button-next,
    .swiper-product .swiper-button-next::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size: 14px !important;
        right: 1px;
        top: 50%;
    }

    .swiper-product .swiper-button-prev,
    .swiper-product .swiper-button-prev::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size: 14px !important;
        right: 1px;
        top: 50%;
    }

</style>
<div class="px-0" style="min-height:650px;">
    <div class="col-12 px-0 mt-2">
        <div class="col-12 px-0">
            <span style="color: #4f72e5;font-size:20px;font-family: Kanit-Regular;">กระดานซื้อ - ขาย > สินค้าของฉัน >
                ......</span>
        </div>
    </div>
    <div class="col-12 swiper-product mt-3 px-0">
        <div class="gallery-container">
            <div class="swiper-container gallery-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide1/600/300" alt="Slide 01">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide2/600/300" alt="Slide 02">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide3/600/300" alt="Slide 03">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide4/600/300" alt="Slide 04">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide5/600/300" alt="Slide 05">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide6/600/300" alt="Slide 06">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide7/600/300" alt="Slide 07">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide8/600/300" alt="Slide 08">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide9/600/300" alt="Slide 09">
                    </div>
                    <div class="swiper-slide text-center">
                        <img src="https://picsum.photos/seed/slide10/600/300" alt="Slide 10">
                    </div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
            <div class="swiper-container gallery-thumbs">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide1/115/100" alt="Slide 01">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide2/115/100" alt="Slide 02">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide3/115/100" alt="Slide 03">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide4/115/100" alt="Slide 04">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide5/115/100" alt="Slide 05">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide6/115/100" alt="Slide 06">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide7/115/100" alt="Slide 07">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide8/115/100" alt="Slide 08">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide9/115/100" alt="Slide 09">
                    </div>
                    <div class="swiper-slide">
                        <img src="https://picsum.photos/seed/slide10/115/100" alt="Slide 10">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-3">
        <span style="color:#4f72e5;font-size:14px">มือสอง</span>
    </div>
    <div class="col-12 mt-2 d-flex">
        <div class="col-10 px-0 text-detel">
            <span>ขายถูกมาก surface pro 3 CPU intel core i5 ssd 128gb สเปคแรง</span>
        </div>
        <div class="col-2 text-end">
            <img src="/img/icon/group-43-copy.png" alt="">
        </div>
    </div>
    <div class="col-12">
        <span class="text-price-detel">6,900</span>
    </div>

    <div class="col-12">
        <span class="text-head-detel">รายละเอียดสินค้า</span><br>
        <label>ขายถูกๆ surface pro 3 ราคานี้คุ้มมากค่ะ หน้าจอมีความละเอียดสูงมากเป็นระดับ 2K
            มาพร้อมกับ CPU core i5 4300U RAM 4GB
            มาพร้อมกับ ssd แรงเรียกว่าเปิดเครื่องแทบไม่ต้องรอเลยขนาด 128 GB
            การ์ดจอ intel ระบบ wifi รองรับ 5g แรงทำงานเล่นเกมสบาย
            สามารถต่อเมาส์คีย์บอร์ดเล่นได้ตามปกติ
        </label>
    </div>
    <div class="col-12 mt-5">
        <div class="col-12 px-0">
            <span>ผู้ประกาศ :</span>
        </div>
        <div class="col-12 px-0 d-flex">
            <div class="col-1 px-0 py-2">
                <img class="photo-profile-product"
                    src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg">
            </div>
            <div class="col-11 px-0">
                <div class="col-12">
                    <span class="text-name-profile">Darisa Patcharachot</span>
                </div>
                <div class="col-12">
                    <span class="text-context-profile">ช่องทางติดต่อ :</span>
                </div>
                <div class="col-12 d-flex">
                    <button class="btn btn-inbox mr-2">ส่งข้อความ</button>
                    <button class="btn btn-call">โทร</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-5 card-comment">
        <div class="col-12 p-3 d-flex">
            <div class="col-10 px-0">
                <span class="text-head-comment">คุณรู้สึกอย่างไรกับบทความนี้</span>
            </div>
            <div class="col-2 px-0 text-end">
                <img src="/img/icon/comment-blue.png" alt="">
                <span>0</span>
            </div>
        </div>
        <div class="col-12 d-flex">
            <div class="col-1 pl-3 pr-0">
                <img class="photo-profile-product"
                    src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg">
            </div>
            <div class="col-11 pl-4 d-flex align-items-center">
                <textarea class="textarea-comment p-1" placeholder="แสดงความคิดเห็น..."></textarea>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {


        var galleryMain = new Swiper(".gallery-main", {
            direction: 'vertical',
            centerSlides:true,
            loop: true,
            spaceBetween: 10,
            slidesPerView: 4,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            effect: 'fade',
            // fadeEffect: {
            //     crossFade: true
            // },
            thumbs: {
                swiper: galleryThumbs
            }
        });

        var galleryThumbs = new Swiper(".gallery-thumbs", {
            direction: 'vertical',
            centerSlides:true,
            loop: true,
            spaceBetween: 10,
            slidesPerView: 4,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });

        galleryMain.on('slideChangeTransitionStart', function () {
            galleryThumbs.slideTo(galleryMain.activeIndex);
        });

        galleryThumbs.on('transitionStart', function () {
            galleryMain.slideTo(galleryThumbs.activeIndex);
        });
    });

</script>
@endsection
