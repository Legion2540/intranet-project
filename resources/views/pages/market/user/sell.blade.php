@extends('layout.market_sell')
@section('contentsell')
<style>
    .text-key {
        font-size: 16px;
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .text-head-sell {
        font-size: 26px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-baht {
        position: absolute;
        top: 5%;
        left: 54%;
    }

    .forms-sell {
        padding: 13px 70px 35px 23px;
        min-height: 650px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }

    .select-type {
        width: 421px;
        height: 30px;
        font-size: 12px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-title {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-price {
        width: 421px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .textarea-detel {
        width: 100%;
        height: 131px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .btn-sell-close {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    .btn-sell-post {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .row-content {
        margin-top: 20px;
    }

    .dropphoto {
        height: 189px;
        border: dashed 2px #4f72e5;
        background-color: #ffffff;
    }

    .detel-photo {
        width: 285px;
        position: absolute;
        top: 12%;
        left: 30%;
    }

    .avatar {
        position: absolute;
        top: 2%;
        left: 0%;
        padding: 26.4px 21px 24.4px 20px;
    }

    .areadrop {
        width: 123px;
        height: 123px;
        margin: 0px 7px 0px 7px;
    }

    .img-del {
        position: absolute;
        top: 5%;
        right: 5%;
        cursor: pointer;
    }

    .profile-pic {
        object-fit: cover;
        width: 123px;
        height: 123px;
        image-rendering: pixelated;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
    }


    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

</style>
<div class="forms-sell">
    <div class="col-12 d-flex align-items-center">
        <div class="mr-2">
            <img src="/img/icon/border-color-material-copy.png" alt="">
        </div>
        <div>
            <span class="text-head-sell">สร้างประกาศขาย</span>
        </div>
    </div>
    <div class="col-12 mt-3 d-flex">
        <div class="col-2 pr-4 text-right">
            <span class="text-key">หมวดสินค้า</span>
        </div>
        <div class="col-10 px-0">
            <select class="form-select p-1 select-type">
                <option selected>เลือกหมวดสินค้า</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pr-4 text-right">
            <span class="text-key">หัวข้อประกาศ</span>
        </div>
        <div class="col-10 px-0">
            <input type="text" class="form-control input-title">
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pr-4 text-right">
            <span class="text-key">ภาพสินค้า</span><br>
            <span style="font-size: 12px">สูงสุด 5 รูป</span>
        </div>
        <div class="col-10 px-0">
            <div class="dropphoto position-relative">
                <input type="file" class="img-upload-input custom-file-input" id="customFile" name="filename"
                    accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple
                    style="width: 100%;height: 100%;">
                <div class="detel-photo text-center">
                    <div class="col-12">
                        <img src="/img/icon/insert-photo-material.png" alt="" width="60px" height="auto">
                    </div>
                    <div class="col-12">
                        <span style="font-size:18px;color:#4f72e5;text-align:center">Choose file from your device or
                            drag image here.</span>
                    </div>
                    <div class="col-12">
                        <span class="d-flex justify-content-center" style="font-size:12px;color:#ee2a27;">(bmp, gif,
                            jpg, jpeg, png)</span>
                    </div>
                </div>
                <div class="d-flex avatar" style="z-index: 2;">
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pl-0 pr-4 text-right">
            <span class="text-key">รายละเอียดสินค้า</span>
        </div>
        <div class="col-10 px-0">
            <textarea class="textarea-detel"></textarea>
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pl-0 pr-4 text-right">
            <span class="text-key">ราคาสินค้า</span>
        </div>
        <div class="col-10 px-0 position-relative">
            <input type="number" class=" form-control input-price"
                placeholder="ระบุราคาเต็มสินค้าหรือราคารวมของแถม ถ้ามี">
            <span class="text-baht">บาท</span>
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pl-0 pr-4 text-right">
            <span class="text-key">สภาพสินค้า</span>
        </div>
        <div class="col-10 px-0 d-flex">
            <div class="col-3 pl-1">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                        value="option1">
                    <label class="form-check-label" for="exampleRadios1">มือหนึ่ง</label>
                </div>
            </div>
            <div class="col-9">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2"
                        value="option2">
                    <label class="form-check-label" for="exampleRadios2">มือสอง</label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 row-content d-flex">
        <div class="col-2 pl-0 pr-4 text-right">
            <span class="text-key">การแสดงผล</span>
        </div>
        <div class="col-10 px-0 d-flex">
            <div class="col-3 pl-1">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="radio_status" id="exampleRadios1"
                        value="option1">
                    <label class="form-check-label" for="exampleRadios1">แสดง</label>
                </div>
            </div>
            <div class="col-9">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="radio_status" id="exampleRadios2"
                        value="option2">
                    <label class="form-check-label" for="exampleRadios2">ไม่แสดง</label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 row-content d-flex align-items-center justify-content-center">
        <button class="btn-sell-close mr-3">ยกเลิก</button>
        <button class="btn-sell-post">ประกาศ</button>
    </div>
</div>
@endsection

@section('script')
<script>
    var countimg = 0;
    $(document).ready(function () {

        // upload img
        var store_img = [];
        countimg = 0;

        var readURL = function (input) {
            countimg += input.files.length;
            if (input.files.length <= 5 && countimg <= 5) {

                $.each(input.files, function (key, img_input) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        store_img.push(e.target.result);
                        $('.avatar').append('<div class="areadrop position-relative">' +
                            '<img class="profile-pic"/>' +
                            '<img class="img-del" src="/img/icon/close-material.png" onclick="remove_img(this)"/>' +
                            '</div>');

                        $('.detel-photo').hide();

                        $.each(store_img, function (key, value) {
                            $('.profile-pic').last().attr('src', value);
                        });
                    }
                    reader.readAsDataURL(input.files[key]);
                });
            } else {
                countimg -= input.files.length;
                alert('Maximum 5 file');
            }
        }

        $(".img-upload-input").on('change', function () {
            readURL(this);
        });
    });

    function remove_img(_this) {
        $(_this).parents('.areadrop').remove();
        countimg -= 1;

        if (countimg == 0) {
            $('.detel-photo').show();
        }
    }

</script>
@endsection
