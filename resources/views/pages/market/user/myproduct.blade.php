@extends('layout.market')
@section('contentmarket')
<style>

    .text-12{
        font-size: 12px;
    }

    .text-headbord {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .text-menu {
        font-size: 12px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-price{
        font-size: 20px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-comment{
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .select-list {
        width: 300px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .btn-menu {
        width: 95px;
        height: 95px;
        border: 0;
        background-color: #4f72e5;
    }

    .menu-market {
        width: 967px;
        height: 216px;
        box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);
        background-color: #eaeaea;
    }

    .select-date {
        width: 125px;
        height: 27px;
        border-radius: 4px;
        font-size: 12px;
        color: #4f72e5;
        background-color: #ffffff;
    }

    .select-price {
        width: 125px;
        height: 27px;
        border-radius: 4px;
        font-size: 12px;
        color: #4f72e5;
        background-color: #ffffff;
    }

    .select-type{
        width: 184px;
        height: 27px;
        border-radius: 4px;
        font-size: 12px;
        color: #4f72e5;
        background-color: #ffffff;
    }

    .card-body{
        padding: 10px;
    }

    .card-text{
        font-size: 14px;
        color: #4a4a4a;
    }

    .position-hard{
        position: absolute;
        top: 110px;
        right: 10px;
        width: 37px;
        height: 37px;
    }

    .position-more{
        position: absolute;
        top: 15px;
        right: 15px;
        width: 15px;
        height: auto;
        cursor: pointer;
    }

    .card .dropdown-menu{
        position:absolute;
        top: 28px;
        left: 200px;
    }

</style>
<div class="px-0" style="min-height:650px;">
    <div class="col-12 px-0 mt-2">
        <div class="col-12 px-0 d-flex">
            <div class="col-4 px-0">
                <span style="color: #4f72e5;font-size:20px;font-family: Kanit-Regular;">กระดานซื้อ - ขาย > สินค้าของฉัน</span>
            </div>
            <div class="col-8 px-0 text-12 d-flex">
                <div class="col-4 pl-0 pr-3 d-flex justify-content-end align-items-center">
                    <select class="form-select py-1 px-2 select-type" aria-label="Default select example">
                        <option value="1">ทุกหมวดหมู่</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <div class="col-4 px-0 d-flex justify-content-end align-items-center">
                    <span class="mr-2">เรียงจาก ราคา</span>
                    <select class="form-select py-1 px-2 select-price" aria-label="Default select example">
                        <option value="1">น้อยสุด - มากสุด</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <div class="col-4 px-0 d-flex justify-content-end align-items-center">
                    <span class="mr-2">วันประกาศ</span>
                    <select class="form-select py-1 px-2 select-date" aria-label="Default select example">
                        <option value="1">ใหม่สุด - เก่าสุด</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-12 px-0 mt-3" style="min-height:500px;">
            @for ($a = 0; $a < 1; $a++) 
                <div class="col-12 mt-2 px-0 d-flex">
                    @for ($i = 0; $i < 2; $i++)
                        <div class="col-3 px-0 position-relative d-flex justify-content-center">
                            <div class="card" style="width: 232px;box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.08);">
                                <img src="/img/icon/rectangle-copy-32.png" class="card-img-top" alt="..." width="232px" height="155px">
                                <img src="/img/icon/more-horiz-material-copy-10.png" alt="" class="position-more">
                                <ul class="dropdown-menu" style="display: none">
                                    <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                                            style="background-color:#4f72e5;">
                                            <div class="d-flex">
                                                <div class="col-2 px-1">
                                                    <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                                </div>
                                                <div class="col-10 px-1">
                                                    <span
                                                        style="color:#ffffff; font-weight:bold; font-size:14px;">ปิดการขาย</span>
                                                </div>
        
                                            </div>
                                        </a></li>
                                    <li><a class="dropdown-item py-1 px-2" href="#">
                                            <div class="d-flex">
                                                <div class="col-2 px-1">
                                                    <img src="/img/icon/stock-view-bule-material.png" alt="" class="mr-1">
                                                </div>
                                                <div class="col-10 px-1">
                                                    <span
                                                        style="color:#4f72e5; font-weight:bold; font-size:14px;">ดูประกาศ</span>
                                                </div>
                                            </div>
        
                                        </a>
                                    </li>
                                    <li><a class="dropdown-item py-1 px-2" href="# ">
                                            <div class="d-flex">
                                                <div class="col-2 px-1">
                                                    <img src="/img/icon/stock-close-bule-material.png" alt="" class="mr-1">
                                                </div>
                                                <div class="col-10 px-1">
                                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">แก้ไขประกาศ</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
        
                                    <li><a class="dropdown-item py-1 px-2" href="#">
                                            <div class="d-flex">
                                                <div class="col-2 px-1">
                                                    <img src="/img/icon/bin-red.png" alt="" class="mr-1">​
                                                </div>
                                                <div class="col-2 px-1">
                                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ลบประกาศ</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <img src="/img/icon/group-43-copy.png" alt="" class="position-hard">
                                <div class="card-body">
                                <p class="card-text">ipad 2 wifi 16 GB เครื่องสีดำ </p>
                                <div class="col-12 px-0 d-flex">
                                    <div class="col-6 px-0">
                                        <span class="text-price">3,200</span>
                                    </div>
                                    <div class="col-6 px-0 text-end">
                                        <img src="/img/icon/comment-blue.png" alt="">
                                        <span class="ml-1 text-comment">1</span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            @endfor
    </div>

    <div class="col-12 mt-5">
        <div class="row next-bar">
            <div class="col-4 px-0 d-flex justify-content-start align-items-center">
                <span>แสดงผล 7 จากทั้งหมด 7</span>
            </div>
            <div class="col-4 d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-back mr-2">&larr; ย้อนกลับ</button>
                <button type="button" class="btn btn-next">หน้าถัดไป &rarr;</button>
            </div>
            <div class="col-4 pr-4 next-back">
                <div class="row d-flex justify-content-end align-items-center">
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                        style="height: 23px; width:34px;font-size:11px;margin-right: 10px;">
                    <div class="d-flex align-items-center px-0" style="width: 15%">
                        <span>จาก 15</span>
                    </div>
                    <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                        <button type="button" class="btn btn-next">&lt;</button>
                        <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection


@section('script')
<script>
    $(document).ready(function () {
        $('.position-more').on('click', function () {
            $(this).parents('.card').find('.dropdown-menu').toggle();
        });
    });

</script>
@endsection
