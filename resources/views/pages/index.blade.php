@extends('layout.home')
@section('style')
<style>
    .arrow {
        border: solid black;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        transition: .2s;
    }

    .down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        transition: .2s;
    }

    .tab_booking_active {
        width: 100% !important;
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .tab_booking_notactive {
        width: 100% !important;
        background-color: #ffffff !important;
        color: #4f72e5 !important;
    }

    .toggle_goods {
        cursor: pointer;
    }

</style>

@endsection
@section('content')

<div class="px-0" style="max-width: 294px;">

    {{-- btn popular --}}
    <div class="btn_popular p-0">
        <table>
            <tbody>
                <tr>
                    <td colspan="2" class="card_div_left">
                        <div class="card_div_left">
                            <img src="/img/icon/group-51.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>

                    <td colspan="2">
                        <div class="card_div_left">
                            <img src="/img/icon/group-46.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>
                </tr>


                <tr>
                    <td colspan="2">
                        <div class="card_div_left">
                            <img src="/img/icon/group-48.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>

                    <td colspan="2">
                        <div class="card_div_left">
                            <img src="/img/icon/group-47.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>
                </tr>


                <tr>
                    <td colspan="2">
                        <div class="card_div_left">
                            <img src="/img/icon/group-49.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>

                    <td colspan="2">
                        <div class="card_div_left">
                            <img src="/img/icon/group-50.png" class="card-img-top img_left_home" alt="...">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- btn popular --}}


    {{-- calendar_div --}}
    <div class="calendar_div p-0 shadow">
        <div class="row ml-2 mt-2 d-flex align-items-center text-start">
            <div class="align-items-center ">
                <img src="/img/icon/date-range-material.png" style="width:15px; height:15px; margin-bottom: 8px;"
                    alt="">
                <span class="schedule_calendar_span">ปฏิทินกิจกรรม</span>
            </div>

        </div>


        <div style="width: 100%;height: 270px;">
            <div class="calendar"></div>
        </div>


        <div class="row ml-2 mt-2 d-flex align-items-center text-start">
            <div class="col-7 p-0">
                <span class="schedule_calendar_span">กิจกรรมล่าสุด</span>
            </div>
            <div class="col-5 text-center p-0">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>

        <div class="swiper-container">
            <div class="swiper-wrapper mb-5">
                <div class="swiper-slide">
                    <div class="container p-3">
                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span class="three_dots" style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">27-29</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span class="three_dots" style="font-size: 14px">กิจกรรม CSR
                                    โครงการติดตั้งโซลาเซลล์ให้กับมูลนิธิดอยสะเก็ด</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0 detail_event_calendar_div">
                                <span class="three_dots" style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="swiper-slide">

                    <div class="container p-3">
                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="swiper-slide">

                    <div class="container p-3">
                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">27-29</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">กิจกรรม CSR
                                    โครงการติดตั้งโซลาเซลล์ให้กับมูลนิธิดอยสะเก็ด</span>
                            </div>
                        </div>
                        <div class="col-12 border-bottom px-2"></div>

                        <div class="row mb-3">
                            <div class="col-4 pr-0">
                                <div class="row pr-0 ">
                                    <div class="col-12 ">
                                        <span style="font-size: 24px">19</span>
                                    </div>
                                    <div class="col-12" style="line-height: 1px">
                                        <span style="font-size: 9px">JAN 21</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-8 pl-0">
                                <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    {{-- calendar_div --}}


    {{-- File Popular --}}
    <div class="files_popular shadow">
        <div class="row">
            <div class="col-12 p-0">
                <div class="col-12 d-flex align-content-center">
                    <div class="col-9 p-0">
                        <span style=" font-size: 24px;
                font-weight: bold; color: #4f72e5;">เอกสารยอดนิยม</span>
                    </div>
                    <div class="col-3 p-0">
                        <img src="/img/icon/group-14-copy-2.png" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 d-flex p-0 mt-2">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 25.1px; height:29.8;" alt="">
            </div>
            <div class="col-11 pr-0" style="line-height: 1.07;">
                <div class="row">
                    <div class="col-12">
                        <span style="color: #4a4a4a;font-size: 14px;font-weight: bold;">ประกาศวันหยุดตามประเพณีปี
                            2564
                            19</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;">321 Downloads</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 border-bottom mt-3"></div>


        <div class="col-12 d-flex p-0 mt-3">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 25.1px; height:29.8;" alt="">
            </div>
            <div class="col-11 pr-0" style="line-height: 1.07;">
                <div class="row">
                    <div class="col-12">
                        <span style="color: #4a4a4a;font-size: 14px;font-weight: bold;">ประกาศวันหยุดตามประเพณีปี
                            2564</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;">321 Downloads</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 border-bottom mt-3"></div>



        <div class="col-12 d-flex p-0 mt-3">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 25.1px; height:29.8;" alt="">
            </div>
            <div class="col-11 pr-0" style="line-height: 1.07;">
                <div class="row">
                    <div class="col-12">
                        <span style="color: #4a4a4a;font-size: 14px;font-weight: bold;">ประกาศวันหยุดตามประเพณีปี
                            2564</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;">321 Downloads</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 border-bottom mt-3"></div>


        <div class="col-12 d-flex p-0 mt-3">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 25.1px; height:29.8;" alt="">
            </div>
            <div class="col-11 pr-0" style="line-height: 1.07;">
                <div class="row">
                    <div class="col-12">
                        <span
                            style="color: #4a4a4a;font-size: 14px;font-weight: bold;">ประกาศการปฏิบัติงานเนื่องในสถานการณ์โรคระบาดโควิด
                            19</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;">321 Downloads</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 border-bottom mt-3"></div>



        <div class="col-12 d-flex p-0 mt-2">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 25.1px; height:29.8;" alt="">
            </div>
            <div class="col-11 pr-0" style="line-height: 1.07;">
                <div class="row">
                    <div class="col-12">
                        <span style="color: #4a4a4a;font-size: 14px;font-weight: bold;">แบบฟอร์มคำขอใบรับรอง</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;">321 Downloads</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- File Popular --}}

    {{-- Position --}}
    <div class="position shadow p-3 mt-3">
        <div class="row d-flex">
            <div class="col-8">
                <h3 style="font-size: 24px;
                        font-weight: bold;
                        color: #4f72e5;"><b>ทำเนียบผู้บริหาร</b></h3>
            </div>
            <div class="col-4">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>

        <div class="row">
            @for ($i = 1; $i <= 9; $i++)
            <div class="col-4 justify-content-center p-0">
                <div class="col-12 p-0 text-center">
                    <img class="img-thumbnail shadow"
                        style="width: 73px; height:72.8px; object-fit: contain; border-radius:50%;"
                        src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                </div>

                <div class="col-12 text-center mt-2">
                    <h4
                        style="font-family:Kanit-Regular; font-size: 12px; line-height: 0.5; font-weight:600; color: #4a4a4a;">
                        Wanchai</h4>
                    <p style="font-size: 10px; color: #4a4a4a;">Programmer Ibusiness</p>
                </div>
        </div>
        @endfor
    </div>


    <div class="row d-flex">
        <div class="col-8">
            <h3 style="font-size: 24px; font-weight: bold; color: #4f72e5;"><b>สมาชิกใหม่</b></h3>
        </div>
        <div class="col-4">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>



    <div class="row">
        @for ($i = 1; $i <= 6; $i++) <div class="col-4 justify-content-center p-0">
            <div class="col-12 p-0 text-center">
                <img class="img-thumbnail shadow"
                    style="width: 73px; height:72.8px; object-fit: contain; border-radius:50%;"
                    src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>

            <div class="col-12 text-center mt-2">
                <h4
                    style="font-family:Kanit-Regular; font-size: 12px; line-height: 0.5; font-weight:600; color: #4a4a4a;">
                    Wanchai</h4>
                <p style="font-size: 10px; color: #4a4a4a;">Programmer Ibusiness</p>
            </div>
    </div>
    @endfor
</div>


<div class="row">
    <div class="col-12">
        <h3 style="font-size: 24px; font-weight: bold; color: #4f72e5;"><b>สุขสันต์วันเกิด</b></h3>
    </div>
</div>

<div class="row d-flex px-2">

    @for($i = 1; $i <= 3; $i++) <div class="col-12 px-1 py-2 mb-1 d-flex"
        style="background-color: #ffffff; border-radius: 3px;">
        <div class="col-3 pr-0">
            <img class="img-thumbnail shadow" style="width: 40px; height: 40px; object-fit: contain; border-radius:50%;"
                src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
        </div>

        <div class="col-9 d-flex">
            <div class="row">

                <div class="col-12 p-0">
                    <span><b>Wanchai Nagtang</b></span>
                </div>

                <div class="col-12 p-0 d-flex">

                    <div class="col-1 p-0">
                        <img src="/img/icon/cake-material.png" style="padding-bottom: 5px;" alt="">
                    </div>
                    <div class="col-11 pl-1">
                        <span>18 NOVEMBER</span>
                    </div>

                </div>

            </div>

        </div>
</div>
@endfor
</div>
</div>
{{-- Position --}}

</div>






<div class="mx-3" style="max-width: 689px;">
    {{-- middle_index --}}
    <div class="middle_index p-0">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                        style="max-width:100%; max-height:100%;" alt="">
                </div>

                <div class="swiper-slide">
                    <img src="/img/profile/176198235_168911881677682_7343545927848232125_n.jpeg"
                        style="max-width:100%; max-height:100%;" alt="">
                </div>

                <div class="swiper-slide">
                    <img src="/img/profile/176223744_360115922087373_748574166255929935_n.jpeg"
                        style="max-width:100%; max-height:100%;" alt="">
                </div>

                <div class="swiper-slide">
                    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                        style="max-width:100%; max-height:100%;" alt="">
                </div>

                <div class="swiper-slide">
                    <img src="/img/profile/176449336_294627282067968_8570528169035308806_n.jpeg"
                        style="max-width:100%; max-height:100%;" alt="">
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    {{-- middle_index --}}

    {{-- Knowledge Center --}}
    <div class="knowledgeCenter shadow mt-4">
        <div class="row">
            <div class="col-4 pr-0">
                <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Knowledge Center</span>
            </div>

            <div class="col-3 d-flex align-content-center">
                <select class="form-select form-select-2x px-2 pt-2" aria-label="สำนักทั้งหมด">
                    <option class="mr-5" selected>สำนักทั้งหมด</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>


            </div>

            <div class="col-2 pr-0"></div>

            <div class="col-3 pl-0 text-center">
                <img class="pt-1" src="/img/icon/group-14-copy-2.png" alt="">
            </div>


        </div>

        <div class="row mt-3" style="margin: 5px 0px 8px 0px">
            <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">

                <div class="logo_blue_knowledge">
                    <img src="/img/icon/group-8-copy.png" alt="">
                </div>

                <div class="col-8 pl-0">
                    <div class="row">
                        <div>
                            <a href="#" class="link_text"
                                style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>

                        <div class="content_knowledge">
                            <span>ขั้นตอนการส่งออก : กล้องถ่ายรูปและชิ้นส่วน</span>
                        </div>
                    </div>
                </div>

                <div class="col-4 pt-2 pl-4">
                    <div class="row by_knowledge">
                        <div class="col-12 ">
                            <span>by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span>19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
                <div class="logo_blue_knowledge">
                    <img src="/img/icon/group-8-copy.png" alt="">
                </div>

                <div class="col-8 pl-0">
                    <div class="row">
                        <div>
                            <a href="#" class="link_text"
                                style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>

                        <div class="content_knowledge">
                            <span>ขั้นตอนการส่งออก : เครื่องเงินและเครื่องประดับเงิน</span>
                        </div>
                    </div>
                </div>

                <div class="col-4 pt-2 pl-4">
                    <div class="row by_knowledge">
                        <div class="col-12 ">
                            <span>by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span>19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
                <div class="logo_violet_knowledge">
                    <img src="/img/icon/group-18.png" alt="">
                </div>

                <div class="col-8 pl-0">
                    <div class="row">
                        <div>
                            <a href="#" class="link_text" style="text-decoration: underline;">กลุ่มตรวจสอบภายใน</a>
                        </div>

                        <div class="content_knowledge">
                            <span>ขั้นตอนการเตรียมเอกสารเพื่อยื่นภาษีเงินได้ส่วนบุคคล ประจำปี 2563 </span>
                        </div>
                    </div>
                </div>

                <div class="col-4 pt-2 pl-4">
                    <div class="row by_knowledge">
                        <div class="col-12 ">
                            <span>by Darisa Patcharachot</span>
                        </div>
                        <div class="col-12">
                            <span>17 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
                <div class="logo_pink_knowledge">
                    <img src="/img/icon/group-3.png" alt="">
                </div>

                <div class="col-8 pl-0">
                    <div class="row">
                        <div>
                            <a href="#" class="link_text"
                                style="text-decoration: underline;">สำนักส่งเสริมการค้าสินค้าไลฟ์สไตล์</a>
                        </div>

                        <div class="content_knowledge">
                            <span>รายละเอียดและระยะเวลาดำเนินการ โครงการ “STYLE Bangkok” งานแสดงสินค้าไลฟ์สไตล์ ประจำปี
                                2564</span>
                        </div>
                    </div>
                </div>

                <div class="col-4 pt-2 pl-4">
                    <div class="row by_knowledge">
                        <div class="col-12 ">
                            <span>by Chiraphon Thongthai</span>
                        </div>
                        <div class="col-12">
                            <span>16 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 pl-0 d-flex mb-2" style="background-color: #eaeaea">
                <div class="logo_blue_knowledge">
                    <img src="/img/icon/group-8-copy.png" alt="">
                </div>

                <div class="col-8 pl-0">
                    <div class="row">
                        <div>
                            <a href="#" class="link_text"
                                style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                        </div>

                        <div class="content_knowledge">
                            <span>ขั้นตอนการส่งออก : รถดับเพลิง เครื่องดับเพลิงและอุปกรณ์ดับเพลิง </span>
                        </div>
                    </div>
                </div>

                <div class="col-4 pt-2 pl-4">
                    <div class="row by_knowledge">
                        <div class="col-12 ">
                            <span>by กฤตวิทย์ เสนาลอย</span>
                        </div>
                        <div class="col-12">
                            <span>19 ม.ค. 2563 09:00</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 border border-bottom-2 mt-3"></div>
        </div>

        <div class="row mt-3">
            <div class="col-8">
                <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Blog</span>
            </div>

            <div class="col-2 pr-0"></div>

            <div class="col-2 pl-0 text-center">
                <img class="pt-1" src="/img/icon/group-14-copy-2.png" alt="">
            </div>

        </div>

        <div class="row" style="margin: 5px 0px 8px 0px">
            <div class="col-4 pr-0 pl-0">
                <img src="/img/icon/rectangle.png" alt="">
            </div>
            <div class="col-8 p-3" style="background-color: #eaeaea">
                <div class="row">
                    <div class="col-12">
                        <span class="" style="font-family: Kanit-Regular;
                        font-size: 18px;
                        font-weight: bold;
                        color: #4a4a4a;">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries
                            ในเนเธอร์แลนด์สูงกว่า
                            50%</span>
                    </div>
                    <div class="col-12 mt-2">
                        <a href="#" style="text-decoration: underline">สถานการณ์การค้าผลกระทบ COVID-19</a>
                    </div>
                    <div class="col-12 d-flex  mt-3">
                        <div class="col-6 pl-0">
                            <span>5 ม.ค. 2564 11:00</span>
                        </div>
                        <div class="col-6 px-0">
                            <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                            <span class="rating_blog">1</span>
                            <img src="/img/icon/comment-blue.png" alt="">
                            <span class="rating_blog">2</span>
                            <img src="/img/icon/visibility-material-bule.png" alt="">
                            <span class="rating_blog">25</span>
                        </div>
                    </div>

                    <div class="col-12 d-flex mt-2">
                        <div class="col-1 px-0 d-flex">
                            <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg"
                                alt="">
                        </div>


                        <div class="col-11">
                            <div class="row mt-2" style="line-height: 1.2">
                                <div class="col-12">
                                    <span style="font-size: 14px;">Wanchai Nagtang</span>
                                </div>
                                <div class="col-12">
                                    <span style="font-size: 12px;
                                    ">Programmer most famous</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row mt-3">
            <div class="col-12">
                <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Highlight</span>
            </div>

            <div class="col-12 d-flex">
                <div class="mr-2" style="width: fit-content; max-height:fit-content; background-color:#eaeaea">
                    <div class="row">
                        <div class="col-12">
                            <img src="/img/icon/rectangle-copy-32.png" class="w-100" alt="">
                        </div>
                        <div class="col-12">
                            <div class="row p-2">
                                <div class="col-12">
                                    <span class="text_highlight">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564
                                        (สคต.เซี่ยเหมิน)</span>
                                </div>
                                <div class="col-12">
                                    <a href="#" style="text-decoration: underline;font-size: 14px;">เกษตรและอาหาร</a>
                                </div>
                                <div class="col-12 d-flex">
                                    <div class="col-6 pl-0">
                                        <span>31 ธ.ค. 2563 09:00</span>
                                    </div>
                                    <div class="col-6 px-0">
                                        <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                                        <span class="rating_highlight">1</span>
                                        <img src="/img/icon/comment-blue.png" style="padding-bottom:5px" alt="">
                                        <span class="rating_highlight">2</span>
                                        <img src="/img/icon/visibility-material-bule.png" style="padding-bottom:5px"
                                            alt="">
                                        <span class="rating_highlight">25</span>
                                    </div>
                                </div>
                                <div class="col-12 d-flex pl-0">
                                    <div class="col-2">
                                        <img id="blog_profile"
                                            src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg"
                                            alt="">
                                    </div>
                                    <div class="col-10">
                                        <div class="row mt-2" style="line-height: 1.2">
                                            <div class="col-12">
                                                <span style="font-size: 14px;">Wanchai Nagtang</span>
                                            </div>
                                            <div class="col-12">
                                                <span style="font-size: 12px;
                                                ">Programmer most famous</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mr-2" style="width: fit-content; max-height:fit-content; background-color:#eaeaea">
                    <div class="row">
                        <div class="col-12">
                            <img src="/img/icon/rectangle-copy-33.png" class="w-100" alt="">
                        </div>
                        <div class="col-12">
                            <div class="row p-2">
                                <div class="col-12">
                                    <span
                                        class="text_highlight">ยอดจำหน่ายอาหารกึ่งสำเร็จรูปในตลาดจีนมีแนวโน้มเติบโตเพิ่มขึ้น</span>
                                </div>
                                <div class="col-12">
                                    <a href="#" style="text-decoration: underline;font-size: 14px;">เกษตรและอาหาร</a>
                                </div>
                                <div class="col-12 d-flex">
                                    <div class="col-6 pl-0">
                                        <span>27 ธ.ค. 2563 09:00</span>
                                    </div>
                                    <div class="col-6 px-0">
                                        <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                                        <span class="rating_highlight">1</span>
                                        <img src="/img/icon/comment-blue.png" style="padding-bottom:5px" alt="">
                                        <span class="rating_highlight">2</span>
                                        <img src="/img/icon/visibility-material-bule.png" style="padding-bottom:5px"
                                            alt="">
                                        <span class="rating_highlight">25</span>
                                    </div>
                                </div>
                                <div class="col-12 d-flex pl-0">
                                    <div class="col-2">
                                        <img id="blog_profile"
                                            src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg"
                                            alt="">
                                    </div>
                                    <div class="col-10">
                                        <div class="row mt-2" style="line-height: 1.2">
                                            <div class="col-12">
                                                <span style="font-size: 14px;">Wanchai Nagtang</span>
                                            </div>
                                            <div class="col-12">
                                                <span style="font-size: 12px;
                                                ">Programmer most famous</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    {{-- Knowledge Center --}}

    {{-- ETC --}}
    {{-- Not Really Finished --}}
    <div class="etc shadow">
        <div class="row m-4">
            <div class="col-12 p-0 mt-3" style="background-color: #ffffff; height:395.2px;">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation" style="width:353px">
                        <button class="nav-link active tab_booking_active" id="home-tab" style="width:100%"
                            data-bs-toggle="tab" data-bs-target="#room" type="button" role="tab" aria-controls="home"
                            aria-selected="true">จองห้องประชุม</button>
                    </li>
                    <li class="nav-item" role="presentation" style="width:353px">
                        <button class="nav-link tab_booking_notactive" id="profile-tab" style="width:100%"
                            data-bs-toggle="tab" data-bs-target="#car" type="button" role="tab" aria-controls="profile"
                            aria-selected="false">จองรถส่วนกลาง</button>


                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="room" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row p-3">
                            <div class="col-12 d-flex">
                                <div class="col-3 pr-0 d-flex align-items-center">
                                    <h3>Wednesday </h3>
                                    <h3>, 27</h3>
                                </div>
                                <div class="col-3 pl-0 d-flex align-items-center">
                                    <span class="mr-2 mb-0">April</span>
                                    <span class="mr-4 mb-0">2021</span>
                                    <img src="/img/icon/date-range-material.png"
                                        class="mb-1 mr-4 justify-content-center" alt="">

                                    <span class="filter_room_index" data-bs-placement="bottom" data-bs-toggle="popover"
                                        data-bs-title="Filters">
                                        <img src="/img/icon/tune-material-copy.png" class="mb-1 mr-2" alt="">
                                    </span>
                                </div>

                                <div class="col-6 d-flex justify-content-end">
                                    {{-- DaY --}}
                                    <select class="form-select form-select-1x mx-2 filter_day"
                                        aria-label="สำนักทั้งหมด">
                                        <option class="mr-5" selected>Day</option>
                                        <option value="day">Day</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                    </select>

                                    {{-- province --}}
                                    <select class="form-select form-select-1x mx-2 filter_province"
                                        aria-label="สำนักทั้งหมด">
                                        <option class="mr-5" selected>เมืองทอง</option>
                                        <option value="1">รัชดา</option>
                                        <option value="2">บางกระสอ</option>
                                        <option value="3">สีลม</option>
                                    </select>

                                    {{-- <button class="btn booking_rooms mt-2 mb-2">+ จองห้องประชุม</button> --}}
                                    <a href="/reserve/room" style="margin-top:11px;"><span class="booking_rooms">+
                                            จองห้องประชุม</span></a>



                                </div>
                            </div>
                        </div>
                        <div id="calendarRoom"></div>
                    </div>

                    <div class="tab-pane fade show active" id="car" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row p-3">
                            <div class="col-12 d-flex">
                                <div class="col-3 pr-0 d-flex align-items-center">
                                    <h3>Wednesday </h3>
                                    <h3>, 27</h3>
                                </div>
                                <div class="col-3 pl-0 d-flex align-items-center">
                                    <span class="mr-2 mb-0">April</span>
                                    <span class="mr-4 mb-0">2021</span>
                                    <img src="/img/icon/date-range-material.png"
                                        class="mb-1 mr-4 justify-content-center" alt="">
                                    <span class="filter_car_index" data-bs-placement="bottom" data-bs-toggle="popover"
                                        data-bs-title="Filters">
                                        <img src="/img/icon/tune-material-copy.png" class="mb-1 mr-2" alt="">
                                    </span>
                                </div>

                                <div class="col-6 d-flex justify-content-end">
                                    {{-- DaY --}}
                                    <select class="form-select form-select-1x mx-2 filter_day"
                                        aria-label="สำนักทั้งหมด">
                                        <option class="mr-5" selected>Day</option>
                                        <option value="day">Day</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                    </select>

                                    {{-- province --}}
                                    <select class="form-select form-select-1x mx-2 filter_province"
                                        aria-label="สำนักทั้งหมด">
                                        <option class="mr-5" selected>เมืองทอง</option>
                                        <option value="1">รัชดา</option>
                                        <option value="2">บางกระสอ</option>
                                        <option value="3">สีลม</option>
                                    </select>

                                    <a href="/reserve/car" style="margin-top:11px;"><span class="booking_cars">+
                                            จองรถส่วนกลาง</span></a>



                                </div>
                            </div>
                        </div>
                        <div id="calendarCar"></div>


                    </div>


                </div>
            </div>



            {{-- MaketPlace --}}
            <div class="col-12 d-flex justify-content-between p-0 mt-4">

                <div class="align-items-start p-3" style="height:572px; width:508px; background-color:#4f72e5;">
                    <div class="row">
                        <div class="col-9 pl-4">
                            <span style="font-size: 18px;
                                font-weight: bold;
                                color:#ffffff">กระดานซื้อ - ขาย</span>
                        </div>
                        <div class="col-3 text-center pr-4">
                            <img src="/img/icon/group-14-copy-7.png" alt="">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-7 pr-0">

                            <select class="form-select form-select-2x filter_goods" aria-label="ทุกหมวดหมู่">
                                <option selected>ทุกหมวดหมู่</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                        </div>

                        <div class="col-5 px-0 d-flex align-items-center ">
                            <img style="margin-right:20px; margin-left: 10px; width:20px; height:20px; "
                                src="/img/icon/favorite-material.png" alt="">
                            <img style="margin-right:20px; width:20px; height:20px; "
                                src="/img/icon/tune-material-copy-2.png" alt="">
                            <button class="btn btn-sm ml-1"
                                style="background-color:#ee2a27; color:#ffffff; width:100px"> +
                                ขาย</button>
                        </div>
                    </div>



                    <div class="row mt-3 mx-0 d-flex justify-content-between">
                        @for($i = 1; $i <= 9; $i++) <div class="px-0 mt-3"
                            style="border-radius: 2px; background-color: #ffffff; width: 144px; height: 135px;">
                            <div class="col-12 px-0">
                                <img style="width: 144px; height: 89.5px; object-fit:contain;"
                                    src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                                <img src="/img/icon/group-43.png" alt=""
                                    style="position:absolute; transform: translate(-28px, 65px);">
                            </div>
                            <div class="col-12 px-2" style="line-height: 1.3; margin-top: 3px;">
                                <div class="col-12 px-0">
                                    <span><b>คอมพิวเตอร์</b></span>
                                </div>

                                <div class="col-12 px-0 align-items-center">
                                    <img class="mr-2" src="/img/icon/like.png" alt="">
                                    <span class="mr-2"><b>1</b></span>
                                    <img class="mr-2" src="/img/icon/comment-blue.png" alt="">
                                    <span class="mr-2"><b>0</b></span>

                                </div>
                            </div>
                    </div>
                    @endfor
                </div>
            </div>
            {{-- MaketPlace --}}



            <div class="align-items-center p-3" style="height:572px; width:508px; background-color:#ffffff;">
                <div class="toggle_goods p-2 border mt-2">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-between">
                            <div class="col-8 px-0">
                                <span style="font-size: 16px; font-weight: bold; color: #4a4a4a;">คอมพิวเตอร์โน้ตบุค
                                    Macbook
                                    Pro</span>
                            </div>
                            <div class="col-4 px-0 text-end" style="cursor: pointer;">
                                <i class="arrow right"></i><span>View</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <span style="font-size: 14px; font-weight: 500; color: #f5a623;">ยืม</span>
                        </div>

                        <div class="goods" style="display: none; transition: .2s;">
                            <div class="col-12 text-center">
                                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                    style="  width: 249px;
                                        height: 229px;
                                        /* margin: 18.5px 56px 8px 92px; */
                                        object-fit: contain;">

                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: bold; color: #4a4a4a;">Wanchai</span>
                                </div>
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: 500; color: #4a4a4a;">หมายเลขประจำเครื่อง
                                        :
                                        2564/00001</span>
                                </div>
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: 500; color: #4a4a4a;">อายุการใช้งาน : 24
                                        ปี</span>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

                <div class="toggle_goods p-2 border mt-2">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-between">
                            <div class="col-8 px-0">
                                <span style="font-size: 16px; font-weight: bold; color: #4a4a4a;">คอมพิวเตอร์โน้ตบุค
                                    Macbook
                                    Pro</span>
                            </div>
                            <div class="col-4 px-0 text-end" style="cursor: pointer;">
                                <i class="arrow right"></i><span>View</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <span style="font-size: 14px; font-weight: 500; color: #f5a623;">ยืม</span>
                        </div>

                        <div class="goods" style="display: none; transition: .2s;">
                            <div class="col-12 text-center">
                                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                                    style="  width: 249px;
                                        height: 229px;
                                        /* margin: 18.5px 56px 8px 92px; */
                                        object-fit: contain;">

                            </div>
                            <div class="col-12">
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: bold; color: #4a4a4a;">Wanchai</span>
                                </div>
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: 500; color: #4a4a4a;">หมายเลขประจำเครื่อง
                                        :
                                        2564/00001</span>
                                </div>
                                <div class="col-12">
                                    <span style="font-size: 14px; font-weight: 500; color: #4a4a4a;">อายุการใช้งาน : 24
                                        ปี</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
{{-- ETC --}}



</div>










<div class="px-0" style="max-width: 373px;">
    {{-- end_index --}}
    <div class="end_index p-0">
        <div class="row d-flex mx-2">
            <div class="col-5 pr-0">
                <img src="/img/icon/tag-name.png" class="mb-2" alt="">
                <span class="ads">ประกาศ</span>
            </div>
            <div class="col-7 pl-5 text-end">
                <span class="ads text-end">
                    <>
                </span>
            </div>
        </div>
        <div class="row d-flex" style="margin: 8px 50px 0px 23px">
            <span class="p-0 m-0" id="date_ads"> 22 April 2021</span>
        </div>

        <div class="row d-flex" style="margin: 0px 50px 0px 23px">
            <span class="p-0 m-0" id="title_ads">ประกาศวันหยุดตามประเพณีสำหรับปี 2564
                สามารถตรวจสอบได้ทางอีเมลของท่าน</span>
        </div>

        <div class="row d-flex" style="margin:10px 50px 0px 15px">
            <span id="by">by สํานักบริหารกลาง</span>
        </div>
    </div>
    {{-- end_index --}}

    {{-- Board --}}
    <div class="Board shadow mt-4">
        <div class="row mb-3">
            <div class="col-8 d-flex pt-1 align-items-center">
                <span style="font-size: 24px;
                font-weight: bold;
                color: #ffffff;">Board</span>
                <div class="lineUp_Blog"></div>
                <span style="  font-size: 12px;
                font-weight: bold;color:#ffffff">การพูดคุยล่าสุด</span>
            </div>

            <div class="col-4 d-flex align-items-center" style="padding-top: 4px;">
                <img src="/img/icon/read-all.png" alt="">
            </div>
        </div>

        {{-- Card Board --}}
        @for($i=1; $i<=7 ; $i++) <div class="row mx-auto my-2"
            style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px; width:343px;height:110px;">
            <div class="col-12 d-flex px-0">
                <div class="col-1">
                    <img src="/img/icon/comment-material-copy-5.png" alt="">
                </div>
                <div class="col-10">
                    <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
                </div>
                <div class="col-1 text-start pl-0" style="top:5px;">
                    <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
                </div>
            </div>



            {{-- Card Board --}}
            <div class="col-12 d-flex">
                <div class="col-2 px-0">
                    <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                </div>
                <div class="col-6 px-0 mr-3">
                    <div class="row mt-2" style="line-height: 1">
                        <div class="col-12">
                            <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                        </div>
                        <div class="col-12">
                            <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                        </div>
                    </div>
                </div>
                <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                    <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                    <span class="rating_Board">10</span>
                    <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                    <span class="rating_Board">10</span>
                </div>
            </div>

    </div>
    @endfor
</div>
{{-- Board --}}

{{-- Survay Form --}}
<div class="survay_form shadow mt-4">
    <div class="row">

        <div class="col-12 pr-0">
            <div class="col-12 d-flex align-content-center">
                <div class="col-9 pl-0">
                    <span style=" font-size: 24px;
                            font-weight: bold; color: #4f72e5;">แบบสอบถาม</span>
                </div>
                <div class="col-3 pr-0">
                    <img src="/img/icon/group-14-copy-2.png" alt="">
                </div>
            </div>
        </div>

    </div>


    <div class="col-12 d-flex mt-3" style="line-height: 1.07;">
        <div class="col-1 p-0">
            <img src="/img/icon/forum-material.png" alt="">
        </div>

        <div class="col-11 p-0">
            <span style="color: #4a4a4a;  font-size: 14px;">แบบประเมินการทำงานที่บ้านในสถานการณ์โควิด 19</span>
        </div>
    </div>
    <div class="col-12 border-bottom mt-3"></div>


    <div class="col-12 d-flex mt-3" style="line-height: 1.07;">
        <div class="col-1 p-0">
            <img src="/img/icon/forum-material.png" alt="">
        </div>

        <div class="col-11 p-0">
            <span style="color: #4a4a4a;  font-size: 14px;">แบบประเมินการทำงานที่บ้านในสถานการณ์โควิด 19</span>
        </div>
    </div>
    <div class="col-12 border-bottom mt-3"></div>


    <div class="col-12 d-flex mt-3" style="line-height: 1.07;">
        <div class="col-1 p-0">
            <img src="/img/icon/forum-material.png" alt="">
        </div>

        <div class="col-11 p-0">
            <span
                style="color: #4a4a4a;  font-size: 14px;">แบบสอบถามความเสี่ยงในการเดินทางไปจุดเสี่ยงในช่วงเทศกาลปีใหม่</span>
        </div>
    </div>
</div>
{{-- Survay Form --}}


</div>







{{-------------------------------------- Room ---------------------------------------}}
<div id="room_popover_content" style="display: none;">
    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ช่วงเวลา</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="all_room" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="small_room" id="" class="mr-2">
            <span class="text_filter">ช่วงเช้า</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="big_room" id="" class="mr-2">
            <span class="text_filter">ช่วงบ่าย</span>
        </div>
    </div>

    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ประเภทห้องประชุม</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="all_room" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="small_room" id="" class="mr-2">
            <span class="text_filter">ห้องประชุมเล็ก</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="big_room" id="" class="mr-2">
            <span class="text_filter">ห้องประชุมใหญ่</span>
        </div>
    </div>

    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="status_all" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_empty" id="" class="mr-2">
            <span class="text_filter">ว่าง</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">ไม่ว่าง</span>
        </div>
    </div>

    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">อุปกรณ์</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="status_all" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_empty" id="" class="mr-2">
            <span class="text_filter">โปรเจ็คเตอร์</span>
        </div>

        <div class="col-12 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">ทีวี</span>
        </div>
        <div class="col-12 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">กระดานไวท์บอร์ด</span>
        </div>
    </div>


</div>
{{-------------------------------------- Room ---------------------------------------}}





{{-------------------------------------- Popover Car --------------------------------}}
<div id="car_popover_content" style="display: none;">
    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ประเภทรถ</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="all_room" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="small_room" id="" class="mr-2">
            <span class="text_filter">รถ 7 ที่นั่ง</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="big_room" id="" class="mr-2">
            <span class="text_filter">รถ 4 ที่นั่ง</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="big_room" id="" class="mr-2">
            <span class="text_filter">รถ 16 ที่นั่ง</span>
        </div>
    </div>

    <div class="row px-3 py-2">
        <div class="col-12 p-0">
            <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
        </div>
    </div>

    <div class="row px-3 py-2 mb-2 d-flex">
        <div class="col-6 d-flex align-items-center p-0">
            <input type="checkbox" name="status_all" id="" class="mr-2">
            <span class="text_filter">ทั้งหมด</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_empty" id="" class="mr-2">
            <span class="text_filter">ส่งคืนแล้ว</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">อนุมัติ</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">ไม่อนุมัติ</span>
        </div>

        <div class="col-6 d-flex align-items-center p-0 ">
            <input type="checkbox" name="status_not" id="" class="mr-2">
            <span class="text_filter">รออนุมัติ</span>
        </div>
    </div>


</div>
{{-------------------------------------- Popover Car --------------------------------}}


@endsection






@section('news')

<div class="container-fluid news_card">
    <div class="row d-flex">
        <div class="col-10 pl-3">
            <h3 style="color: #ffffff;"><b>ข่าวสาร</b></h3>
        </div>
        <div class="col-2 text-end p-0">
            <img src="/img/icon/group-14-copy-7.png" alt="">
        </div>
    </div>


    <div class="swiper-container mt-2" id="news_swiper">
        <div class="swiper-wrapper">

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide ">
                <div class="row">
                    <div class="col-12 card_news">
                        <div class="col-12 text-center p-0">
                            <img class="img_news"
                                src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="">
                        </div>
                        <div class="col-12" style="background-color: #ffffff">
                            <div class="row p-3">
                                <div class="col-12 content_news text-start">
                                    แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                                    ในช่วงระยะความเสี่ยงระดับต่างๆ
                                </div>
                                <div class="col-12 text-start">
                                    <a href="#" style="text-decoration: underline;
                    font-size: 14px;
                    line-height: 2.29;
                    color: #4a4a4a;">IT NEWS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>


        <div class="swiper-pagination"></div>
    </div>



</div>

@endsection



@section('media')
<div class="px-0 d-flex">
    {{-- gallery --}}
    <div class="gallery px-4 py-3 mt-4 mr-3">
        <div class="row">
            <div class="col-10">
                <h3 style="color:#4f72e5"><b>คลังภาพ</b></h3>
            </div>
            <div class="col-2">
                <img src="/img/icon/group-52.png" alt="">
            </div>
        </div>


        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">

                    <div class="row">
                        <div class="col-7 pr-0 gradient_img">
                            <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt="" style="width: 418px;
                            height: 455px;
                            object-fit: cover;">
                        </div>

                        <div class="col-5 px-0 text-end">
                            <div class="col-12" style="padding-right: 5px;">
                                <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
                                    style="width:218px; height:218px; object-fit: cover;">
                            </div>
                            <div class="col-12" style="padding-right: 5px;margin-top: 19px;">
                                <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
                                    style="width:218px; height:218px; object-fit: cover;">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    {{-- gallery --}}

    {{-- vdo --}}
    <div class="vdo px-4 py-3 mt-4">
        <div class="row">
            <div class="col-10">
                <h3 style="color:#4f72e5"><b>VDO</b></h3>
            </div>
            <div class="col-2">
                <img src="/img/icon/group-52.png" alt="">
            </div>
        </div>

        <div class="row ">
            <div class="col-12 d-flex">
                <div class="col-5 px-0">
                    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
                        style="width:100%; height:200px; object-fit: contain;">

                </div>
                <div class="col-7 px-0">
                    <div class="col-12">
                        <h4 style="font-size: 20px;
                        font-weight: bold;line-height: 1.2;"><b>รายงานสถานการณ์ / โอกาสทางการค้า สาธารณรัฐประชาชนจีน
                                (สคต.เซี่ยงไฮ้ กวางโจว
                                คุนหมิง เฉิงตู หนานหนิง เซี่ยเหมิน ชิงต่าว มะนิลา (ส่วนที่ 2) และฮ่องกง)</b></h4>
                    </div>
                    <div class="col-12">
                        <h6 style="font-size: 14px; color: #4a4a4a;line-height: 2.29;">8 มกราคม 2564</h6>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 dash_vdo my-3"></div>


        <div class="row ">
            <div class="col-12 d-flex">
                <div class="col-5 px-0">
                    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
                        style="width:100%; height:200px; object-fit: contain;">

                </div>
                <div class="col-7 px-0">
                    <div class="col-12">
                        <h4 style="font-size: 20px;
                        font-weight: bold;line-height: 1.2;"><b>รายงานสถานการณ์ / โอกาสทางการค้า สาธารณรัฐประชาชนจีน
                                (สคต.เซี่ยงไฮ้ กวางโจว
                                คุนหมิง เฉิงตู หนานหนิง เซี่ยเหมิน ชิงต่าว มะนิลา (ส่วนที่ 2) และฮ่องกง)</b></h4>
                    </div>
                    <div class="col-12">
                        <h6 style="font-size: 14px; color: #4a4a4a;line-height: 2.29;">8 มกราคม 2564</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- vdo --}}
</div>
@endsection




@section('script')

<script>
    $(document).ready(function () {

        var swiper = new Swiper('.middle_index .swiper-container', {
            direction: 'horizontal',
            loop: true,
            pagination: {
                el: '.middle_index .swiper-pagination',
            },
        });

        var swiper_container_event = new Swiper('.calendar_div .swiper-container', {
            direction: 'horizontal',
            loop: true,
            pagination: {
                el: '.calendar_div .swiper-pagination',
            },
        });


        var news_card_event = new Swiper('.news_card .swiper-container', {
            slidesPerView: 3,
            spaceBetween: 30,
            loop: true,
            pagination: {
                el: '.news_card .swiper-pagination',
                clickable: true,
            },
        });

        var gallery = new Swiper('.gallery .swiper-container', {
            // slidesPerView: 3,
            // slidesPerColumn	2
            spaceBetween: 30,
            loop: true,
            pagination: {
                el: '.gallery .swiper-pagination',
                clickable: true,
            },
        });


        $(".toggle_goods").on('click', function () {
            $(this).find('i').toggleClass('down');
            $(this).find('.goods').toggle();
        });



        $(function () {
            $('.calendar_div .calendar').pignoseCalendar({
                multiple: true
            });
        });




        $('.filter_room_index').popover({
            html: true,
            container: '.filter_room_index',
            sanitize: false,
            trigger: 'manual',
            content: function () {
                return $('#room_popover_content').html();
            }
        }).on('click', function (e) {
            $(this).popover('show');
        });
        $('.filter_room_index').on('click', function (e) {
            e.stopPropagation();
        });


        $('.filter_car_index').popover({
            html: true,
            container: '.filter_car_index',
            sanitize: false,
            trigger: 'manual',
            content: function () {
                return $('#car_popover_content').html();
            }
        }).on('click', function (e) {
            $(this).popover('show');
        });
        $('.filter_car_index').on('click', function (e) {
            e.stopPropagation();
        });


        $('html').click(function (e) {

            if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
                $('.filter_calendar_room').popover('hide');
                $('.filter_room_index').popover('hide');
                $('.filter_car_index').popover('hide');
            }
        });




    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var calendarCar = document.getElementById('calendarCar');
        var calendarRoom = document.getElementById('calendarRoom');

        var calendarCar = new FullCalendar.Calendar(calendarCar, {
            initialView: 'dayGridMonth'
        });
        var calendarRoom = new FullCalendar.Calendar(calendarRoom, {
            initialView: 'dayGridMonth'
        });
        calendarCar.render();
        calendarRoom.render();
        $('#car').css('display', 'none');

        $('#home-tab').on('click', function () {
            $(this).removeClass('tab_booking_notactive');
            $(this).addClass('active tab_booking_active');
            $('#profile-tab').removeClass('active  tab_booking_active');
            $('#profile-tab').addClass('tab_booking_notactive');


        });

        $('#profile-tab').on('click', function () {
            $('#car').css('display', 'block');

            $(this).removeClass('tab_booking_notactive');
            $(this).addClass('active tab_booking_active');
            $('#home-tab').removeClass('active  tab_booking_active');
            $('#home-tab').addClass('tab_booking_notactive');

        });


    });
    //     tab_booking_active
    // tab_booking_notactive

</script>
@endsection
