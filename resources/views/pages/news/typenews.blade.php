@extends('/layout/type_news')
@section('style')
<style>
    .title_type_news {
        font-family: Kanit-Regular;
        font-size: 39px;
        color: #4f72e5;
    }

    .title_head_type_news {
        font-family: Kanit-Regular;
        font-size: 24px;
        color: #4a4a4a;
    }

    .type_news_date {
        font-size: 14px;
        color: #4a4a4a;
    }

    .type_news_content {
        font-size: 14px;
        color: #4a4a4a;
    }

    .list_type_news {
        overflow: hidden;
    }

    .count_type_news {
        font-family: Kanit-Regular;
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .next_page_type_news,
    .next_page_type_news:hover {
        color: #ffffff;
        width: 183px !important;
        height: 35px;
        border-radius: 3px;
        background-color: #4f72e5;
        text-decoration: none;
    }

    .text_pages_type_news {
        font-family: Kanit-Regular;
        font-size: 12px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .page_prev {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;
        margin-right: 1px;
    }

    .page_next {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;

    }

    input[name="page_type_news"] {
        width: 34px;
        height: 23px;
        border-radius: 4px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .arrow_type_news {
        border: solid #ffffff;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
        height: 1px !important;
        width: 1px !important;
    }

    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }
    a:hover{
        text-decoration: none;
    }
    .count_viewer {
        font-size: 12px;
        font-weight: 500;
        color: #4f72e5;
    }

</style>
@endsection

@section('content')
<div class="col-12" style="min-height:1780px; max-height: 1780px;overflow:hidden">
    <div class="col-12 px-0">
        <span class="title_type_news">IT NEWS</span>
    </div>

    @for($i=1; $i<=6; $i++)
    <a href="/news/it_news/{{ $i }}" class="tab_link_content">
        <div class="col-12 d-flex align-items-center mt-3 p-3 list_type_news"
            style="background-color: #ffffff; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);">
            <div class="col-5 p-0">
                <img src="/img/icon/news5@3x.png"
                    style="width:373px; height:231px;object-fit:cover; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);"
                    alt="">
            </div>
            <div class="col-7 d-flex align-content-between flex-wrap justify-content-between p-0"
                style="height: 231px !important;">
                <div class="col-12 d-flex p-0">
                    <span class="title_head_type_news">Google Maps ออกฟีเจอร์สุดเจ๋ง! เตือนเมื่อใกล้ ‘ทางรถไฟ’</span>
                </div>

                <div class="col-12 d-flex align-items-center p-0">
                    <div class="col-3 d-flex align-items-center p-0">
                        <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                            style="width: 18; height: 14px; object-fit:contain;">
                        <span class="type_news_date">16 มีนาคม 2564</span>
                    </div>

                    <div class="col-7">
                        <span class="type_news_date">โดย บริบูรณ์ ตั้งชาญกิจ</span>
                    </div>

                    <div class="col-2 d-flex align-items-center">
                        <img src="/img/icon/share-material.png" alt="" class="mr-2">
                        <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                        <span class="count_viewer">145</span>
                    </div>
                </div>

                <div class="col-12 d-flex p-0">
                    <span class="type_news_content">เป็นที่ฮือฮาหลังผู้ใช้งาน Reddit ท่านหนึ่ง
                        ออกมาโพสต์ถึงฟีเจอร์ใหม่ของ Google Maps ว่า Google Maps โชว์ไอคอนวงกลมสีเหลือง
                        และมีรางรถไฟอยู่ด้านใน ซึ่งจะแสดงเฉพาะตำแหน่งที่มีทางรถไฟ
                        พร้อมทั้งยังมีการแจ้งเตือนจากด้านล่างของหน้าจอกว่า อาจทำให้เกิดความล่าช้า
                        แต่อีกนัยมองว่ากา...</span>
                </div>


            </div>
        </div>
        @endfor
    </a>
</div>


{{-- Footer Type News --}}
<div class="col-12 p-0 d-flex align-items-center p-0">
    <div class="col-12 d-flex justify-content-between align-items-center">
        <div class="col-4 text-start p-0">
            <span class="count_type_news">แสดงผล <b>10</b> จากทั้งหมด <b>150</b></span>
        </div>
        <div class="col-4 d-flex align-items-center justify-content-center">
            <a href="#" class="d-flex justify-content-center align-items-center next_page_type_news">
                <img src="/img/icon/group-14.png" alt="">
            </a>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center pr-0">
            <span class="text_pages_type_news mr-2">หน้่า</span>
            <input type="text" name="page_type_news" class="text_pages_type_news mr-2 text-center" value="1">
            <span class="text_pages_type_news mr-2">จาก 15</span>

            <button class="d-flex align-items-center page_prev pl-2" type="button">
                <span class="arrow_type_news left"></span>
            </button>

            <button class="d-flex align-items-center page_next" type="button">
                <span class="arrow_type_news right"></span>
            </button>


        </div>
    </div>
</div>


@endsection

@section('script')
<script>

</script>
@endsection
