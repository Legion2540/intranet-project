@extends('/layout/news')
@section('style')
<style>
    .title_primary {
        font-family: Kanit-Regular;
        font-size: 24px;
        font-weight: bold;
        line-height: 1.33;
        color: #4a4a4a;
    }

    .tag_news {
        font-size: 14px;
        line-height: 2.29;
        color: #4a4a4a;
        text-decoration: underline;
    }

    .tag_news_sub {
        font-size: 12px;
        line-height: 2.29;
        color: #4a4a4a;
        text-decoration: underline;
    }

    .tiltle_sub_news {
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .title_type_news {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }

    .tiltle_hot_news {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4a4a4a;
    }

    .tiltle_hot_date {
        font-size: 14px;
        color: #4a4a4a;
    }
    

</style>
@endsection





@section('content')
<div class="col-12 px-0" style="box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
    <div class="col-12 px-0">
        <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg" alt=""
            style="width: 100%;height:482px; object-fit:cover;">
    </div>

    <div class="col-12 py-2" style="background-color:#ffffff; ">
        <div class="col-12">
            <span class="title_primary">แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
                ในช่วงระยะความเสี่ยงระดับต่างๆ</span>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-between px-0 mt-2">
            <div class="col-6">
                <span class="tag_news">IT NEWS</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-end px-0">
                <img src="/img/icon/share-material.png" alt="" class="mr-2">
                <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                <span class="count_viewer">145</span>

            </div>
        </div>
    </div>
</div>


<div class="col-12 px-0 d-flex align-items-center justify-content-between mt-3">

    @for($i=0; $i<3; $i++)
    <div class="px-0" style="width: 325px; background-color:#ffffff;box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
        <div class="col-12 px-0">
            <img src="/img/icon/news5.png" alt="" style="width: 100%; height: 173px; object-fit:cover;">
        </div>

        <div class="px-2 pb-3 pt-1">
            <div class="col-12 px-0">
                <span class="tiltle_sub_news">Google Maps ออกฟีเจอร์สุดเจ๋ง! เตือนใกล้ "ทางรถไฟ"</span>
            </div>
            <div class="col-12 d-flex justify-content-between px-0 mt-2">
                <div class="col-6 px-0">
                    <span class="tag_news_sub">IT NEWS</span>
                </div>

                <div class="col-6 d-flex align-items-center justify-content-end">
                    <img src="/img/icon/share-material.png" alt="" class="mr-2">
                    <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                    <span class="count_viewer">145</span>

                </div>
            </div>
        </div>
    </div>
    @endfor

</div>
@endsection

@section('contentTitleNews')

<div class="col-12 p-3 my-3" style="background-color: #ededed">
    <div class="col-12 d-flex align-items-center justify-content-between content_type_news mb-2 px-0">
        <div class="col-6 px-0">
            <span class="title_type_news">HOT NEWS</span>
        </div>
        <div class="col-6 text-end">
            <a href="/news/hot_news"><img src="/img/icon/group-52.png" alt=""></a>
        </div>
    </div>




    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 px-0"
            style="width: 661px;height: auto; background-color:#ffffff;box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
            <div class="col-12 px-0">
                <img src="/img/icon/news8.png" alt="" style="width:100%; height: 366px;">
            </div>

            <div class="pt-3 pb-4 px-2">
                <div class="col-12">
                    <span class="tiltle_hot_news">เตรียมแผนการตลาดบุกตลาดไต้หวัน ฮ่องกง จีน หลัง COVID-19</span>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-between mt-3">
                    <div class="col-6 px-0">
                        <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                            style="width: 18; height: 14px; object-fit:contain;">
                        <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                    </div>

                    <div class="col-6 d-flex align-items-center justify-content-end">
                        <img src="/img/icon/share-material.png" alt="" class="mr-2">
                        <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                        <span class="count_viewer">145</span>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-6 d-flex align-content-between flex-wrap pr-0" style="height: 474px !important;">

            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>





</div>

<div class="col-12 p-3 my-3" style="background-color: #ededed">
    <div class="col-12 d-flex align-items-center justify-content-between content_type_news mb-2 px-0">
        <div class="col-6 px-0">
            <span class="title_type_news">FACT SHEET</span>
        </div>
        <div class="col-6 text-end">
            <a href="/news/factsheet"><img src="/img/icon/group-52.png" alt=""></a>
        </div>
    </div>




    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 px-0"
            style="width: 661px;height: auto; background-color:#ffffff;box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
            <div class="col-12 px-0">
                <img src="/img/icon/news8.png" alt="" style="width:100%; height: 366px;">
            </div>

            <div class="pt-3 pb-4 px-2">
                <div class="col-12">
                    <span class="tiltle_hot_news">เตรียมแผนการตลาดบุกตลาดไต้หวัน ฮ่องกง จีน หลัง COVID-19</span>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-between mt-3">
                    <div class="col-6 px-0">
                        <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                            style="width: 18; height: 14px; object-fit:contain;">
                        <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                    </div>

                    <div class="col-6 d-flex align-items-center justify-content-end">
                        <img src="/img/icon/share-material.png" alt="" class="mr-2">
                        <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                        <span class="count_viewer">145</span>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-6 d-flex align-content-between flex-wrap pr-0" style="height: 474px !important;">

            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>





</div>

<div class="col-12 p-3 my-3" style="background-color: #ededed">
    <div class="col-12 d-flex align-items-center justify-content-between content_type_news mb-2 px-0">
        <div class="col-6 px-0">
            <span class="title_type_news">IT NEWS</span>
        </div>
        <div class="col-6 text-end">
            <a href="/news/it_news"><img src="/img/icon/group-52.png" alt=""></a>
        </div>
    </div>




    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-6 px-0"
            style="width: 661px;height: auto; background-color:#ffffff;box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
            <div class="col-12 px-0">
                <img src="/img/icon/news8.png" alt="" style="width:100%; height: 366px;">
            </div>

            <div class="pt-3 pb-4 px-2">
                <div class="col-12">
                    <span class="tiltle_hot_news">เตรียมแผนการตลาดบุกตลาดไต้หวัน ฮ่องกง จีน หลัง COVID-19</span>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-between mt-3">
                    <div class="col-6 px-0">
                        <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                            style="width: 18; height: 14px; object-fit:contain;">
                        <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                    </div>

                    <div class="col-6 d-flex align-items-center justify-content-end">
                        <img src="/img/icon/share-material.png" alt="" class="mr-2">
                        <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                        <span class="count_viewer">145</span>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-6 d-flex align-content-between flex-wrap pr-0" style="height: 474px !important;">

            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-2"
                style="background-color: #ffffff;height:145px;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.14);">
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/news9.png" alt="" style="width: 124px; height: 124px; object-fit:contain;">
                </div>

                <div class="col-9 d-flex align-content-between flex-wrap">
                    <div class="col-12 px-0">
                        <span class="tiltle_hot_news">พาณิชย์เผยชาวจีนปลื้มพลอยสี เครื่องประดับเงินไทย</span>
                    </div>
                    <div class="col-12 mb-2 d-flex align-items-center px-0">
                        <div class="col-6 px-0">
                            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                                style="width: 18; height: 14px; object-fit:contain;">
                            <span class="tiltle_hot_date">16 มีนาคม 2564</span>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end px-0">
                            <img src="/img/icon/share-material.png" alt="" class="mr-2">
                            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                            <span class="count_viewer">145</span>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>





</div>








@endsection


@section('script')
<script>

</script>
@endsection
