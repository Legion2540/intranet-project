@extends('/layout/type_news')
@section('style')
<style>
    .head_content_news {
        font-family: Kanit-Regular;
        font-size: 32px;
        color: #4a4a4a;
    }

    .tag_news,
    .tag_news:hover {
        font-size: 21px;
        color: #4a4a4a;
        text-decoration: underline #4a4a4a;
    }

    .type_news_date {
        font-size: 14px;
        color: #4a4a4a;
    }

    .count_viewer {
        font-size: 12px;
        font-weight: 500;
        color: #4f72e5;
    }

    .content_news {
        font-size: 18px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.89;
        color: #4a4a4a;
    }

    .title_album {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .name_album {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4a4a4a;
    }

    .count_imgAlbum {
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .unit_album {
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .name_files {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .file_size {
        font-size: 10px;
        color: #4a4a4a;
    }




    .album_news .swiper-container {
        width: 100%;
        height: 100%;
    }

    .album_news .swiper-slide {
        display: flex;
        justify-content: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .album_news .swiper-slide img {
        display: block;
        /* width: 218px;
        height: 180px; */
        width: 180px;
        height: 150px;
        object-fit: cover;
    }

    .album_news .swiper-button-next,
    .album_news .swiper-button-next::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size:14px !important;
        right: 1px;
        top: 39%;
    }

    .album_news .swiper-button-prev,
    .album_news .swiper-button-prev::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size:14px !important;
        right: 1px;
        top: 39%;
    }







    .vdo_news .swiper-container {
        width: 100%;
        height: 100%;
    }

    .vdo_news .swiper-slide {
        display: flex;
        justify-content: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .vdo_news .swiper-slide img {
        display: block;
        /* width: 218px;
        height: 180px; */
        width: 180px;
        height: 150px;
        object-fit: cover;
    }
    .vdo_news .swiper-button-next,
    .vdo_news .swiper-button-next::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size:14px !important;
        right: 1px;
        top: 39%;
    }

    .vdo_news .swiper-button-prev,
    .vdo_news .swiper-button-prev::after {
        width: 15px !important;
        color: #4a4a4a !important;
        font-size:14px !important;
        right: 1px;
        top: 39%;
    }







    .head_tag {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .name_tag {
        font-size: 16px;
        color: #4f72e5;
    }

    .from_text {
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .written_news {
        font-family: Kanit-Regular;
        font-size: 20px;
        color: #4a4a4a;
    }

    .tag_guru {
        width: 26px;
        height: 11px;
        border-radius: 3px;
        background-color: #0066b2;

        font-size: 7px;
        color: #ffffff;
    }

    .position_written {
        font-size: 16px;
        color: #4a4a4a;
    }

    .head_comments {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .count_comments {
        font-family: Kanit-Regular;
        font-size: 20px;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .comment_news {
        border-radius: 8px;
        border: solid 0.5px #8a8c8e;
        padding: 4px 3px;
        resize: none;
    }

</style>
@endsection


@section('content')
<div class="col-12">
    <div class="col-12">
        <img src="/img/icon/news5@3x.png" alt="" style="width: 947px; height:442px; object-fit:cover;">
    </div>

    <div class="col-12 mt-2">
        <span class="head_content_news">แนวทางปฏิบัติงานที่บ้านในสถานการณ์การแพร่ระบาดของโรคระบาดโควิด 19
            ในช่วงระยะความเสี่ยงระดับต่างๆ</span>
    </div>

    <div class="col-12">
        <a href="#" class="tag_news">IT NEWS</a>
    </div>

    <div class="col-12 d-flex d-flex align-items-center mt-4">
        <div class="col-6 d-flex align-items-center p-0">
            <img src="/img/icon/schedule-material.png" alt="" class="mr-2"
                style="width: 18; height: 14px; object-fit:contain;">
            <span class="type_news_date">16 มีนาคม 2564</span>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end">
            <img src="/img/icon/share-material.png" alt="" class="mr-2">
            <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
            <span class="count_viewer">145</span>
        </div>
    </div>


    <div class="col-12 mt-5">
        <p class="content_news">จากการระบาดของโรคติดเชื้อโคโรนาไวรัส 2019 (COVID-19) ไปทั่วโลก นายกรัฐมนตรี
            ด้วยความเห็นชอบของคณะรัฐมนตรี ในการประชุมเมื่อวันที่ 24 มีนาคม 2563 และตามคำแนะนำของผู้บริหารและ
            นักวิชาการด้านการแพทย์และ สาธารณสุข ได้ประกาศสถานการณ์ฉุกเฉินตามพระราชกำหนดการบริหารราชการในสถานการณ์ฉุกเฉิน
            พ.ศ. 2548 จากการแพร่ระบาดของโรคติดเชื้อไวรัสโคโรนา 2019 (COVID-19) และได้มีข้อกำหนดออกตามความในมาตรา 9
            แห่งพระราชกำหนดการบริหารราชการในสถานการณ์ฉุกเฉิน พ.ศ. 2548 จำนวน 2 ฉบับ โดยฉบับที่ 1
            มีผลบังคับในทุกเขตท้องที่ทั่วราชอาณาจักร ตั้งแต่วันที่ 26 มีนาคม 2563 และฉบับที่ 2 มีผลบังคับตั้งแต่วันที่ 3
            เมษายน 2563 เป็นต้นไป นั้นกระทรวงสาธารณสุข
            ได้จัดทำแนวทางปฏิบัติด้านสาธารณสุขเพื่อการจัดการภาวะระบาดของโรคติดเชื้อโคโรนาไวรัส (COVID-19)
            เพื่อให้หน่วยงาน บุคคล และประชาชนทั่วไป ได้ใช้ประโยชน์เพื่อการคุ้มครอง ป้องกันสุขภาพของประชาชน
            ตลอดจนการจัดการสภาพแวดล้อมในสถานที่ต่างๆ (Setting) เพื่อควบคุมป้องกันโรค มิให้ แพร่กระจายไปสู่คนหมู่มาก
            และสถานการณ์การระบาดของโรคได้มีการเปลี่ยนแปลงตลอดเวลา
            จึงมีความจำเป็นที่จะต้องมีการทบทวนและปรับเปลี่ยนคำแนะนำ แนวทางการดำเนินงาน ให้สอดคล้องและทันเวลา จึงขอให้
            หน่วยงานสาธารณสุข หน่วยงานที่เกี่ยวข้องได้ติดตามข้อสั่งการ และมาตรการต่างๆบนทางเวบไซต์กระทรวงสาธารณสุข
            www.covid-19.moph.go.th</p>
        <p class="content_news">โรคติดเชื้อไวรัสโคโรนา 2019 (COVID-19)
            เป็นไวรัสอุบัติใหม่ที่พบว่ามีการระบาดตั้งแต่เดือนธันวาคม 2562
            โดยรายงานครั้งแรกที่มณฑลหูเป่ย สาธารณรัฐประชาชนจีน เชื้อไวรัส COVID-19
            สามารถแพร่กระจายจากคนสู่คนผ่านทางการไอ จาม สัมผัสโดยตรงกับสารคัดหลั่ง เช่น น้ำมูก น้ำลายของคน
            ผู้ติดเชื้อจะมีอาการหลายแบบตั้งแต่ติดเชื้อไม่มีอาการ อาการเล็กน้อยคล้ายเป็นไข้หวัดธรรมดา
            อาการปานกลางเป็นปอดอักเสบ และอาการรุนแรงมากจนอาจเสียชีวิต</p>
        <p class="content_news">การแพร่ระบาดของโรคติดเชื้อไวรัสโคโรนา 2019 เกิดขึ้นอย่างกว้างขวางในหลายประเทศทั่วโลก
            องค์การอนามัยโลกได้ประกาศให้โรคติดเชื้อไวรัสโคโรนา 2019 เป็นภาวะฉุกเฉินทางสาธารณสุขระหว่างประเทศ (Public
            Health Emergency of International Concern) เมื่อวันที่ 30 มกราคม 2563 และแนะนำทุกประเทศให้
            เร่งรัดการเฝ้าระวัง ป้องกัน และควบคุมโรค ข้อมูล ณ วันที่ 28 มีนาคม 2563 มีรายงานผู้ป่วยใน 196 ประเทศ 2
            เขตบริหารพิเศษ 1 นครรัฐ เรือสำราญ เป็นผู้ป่วยที่ยืนยัน 597,458 ราย และเสียชีวิต 27,370 ราย สำหรับประเทศไทย
            ได้มีมาตรการในการเฝ้าระวัง ป้องกันและควบคุมโรคตั้งแต่เริ่มพบการระบาดใน ประเทศจีน
            ตรวจพบผู้ป่วยรายแรกเป็นผู้เดินทางจากประเทศจีนในวันที่ 8 มกราคม 2653 ได้รับการตรวจยืนยัน
            ผลและแถลงสถานการณ์ในวันที่ 14 มกราคม 2563 ผู้ป่วยช่วงแรกเป็นผู้เดินทางจากพื้นที่เสี่ยง การขยายพื้นที่
            ระบาดในต่างประเทศเกิดขึ้นอย่างต่อเนื่อง
            ประกอบกับผู้ติดเชื้อจากการเดินทางไปยังพื้นที่ระบาดอาจมีอาการเล็กน้อยทำให้ไม่มีการตรวจหาการติดเชื้อ
            การพบผู้ป่วยกลุ่มถัดมาคือผู้ประกอบอาชีพสัมผัสกับนักท่องเที่ยว
            จากนั้นพบการติดเชื้อในกลุ่มนักท่องเที่ยวไทยที่นิยมไปกินดื่ม
            การระบาดที่เป็นกลุ่มก้อนใหญ่คือการระบาดในสนามมวยลุมพินี สนามมวยราชดำเนิน
            ซึ่งทำให้มีผู้ติดเชื้อจำนวนเพิ่มมากขึ้นอย่างรวดเร็ว จนต้องมีการประกาศให้ โรคติดเชื้อไวรัสโคโรนา 2019
            (Coronavirus Disease 2019 (COVID-19) เป็นโรคติดต่ออันตรายตามพระราชบัญญัติโรคติดต่อ พ.ศ. ๒๕๕๘ เมื่อวันที่ 26
            กุมภาพันธ์ 2563 และต่อมาเมื่อวันที่ 25 มีนาคม 2563
            นายกรัฐมนตรีได้ประกาศสถานการณ์ฉุกเฉินตามพระราชกำหนดการบริหารราชการในสถานการณ์ฉุกเฉิน พ.ศ.
            2548 และวันที่ 26 มีนาคม 2563 ได้ประกาศข้อกำหนดตามความในมาตรา 9
            แห่งพระราชกำหนดการบริหารราชการในสถานการณ์ฉุกเฉิน พ.ศ. 2548 (ฉบับที่ 1)
            โดยได้ออกข้อกำหนดและข้อปฏิบัติแก่ส่วนราชการ ที่เกี่ยวข้อง
            เพื่อให้สามารถแก้ไขสถานการณ์ฉุกเฉินให้ยุติลงได้โดยเร็วและป้องกันมิให้เกิดเหตุการณ์ร้ายแรงมากขึ้น </p>
    </div>



    <div class="col-12 mt-5 p-2 album_news"
        style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); ">
        <div class="col-12 d-flex align-items-center p-0">
            <div class="col-6">
                <span class="title_album">อัลบั้มรูปภาพ</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-end">
                <span class="name_album mr-2">Work from home</span>
                <span class="count_imgAlbum mr-2">10</span>
                <span class="unit_album mr-2">ภาพ</span>
            </div>
        </div>
        <div class="row mt-3">
            <div class="swiper-container">
                <div class="swiper-wrapper mb-5">

                    <div class="swiper-slide">
                        <img src="/img/icon/news3@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news4@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news5@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news6@2x.png" alt="">
                    </div>

                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>







    <div class="col-12 mt-5 p-2 vdo_news"
        style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); ">
        <div class="col-12 d-flex align-items-center p-0">
            <div class="col-6">
                <span class="title_album">วีดิโอ</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-end">
                <span class="count_imgAlbum mr-2">4</span>
                <span class="unit_album mr-2">คลิป</span>

            </div>
        </div>
        <div class="row mt-3">
            <div class="swiper-container">
                <div class="swiper-wrapper mb-5">

                    <div class="swiper-slide">
                        <img src="/img/icon/news3@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news4@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news5@2x.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="/img/icon/news6@2x.png" alt="">
                    </div>

                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>









    <div class="col-12 mt-5 px-2 py-3 files_news"
        style="background-color:#ffffff;  box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); ">
        <div class="col-12 d-flex align-items-center p-0">
            <div class="col-6">
                <span class="title_album">ไฟล์แนบ</span>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-end">
                <span class="count_imgAlbum mr-2">1</span>
                <span class="unit_album mr-2">ไฟล์</span>
            </div>
        </div>

        <div class="col-12 d-flex align-items-center justify-content-between mt-3">

            <div class="col-3 d-flex align-items-center px-2 py-1"
                style=" border-radius: 4px; background-color: #eaeaea;">
                <div class="col-2 p-0 text-center">
                    <img src="/img/type_files/group-20-copy-9.png" alt="">
                </div>

                <div class="col-9 p-1">
                    <div class="col-12 p-0">
                        <span class="name_files">ประกาศการทำงานที่บ้าน</span>
                    </div>
                    <div class="col-12 p-0 d-flex align-items-center ">
                        <span class="file_size">57</span>
                        <span class="file_size">KB</span>
                    </div>
                </div>

                <div class="col-1 p-0">
                    <img src="/img/icon/file-download-material-black.png" alt="">
                </div>

            </div>

        </div>

    </div>


    <div class="col-12 mt-5 p-0 d-flex align-items-center">

        <span class="head_tag mr-2">Tag : </span>
        <span class="name_tag">COVID19, เชื้อไวรัสโคโรนา, โควิด 19, องค์การอนามัยโลก , กระทรวงสาธารณสุข</span>

    </div>


    <div class="col-12 p-0 mt-5">
        <div class="col-12 p-0">
            <span class="from_text">ข่าวสารโดย : </span>
        </div>
        <div class="col-12 p-0 d-flex align-items-center mt-1">
            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    style="width:62px; height:62px; object-fit:cover; border-radius:50%;">
            </div>
            <div class="col-11 p-0">
                <div class="col-12 p-0 d-flex align-items-center">
                    <span class="written_news mr-2">Nissana Thaveepanit</span>
                    <span class="tag_guru text-center">GURU</span>
                </div>
                <div class="col-12 p-0">
                    <span class="position_written">สำนักพัฒนาตลาดและธุรกิจไทยในต่างประเทศ</span>
                </div>
            </div>

        </div>
    </div>


    <div class="col-12 mt-5"
        style="background-color:#ffffff; box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1); padding: 20px 24px 33px 24px;">
        <div class="col-12 p-0 d-flex align-items-center justify-content-between">
            <div class="col-6 p-0">
                <span class="head_comments">คุณรู้สึกอย่างไรกับบทความนี้</span>
            </div>
            <div class="col-6 p-0 d-flex align-items-center justify-content-end pr-2">
                <img src="/img/icon/comment-blue.png" class="mr-2" alt="" style="object-fit: contain;">
                <span class="count_comments">0</span>
            </div>
        </div>

        <div class="col-12 d-flex align-items-center mt-3">

            <div class="col-1 p-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    style="width:62px; height:62px; object-fit:cover; border-radius:50%;">
            </div>

            <div class="col-11 p-0 d-flex align-items-center">
                <textarea name="comment_news" class="comment_news" cols="104" rows="2"
                    placeholder="แสดงความคิดเห็น..."></textarea>
            </div>

        </div>
    </div>


</div>
@endsection


@section('script')
<script>
    var gallery = new Swiper('.album_news .swiper-container', {
        slidesPerView: 4,
        loop: true,
        pagination: {
            el: '.album_news .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

    var gallery = new Swiper('.vdo_news .swiper-container', {
        slidesPerView: 4,
        loop: true,
        pagination: {
            el: '.vdo_news .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

</script>
@endsection
