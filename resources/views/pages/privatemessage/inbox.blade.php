@extends('layout.pm')
@section('style')
<style>
    .div_2 {
        line-height: 19px;
        width: 774px;
        transition: .3s;
        /* transition: width 2s; */
    }

    .div_2_after {
        line-height: 19px;
        width: 658px;
        transition: .9s;
        /* transition: width 2s; */
    }

    .class_more {
        display: none;
        width: 78px;
        padding: 36px 23px;
        background-color: #d3d3d3;
        cursor: pointer;
    }

    .class_all {
        /* display: none; */
        width: 160px;
        background-color: #d3d3d3;
    }

    .class_del {
        width: 0px;
        background-color: #ee2a27;
    }

    .class_del_after {
        width: 78px;
        padding: 36px 23px;
        background-color: #ee2a27;
        cursor: pointer;
        display: block;
    }

    .test_fade {
        width: 0px;
        background: red;
        transition: width .25s;
    }

    .test_fade_af {
        width: 78px !important;
    }

    .text_test {
        display: none;
    }

    .text_test_af {
        display: block;
    }

</style>



@endsection


@section('content_pm')
    <div class="col-12 mt-4 px-2 d-flex">
        <div class="col-6 px-0 d-flex align-items-center">
            <div class="mr-3 d-flex justify-content-center align-items-center">
                <img src="/img/icon/email-material.png" alt="">
            </div>
            <div>
                <span class="text-head">กล่องข้อความ</span>
            </div>
        </div>
        <div class="col-6 px-0 d-flex align-items-center justify-content-end">
            <div class="position-relative">
                <input class="input-cearch form-control" type="text" placeholder=" ค้นหารายการ">
                <img class="position-icon-ceach" src="/img/icon/search-material-bule.png" alt="">
            </div>
            <div class="mx-2">
                <img src="/img/icon/tune-material-copy-3@2x.png" alt="" style="width: 23px; height:auto;">
            </div>
            <button class="btn btn-create-form py-0">+ ส่งข้อความใหม่</button>
        </div>
    </div>
    <hr class="mx-5" style="border-top: 1px dashed #8c8b8b;background-color: white;">
    <div class="col-12 px-5 mt-3">
        @for ($i = 0; $i < 2; $i++) 
            <div class="col-12 mb-2 px-0 py-3 d-flex" style="background-color: #e4e9f8;">
                <div class="col-1 d-flex align-items-center">
                    <img class="img-avatar" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                </div>
                <div class="col-9 pl-4" style="line-height: 19px;">
                    <span class="chat-name">Wittayakorn Maneenetr</span>
                    <p class="chat-time">10 มี.ค. 2564 09:47</p>
                    <span class="text-chat">ประชุมเริ่มแล้วนะ</span>
                </div>
                <div class="col-2 pr-4 d-flex align-items-end justify-content-end">
                    <div class="mr-2">
                        <img src="/img/icon/comment-blue.png">
                    </div>
                    <div class="mr-4">
                        <span class="text-count-chat">123</span>
                    </div>
                    <div>
                        <img src="/img/icon/more-horiz-material-copy-10.png">
                    </div>
                </div>
            </div>
        @endfor

        @for ($i = 0; $i < 2; $i++) 
            <div class="col-12 mb-2 px-0 d-flex bg-inbox">
                <div class="col-12 d-flex m-0 p-0 chat_more">
                    <div class="col-1 d-flex align-items-center div_1 py-3">
                        <img class="img-avatar" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
                    </div>
                    <div class="pl-4 div_2 py-3">
                        <span class="chat-name">Wittayakorn Maneenetr</span>
                        <p class="chat-time">10 มี.ค. 2564 09:47</p>
                        <span class="text-chat">ประชุมเริ่มแล้วนะ</span>
                    </div>
                    <div class="d-flex align-items-end justify-content-end div_3 py-3">
                        <div class="mr-2">
                            <img src="/img/icon/comment-blue.png">
                        </div>
                        <div class="mr-4">
                            <span class="text-count-chat">123</span>
                        </div>
                        <div class="div-posi-more" style="display: block;">
                            <img src="/img/icon/more-horiz-material-copy-10.png" class="more" style="cursor: pointer;">
                        </div>
                    </div>

                    <div class="class_all" style="display: none;">
                        <div class="row d-flex h-100">
                            <div class="more"
                                style="padding: 36px 23px;width:78px !important ;cursor:pointer;background-color: #d3d3d3;">
                                <div class="d-flex align-items-center justify-content-center"
                                    style="border-radius: 50%;height: 32px;width: 32px;background-color: #ffffff;">
                                    <img src="/img/icon/more-horiz-material-copy-10.png">
                                </div>
                            </div>

                            <div class="del" style="padding: 36px 23px;width:78px;!important;background-color: #ee2a27;">
                                <div class="d-flex align-items-center justify-content-center"
                                    style="border-radius: 50%;height: 32px;width: 32px;background-color: #ffffff;">
                                    <img src="/img/icon/bin-red.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
@endsection


@section('script')
    <script>
        $(document).ready(function () {

            $('.div-posi-more').on('click', function () {
                $(this).hide();
                $(this).parents('.chat_more').find('.class_all').fadeIn('slow');
            });

            $('.more').on('click', function () {
                $(this).parents('.chat_more').find('.class_all').fadeOut('fast', function () {
                    $('.div-posi-more').show();
                });
            });

        });

    </script>
@endsection
