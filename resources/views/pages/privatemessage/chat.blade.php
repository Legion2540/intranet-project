@extends('layout.pm')
@section('content_pm')


@section('style')
<style>
    .modal-dialog {
        max-width: 1004px;
        margin: 1.75rem auto;
    }

    .modal-body {
        padding: 18px 55px 36px 36px;
    }

    .text-head-newinbox {
        font-size: 26px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-sendto {
        width: 437px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text-key{
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .text-titel {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text-link {
        width: 437px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select-album {
        width: 437px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .btn-post {
        width: 112px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .btn-cancel {
        width: 112px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    .file-upload-btn {
        width: 95px;
        height: 30px;
        border-radius: 4px;
        color: #ffffff;
        border: 0;
        font-size: 14px;
        font-family: Kanit-ExtraLight;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
    }

    /* dropFiles */

    .dropFiles {
        width: 100%;
        min-height: 133px;
        height: 100px;
        border: 2px #4f72e5 dashed;
        text-align: center;
        font-size: 25px;
    }

    .highlightDropArea {
        border: 3px green dashed;
    }

    .text-choosefile {
        font-size: 20px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .text-typefile {
        font-size: 14px;
        color: #ee2a27;
    }

</style>
@endsection


<div class="col-12 mt-4 px-2 d-flex">
    <div class="col-6 px-0 d-flex align-items-center">
        <div class="mr-3 d-flex justify-content-center align-items-center">
            <img src="/img/icon/email-material.png" alt="">
        </div>
        <div>
            <span class="text-head">กล่องข้อความ</span>
        </div>
    </div>
    <div class="col-6 px-0 d-flex align-items-center justify-content-end">
        <div class="position-relative">
            <input class="input-cearch form-control" type="text" placeholder=" ค้นหารายการ">
            <img class="position-icon-ceach" src="/img/icon/search-material-bule.png" alt="">
        </div>
        <div class="mx-2">
            <img src="/img/icon/tune-material-copy-3@2x.png" alt="" style="width: 23px;height:auto;">
        </div>
        <button class="btn btn-create-form py-0" data-bs-toggle="modal" data-bs-target="#Newmessage">+
            ส่งข้อความใหม่</button>
    </div>
</div>
<hr class="mx-5" style="border-top: 1px dashed #8c8b8b;background-color: white;">
<div class="col-12 px-5 mt-3">
    <div class="col-12 mb-2 px-0 py-3 d-flex bg-chat">
        <div class="col-1 d-flex align-items-center">
            <img class="img-avatar" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
        </div>
        <div class="col-11 pl-4" style="line-height: 19px;">
            <span class="chat-name">Wittayakorn Maneenetr</span>
            <p class="chat-time">23 ธ.ค. 2563 10:00</p>
            <span class="text-chat">เอกสารการประชุมเสร็จหรือยังครับ</span>
        </div>
    </div>
    <div class="col-12 mb-2 px-0 py-3 d-flex bg-chat">
        <div class="col-11 pr-4 text-end" style="line-height: 19px;">
            <span class="chat-name">Darisa Patcharachot</span>
            <p class="chat-time">4 ม.ค. 2564 09:47</p>
            <span class="text-chat">เรียบร้อยแล้วค่ะ</span>
            <div class="mt-2">
                <img src="/img/icon/attach-file-material-copy.png" alt="">
                <span class="text-file mr-1">วาระการประชุม.docx</span>
                <img src="/img/icon/files-download-materail-blue.png" alt="">
            </div>
        </div>
        <div class="col-1 px-0 text-center d-flex justify-content-start">
            <img class="img-avatar" src="/img/icon/rectangle.png">
        </div>
    </div>
    <div class="col-12 mb-2 px-0 py-3 d-flex bg-chat">
        <div class="col-1 d-flex align-items-center">
            <img class="img-avatar" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
        </div>
        <div class="col-11 pl-4" style="line-height: 19px;">
            <span class="chat-name">
                Wittayakorn Maneenetr
                <img src="/img/icon/group-material.png" alt="">
                <span style="font-size: 14px">Darisa, Chiraphon, Monsicha and 156 people</span>
            </span>
            <p class="chat-time">1 ม.ค. 2564 09:47</p>
            <span class="text-chat">สวัสดีปีใหม่ครับ พนักงานทุกท่าน</span>
        </div>
    </div>
    <div class="col-12 mb-2 px-0 py-3 d-flex bg-chat">
        <div class="col-11 pr-4 text-end" style="line-height: 19px;">
            <span class="chat-name">Darisa Patcharachot</span>
            <p class="chat-time">4 ม.ค. 2564 09:47</p>
            <span class="text-chat">สวัสดีปีใหม่ค่ะ :)</span>
        </div>
        <div class="col-1 px-0 text-center d-flex justify-content-start">
            <img class="img-avatar" src="/img/icon/rectangle.png">
        </div>
    </div>

    {{-- Hr New chat --}}

    <div class="hr-sect">ข้อความใหม่</div>

    <div class="col-12 mb-2 px-0 py-3 d-flex bg-chat">
        <div class="col-1 d-flex align-items-center">
            <img class="img-avatar" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg">
        </div>
        <div class="col-11 pl-4" style="line-height: 19px;">
            <span class="chat-name">
                Wittayakorn Maneenetr
            </span>
            <p class="chat-time">10 มี.ค. 2564 09:47</p>
            <span class="text-chat">เป็นยังไงบ้าง</span>
        </div>
    </div>

    {{-- CK Editer --}}
    <div class="col-12 px-0 py-3 d-flex" style="background-color:#e1e1e1;">
        <div class="col-1">
            <img class="img-avatar" src="/img/icon/rectangle.png">
        </div>
        <div class="col-11">
            <div id="editor" style="height: 210px;">
                {{-- <p>This is some sample content.</p> --}}
            </div>
            <div class="mt-3 ml-1">
                <button class="btn-chat-send">ส่ง</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="Newmessage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-12 d-flex mb-3">
                    <div class="mr-3 align-self-center">
                        <img src="/img/icon/border-color-material-copy.png" alt="">
                    </div>
                    <div>
                        <span class="text-head-newinbox">ส่งข้อความใหม่</span>
                    </div>
                </div>
                <div class="col-12 d-flex">
                    <div class="col-1 px-0">
                        <span class="text-key">ส่งถึง</span>
                    </div>
                    <div class="col-11 px-0">
                        <input class="form-control text-sendto" type="text">
                    </div>
                </div>
                <div class="col-12 d-flex mt-2">
                    <div class="col-1 px-0">
                        <span class="text-key">หัวเรื่อง</span>
                    </div>
                    <div class="col-11 px-0">
                        <input class="form-control text-titel" type="text"
                            placeholder="ระบุชื่อบทความ หัวเรื่อง...">
                    </div>
                </div>
                <div class="col-12 d-flex mt-4">
                    <div class="col-1 px-0">
                        <span class="text-key">ข้อความ</span>
                    </div>
                    <div class="col-11 px-0">
                        <div id="editor_inbox" style="height: 210px;">
                            {{-- <p>This is some sample content.</p> --}}
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex mt-4">
                    <div class="col-1 px-0">
                        <span class="text-key">แนบรูป</span>
                    </div>
                    <div class="col-11 px-0">
                        <div id="dropFiles" class="dropFiles">
                            <div class="col-12 d-flex p-0" style="height: 100%">
                                <div class="col-3 p-0 d-flex justify-content-end align-self-center">
                                    <img src="/img/icon/insert-photo-material@3x.png" alt="" width="48px" height="48px">
                                </div>
                                <div class="col-9 py-4" style="text-align: start;">
                                    <span class="text-choosefile">Choose file from your device or drag image
                                        here.</span><br>
                                    <span class="text-typefile">(bmp, gif, jpg, jpeg, png)</span>
                                </div>
                            </div>
                        </div>
                        <div id="messages">
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex mt-4">
                    <div class="col-1 px-0">
                        <span class="text-key">แนบอัลบั้ม</span>
                    </div>
                    <div class="col-11 px-0">
                        <select class="form-select select-album text-14" aria-label="Default select example">
                            <option selected>เลือกอัลบั้ม</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 d-flex mt-4">
                    <div class="col-1 px-0">
                        <span class="text-key">แนบไฟล์</span>
                    </div>
                    <div class="col-11 px-0">
                        <input class="file-upload-input" type='file' onchange="uploadfile()" hidden />
                        <button class="file-upload-btn mr-2" type="button"
                            onclick="$('.file-upload-input').trigger( 'click' )">Browse File</button>
                        <img src="/img/icon/check-material.png" alt="" class="file-upload-content">
                        <span class="typefile" style="color: #ee2a27;">(.pdf, .mp4, .docx, .xls)</span>
                    </div>
                </div>
                <div class="col-12 d-flex mt-4">
                    <div class="col-1 px-0">
                        <span class="text-key">แนบลิงค์</span>
                    </div>
                    <div class="col-11 px-0">
                        <input class="form-control text-link" type="text" name="" id="" placeholder="ระบุ URL">
                    </div>
                </div>

                <div class="col-12 d-flex justify-content-center mt-5">
                    <button type="button" class="btn btn-cancel p-0 mr-3" data-bs-dismiss="modal">ยกเลิก</button>
                    <button class="btn-post">โพสต์</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.file-upload-content').hide();


        $("#dropFiles").on('dragenter', function (ev) {
            // Entering drop area. Highlight area
            $("#dropFiles").addClass("highlightDropArea");
        });

        $("#dropFiles").on('dragleave', function (ev) {
            // Going out of drop area. Remove Highlight
            $("#dropFiles").removeClass("highlightDropArea");
        });

        $("#dropFiles").on('drop', function (ev) {
            // Dropping files
            ev.preventDefault();
            ev.stopPropagation();
            // Clear previous messages
            $("#messages").empty();
            if (ev.originalEvent.dataTransfer) {
                if (ev.originalEvent.dataTransfer.files.length) {
                    var droppedFiles = ev.originalEvent.dataTransfer.files;
                    for (var i = 0; i < droppedFiles.length; i++) {
                        console.log(droppedFiles[i]);
                        $("#messages").append("<br /> <b>Dropped File </b>" + droppedFiles[i].name);
                        // Upload droppedFiles[i] to server
                    }
                }
            }

            $("#dropFiles").removeClass("highlightDropArea");
            return false;
        });

        $("#dropFiles").on('dragover', function (ev) {
            ev.preventDefault();
        });
    })

    function uploadfile() {
        $('.file-upload-content').show();
        $('.typefile').toggle();
    }

    // CK Editor //
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });

    ClassicEditor
        .create(document.querySelector('#editor_inbox'))
        .catch(error => {
            console.error(error);
        });

</script>
@endsection
