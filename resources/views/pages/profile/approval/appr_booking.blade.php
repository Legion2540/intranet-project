@extends('layout.profile')
@section('style')
    <style>
        .card_booking {
            width: fit-content;
            height: fit-content;
            background-color: #ededed;
            margin: 10px 25px 30px 25px;
        }

        .waiting_approve {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: rgb(252, 144, 2);
            border-radius: 4px
        }

        .approve {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            background-color: green;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            border-radius: 4px
        }

        .cancel_approve {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            background-color: red;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            border-radius: 4px
        }

        .nav-tabs .nav-item.show .nav-link,
        .nav-tabs .nav-link.active {
            background-color: #4f72e5 !important;
            color: #ffffff !important;
        }

        .change_booking_btn {
            background-color: #ffffff !important;
            color: #4a4a4a !important;
        }

        .change_booking_btn:focus {
            background-color: #4f72e5 !important;
            color: #ffffff !important;
        }

        .display,
        .table {
            background-color: #ffffff !important;
        }

        .title_booking {
            font-size: 20px;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4f72e5;
        }

        #booking_car_btn {
            width: 128px;
            height: 30px;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #ffffff;
            border-radius: 8px;
            box-shadow: 2px 3px 4px 1px rgba(0, 0, 0, 0.1);
            background-image: linear-gradient(196deg, #4f72e5 84%, #314d7b -6%);
        }

        .date_appr_booking {
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            width: 103px;
            height: 30px;
            color: #4f72e5;
        }

        .city_appr_booking {
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            width: 127px;
            height: 30px;
            color: #4f72e5;
        }

    </style>
@endsection

@section('content')

    <div class="card_booking d-flex justify-content-center">
        <div>

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active mr-2 change_booking_btn" id="req_rooms" data-bs-toggle="tab"
                        data-bs-target="#req_room" type="button" role="tab" aria-controls="req_room"
                        aria-selected="true">คำขอใช้ห้องประชุม</button>

                    <button class="nav-link change_booking_btn" id="req_cars" data-bs-toggle="tab" data-bs-target="#req_car"
                        type="button" role="tab" aria-controls="req_car" aria-selected="false">คำขอใช้รถ</button>

                    <button class="nav-link change_booking_btn mr-2" id="history_rooms" data-bs-toggle="tab"
                        data-bs-target="#history_room" type="button" role="tab" aria-controls="history_room"
                        aria-selected="false">ประวัติการใช้ห้องประชุม</button>
                </div>
            </nav>



            <div class="tab-content pt-3" id="nav-tabContent" style="background-color:#ffffff;">

                <div class="tab-pane fade show active mx-4" id="req_room" role="tabpanel" aria-labelledby="req_room">
                    <div class="col-12 d-flex pr-0 mb-2">
                        <div class="col-2 text-start d-flex align-items-center">
                            <span class="title_booking">คำขอรออนุมัติ</span>
                        </div>
                        <div class="col-10 align-items-center pr-0 d-flex justify-content-end">
                            <img src="/img/icon/date-range-material-copy-4.png" alt="">
                            <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-3" alt="">
                            <input type="search" style=" width: 280px;
                                                    height: 29px;
                                                    border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute;
                                                    left: 610px;
                                                    top: 6px;" alt="">

                            <select class="form-select form-select-1x date_appr_booking mx-2" aria-label="ทุกหมวดหมู่">
                                <option selected>Month</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <select class="form-select form-select-1x city_appr_booking" aria-label="ทุกหมวดหมู่">
                                <option selected>เมืองทอง</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="booking_room"></th>
                                <th>ผู้ขอใช้</th>
                                <th>ห้องประชุม</th>
                                {{-- <th>จำนวนคน</th> --}}
                                <th>วันที่ใช้</th>
                                <th>เวลาเริ่มใช่</th>
                                <th>เวลาสิ้นสุด</th>
                                <th>แผนก</th>
                                <th>อาหาร / ของว่าง</th>
                                <th><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <th>สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @if (empty($approve_room))
                                <tr>
                                    <td colspan="11" class="text-center">ไม่มีคำร้องขอ</td>
                                </tr>
                            @else --}}
                            @foreach ($approve_room as $room)
                                <tr>
                                    <td><input type="checkbox" name="booking_room"></td>
                                    <td><a href="/manager/approved_room/?book_id={{ $room['id'] }}"><span>{{ $room->datauser->name }}</span></a></td>
                                    <td>{{ $room['room_request'] }}</td>
                                    {{-- <td>$room['member_count']</td> --}}
                                    <td>{{ $room['start_date'] . ' - ' . $room['end_date'] }}</td>
                                    <td>{{ $room['start_time'] }}</td>
                                    <td>{{ $room['end_time'] }}</td>
                                    <td>ฝ่ายบุคคล</td>
                                    <td>
                                        @if (isset($room['food_detail']))
                                            <img src="/img/icon/check-material.png" alt="">
                                        @else
                                            <img src="/img/icon/close-material.png" alt="">
                                        @endif

                                    </td>
                                    <th><img src="/img/icon/local-printshop-material-copy.png" alt=""
                                            style="cursor:pointer;"></th>
                                    @if ($room['status_booking'] == 'waiting')
                                        <td><span class="waiting_approve">รออนุมัติ</span></td>

                                    @elseif($room['status_booking'] == 'approved')
                                        <td><span class="approve">รออนุมัติ</span></td>

                                    @elseif($room['status_booking'] == 'cancelled')
                                        <td><span class="cancel_approve">รออนุมัติ</span></td>

                                    @endif
                                </tr>
                            @endforeach

                            {{-- @endif --}}

                        </tbody>
                    </table>

                    <div class="col-12 text-end">
                        <span>
                            < 1 >
                        </span>
                    </div>
                </div>


                <div class="tab-pane fade mx-4" id="req_car" role="tabpanel" aria-labelledby="req_car">
                    <div class="col-12 d-flex pr-0 mb-2">
                        <div class="col-2 text-start d-flex align-items-center">
                            <span class="title_booking">คำขอรออนุมัติ</span>
                        </div>

                        <div class="col-10 align-items-center pr-0 d-flex justify-content-end">
                            <img src="/img/icon/date-range-material-copy-4.png" alt="">
                            <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-3" alt="">
                            <input type="search" style=" width: 280px;
                                                    height: 29px;
                                                    border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute;
                                                    left: 610px;
                                                    top: 6px;" alt="">

                            <select class="form-select form-select-1x date_appr_booking mx-2" aria-label="ทุกหมวดหมู่">
                                <option selected>Month</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <select class="form-select form-select-1x city_appr_booking" aria-label="ทุกหมวดหมู่">
                                <option selected>เมืองทอง</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>



                        </div>
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="booking_room"></th>
                                <th>ผู้ขอใช้</th>
                                <th>ห้องประชุม</th>
                                <th>จำนวนคน</th>
                                <th>วันที่ใช้</th>
                                <th>เวลาเริ่มใช่</th>
                                <th>เวลาสิ้นสุด</th>
                                <th>แผนก</th>
                                <th>อาหาร / ของว่าง</th>
                                <th><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <th>สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td><input type="checkbox" name="booking_room"></td>
                                <td>นางสาวดาริสา พัชรโชด</td>
                                <td>ห้องประชุม 1</td>
                                <td>1</td>
                                <td>21 ธันวาคม 2563</td>
                                <td>09:00</td>
                                <td>09:00</td>
                                <td>ฝ่ายบุคคล</td>
                                <td><img src="/img/icon/check-material.png" alt=""></td>
                                <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                                <td><span class="waiting_approve">รออนุมัติ</span></td>
                            </tr>

                            <tr>
                                <td><input type="checkbox" name="booking_room"></td>
                                <td>นางสาวดาริสา พัชรโชด</td>
                                <td>ห้องประชุม 1</td>
                                <td>1</td>
                                <td>21 ธันวาคม 2563</td>
                                <td>09:00</td>
                                <td>09:00</td>
                                <td>ฝ่ายบุคคล</td>
                                <td><img src="/img/icon/close-material.png" alt=""></td>
                                <th><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <td><span class="waiting_approve">รออนุมัติ</span></td>
                            </tr>



                        </tbody>
                    </table>
                    <div class="col-12 text-end">
                        <span>
                            < 1>
                        </span>
                    </div>
                </div>


                <div class="tab-pane fade mx-4 " id="history_room" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="col-12 d-flex pr-0 mb-2">
                        <div class="col-2 text-start d-flex align-items-center">
                            <span class="title_booking">คำขอรออนุมัติ</span>
                        </div>

                        <div class="col-10 align-items-center pr-0 d-flex justify-content-end">
                            <img src="/img/icon/date-range-material-copy-4.png" alt="">
                            <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-3" alt="">
                            <input type="search" style=" width: 280px;
                                                    height: 29px;
                                                    border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute;
                                                    left: 610px;
                                                    top: 6px;" alt="">

                            <select class="form-select form-select-1x date_appr_booking mx-2" aria-label="ทุกหมวดหมู่">
                                <option selected>Month</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <select class="form-select form-select-1x city_appr_booking" aria-label="ทุกหมวดหมู่">
                                <option selected>เมืองทอง</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>



                        </div>
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="booking_room"></th>
                                <th>ผู้ขอใช้</th>
                                <th>ห้องประชุม</th>
                                <th>จำนวนคน</th>
                                <th>วันที่ใช้</th>
                                <th>เวลาเริ่มใช่</th>
                                <th>เวลาสิ้นสุด</th>
                                <th>แผนก</th>
                                <th>อาหาร / ของว่าง</th>
                                <th><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <th>สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td><input type="checkbox" name="booking_room"></td>
                                <td>นางสาวดาริสา พัชรโชด</td>
                                <td>ห้องประชุม 1</td>
                                <td>1</td>
                                <td>21 ธันวาคม 2563</td>
                                <td>09:00</td>
                                <td>09:00</td>
                                <td>ฝ่ายบุคคล</td>
                                <td><img src="/img/icon/check-material.png" alt=""></td>
                                <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                                <td><span class="waiting_approve">รออนุมัติ</span></td>
                            </tr>

                            <tr>
                                <td><input type="checkbox" name="booking_room"></td>
                                <td>นางสาวดาริสา พัชรโชด</td>
                                <td>ห้องประชุม 1</td>
                                <td>1</td>
                                <td>21 ธันวาคม 2563</td>
                                <td>09:00</td>
                                <td>09:00</td>
                                <td>ฝ่ายบุคคล</td>
                                <td><img src="/img/icon/close-material.png" alt=""></td>
                                <th><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <td><span class="waiting_approve">รออนุมัติ</span></td>
                            </tr>



                        </tbody>
                    </table>
                    <div class="col-12 text-end">
                        <span>
                            < 1>
                        </span>
                    </div>
                </div>



            </div>











        </div>
    </div>
@endsection


@section('script')

@endsection
