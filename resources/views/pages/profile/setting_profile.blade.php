@extends('layout.settingProfile')
@section('style')
<style>
    .setting_left {
        width: 531px;
        height: 669px;
        background-color: #ffffff;
        margin: 10px 0px 15px 0px;
    }

    .setting_right {
        width: 848px;
        height: 669px;
        background-color: #ffffff;
        margin: 10px 0px 15px 25px;
    }

    .head_setting_left {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        /* line-height: 0.83; */
        color: #4f72e5;
    }

    .head_content_left {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        /* line-height: 0.83; */
        color: #4a4a4a;
    }

    .content_left {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .change_password {
        width: 95;
        height: 30px;
        border-radius: 6px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        padding: 5px 8px;
        text-decoration: none;
        font-size: 12px;
        font-weight: bold;
        text-align: center;
        color: #ffffff;
    }

    .input_left {
        width: 100%;
        height: 30px;
        padding: 0 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text_img {
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: 600;
        color: #d0021b;
    }

    .img_user {
        width: 120px;
        height: 120px;
        object-fit: cover;
        border-radius: 50%;
    }

    #introducing {
        padding: 8px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .cancel_updateProfile {
        width: 112px;
        height: 30px;
        padding: 3px 10px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
    }

    .accept_updateProfile {
        color: #ffffff;
        width: 112px;
        height: 30px;
        padding: 3px 10px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b -15%);
    }

    sup {
        color: red;
    }

    input[type="email"]:read-only {
        background-color: #f4f4f4;
    }

    input[type="date"]::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 530px;
        cursor: pointer;
    }

</style>
@endsection



@section('content')

<form action="/profile/updateprofile" method="post">
    @csrf

    <div class="d-flex justify-content-center">
        <div class="setting_left p-4">
            <div class="col-12 d-flex align-items-center">
                <img src="/img/icon/settings-material@2x.png" alt="" class="mr-2">
                <span class="head_setting_left ml-2">Setting</span>
            </div>
            <div class="row mt-3 px-4 py-1">
                <div class="col-12">
                    <div class="col-12 text-start">
                        <span class="head_content_left">ข้อมูลบัญชี</span>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ชื่อบัญชี</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="username" class="input_left">
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">อีเมล์</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="email" class="input_left" name="email" value="{{ @Auth::user()->email }}"
                                readonly>
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">พาสเวิร์ด</span>
                        </div>
                        <div class="col-9 pr-0 text-start">
                            <a href="#" class="change_password">เปลี่ยนพาสเวิร์ด</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex" style="margin-top:90px;">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-12 px-0 text-start">
                            <span class="content_left">ภาพประจำตัว</span><br>
                            <span class="text_img">.jpg .png</span><br>
                            <span class="text_img">ขนาด 120x120 px</span>
                        </div>
                    </div>
                    <div class="col-8 text-start">
                        <img src="{{ (Auth::user()->pic_profile !=  null) ? '/img/profile/'.Auth::user()->pic_profile : '/img/profile/profile-default.jpg' }}"
                            class="img_user" alt="">
                        <img src="/img/icon/icon-camera.png" alt=""
                            style="position: absolute;transform: translate(-30px, 90px);">
                    </div>
                </div>
            </div>
        </div>

        <div class="setting_right">
            <div class="row mt-3 px-4 py-1">
                <div class="col-12">
                    <div class="col-12 text-start">
                        <span class="head_content_left">ข้อมูลตำแหน่งงาน</span>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">สำนัก/หน่วยงาน<sup>*</sup></span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="office" class="input_left" value="{{ @Auth::user()->office_name }}"
                                placeholder="ระบุชื่อสำนัก/หน่วยงาน" required>
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">กลุ่มงาน<sup>*</sup></span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="group" class="input_left" value="{{ @Auth::user()->group }}"
                                placeholder="ระบุชื่อกลุ่มงาน" required>
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ตำแหน่ง<sup>*</sup></span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="position" class="input_left" value="{{ @Auth::user()->position }}"
                                placeholder="ระบุชื่อตำแหน่ง" required>
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ผู้รับผิดชอบ<sup>*</sup></span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="manager" class="input_left"
                                value="{{ @Auth::user()->manager_name}}" placeholder="ระบุชื่อผู้บังคับบัญชา" required>
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">เบอร์ภายใน<sup>*</sup></span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="tel_internal" class="input_left"
                                value="{{ @Auth::user()->tel_internal }}" placeholder="ระบุเบอร์โทรศัพท์ภายใน" required>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-5">
                    <div class="col-12 text-start">
                        <span class="head_content_left">เกี่ยวกับตัวฉัน</span>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ชื่อ-นามสกุล</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="name" class="input_left" value="{{ @Auth::user()->name }}"
                                placeholder="ระบุชื่อ-นามสกุล">
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ชื่อเล่น</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="nickname" class="input_left" value="{{ @Auth::user()->nickname }}"
                                placeholder="ระบุชื่อเล่น">
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">วันเกิด</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="date" name="birthday" class="input_left" value="{{ @Auth::user()->birthday }}"
                                placeholder="ระบุวันเกิด">
                            {{-- <img src="/img/icon/date-range-material-copy-4.png" alt=""
                                style="position: absolute;transform: translate(-538px, 4px);"> --}}
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-center mt-3">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ภูมิลำเนา</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" name="address" class="input_left" value="{{ @Auth::user()->address }}"
                                placeholder="ระบุที่อยู่">
                        </div>
                    </div>

                    <div class="col-12 d-flex align-items-start mt-4">
                        <div class="col-3 text-start pl-0">
                            <span class="content_left">ข้อความแนะนำตัว</span>
                        </div>
                        <div class="col-9 pr-0">
                            <textarea name="introducing" id="introducing" cols="64" rows="3"
                                placeholder="เขียนแนะนำตัวเพื่อให้ทุกคนรู้จักคุณมากขึ้น">{{ @Auth::user()->introduce }}</textarea>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="col-12 d-flex align-content-center justify-content-center mt-5" style="margin-bottom:100px;">
        {{-- <a href="#" class="cancel_updateProfile mr-3 text-center">ยกเลิก</a>
    <a href="#" class="accept_updateProfile">บันทึก</a> --}}

        <button class="cancel_updateProfile mr-3 text-center">ยกเลิก</button>
        <button type="submit" class="accept_updateProfile">บันทึก</button>
    </div>
</form>

@endsection



@section('script')
<script>

</script>
@endsection
