@extends('layout.profile')
@section('style')

<style>
    .card_booking {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 10px 25px 15px 25px;
    }

    .card_booking2 {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 5px 25px 30px 25px;
    }

    .title_form {
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    #search_board {
        width: 345px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding: 0 8px;
    }

    .head_content {
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .name_user_board {
        font-size: 16px;
        font-weight: bold;
        /* line-height: 2; */
        color: #4a4a4a;
    }

    .tag_office {
        font-size: 10px;
        font-weight: 600;
        text-decoration: underline;
    }

    .by_user {
        font-size: 10px;
        font-weight: 600;
    }

    .like_comment {
        font-size: 12px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .text_files_head {
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    #search_knowledge {
        width: 122px;
        height: 29px;
        padding: 0 5px 0 24px;
        object-fit: contain;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .head_content_card1 {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.36;
        color: #4a4a4a;
    }

</style>
@endsection

@section('content')

<div class="card_booking justify-content-center py-3 px-4">
    {{-- Head Card 1 --}}
    <div class="col-12 d-flex">
        <div class="col-6 d-flex align-content-center p-0">
            <div class="col-1 d-flex align-items-center pr-0">
                <img src="/img/icon/recording@2x.png" alt="">
            </div>
            <div class="col-10 text-start pl-0">
                <span class="title_form ml-3">Knowledge Center</span>
            </div>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end pr-0">
            <img src="/img/icon/group-45-copy.png" alt="">
        </div>
    </div>

    {{-- sub head about filters --}}
    <div class="col-12 d-flex align-items-center mt-4">
        <div class="col-2 text-start">
            <span class="text_files_head">เอกสารของฉัน</span>
        </div>
        <div class="col-10 d-flex align-items-center justify-content-end pl-0">

            {{-- Search --}}
            <img src="/img/icon/search-material-copy.png" alt=""
                style="postion:absolute; transform: translate(20px, -1px); width: 14px; height: 14px;">

            <input type="text" class="mr-2" name="search_profile_knowledge" id="search_knowledge" placeholder="ค้นหา">

            {{-- Filter Category --}}
            <select class="form-select form-select-2x mr-2" aria-label="แสดงหมวดทั้งหมด"
                style="max-width: 205px;max-height: 29px;">
                <option selected>แสดงหมวดทั้งหมด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>

            {{-- Filter office --}}
            <select class="form-select form-select-2x mr-2" aria-label="แสดงสำนักทั้งหมด"
                style="max-width: 205px;max-height: 29px;">
                <option selected>แสดงสำนักทั้งหมด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>

            {{-- sort Data --}}
            <select class="form-select form-select-2x mr-2" aria-label="เรียงจากใหม่สุด"
                style="max-width: 127px;max-height: 29px;">
                <option selected>เรียงจากใหม่สุด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>

            {{-- Filter input Files --}}
            <select class="form-select form-select-blind-text" aria-label="ระบุไฟล์แนบ"
                style="max-width: 127px;max-height: 29px;">
                <option selected>ระบุไฟล์แนบ</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

    </div>



    @for($i=1; $i<=2; $i++) <div class="col-12 d-flex mt-3">

        <div class="col-12 d-flex align-items-center">
            <div class="col-1 d-flex align-items-center justify-content-center pl-2"
                style="width: 70px;height: 70px; background-color:#a0e6b0">
                <img src="/img/icon/group-13.png" alt="">
            </div>
            <div class="col-11 d-flex  align-items-center h-100 pl-0 py-2" style="background-color: #ceced4">

                <div class="col-6 text-start">
                    <div class="col-12 pl-0">
                        <a href="#" class="tag_office">สำนักประชาสัมพันธ์และสื่อสารองค์กร</a>
                    </div>

                    <div class="col-12 pl-0">
                        <span class="head_content_card1">ขั้นตอนการส่งออก : กล้องถ่ายรูปและชื้นส่วน</span>
                    </div>
                </div>

                <div class="col-1 d-flex align-items-center px-0">
                    <div class="col-6">
                        <img src="/img/icon/attach-file-material-copy.png" alt="">
                    </div>
                    <div class="col-6">
                        <img src="/img/icon/group-20-copy-9.png" alt="">
                    </div>
                </div>

                <div class="col-3 align-items-center">
                    <div class="col-12 px-0">
                        <span class="by_user">by Wanchai Nagtang</span>
                    </div>
                    <div class="col-12 px-0">
                        <span class="by_user">15 ม.ค. 2563 09:00</span>
                    </div>
                </div>

                <div class="col-2 d-flex align-items-center">
                    <div class="col-6">
                        <img src="/img/icon/flag-material-copy.png" alt="">
                    </div>
                    <div class="col-6">
                        <img src="/img/icon/more-horiz-material-copy-7.png" alt="">
                    </div>
                </div>


            </div>
        </div>

</div>
@endfor


</div>


<div class="card_booking2 justify-content-center py-3 px-4">
    {{-- Head Card 1 --}}
    <div class="col-12 d-flex">
        <div class="col-6 d-flex align-content-center p-0">
            <div class="col-1 d-flex align-items-center pr-0">
                <img src="/img/icon/recording@2x.png" alt="">
            </div>
            <div class="col-10 text-start pl-0">
                <span class="title_form ml-3">บล้อกของฉัน</span>
            </div>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end pr-0">
            <img src="/img/icon/group-45-copy.png" alt="">
        </div>
    </div>

    <div class="row d-flex">

        @for($i = 1; $i <= 6;$i++)
        <div class="col-4 mt-4">
            <div class="col-12 px-0">
                <img src="/img/icon/rectangle-copy-32.png" alt="" style="width: 100%;height:100%; object-fit:cover;">
            </div>
            <div class="col-12 px-0 py-2" style="background-color: #eaeaea">
                <div class="col-12 text-start">
                    <span>โอกาสในวิกฤต ... ธุรกิจอาหารที่ต้องปรับตัว</span>
                </div>
                <div class="col-12 text-start mt-3">
                    <a href="#" style="text-decoration: underline;">เกษตรและอาหาร</a>
                </div>
                <div class="col-12 text-start d-flex align-items-center px-0">
                    <div class="col-6">
                        <span class="like_comment">26 ม.ค. 2564 09:00</span>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <img src="/img/icon/grade-material.png" alt="" class="mr-2">
                        <span class="like_comment mr-3">0</span>
                        <img src="/img/icon/comment-blue.png" alt="" class="mr-2">
                        <span class="like_comment mr-3">0</span>
                        <img src="/img/icon/visibility-material-bule.png" alt="" class="mr-2">
                        <span class="like_comment">6</span>
                    </div>
                </div>
                {{-- Profile Users --}}
                <div class="col-12 d-flex align-items-center text-start mt-2">
                    <div class="col-2 px-0">
                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            style="width: 26px;height: 26.2px; object-fit: contain;border: solid 1px #ffffff;border-radius:50% ">
                    </div>
                    <div class="col-10 align-items-center px-0">
                        <div class="col-12 text-start px-0">
                            <span>Darisa Patcharachot</span>
                        </div>
                        <div class="col-12 text-start px-0">
                            <span>ประชาสัมพันและสื้่อสารองค์กร</span>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    @endfor



</div>
</div>
@endsection


@section('script')

@endsection
