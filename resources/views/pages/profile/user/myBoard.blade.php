@extends('layout.profile')
@section('style')
<style>
    .card_booking {
        width: 1098px;
        height: fit-content;
        background-color: #ededed;
        margin: 10px 25px 30px 25px;
    }

    .title_form {
        font-size: 26px;
        font-weight: 600;
        color: #4a4a4a;
    }

    #search_board {
        width: 345px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding: 0 8px;
    }

    .head_content {
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .name_user_board {
        font-size: 16px;
        font-weight: bold;
        /* line-height: 2; */
        color: #4a4a4a;
    }

    .date_board {
        font-size: 12px;
        font-weight: 600;
        /* line-height: 2.67; */
        color: #4a4a4a;
    }

    .like_comment {
        font-size: 12px;
        font-weight: 600;
        color: #4a4a4a;
    }

</style>
@endsection


@section('content')

<div class="card_booking justify-content-center py-3 px-4">
    <div class="col-12 d-flex">
        <div class="col-6 d-flex align-content-center p-0">
            <div class="col-2 d-flex align-items-center">
                <img src="/img/icon/comment-blue@2x.png" alt="">
            </div>
            <div class="col-10 text-start pl-0">
                <span class="title_form">บอร์ดของฉัน</span>
            </div>
        </div>
        <div class="col-6 d-flex justify-content-end pr-0">
            <div class="col-9 d-flex align-items-center">
                {{-- <img src="/img/icon/search-material.png" alt="" style="position: absolute; color:#8a8c8e;"> --}}
                <input type="text" name="search_board" id="search_board" placeholder="ค้นหาบอร์ด">
            </div>
            <div class="col-2 d-flex align-items-center px-0">
                <img src="/img/icon/group-15.png" alt="">
            </div>
        </div>
    </div>


    @for($i=1; $i<= 3; $i++)


    <div class="col-12 d-flex mt-3">
        <div style="width: 227px; height: 135px;">
            <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                style="width:100%; height:100%; border-radius: 2px; object-fit:cover;">
        </div>
        <div class="col py-2 px3 d-flex" style="background-color: #ffffff;">

            <div class="row">
                <div class="col-12 px-3 d-flex">
                    <div class="col-1 px-0">
                        <img src="/img/icon/comment-blue.png" alt="" class="mt-1">
                    </div>
                    <div class="col-9 px-0 text-start">
                        <span class="head_content">ขอแจ้งสมาชิกทุกท่านร่วมทำบุญในกิจกรรม
                            บริจาคเงินสมทบทุนโครงการปันน้ำใจให้แมวจร</span>
                    </div>
                    <div class="col-2 text-end px-0">
                        <img src="/img/icon/more-horiz-material-copy-3.png" alt=""
                            style="background-color: #4a4a4a; cursor: pointer;">
                    </div>
                </div>

                <div class="col-12 d-flex align-items-center px-4">
                    <div class="col-1 px-0 ">
                        <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                            style="width: 38px; height: 38px; border: solid 1px #ffffff;object-fit:cover; border-radius:50%;">
                    </div>
                    <div class="col-7 px-0 text-start">
                        <div class="col-12 px-0">
                            <span class="name_user_board">Wanchai Nagtang</span>
                        </div>
                        <div class="col-12 px-0">
                            <span class="date_board ">4 มี.ค. 2564 09:40</span>
                        </div>
                    </div>
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-4 d-flex px-0 text-center">
                            <img src="/img/icon/forum-material.png" style="width: 18px; height: 18px;" alt="" class="mr-2">
                            <span class="like_comment">3</span>
                        </div>
                        <div class="col-4 d-flex px-0 text-center">
                            <img src="/img/icon/like.png" alt=""  style="width: 18px; height: 18px;" class="mr-2">
                            <span class="like_comment">55</span>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>

    @endfor

</div>
@endsection

@section('script')

@endsection
