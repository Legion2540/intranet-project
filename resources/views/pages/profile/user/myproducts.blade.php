@extends('layout.profile')
@section('style')
<style>
    .card_booking {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 10px 25px 15px 25px;
    }

    .card_booking2 {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 5px 25px 30px 25px;
    }

    .title_form {
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    #search_board {
        width: 345px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding: 0 8px;
    }

    .head_content {
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .name_user_board {
        font-size: 16px;
        font-weight: bold;
        /* line-height: 2; */
        color: #4a4a4a;
    }

    .tag_office {
        font-size: 10px;
        font-weight: 600;
        text-decoration: underline;
    }

    .by_user {
        font-size: 10px;
        font-weight: 600;
    }

    .like_comment {
        font-size: 12px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .text_files_head {
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    #search_knowledge {
        width: 122px;
        height: 29px;
        padding: 0 5px 0 24px;
        object-fit: contain;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .head_content_card1 {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.36;
        color: #4a4a4a;
    }

</style>
@endsection


@section('content')
<div class="card_booking justify-content-center py-3 px-4">
    {{-- Head Card 1 --}}
    <div class="col-12 d-flex px-0">
        <div class="col-6 d-flex align-content-center p-0">
            <div class="col-1 d-flex align-items-center px-0">
                <img src="/img/icon/widgets-material@2x.png" alt="">
            </div>
            <div class="col-10 text-start pl-0 ml-3">
                <span class="title_form">สินค้าของฉัน</span>
            </div>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end pr-0">
            <img src="/img/icon/group-45-copy.png" alt="">
        </div>
    </div>

    <div class="row d-flex">
        @for($i=1; $i<=6; $i++)
        <div class="col-4 mt-4">
            <div class="col-12 px-0">
                <img src="/img/icon/more-horiz-material-copy-7.png" alt=""
                    style="position: absolute;transform: translate(305px, 10px); cursor: pointer;">
                <img src="/img/icon/rectangle-copy-33.png" alt="" style="width: 100%;height:100%; object-fit:cover;">
                <img src="/img/icon/group-43-copy.png" alt="" style="position: absolute;width: 37px;
                height: 37px;cursor: pointer;
                transform: translate(-44px, 145px);">
            </div>
            <div class="col-12 px-0 py-2" style="background-color: #eaeaea">
                <div class="col-12 text-start">
                    <span>ขายถูกมาก surface pro 3 CPU intel core i5 ssd 128gb เสปคแรง</span>
                </div>
                <div class="col-12 text-start mt-3 d-flex">
                    <div class="col-6 text-start px-0">
                        <span class="title_form">6,900</span>
                    </div>
                    <div class="col-6 d-flex justify-content-end align-items-center">
                        <img src="/img/icon/comment-blue.png" style="width: 22.4px; height: 14px; object-fit:contain"
                            alt="" class="mr-2">
                        <span class="text_files_head">0</span>
                    </div>
                </div>
            </div>
        </div>
    @endfor
    </div>
</div>
@endsection


@section('script')

@endsection
