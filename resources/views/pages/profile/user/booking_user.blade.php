@extends('layout.profile')
@section('style')
<style>
    .text_name_type{
        white-space: nowrap; 
        width: 300px; 
        overflow: hidden;
        text-overflow: ellipsis; 
    }
    .card_booking {
        width: fit-content;
        height: fit-content;
        background-color: #ededed;
        margin: 10px 25px 30px 25px;
    }

    .btn-lend,
    .btn-lend:hover {
        width: 128px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #314d7b 99%);
        font-family: Kanit-ExtraLight;
    }

    .waiting_approve {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 4px
    }

    .approve {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        background-color: #6dd005;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        border-radius: 4px
    }

    .draft {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        background-color: #cecece;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        border-radius: 4px
    }

    .cancel_approve {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        background-color: #ee2a27;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        border-radius: 4px
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .change_booking_btn {
        background-color: #ffffff !important;
        color: #4a4a4a !important;
    }

    .change_booking_btn:focus {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .display,
    .table {
        background-color: #ffffff !important;
    }

    .title_booking {
        font-size: 20px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4f72e5;
    }

    #booking_car_btn {
        width: 128px;
        height: 30px;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: 2px 3px 4px 1px rgba(0, 0, 0, 0.1);
        background-image: linear-gradient(196deg, #4f72e5 84%, #314d7b -6%);
    }

</style>
@endsection

@section('content')

<div class="card_booking d-flex justify-content-center">
    <div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link mr-2 change_booking_btn" id="nav-home-tab" data-bs-toggle="tab"
                    data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                    aria-selected="true">จองรถส่วนกลาง</button>
                <button class="nav-link change_booking_btn active" id="nav-profile-tab" data-bs-toggle="tab"
                    data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile"
                    aria-selected="false">จองห้องประชุม</button>
                <button class="nav-link ml-2 change_booking_btn" id="nav-equipment-tab" data-bs-toggle="tab"
                    data-bs-target="#nav-equipment" type="button" role="tab" aria-controls="nav-equipment"
                    aria-selected="true">ยืมครุภัณฑ์</button>
            </div>
        </nav>

        <div class="tab-content pt-3" id="nav-tabContent" style="background-color:#ffffff;">
            <div class="tab-pane fade mx-4" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="col-12 d-flex pr-0">
                    <div class="col-4 text-start d-flex align-items-center">
                        <span class="title_booking">สถานะคำขอของฉัน</span>
                    </div>

                    <div class="col-8 align-items-center text-end pr-0">
                        <input type="search" style=" width: 280px;
                        height: 29px;
                        border-radius: 8px; border: solid 0.5px #ceced4;" placeholder="ค้นหารายการ">
                        <img src="/img/icon/search-material-bule.png" style="position: absolute;
                        left: 435px;
                        top: 6px;" alt="">
                        <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-2" alt="">
                        <button id="booking_car_btn">+ จองรถ</button>

                    </div>
                </div>


                <table class="table ">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="booking_room"></th>
                            <th>รายการคำขอใช้รถ</th>
                            <th>สถานที่</th>
                            <th>วันที่เริ่มใช้</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>ผู้อนุมัติ</th>
                            <th>สถานะ</th>
                            <th><img src="/img/icon/description-material.png" alt=""></th>
                            <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td><input type="checkbox" name="booking_room"></td>
                            <td>สัมมนาวิชาการ</td>
                            <td>โรงแรมเซนทารา แกรนด์</td>
                            <td>
                                <span>21 มีนาคม 2564</span>
                                <span>10.00 น.</span>
                            </td>
                            <td>
                                <span>21 มีนาคม 2564</span><br>
                                <span>10.00 น.</span>
                            </td>
                            <td>นายกันต์ สินธร</td>
                            <td>
                                <span class="waiting_approve">รออนุมัติ</span>
                            </td>
                            <td><img src="/img/icon/description-material.png" alt=""></td>
                            <td><img src="/img/icon/local-printshop-material-copy.png" alt=""></td>
                        </tr>


                    </tbody>
                </table>
                <div class="col-12 text-end">
                    <span>
                        < 1 >
                    </span>
                </div>
            </div>


            <div class="tab-pane fade show active mx-4" id="nav-profile" role="tabpanel"
                aria-labelledby="nav-profile-tab">
                <div class="col-12 d-flex pr-0">
                    <div class="col-4 text-start d-flex align-items-center">
                        <span class="title_booking">สถานะคำขอของฉัน</span>
                    </div>

                    <div class="col-8 align-items-center text-end pr-0">
                        <input type="search" style=" width: 280px;
                        height: 29px;
                        border-radius: 8px; border: solid 0.5px #ceced4;" placeholder="ค้นหารายการ">
                        <img src="/img/icon/search-material-bule.png" style="position: absolute;
                        left: 435px;
                        top: 6px;" alt="">
                        <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-2" alt="">
                        <button id="booking_car_btn">+ จองห้องประชุม</button>

                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="booking_room"></th>
                            <th>รายการคำขอใช้ห้องประชุม</th>
                            <th>ห้องประชุม</th>
                            <th>วันที่เริ่มใช้</th>
                            <th>คำขอเพิ่มเติม</th>
                            <th>ผู้อนุมัติ</th>
                            <th>สถานะ</th>
                            <th><img src="/img/icon/description-material.png" alt=""></th>
                            <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (empty($room))
                            <tr>
                                <td class="text-center" colspan="9">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else

                            @foreach (@$room as $item)

                                <tr>
                                    <td><input type="checkbox" name="booking_room"></td>
                                    <td>{{ @$item['objective'] }}</td>
                                    <td>{{ @$item['room_request'] != null ? @$item['room_request'] : 'VDO Conference' }}
                                    </td>

                                    <td>
                                        @if (@$item['start_date'] == @$item['end_date'])
                                            <span>{{ @$item['start_date'] }}</span>
                                        @elseif(@$item['start_date'] != @$item['end_date'])
                                            <span>{{ @$item['start_date'] . ' - ' . @$item['end_date'] }}</span>
                                        @endif

                                        <br>

                                        @if (@$item['start_time'] == @$item['end_time'])
                                            <span>{{ @$item['start_time'] }}</span>
                                        @else
                                            <span>{{ @$item['start_time'] . ' - ' . @$item['end_time'] }}</span>
                                        @endif
                                    </td>

                                    <td>

                                        @if (@$item['device_id'] != null)
                                            @foreach (@$item['device_id'] as $keydevice => $device)
                                                @if (@$keydevice >= 2)
                                                    <span>...</span>
                                                @else
                                                    <span>{{ @$device }}</span><br>
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>


                                    <td>{{ @$item['request_to'] }}</td>


                                    @if (@$item['status_booking'] == 'waiting')
                                        <td class="text-center"><span class="waiting_approve">รออนุมัติ</span></td>

                                    @elseif(@$item['status_booking'] == 'approved')
                                        <td class="text-center"><span class="approve">อนุมัติ</span></td>

                                    @elseif(@$item['status_booking'] == 'cancelled')
                                        <td class="text-center"><span class="cancel_approve">ไม่อนุมัติ</span></td>
                                    @endif



                                    <td><a href="/reserve/room/form_room1?book_id={{ @$item['id'] }}"><img
                                                src="/img/icon/description-material.png" alt=""></a></td>
                                    <td><img src="/img/icon/local-printshop-material-copy.png" alt=""></td>

                                </tr>
                            @endforeach

                        @endif


                    </tbody>
                </table>
                <div class="col-12 text-end">
                    <span>
                        < 1 >
                    </span>
                </div>
            </div>

            <div class="tab-pane fade mx-4" id="nav-equipment" role="tabpanel" aria-labelledby="nav-equipment-tab" style="min-width: 1032px">
                <div class="col-12 d-flex pr-0">
                    <div class="col-4 text-start d-flex align-items-center">
                        <span class="title_booking">สถานะคำขอของฉัน</span>
                    </div>

                    <div class="col-8 align-items-center text-end pr-0">
                        <input type="search" style=" width: 280px;
                        height: 29px;
                        border-radius: 8px; border: solid 0.5px #ceced4;" placeholder="ค้นหารายการ">
                        <img src="/img/icon/search-material-bule.png" style="position: absolute;
                        left: 435px;
                        top: 6px;" alt="">
                        <img src="/img/icon/tune-material-copy-3.png" class="mr-3 ml-2" alt="">
                        <a href="/equipment/user/lend" class="btn-lend">
                            <button class="btn-lend">+ ยืมครุภัณฑ์</button>
                        </a>

                    </div>
                </div>


                <table class="table ">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 6%">
                                <input type="checkbox" name="booking_device">
                            </th>
                            <th style="width: 30%">รายการที่ขอยืม</th>
                            <th class="text-center" style="width: 10%">จำนวนชิ้น</th>
                            <th class="text-center" style="width: 20%">เจ้าหน้าที่</th>
                            <th class="text-center" tyle="width: 15%">วันที่ขอยืม</th>
                            <th style="width: 14%">สถานะ</th>
                            <th style="width: 5%"></th>
                        </tr>
                        
                    </thead>
                    <tbody>
                        {{-- {{dd($devices)}} --}}
                        @if (is_null($devices))
                            <tr>
                                <td class="text-center" colspan="9">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else
                            @foreach ($devices as $key_booking => $item_booking)
                                <tr>
                                    <td><input type="checkbox" name="booking_device"></td>
                                    <td>
                                        <div class="text_name_type">
                                            {{$item_booking->name_type}}
                                        </div>
                                    </td>
                                    <td>{{$item_booking['count_device']}}</td>
                                    <td>{{$item_booking['who_app']}}</td>
                                    <td>{{ date('d/m/Y', strtotime($item_booking['created_at'])) }}</td>
                                    <td>
                                        @if($item_booking['status'] == 'waiting' && $item_booking['form_status'] == 1)
                                            <span class="waiting_approve" style="padding: 0px 17px">รออนุมัติ</span>
                                        
                                        @elseif($item_booking['status'] == 'waiting' && $item_booking['form_status'] == 0)
                                            <span class="draft">แบบร่าง</span>

                                        @elseif($item_booking['status'] == 'approved')
                                            <span class="approve" style="padding: 0px 24px">อนุมัติ</span>
                                        @elseif(($item_booking->status == 'cancelled') and ($item_booking->who_approve > 0))
                                            <span class="cancel_approve" style="padding: 0px 17px">ไม่อนุมัติ</span>
                                        @elseif(($item_booking->status == 'cancelled') and ($item_booking->who_approve == 0))
                                            <span class="cancel_approve" style="padding: 0px 21px">ยกเลิก</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item_booking['status'] == 'waiting' && $item_booking['form_status'] == 0)
                                            <a href="/equipment/user/list?booking_id={{ $item_booking['id'] }}">
                                                <img src="/img/icon/description-material.png" alt="">
                                            </a>
                                        @else
                                            <a href="/equipment/user/check?booking_id={{ $item_booking['id'] }}">
                                                <img src="/img/icon/description-material.png" alt="">
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="col-12 text-end">
                    <span>
                        < 1>
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection


@section('script')

@endsection
