@extends('layout.profile')
@section('style')
<style>
    .card_booking {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 10px 25px 15px 25px;
    }

    .card_booking2 {
        width: 1098px;
        height: fit-content;
        background-color: #ffffff;
        margin: 5px 25px 30px 25px;
    }

    .title_form {
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    #search_board {
        width: 345px;
        height: 34px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding: 0 8px;
    }

    .head_content {
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .name_user_board {
        font-size: 16px;
        font-weight: bold;
        /* line-height: 2; */
        color: #4a4a4a;
    }

    .tag_office {
        font-size: 10px;
        font-weight: 600;
        text-decoration: underline;
    }

    .by_user {
        font-size: 14px;
        font-weight: 600;
    }

    .like_comment {
        font-size: 12px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .text_files_head {
        object-fit: contain;
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    #search_knowledge {
        width: 122px;
        height: 29px;
        padding: 0 5px 0 24px;
        object-fit: contain;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .head_content_card1 {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.36;
        color: #4a4a4a;
    }

    .upload_videos {
        width: 140px;
        height: 30px;
        border-radius: 8px;
        border: 0px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(198deg, #4f72e5 82%, #314d7b 0%);
        color: #ffffff;
    }

    #search_album {
        width: 420px;
        height: 30px;
        padding: 0 30px 0 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #search_media {
        width: 420px;
        height: 30px;
        padding: 0 30px 0 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    #search_file {
        width: 420px;
        height: 30px;
        padding: 0 30px 0 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .head_videos {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
        line-height: 1.56;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .see-more-arrow {
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-top: 6px solid #4a4a4a;
        display: inline-block;
        transition: .2s ease;
    }

    .see-more-right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .see-more-down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

</style>
@endsection

@section('content')

<div class="card_booking justify-content-center py-3 px-4">
    {{-- Head Card 1 --}}
    <div class="col-12 d-flex px-0">
        <div class="col-6 d-flex align-content-center p-0">
            <div class="col-1 d-flex align-items-center px-0">
                <img src="/img/icon/image-collections-blue@2x.png" alt="">
            </div>
            <div class="col-10 text-start pl-0 ml-3">
                <span class="title_form">คลังของฉัน</span>
            </div>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end pr-0">
            <img src="/img/icon/group-45-copy.png" alt="">
        </div>
    </div>
    <div class="col-12 d-flex mt-2 px-0">
        <div class="col-2 d-flex align-items-center text-start">
            <span class="text_files_head">อัลบั้มของฉัน</span>
        </div>
        <div class="col-10 d-flex align-items-center justify-content-end pr-0">
            <input type="text" name="search_album" class="mr-3" id="search_album" placeholder="ค้นหารายการ">
            <img src="/img/icon/search-material-bule.png" alt=""
                style="position: absolute;transform: translate(-200px, 0px);">

            <img src="/img/icon/tune-material-copy-3.png" alt="" class="mr-3">
            <button class="upload_videos">+ อัพโหลดวิดีโอ</button>
        </div>
    </div>

    <div class="row d-flex">
        @for($i=1; $i<=7; $i++) <div class="col-3 mt-4" style="width: 229px; height: 229px;">
            <div class="col-12 px-0" style="width:100%;height:100%;">
                {{-- status files img, files, videos --}}
                <img src="/img/icon/photo-library-material-copy.png" alt=""
                    style="position: absolute;transform: translate(195px, 10px);">

                <img src="/img/icon/invalid-name-star-white.png" alt=""
                    style="position: absolute;transform: translate(18px, 193px);">
                <span style="position: absolute;transform: translate(44px, 189.5px);color: #ffffff;">0</span>

                <img src="/img/icon/invalid-name-white.png" alt=""
                    style="position: absolute;transform: translate(75px, 196px);">
                <span style="position: absolute;color:#ffffff;transform: translate(100px, 190px);">4</span>

                <img src="/img/icon/visibility-material-white.png" alt=""
                    style="position: absolute;transform: translate(127px, 196px);">
                <span style="position: absolute;color:#ffffff;transform: translate(152px, 190px);">12</span>


                <img src="/img/icon/rectangle-copy-3.png" alt="" style="width: 100%;height:100%; object-fit:cover;">
            </div>
    </div>
    @endfor

    <div class="col-12 mt-3">
        <span class="see-more-arrow mr-2"></span>
        <span>ดูทั้งหมด</span>
    </div>

</div>



</div>


<div class="card_booking justify-content-center py-3 px-4">
    {{-- Head Card 2 --}}
    <div class="col-12 d-flex px-0">
        <div class="col-2 d-flex align-content-center p-0">
            <span class="title_form">วิดีโอของฉัน</span>
        </div>
        <div class="col-10 d-flex align-items-center justify-content-end pr-0">
            <input type="text" name="search_media" class="mr-3" id="search_media" placeholder="ค้นหารายการ">
            <img src="/img/icon/search-material-bule.png" alt=""
                style="position: absolute;transform: translate(-200px, 0px);">

            <img src="/img/icon/tune-material-copy-3.png" alt="" class="mr-3">
            <button class="upload_videos">+ อัพโหลดวิดีโอ</button>
        </div>
    </div>

    <div class="row d-flex">
        @for($i=1; $i<=3; $i++) <div class="col-3 mt-4">
            <div class="col-12 px-0" style="width: 100%; height: 148px;">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt=""
                    style="width:100%;height:100%; object-fit:cover;">
            </div>
            <div class="col-12 px-2 text-start">
                <span class="head_vidoes">รายงานสถานการณ์ / โอกาสทางการค้าจีน</span>
            </div>
            <div class="col-12 px-2 text-start mt-2">
                <span class="by_user">Darisa Patcharachot</span>
            </div>

            <div class="col-12 px-2 text-start">
                <span class="by_user">18 มกราคม 2564</span>
            </div>
    </div>
    @endfor

    <div class="col-12 mt-3">
        <span class="see-more-arrow mr-2"></span>
        <span>ดูทั้งหมด</span>
    </div>

</div>



</div>


<div class="card_booking justify-content-center py-3 px-4">
    {{-- Head Card 2 --}}
    <div class="col-12 d-flex px-0">
        <div class="col-2 d-flex align-content-center p-0">
            <span class="title_form">ไฟล์ของฉัน</span>
        </div>
        <div class="col-10 d-flex align-items-center justify-content-end pr-0">
            <input type="text" name="search_file" class="mr-3" id="search_file" placeholder="ค้นหารายการ">
            <img src="/img/icon/search-material-bule.png" alt=""
                style="position: absolute;transform: translate(-200px, 0px);">

            <img src="/img/icon/tune-material-copy-3.png" alt="" class="mr-3">
            <button class="upload_videos">+ อัพโหลดวิดีโอ</button>
        </div>
    </div>

    <div class="row d-flex px-3 justify-content-around">
        @for($i=1; $i<= 9; $i++)

        <div class="col-3 mt-4 mr-2 px-2 d-flex align-items-center"
            style="border-radius:8px;background-color:#eaeaea; width: 238px; height: 52px;">
            <div class="col-2 p-2 d-flex align-items-center" style=" ">
                <img src="/img/icon/group-20-copy-9.png" alt="" style="object-fit:contain;">
            </div>
            <div class="col-8 px-1 py-1">
                <div class="col-12 d-flex align-items-center px-0">
                    <span class="three_dots">การล็อกดาวน์ช่วยลดการกระจายของเชื่อโรค</span>
                </div>
                <div class="col-12 d-flex align-items-center px-0">
                    <span>57 KB</span>
                </div>
            </div>
            <div class="col-2 d-flex align-items-center justify-content-center px-0">
                <img src="/img/icon/file-download-material-black.png" alt="">
            </div>
    </div>
    @endfor

</div>


<div class="col-12 mt-3">
    <span class="see-more-arrow mr-2"></span>
    <span>ดูทั้งหมด</span>
</div>



</div>
@endsection


@section('script')

@endsection
