@extends('layout.equipment')
@section('contentequipment')
<style>
    .input_evr {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_lend_no_device{
        width: 294px;
        height: 30px;
        border-radius: 8px;
        padding: 10px 10px;
        border: solid 0.5px #ceced4;
        background-color: rgb(243, 243, 243);
    }

    .form-select {
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-size: 14px !important;
    }

    .form-No-device{
        width: 294px;
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-size: 14px !important;
    
    }

    .num_deivce_input {
        width: 108px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .list_device {
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_device {
        width: 385px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_count_device {
        width: 200px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_btn {
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .btn-print {
        border: none;
        background: none;
    }

    .btn-back,
    .btn-back:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        margin-right: 20px;
        border: solid 0.5px #ceced4;
        background-color: #ff7f00;
    }

    .btn-cancel, .btn-cancel:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        border: solid 0.5px #ceced4;
        background-color: #ee2a27;
    }

    .btn-confirm, .btn-confirm:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        border: solid 0.5px #ceced4;
        background-color: #4f72e5;
    }

    .status {
        width: 100px;
        color: #ffffff;
        background-color: orange;
        border-radius: 10px;
    }

    .status_warning {
        width: 100px;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 10px;
    }

    .status_approval {
        width: 100px;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 10px;
    }

    .status-cancel{
            width: 100px;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 10px;
        }

    .status-not-approved{
            width: 100px;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 10px;
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        color: #4f72e5 !important;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #555555;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

</style>

<form class="form_approval_lend">
    @csrf
    <div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding-top:17px">
        <div class="row m-0 mr-5" style="padding-left: 10px;;padding-top:17px">
            <div class="col-5 p-0 pl-4">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-1">
                        <h5 class="text-head m-0 text-start">คำขอยืมครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                        <input type="text" value="{{$bookking_deveces[0]['id']}}" name="booking_id" id="booking_id" style="display: none">
                        {{-- <input type="text" value="{{Auth::user()->id}}" name="who_id_app" id="who_id_app" style="display: none"> --}}
                    </div>
                </div>
            </div>
            <div class="col-7 d-flex justify-content-end align-items-center">
                @if ($bookking_deveces[0]['status'] == 'waiting')
                    <div class="status_warning mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">รออนุมัติ</span>
                    </div>
                @elseif($bookking_deveces[0]['status'] == 'approved')
                    <div class="status_approval mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">อนุมัติ</span>
                    </div>
                @elseif (($bookking_deveces[0]['status'] == 'cancelled') && ($bookking_deveces[0]['who_approve'] > 0))
                    <div class="status-not-approved mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">ไม่อนุมัติ</span>
                    </div>
                @elseif (($bookking_deveces[0]['status'] == 'cancelled') && ($bookking_deveces[0]['who_approve'] == 0))
                    <div class="status-cancel mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">ยกเลิก</span>
                    </div>
                @endif
                <button class="btn-print">
                    <img src="/img/icon/print-material.png" alt="">
                </button>
            </div>
        </div>
        <div class="pt-0" style="padding: 67px;">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
            <div class="row row_device m-0 p-0 text-14">
                <div class="col-12 text-16 mb-1">
                    <span class="text-key">ผู้ขอยืมครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>ชื่อ-สกุล</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['name']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>สำนัก</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['office_name']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>กลุ่มงาน</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['group']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ตำแหน่ง</span>
                        </div>
                        <div class="col-10 px-0">

                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['position']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ผู้บังคับบัญชา</span>
                        </div>
                        <div class="col-10">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['manager_name']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span>เบอร์โทรศัพท์ (ภายใน)</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$user['tel_internal']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 text-16 mb-1">
                    <span class="text-key">รายละเอียดการขอยืมครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>เรื่อง</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$bookking_deveces[0]['title']}}" name="title" id="title">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>เรียน</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$bookking_deveces[0]['to']}}" name="to" id="to">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>วัตถุประสงค์</span>
                        </div>
                        <div class="col-10">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$bookking_deveces[0]['objective']}}" name="objective" id="objective">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>รูปแบบ</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_lend_check w-100 input_evr px-1" value="{{$bookking_deveces[0]['pattern']}}" name="pattern" id="pattern">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-1 mt-3">
                    <span class="text-key">รายการยืม</span>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:50px;"></th>
                            <th style="width:500px;">รายการ</th>
                            <th>หมายเลขครุุภัณฑ์</th>
                            <th class="text-center">จำนวน</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($type_devices as $key => $type_device)

                            <tr>
                                <td class="text-end" >{{$key+1}}</td>
                                <td class="text-start">{{$type_device['name']}}</td>
                                <td>
                                    @for ($i = 0; $i < $type_device['count']; $i++)
                                        <div class="col-12 px-0">
                                            @if ($bookking_deveces[0]['status'] == 'waiting')
                                                <select class="form-No-device mb-2" aria-label="Default select example" name="no_device[]" required>
                                                    <option value="">โปรดเลือกครุภัณฑ์</option>
                                                    @foreach ($type_device['deives'] as $key_device=>$item_device)
                                                        <option value="{{$item_device->id}}">{{$item_device->device_no}}</option>
                                                    @endforeach
                                                </select>
                                            @elseif ($bookking_deveces[0]['status'] == 'approved')
                                                <input type="text" class="input_lend_no_device mb-2" value="{{$type_device['show_deives'][$i]['device_no']}}" readonly>
                                            @elseif ($bookking_deveces[0]['status'] == 'cancelled')
                                                <input type="text" class="input_lend_no_device mb-2" readonly>
                                            @endif
                                        </div>
                                    @endfor
                                </td>
                                <td style="padding-left:76px ">
                                    <select class="form-select" style="width: 65px" name="count[]" aria-readonly="true">
                                        <option selected value="{{$type_device['count']}}">{{$type_device['count']}}</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th class="text-end">รวม</th>
                            <th class="text-center">{{$count_deviceall}}</th>
                            <th class="text-center">ชิ้น</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <button class="btn btn-back shadow" type="button" onclick="backpage()" style="width: 122px">ย้อนกลับ</button>
                @if ($bookking_deveces[0]['status'] == 'waiting')
                    <button class="btn btn-borrow shadow" type="submit" style="width: 122px">อนุมัติ</button>
                @endif
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        $('.input_lend_check').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.form-select').css('background-color', '#f3f3f3');


        $('.form_approval_lend').on('submit', function(e){
            // console.log($(this).find('.form-No-device').prop('required',true));
            e.preventDefault();
            
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn-confirm',
                    denyButton: 'btn-cancel',
                },
                buttonsStyling: false
            })


            swalWithBootstrapButtons.fire({
    
                text: "คุณเห็นชอบคำขอยืมใช่หรือไม่ ?",
                icon: 'warning',
                showDenyButton: true,
                confirmButtonText: 'เห็นชอบ',
                denyButtonText: `ไม่เห็นชอบ`,
                reverseButtons: true ,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/equipment/manager/lend',
                        type:'POST',
                        data: new FormData(this),
                        contentType:false,
                        cache:false,
                        processData:false,
                        success:function(datajson){
                            if(datajson.status == 'true'){
                                swalWithBootstrapButtons.fire(
                                    '',
                                    'คุณทำรายการสำเร็จแล้ว',
                                    'success'
                                )
                                status_approve();
                            }
                        }
                    })
                } else if (result.isDenied) {
                    $.ajax({
                        url: '/equipment/manager/cancel',
                        type:'POST',
                        data: new FormData(this),
                        contentType:false,
                        cache:false,
                        processData:false,
                        success:function(datajson){
                            if(datajson.status == 'true'){
                                swalWithBootstrapButtons.fire(
                                    '',
                                    'คุณทำรายการสำเร็จแล้ว',
                                    'success'
                                )
                                status_cancel();
                            }
                        }
                    })
                }
            })

        });
    });

    function status_approve(){
        $('.status_span').html('อนุมัติ');
        $('.status_warning').addClass('status_approval');
        $('.btn-borrow').hide();
        $('.form-No-device').attr('readonly', true).css('background-color', '#f3f3f3');
    }

    function status_cancel(){
        $('.status_span').html('ไม่อนุมัติ');
        $('.status_warning').addClass('status_cancel');
        $('.btn-borrow').hide();
        $('.form-No-device').attr('readonly', true).css('background-color', '#f3f3f3');
    }
    
    function backpage() {
        window.location.href = '/equipment/manager';
    }

</script>
@endsection
