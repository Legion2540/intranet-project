@extends('layout.booking')
@section('style')
<style>
    .text_name_type{
        white-space: nowrap; 
        width: 300px; 
        overflow: hidden;
        text-overflow: ellipsis; 
    }
    .waiting_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 11px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ffbf00;
    }

    .cancel_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 11px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ee2a27;
    }

    .approve_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 11px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #6dd005;
    }

    .count_num {
        font-size: 50px;
        font-weight: bold;
        text-align: center;
        color: #ffffff;
    }

    .container {
        margin-right: auto !important;
        margin-left: 270px !important;
        max-width: 1400px !important;
    }

    .text_request {
        font-size: 18px;
        font-weight: bold;
        color: #ffffff;
        margin-left: 14px;
    }

    .nav-pills .nav-item.show .nav-link,
    .nav-pills .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
        border-radius: 4px 4px 0 0;
    }

    .nav-pills .nav-link {
        background-color: #ffffff !important;
        color: #4f72e5 !important;
        border-radius: 4px 4px 0 0;
    }

    #return_equipment {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #history_equipment {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #request_equipment {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #room_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #drink_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #lunch_meal_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    .change_booking_btn {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #approved_all {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    .filter_all {
        width: 133px;
        height: 35px;
        margin: 0 20px 0 15px;
        padding: 3px 4px 3px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .form-select-1x {
        line-height: normal !important;
    }

    .search_request_equipment {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_datial_equipment {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_return_equipment {
        width: 288px;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_history_equipment {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .waiting_status {
        width: 120px;
        height: 33px;
        padding: 0 36px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 8px
    }

    .approve_status {
        width: 98px;
        height: 33px;
        padding: 0 44px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 8px
    }

    .approve_return_status {
        width: 98px;
        height: 33px;
        padding: 0 18px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 8px
    }

    .cancel_status {
        width: 98px;
        height: 33px;
        padding: 0 36px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        border-radius: 8px
    }

    .device_use_status {
        width: 120px;
        height: 33px;
        padding: 0 3px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        border-radius: 8px
    }

    .device_stock_status {
        width: 120px;
        height: 33px;
        padding: 0 36px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 8px
    }

    .thead_equipment {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        line-height: 0.94;
        color: #4a4a4a;
    }

    .export_file {
        width: 117px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }


    .add_meal,
    .add_meal:hover {
        width: 260px;
        height: 30px;
        color: #ffffff;
        border: none;
        font-size: 15px;
        text-decoration: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }

    /* Arrow for change month */
    .arrow-booking {
        border: solid black;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
        height: 1px !important;
        width: 1px !important;
    }

    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

    .filter_month {
        width: 103px;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .filter_province {
        width: 103px;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .filter_agency {
        width: 100%;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .toggle-switch:checked {
        background-color: #4BD763;
        border-color: #4BD763;
        border: none;
    }

    .toggle-switch {
        background-color: #ceced4;
        border-color: #ceced4;
        border: none;

    }

    #request_equipment_table td {
        vertical-align: middle;
        font-size: 14px;
    }

    #return_equipment_table td {
        vertical-align: middle;
        font-size: 14px;
    }

    #request_equipment_history td {
        vertical-align: middle;
        font-size: 14px;
    }

    .form-check-input {
        width: 35.7px !important;
        height: 21.7px !important;
        padding: 1px 1.1px 1.1px 15px !important;
        border-radius: 10.9px !important;
        background-color: #ffffff !important;
        border: 1 !important;
    }

    .form-check-input:checked {
        width: 35.7px !important;
        height: 21.7px !important;
        padding: 1px 1.1px 1.1px 15px !important;
        border-radius: 10.9px !important;
        background-color: #4cd964 !important;
        border: 0 !important;
    }

    .hr-head {
        padding: 0.5em 0;
        border-top: solid 1px #4f72e5 !important;
        border-bottom: solid 1px #4f72e5 !important;
        font-family: Kanit-Regular;
    }

    .hr-bottom {
        padding: 0.5em 0;
        border-bottom: solid 1px #4f72e5;
    }


    /* Edit border table >>  */
    .table thead th {
        vertical-align: middle !important;
        border:0;
    }
    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 0px solid #dee2e6;
    }
    /* << */

</style>
@endsection

@section('content')
<div class="my-4">
    <div class="row d-flex align-items-center">
        <div class="waiting_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/white-access-time-material.png" alt="">
                <span class="text_request">รออนุมัติ</span>
            </div>
        </div>

        <div class="cancel_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/times-circle-outlined-font-awesome.png" alt="">
                <span class="text_request">ไม่อนุมัติ</span>
            </div>
        </div>

        <div class="approve_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/ion-android-checkbox-outline-ionicons.png" alt="">
                <span class="text_request">อนุมัติ</span>
            </div>
        </div>
    </div>


    <div class="row mt-4" style="background-color: transparent;">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item px-2" role="presentation">
                <button class="nav-link active" id="request_equipment" data-bs-toggle="pill"
                    data-bs-target="#request_equipments" type="button" role="tab" aria-controls="room"
                    aria-selected="true" style="box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">คำขอยืมครุภัณฑ์</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="return_equipment" data-bs-toggle="pill" data-bs-target="#return_equipments"
                    type="button" role="tab" aria-controls="food" aria-selected="false" style="box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">คำขอคืนครุภัณฑ์</button>
            </li>
        </ul>

        <div class="tab-content pt-3" id="pills-tabContent" style="background-color: #ffffff;box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">
            <div class="tab-pane fade show active" id="request_equipments" role="tabpanel"
                aria-labelledby="request_equipment">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-family:Kanit-Regular; font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="font-family:Kanit-Regular;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                        </div>
                        <div class="col-6 px-0 d-flex justify-content-end align-items-center">
                            <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;margin-right: 15px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;margin-right: 15px;" alt="">
                            <input type="text" name="search_return_equipment_lend" id="search_return_equipment_lend"
                                class="search_return_equipment" placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; right:2%" alt="">
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_equipment_table">
                    <thead>
                        <tr class="text-center hr-head">
                            <th><input type="checkbox" name="key_req_equipment"></th>
                            <th class="thead_equipment text-start">ผู้ขอใช้</th>
                            <th class="thead_equipment text-start">รายการที่ขอ</th>
                            <th class="thead_equipment text-start">วัตถุประสงค์</th>
                            <th class="thead_equipment">จำนวน</th>
                            <th class="thead_equipment text-start">วันที่ขอ</th>
                            <th class="thead_equipment text-start">แผนก</th>
                            <th class="thead_equipment"><img src="/img/icon/local-printshop-material-copy-5.png" alt="">
                            </th>
                            <th class="thead_equipment text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($booking_devices_lend) == 0)
                            <tr>
                                <td class="text-center" colspan="9">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else
                            @foreach ($booking_devices_lend as $booking_device_lend)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="list_request_equipment" class="list_request_equipment">
                                    </td>
                                    <td class="text-start">{{$booking_device_lend->user_name}}</td>
                                    
                                    <td class="text-start">
                                        <a href='/equipment/manager/lend?booking_id={{$booking_device_lend->id}}'>
                                            <div class="text_name_type">
                                                {{$booking_device_lend['name_type']}}
                                            </div>
                                        </a>
                                    </td>
                                    <td class="text-start"><span>{{$booking_device_lend->objective}}</span></td>
                                    <td><span>{{$booking_device_lend->count}}</span></td>
                                    <td class="text-start"><span>{{ date('d/m/Y', strtotime($booking_device_lend['created_at'])) }}</span></td>
                                    <td class="text-start"><span>{{$booking_device_lend->group}}</span></td>​
                                    <td>
                                        @if ($booking_device_lend->status_print == 'print')
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="">
                                        @elseif($booking_device_lend->status_print == 'not print')
                                            <img src="/img/icon/local-printshop-material-copy-5.png" alt="">
                                        @endif
                                    </td>
                                    <td>
                                        @if($booking_device_lend->status == 'approved')
                                            <span class="approve_status">อนุมัติ</span>
                                        @elseif($booking_device_lend->status == 'waiting')
                                            <span class="waiting_status">รออนุมัติ</span>
                                        @elseif(($booking_device_lend->status == 'cancelled') and ($booking_device_lend->who_approve > 0))
                                            <span class="cancel_status">ไม่อนุมัติ</span>
                                        @elseif(($booking_device_lend->status == 'cancelled') and ($booking_device_lend->who_approve == 0))
                                            <span class="cancel_status" style="padding: 1px 41px">ยกเลิก</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

            </div>

            <div class="tab-pane fade" id="return_equipments" role="tabpanel" aria-labelledby="return_equipment">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-family:Kanit-Regular; font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="font-family:Kanit-Regular;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 px-0 d-flex justify-content-end align-items-center">
                            <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;margin-right: 15px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;margin-right: 15px;" alt="">
                            <input type="text" name="search_return_equipment_return" id="search_return_equipment_return"
                                class="search_return_equipment" placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; right:2%" alt="">
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="return_equipment_table">
                    <thead>
                        <tr class="text-center hr-head">
                            <th><input type="checkbox" name="key_req_equipment"></th>
                            <th class="thead_equipment text-start">ผู้ขอใช้</th>
                            <th class="thead_equipment text-start">รายการที่ขอ</th>
                            <th class="thead_equipment text-start">วัตถุประสงค์</th>
                            <th class="thead_equipment">จำนวน</th>
                            <th class="thead_equipment text-start">วันที่ขอ</th>
                            <th class="thead_equipment text-start">แผนก</th>
                            <th class="thead_equipment"><img src="/img/icon/local-printshop-material-copy-5.png" alt="">
                            </th>
                            <th class="thead_equipment text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($booking_devices_return) == 0)
                            <tr>
                                <td class="text-center" colspan="9">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else
                            @foreach ($booking_devices_return as $booking_device_return)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="list_request_equipment" class="list_request_equipment">
                                    </td>
                                    <td class="text-start">{{$booking_device_return->user_name}}</td>
                                    <td class="text-start">
                                        <a href='/equipment/manager/return?booking_id={{$booking_device_return->id}}'>
                                            <div class="text_name_type">
                                                {{$booking_device_return['name_type']}}
                                            </div>
                                        </a>
                                    </td>
                                    <td class="text-start"><span>{{$booking_device_return->objective}}</span></td>
                                    <td><span>{{$booking_device_return->count}}</span></td>
                                    <td class="text-start"><span>{{ date('d/m/Y', strtotime($booking_device_return['updated_at'])) }}</span></td>
                                    <td class="text-start"><span>{{$booking_device_return->group}}</span></td>​
                                    <td>
                                        @if ($booking_device_return->status_print == 'print')
                                            <img src="/img/icon/local-printshop-material-copy.png" alt="">
                                        @elseif($booking_device_return->status_print == 'not print')
                                            <img src="/img/icon/local-printshop-material-copy-5.png" alt="">
                                        @endif
                                    </td>
                                    <td>
                                        @if($booking_device_return->status == 'approved')
                                            <span class="approve_return_status">ส่งคืนเรียบร้อย</span>
                                        @elseif($booking_device_return->status == 'waiting')
                                            <span class="waiting_status">รออนุมัติส่งคืน</span>
                                        @elseif($booking_device_return->status == 'cancelled')
                                            <span class="cancel_status">ไม่อนุมัติส่งคืน</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="row mt-5" style="background-color: transparent;">
        <ul class="nav nav-pills ml-2" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="detail_equipments" data-bs-toggle="pill"
                    data-bs-target="#detail_equipment" type="button" role="tab" aria-controls="detail_equipment"
                    aria-selected="true" style="box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">ข้อมูลครุภัณฑ์</button>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent" style="background-color: #ffffff;box-shadow: 0 3px 8px 1px rgba(0, 0, 0, 0.08);">
            <div class="tab-pane fade show active mt-3" id="detail_equipment" role="tabpanel"
                aria-labelledby="detail_equipments">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_datial_equipment" id="search_datial_equipment"
                                class="search_datial_equipment" placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/equipment/manager/adddevice" type="button" class="add_meal text-center pt-1">+
                                ลงทะเบียนครุภัณฑ์ใหม่</a>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_equipment_history">
                    <thead>
                        <tr class="text-start hr-head">
                            <th class="text-center">
                                <input class="all_device" type="checkbox" name="key_thead">
                            </th>
                            <th class="thead_equipment text-center"></th>
                            <th class="thead_equipment">เลขครุภัณฑ์</th>
                            <th class="thead_equipment">ประเภท</th>
                            <th class="thead_equipment text-center">ผู้ครอบครอง</th>
                            <th class="thead_equipment">วันหมดอายุ</th>
                            <th class="thead_equipment text-center" colspan="3">การแสดงผล</th>
                            <th class="thead_equipment text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($devices) == 0)
                            <tr>
                                <td class="text-center" colspan="9">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else
                            @foreach ($devices as $device)
                                <tr class="text-center">
                                    <td><input type="checkbox" name="device_checkbox" class="device_checkbox"></td>
                                    <td><img src="/img/device/{{$device->img}}"
                                            style="width:80px; height:80px; object-fit:containl;" alt=""></td>
                                    <td class="text-start"><span>{{substr($device->device_no, -8)}}</span></td>
                                    <td class="text-start"><span>{{$device->no_type}}-{{$device->name_type}}</span></td>
                                    <td class="text-center"><span>{{$device->user_name}}</span></td>
                                    <td class="text-start"><span>{{ date('d/m/Y', strtotime($device['date_exp'])) }}</span></td>

                                    <td>
                                        <div class="form-check form-switch">
                                            @if ($device->display == 1)
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                            @elseif($device->display == 0)
                                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <a href="/equipment/manager/detail?device_id={{ $device['id'] }}">
                                            <img src="/img/icon/description-material.png" alt="">
                                        </a>
                                    </td>
                                    <td>
                                        <img src="/img/icon/qrcode-anticon.png" alt="" 
                                        data-name="{{$device['device_name']}}" data-device_no="{{$device['device_no']}}" data-qr="{{$device['qrcode']}}"
                                        style="width:19px;height: 19px;" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="model(this)">
                                    </td>
                                    <td>
                                        @if ($device->who_lend == 0)
                                            <span class="device_stock_status w-100">คลัง</span>
                                        @elseif($device->who_lend > 0)
                                            <span class="device_use_status w-100">อยู่ระหว่างใช้งาน</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="border border-primary d-flex justify-content-center mb-3 p-5">
                    <img src="/img/icon/QRCode.png" id="modal_qr" width="100%" height="100%" alt="">
                </div>
                <div class="row text-center">
                    <div class="col-12 mb-3">
                        <span class="modal_device_name" style="font-size: 18px;font-family: Kanit-Regular;">คอมพิวเตอร์โน้ตบุค Macbook Pro</span>
                    </div>
                    <div class="col-12">
                        <span class="modal_device_no" style="font-size: 22px;font-family: Kanit-Regular;">0000321</span>
                    </div>
                    <div class="col-12" style="font-size: 14px">
                        <span>หมายเลขประจำเครื่อง</span>
                    </div>
                </div>

            </div>
            <div class="modal-footer border-0 d-flex justify-content-center">
                {{-- <button class="btn btn-export shadow" type="submit" id="createphoto">
                    <img src="/img/icon/file-download-material.png" alt="" class="pr-2">Export
                </button> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.all_device').on('change', function () {
            if ($(this).is(':checked')) {
                $('.device_checkbox').prop('checked', true);
            } else {
                $('.device_checkbox').prop('checked', false);
            }
        })
    });

    function model(_this){
        var device_name = $(_this).data('name');
        var device_no = $(_this).data('device_no');
        var qr_code = $(_this).data('qr');

        $(document).find('.modal_device_name').text(device_name);
        $(document).find('.modal_device_no').text(device_no);
        document.getElementById("modal_qr").src = '/img/qr_device/'+ qr_code;
        
    }

    function goto_lend(){
        alert('sdfsdf');
    }

</script>
@endsection
