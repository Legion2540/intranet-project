@extends('layout.equipment')
@section('contentequipment')
<style>
    .input-serial {
        width: 93px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .input_no_device{
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        text-align: end;
    }

    .input-count {
        width: 42px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-full {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-date-full {
        position: relative;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #input-date-full1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 68%;
    }

    #input-date-full2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 72%;
    }



    .input-dateend-warranty {
        position: relative;
        width: 175px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #input-dateend-warranty::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 72%;
    }


    .textarea-other {
        resize: none;
        height: 231px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .text-upload {
        font-size: 16px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .select-type {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;

    }

    .dropFiles {
        width: 231px;
        height: 231px;
        border: 2px #4f72e5 dashed;
        text-align: center;
        font-size: 25px;
    }

    .btn-upload-photo,
    .btn-upload-photo:hover {
        width: 94px;
        height: 25px;
        color: #ffffff;
        font-size: 12px;
        padding: 0;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .btn-borrow,
    .btn-borrow:hover {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .btn-edit,
    .btn-edit:hover {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #ff7f00 1%, #ff7f00 99%);
    }

    .btn-submit {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #ffffff !important;
    }

    table tbody tr {
        border-top: 2px solid #ffffff !important;
    }

    .btn-add-no-device{
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

</style>

<form class="insert_device" enctype="multipart/form-data">
    @csrf

    {{-- {{QRcode::png('sadfsadfasdf')}} --}}
    <div class="card rounded-0 border-0" style="padding:17px 67px">
        <div class="col-12 p-0">
            <span class="text-head m-0 text-start">เพิ่มครุภัณฑ์รายการใหม่</span>
            {{-- <img src="/img/qr_device/QR-0005-027-00064014.svg" alt="" width="50px"> --}}
        </div>
        <div class="col-12 p-0 forms_adddevice">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">

            <div class="col-12 px-0 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ชื่อเครื่อง</span>
                    </div>
                    <div class="col-10 d-flex px-0">
                        <input class="input-full form-control" type="text" name="name_device" id="name_device">
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ประเภท</span>
                    </div>
                    <div class="col-10 px-0">
                        <select class="form-select select-type text-14" aria-label="Default select example"
                            name="type_device" id="type_device" onchange="change_type()">
                            <option value="0" selected>โปรดเลือกประเภท</option>
                            @foreach ($type_devices as $type_device)
                                <option value="{{$type_device->id}}">( {{$type_device->no_type}} ) {{$type_device->name_type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ยี่ห้อ/รุ่น</span>
                    </div>
                    <div class="col-10 px-0 d-flex">
                        <input class="input-full form-control" type="text" name="brand_device" id="brand_device">
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">ราคา</div>
                    <div class="col-10 px-0">
                        <input class="input-full form-control" type="text" name="price_device" id="price_device">
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-5 px-0 d-flex">
                        <div class="col-4 px-0">
                            <span>วันที่ซื้อ</span>
                        </div>
                        <div class="col-8 pl-3 pr-0">
                            <input class="form-control input-date-full" type="date" name="date_buy"
                                id="input-date-full1">
                        </div>
                    </div>
                    <div class="col-7 px-0 pl-3 d-flex">
                        <div class="col-5 px-0">
                            <span>วันหมดอายุการใช้งาน</span>
                        </div>
                        <div class="col-7 pl-3 pr-0">
                            <input class="form-control input-date-full" type="date" name="date_exp"
                                id="input-date-full2">
                        </div>
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">วันหมดประกัน</div>
                    <div class="col-10 px-0">
                        <input class="form-control input-dateend-warranty" type="date" name="date_warranty_end"
                            id="input-dateend-warranty">
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-3 px-0">
                        <span>รายละเอียดเพิ่มเติ่ม</span>
                    </div>
                    <div class="col-9 px-0">
                        <textarea class="form-control textarea-other" name="content" id="content" cols="30"
                            rows="10"></textarea>
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2">
                        {{--  --}}
                    </div>
                    <div class="col-5 px-0 d-flex">
                        <div class="dropFiles" style="padding-top: 70px">
                            <img src="/img/icon/insert-photo-material.png" alt="" width="50px"><br>
                            <span class="text-upload">Upload Image</span>
                        </div>

                        <div class="align-items-center avatar p-2"
                            style="width: 231px;height: 231px;display:none;border:solid 2px#4f72e5;">
                            <img class="profile-pic" src="" style="width: 211px;height:211px;object-fit: cover;" />
                        </div>

                    </div>
                    <div class="col-5 d-flex align-items-end">
                        <input class="file-upload-input" type='file' name="pic_device" id="pic_device"
                            onchange="uploadfile()" hidden />
                        <button class="btn-upload-photo" type="button"
                            onclick="$('.file-upload-input').trigger( 'click' )">อัพโหลดไฟล์</button>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2 d-flex">
                <span style="font-family: Kanit-Regular;">หมายเลขประจำเครื่อง</span>
            </div>

            <div class="col-12 mt-2 px-0 d-flex">
                <table class="table text-14">
                    <thead>
                        <tr style="border-bottom: 2px solid #4f72e5 !important;border-top: 2px solid #4f72e5 !important;">
                            <th></th>
                            <th style="width:930px;">ลำดับ</th>
                            <th class="text-center">หมายเลขเครื่อง</th>
                            <th class="pl-3">
                                <img src="/img/icon/bin-red.png">
                            </th>
                        </tr>
                    </thead>
                    <tbody class="results">
                        <tr class="add_no_device">
                            <td class="text-end px-0" style="vertical-align: middle"><span class="span_id">1</span></td>
                            <td class="text-start" style="vertical-align: middle">
                                <span class="text_type">โปรดเลือกประเภท</span>
                            </td>
                            <td class="text-center">
                                <input class="input_no_device" name="no_device[]" type="text" maxlength="8">
                            </td>
                            <td><button type="button" onclick="del_list(this)" style="background-color: #ffffff; border:0;"><img src="/img/icon/bin-red.png"></button></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr style="border-top: 0">
                            <td></td>
                            <td class="text-start" style="vertical-align: middle" colspan="4">
                                <button type="button" class="btn-add-no-device" onclick="clone(this)">+ เพิ่มรายการ</button>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="text-end">รวม</th>
                            <th class="text-center"><span class="text_total">1</span></th>
                            <th class="text-center">ชิ้น</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="row m-0 p-0 pt-5 pb-4 d-flex check justify-content-center mt-5">
                <button class="btn-borrow shadow" type="button" onclick="Next()" style="width: 122px">ตรวจสอบข้อมูล</button>
                <button class="btn-edit shadow mr-3" type="button" onclick="Edit()" style="width: 122px;display:none;">แก้ไข</button>
                <button class="btn-submit shadow py-0" type="submit" style="width: 122px;display:none;">เพิ่ม</button>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $(document).ready(function () {

        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.dropFiles').remove();
                    $('.profile-pic').attr('src', e.target.result);
                    $('.avatar').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload-input").on('change', function () {
            readURL(this);
        });

        $('.insert_device').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/equipment/manager/adddevice',
                type: 'POST',
                data: new FormData(this),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == 'true') {
                        Swal.fire({
                            icon: 'success',
                            title: 'บันทึกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1700
                        }).then(() => {
                            window.location.href = '/equipment/manager'
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                }

            })
        })

    });

    function Next() {
        $('input[type="date"]').attr('readonly', 'true').css('background-color', '#f3f3f3');
        $('.forms_adddevice').find('input').attr('readonly', 'true').css('background-color', '#f3f3f3');
        $('.forms_adddevice').find('textarea').attr('readonly', 'true').css('background-color', '#f3f3f3');
        $('.forms_adddevice').find('select').attr('readonly', 'true').css('background-color', '#f3f3f3');
        $('.btn-upload-photo').hide();
        $('.btn-add-no-device').hide();


        $('.btn-borrow').hide();
        $('.btn-submit').show();
        $('.btn-edit').show();

        // if ($('.btn-borrow').hasClass('afterreadonly')) {
        //     Swal.fire({
        //         icon: 'success',
        //         title: 'บันทึกสำเร็จ',
        //         showConfirmButton: false,
        //         timer: 1700
        //     }).then(() => {
        //         window.location.href = '/equipment/manager'
        //     });

        // } else {
        //     $('.btn-borrow').addClass('afterreadonly');

        // }
    }

    function Edit() {
        $('input[type="date"]').prop("readOnly", false).css('background-color', '#ffffff');
        $('.forms_adddevice').find('.input-full, .input_no_device').prop("readOnly", false).css('background-color', '#ffffff');
        $('.forms_adddevice').find('textarea').prop("readOnly", false).css('background-color', '#ffffff');
        $('.forms_adddevice').find('select').prop("readOnly", false).css('background-color', '#ffffff');
        $('.btn-upload-photo').show();
        $('.btn-add-no-device').show();


        $('.btn-borrow').show();
        $('.btn-submit').hide();
        $('.btn-edit').hide();

        // if ($('.btn-borrow').hasClass('afterreadonly')) {
        //     Swal.fire({
        //         icon: 'success',
        //         title: 'บันทึกสำเร็จ',
        //         showConfirmButton: false,
        //         timer: 1700
        //     }).then(() => {
        //         window.location.href = '/equipment/manager'
        //     });

        // } else {
        //     $('.btn-borrow').addClass('afterreadonly');

        // }
    }

    function clone(_this) {
        $('.add_no_device').last().clone().appendTo('.results').find('.input_no_device').val('');

        $('.span_id').last().text($('.span_id').length);
        $('.text_total').last().text($('.span_id').length);
    }

    function del_list(_this){
        // console.log($(_this).parents('tr').remove());

        $(_this).parents('tr').remove();
        $('.span_id').last().text($('.span_id').length);
        $('.text_total').last().text($('.span_id').length);
    }

    function change_type() {
        var text_type = $('#type_device option:selected').text();

        $('.text_type').text(text_type);
    }
</script>
@endsection