@extends('layout.equipment')
@section('contentequipment')
<style>
    .input-serial {
        width: 93px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .input_no_device{
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        text-align: end;
    }

    .input-count {
        width: 42px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-full {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-date-full {
        position: relative;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #input-date-full1::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 68%;
    }

    #input-date-full2::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 72%;
    }



    .input-dateend-warranty {
        position: relative;
        width: 175px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #input-dateend-warranty::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 72%;
    }


    .textarea-other {
        resize: none;
        height: 231px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .text-upload {
        font-size: 16px;
        color: #4f72e5;
        font-family: Kanit-Regular;
    }

    .select-type {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;

    }

    .dropFiles {
        width: 231px;
        height: 231px;
        border: 2px #4f72e5 dashed;
        text-align: center;
        font-size: 25px;
    }

    .btn-upload-photo,
    .btn-upload-photo:hover {
        width: 94px;
        height: 25px;
        color: #ffffff;
        font-size: 12px;
        padding: 0;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .btn-borrow,
    .btn-borrow:hover {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .btn-edit,
    .btn-edit:hover {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #ff7f00 1%, #ff7f00 99%);
    }

    .btn-submit {
        height: 30px;
        color: #ffffff;
        border: 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #ffffff !important;
    }

    table tbody tr {
        border-top: 2px solid #ffffff !important;
    }

    .btn-add-no-device{
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

</style>

<form class="insert_device" enctype="multipart/form-data">
    @csrf

    <div class="card rounded-0 border-0" style="padding:17px 67px">
        <div class="col-12 p-0">
            <span class="text-head m-0 text-start">เพิ่มครุภัณฑ์รายการใหม่</span>
        </div>
        <div class="col-12 p-0 forms_adddevice">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">

            <div class="col-12 px-0 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ชื่อเครื่อง</span>
                    </div>
                    <div class="col-10 d-flex px-0">
                        <input name="device_id" id="device_id" style="display: none" value="{{$device['id']}}">
                        <input class="input-full form-control" type="text" name="name_device" id="name_device" value="{{$device['name']}}">
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ประเภท</span>
                    </div>
                    <div class="col-10 px-0">
                            <input class="input-full form-control" type="text" name="type_device" id="type_device" value="{{$device['name_type']}}" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span>ยี่ห้อ/รุ่น</span>
                    </div>
                    <div class="col-10 px-0 d-flex">
                        <input class="input-full form-control" type="text" name="brand_device" id="brand_device" value="{{$device['brand']}}">
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">ราคา</div>
                    <div class="col-10 px-0">
                        <input class="input-full form-control" type="text" name="price_device" id="price_device" value="{{$device['price']}}">
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-5 px-0 d-flex">
                        <div class="col-4 px-0">
                            <span>วันที่ซื้อ</span>
                        </div>
                        <div class="col-8 pl-3 pr-0">
                            <input class="form-control input-date-full" type="date" name="date_buy" id="input-date-full1" value="{{$device['date_buy']}}">
                        </div>
                    </div>
                    <div class="col-7 px-0 pl-3 d-flex">
                        <div class="col-5 px-0">
                            <span>วันหมดอายุการใช้งาน</span>
                        </div>
                        <div class="col-7 pl-3 pr-0">
                            <input class="form-control input-date-full" type="date" name="date_exp" id="input-date-full2" value="{{$device['date_exp']}}">
                        </div>
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">วันหมดประกัน</div>
                    <div class="col-10 px-0">
                        <input class="form-control input-dateend-warranty" type="date" name="date_warranty_end" id="input-dateend-warranty" value="{{$device['date_warranty_exp']}}">
                    </div>
                </div>
            </div>

            <div class="col-12 px-0 mt-2 d-flex">
                <div class="col-6 d-flex">
                    <div class="col-3 px-0">
                        <span>รายละเอียดเพิ่มเติ่ม</span>
                    </div>
                    <div class="col-9 px-0">
                        <textarea class="form-control textarea-other" name="content" id="content" cols="30" rows="10">{{$device['content']}}</textarea>
                    </div>
                </div>
                <div class="col-6 d-flex">
                    <div class="col-2">
                        {{--  --}}
                    </div>
                    <div class="col-5 px-0 d-flex">
                        {{-- <div class="dropFiles" style="padding-top: 70px">
                            <img class="profile-picc" src="/img/device/{{$device['img']}}" style="width: 211px;height:211px;object-fit: cover;" />
                        </div> --}}

                        <div class="align-items-center avatar p-2"
                            style="width: 231px;height: 231px;border:solid 2px#4f72e5;">
                            <input name="data_img" id="data_img" style="display: none" value="{{$device['img']}}">
                            <img class="profile-pic" src="/img/device/{{$device['img']}}" name="img" style="width: 211px;height:211px;object-fit: cover;" />
                        </div>

                    </div>
                    <div class="col-5 d-flex align-items-end">
                        <input class="file-upload-input" type='file' name="pic_device" id="pic_device"
                            onchange="uploadfile()" hidden />
                        <button class="btn-upload-photo" type="button"
                            onclick="$('.file-upload-input').trigger( 'click' )">อัพโหลดไฟล์</button>
                    </div>
                </div>
            </div>


            <div class="row m-0 p-0 pt-5 pb-4 d-flex check justify-content-center mt-5">
                <button class="btn btn-edit-form shadow" type="button" style="width: 122px">ย้อนกลับ</button>
                <button class="btn-submit shadow py-0" type="submit" style="width: 122px;">บันทึก</button>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.dropFiles').remove();
                    $('.profile-pic').attr('src', e.target.result);
                    $('.avatar').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        $(".file-upload-input").on('change', function () {
            readURL(this);
        });

        $('.insert_device').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/equipment/manager/edit',
                type: 'POST',
                data: new FormData(this),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == 'true') {
                        Swal.fire({
                            icon: 'success',
                            title: 'บันทึกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1700
                        }).then(() => {
                            window.location.href = '/equipment/manager'
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                }

            })
        })

    });

</script>
@endsection