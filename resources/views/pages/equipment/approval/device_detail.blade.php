@extends('layout.equipment')
@section('contentequipment')

<style>
    .textarea-other {
        resize: none;
        margin: 10px 15px;
        height: 231px;
        border: 0;
    }
</style>

<div class="card rounded-0 mb-4 border-0" style="min-height: 650px;padding: 31px;">
        <div class="row p-0 m-0">
            <div class="col-5 p-0">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-2">
                        <h5 class="text-head text-start m-0">งานครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                        {{-- <input type="text" value="{{$booking_id}}" name="booking_id" style="display: none"> --}}
                    </div>
                </div>
            </div>
            <div class="col-7 p-0 d-flex justify-content-end align-items-center">
                <div class="has-search position-relative">
                    <input type="text" class="input-search form-control shadow-sm border border-3"
                        placeholder=" ค้นหารายการ">
                    <img class="input-search-absolute" src="/img/icon/search-material-bule.png" />
                </div>
                <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
                {{-- <a href="/equipment/user/return?device='{{base64_encode('NULL')}}&book_id={{base64_encode($booking_id)}}" class="btn btn-lend d-flex justify-content-center p-1 ml-2" type="submit">+ ขอคืนทั้งหมด</a> --}}
            </div>
        </div>
    <hr style="color: #4f72e5">

    <div class="row">
        <div class="col-12 d-flex mb-3">
            <div class="col-4 text-16" style="width:469px;height: 397px;">

                <div class="border p-3" style="width:100%;height: 100%;">
                    <div class="row">
                        <span class="text-device-name">{{$device['name']}}</span>
                        @if ($device['who_lend'] == 0)
                            <span class="text-14" style="color: #3ea90f">คลัง</span>
                        @else
                            <span class="text-warning text-14">ยืม</span>
                        @endif
                    </div>
                    <div class="d-flex justify-content-center mb-3">
                        <div style="width:304px;height: 278px;">
                            <img src="/img/icon/rectangle.png" width="100%" height="100%" style="object-fit: cover;"
                                alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            {{-- <button class="btn btn-light px-2">
                                <img src="/img/icon/reply-material.png" alt="">
                                <span class="text-14 ml-1">ส่งคืน</span>
                            </button> --}}
                        </div>
                        <div class="col-6 d-flex justify-content-end align align-items-center">
                            {{-- <img src="/img/icon/qrcode-anticon@2x.png" alt="" style="width:19px;height: 19px;" data-bs-toggle="modal" data-bs-target="#exampleModal"> --}}
                            <img src="/img/icon/qrcode-anticon.png" alt="" 
                                    data-name="{{$device['device_name']}}" data-device_no="{{$device['device_no']}}" data-qr="{{$device['qrcode']}}"
                                    style="width:19px;height: 19px;" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="model(this)">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-8 w-100 mt-3">
                <div class="col-12 d-flex">
                    <div class="pr-0" style="width:142px;">
                        <span class="text-key">ชื่อเครื่อง</span>
                    </div>
                    <div class="col-9">
                        <span>{{$device['name']}}</span>
                    </div>
                </div>


                <div class="col-12 d-flex">
                    <div class="pr-0" style="width:142px;">
                        <span class="text-key">ยี่ห้อ / รุ่น</span>
                    </div>
                    <div class="col-9">
                        <span>{{$device['brand']}}</span>
                    </div>
                </div>

                <div class="col-12 d-flex">
                    <div class="pr-0" style="width:142px;">
                        <span class="text-key">หมายเลขประจำเครื่อง</span>
                    </div>
                    <div class="col-9">
                        <span>{{$device['device_no']}}</span>
                    </div>
                </div>

                <div class="col-12 d-flex">
                    <div class="col-4 pl-0 d-flex">
                        <div class="px-0">
                            <span class="text-key">อายุการใช้งาน (เครื่อง)</span>
                        </div>
                        <div class="pl-3">
                            @php
                                $year_buy = date('Y', strtotime($device['date_buy']));
                                $year_exp = date('Y', strtotime($device['date_exp']));
                                $year = $year_exp - $year_buy;
                            @endphp
                            <span>{{$year}} ปี</span>
                        </div>
                    </div>

                    <div class="col-4 pl-5 d-flex">
                        <div class="px-0">
                            <span class="text-key">ซื้อเมื่อ</span>
                        </div>
                        <div class="pl-3">
                            <span>{{ date('d/m/Y', strtotime($device['date_buy'])) }}</span>
                        </div>
                    </div>

                    <div class="col-4 pl-0 d-flex justify-content-end">
                        <div class="col-6 px-0 text-end">
                            <span class="text-key">วันหมดอายุ</span>
                        </div>
                        <div class="col-6 px-0 text-end">
                            <span>{{ date('d/m/Y', strtotime($device['date_exp'])) }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 d-flex">
                    <div class="col-4 pl-0 d-flex">
                        <div class="col-7 px-0">
                            <span class="text-key">ราคาซื้อ</span>
                        </div>
                        <div class="col-5 pl-2">
                            <span>{{$device['price']}} บาท</span>
                        </div>
                    </div>

                    <div class="col-8 pl-5 d-flex justify-content-start">
                        <div class="col-3 px-0 text-start">
                            <span class="text-key">วันหมดประกัน</span>
                        </div>
                        <div class="col-9 px-0 text-start">
                            <span>{{ date('d/m/Y', strtotime($device['date_warranty_exp'])) }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="drop_detial_equipment cal-12 pr-3">
                        <span class="text-decoration-underline text-primary mr-2">รายละเอียดครุภัณฑ์</span>
                        <span class="drop_detial_room-arrow drop_detial_room-right"></span>
                        <div class="list_detial-equipment border col-12" style="display:none;">
                            {{-- <ul>
                                <li>เซ็นเซอร์ APS-C CMOS ความละเอียด 24.1 ล้านพิกเซล พร้อมหน่วยประมวลผลภาพ DIGIC 8</li>
                                <li>เทคโนโลยี Dual Pixel CMOS AF</li>
                                <li>ออโต้โฟกัส 143 จุด</li>
                                <li>ภาพนิ่งและภาพเคลื่อนไหว (One-Shot & Servo AF)</li>
                                <li>บันทึกวิดีโอความละเอียดสูงระดับ 4K</li>
                            </ul> --}}
                            <textarea class="textarea-other w-100" readonly>{{$device['content']}}</textarea>
                        </div>
                    </div>
                    <div class="drop_detial_equipment w-100 pr-3 mt-2">
                        <span class="text-decoration-underline text-primary mr-2">ค่าเสื่อมราคา</span>
                        <span class="drop_detial_room-arrow drop_detial_room-right"></span>
                        <div class="list_detial-equipment" style="display:none;">
                            <table class="table border table_products">
                                <tbody class="text-center">
                                    <tr class="t-head">
                                        <th class="border-right">ปี</th>
                                        <th class="border-right">การคำนวณ</th>
                                        <th class="border-right">ค่าเสื่อม</th>
                                        <th class="border-right">การสะสม</th>
                                        <th class="border-right">ค่าสุทธิ</th>
                                    </tr>
                                    <tr>
                                        <td class="border-right">2562</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="border-right">2563</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="border-right">2564</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="border-right">2565</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="border-right">2566</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mt-5 text-center">
            <a href="/equipment/manager/">
                <button class="btn btn-edit-form shadow" type="button" style="width: 122px">ย้อนกลับ</button>
            </a>
            <a href="/equipment/manager/edit?device_id={{ $device['id'] }}">
                <button class="btn btn-borrow shadow" type="button" style="width: 122px">แก้ไขข้อมูล</button>
            </a>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="border border-primary d-flex justify-content-center mb-3 p-5">
                    <img src="/img/icon/QRCode.png" id="modal_qr" width="100%" height="100%" alt="">
                </div>
                <div class="row text-center">
                    <div class="col-12 mb-3">
                        <span class="modal_device_name" style="font-size: 18px;font-family: Kanit-Regular;">คอมพิวเตอร์โน้ตบุค Macbook Pro</span>
                    </div>
                    <div class="col-12">
                        <span class="modal_device_no" style="font-size: 22px;font-family: Kanit-Regular;">0000321</span>
                    </div>
                    <div class="col-12" style="font-size: 14px">
                        <span>หมายเลขประจำเครื่อง</span>
                    </div>
                </div>

            </div>
            <div class="modal-footer border-0 d-flex justify-content-center">
                {{-- <button class="btn btn-export shadow" type="submit" id="createphoto">
                    <img src="/img/icon/file-download-material.png" alt="" class="pr-2">Export
                </button> --}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        //$('.list_detial-equipment').css('display','none')

        $('.drop_detial_equipment').on('click', function () {
            $(this).find('.drop_detial_room-right').toggleClass('drop_detial_room-down');
            $(this).find('.list_detial-equipment').toggle();

        });

        // $('.return_device_all').on('submit', function (e) {
        //     e.preventDefault();

        //     $.ajax({
        //         url: '/equipment/user/detail',
        //         type: 'POST',
        //         data: new FormData(this),
        //         cache: false,
        //         contentType: false,
        //         processData: false,
        //         success: function (data) {
        //             if (data == 'true') {
        //                 Swal.fire({
        //                     icon: 'success',
        //                     title: 'บันทึกสำเร็จ',
        //                     showConfirmButton: false,
        //                     timer: 1700
        //                 }).then(() => {
        //                     // window.location.href = '/equipment/manager'
        //                 });
        //             }
        //         },
        //         error: function (err) {
        //             console.log(err);
        //         }

        //     })
        // })
    });

    function model(_this){
        var device_name = $(_this).data('name');
        var device_no = $(_this).data('device_no');
        var qr_code = $(_this).data('qr');

        $(document).find('.modal_device_name').text(device_name);
        $(document).find('.modal_device_no').text(device_no);
        document.getElementById("modal_qr").src = '/img/qr_device/'+ qr_code;
        
    }

</script>
@endsection
