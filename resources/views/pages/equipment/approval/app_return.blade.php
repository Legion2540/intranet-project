@extends('layout.equipment')
@section('contentequipment')
<style>
    .input_evr {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .form-select {
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .num_deivce_input {
        width: 159px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .name_deivce_input{
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .list_device {
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_list_device {
        width: 294px;
        height: 30px !important;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_count_device {
        width: 55px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_btn {
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .btn-print {
        border: none;
        background: none;
    }

    .status_warning {
        width: 100px;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 10px;
    }

    .status_approval {
        width: 100px;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 10px;
    }

    .status-cancel{
            width: 100px;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 10px;
        }

    .status-not-approved{
            width: 100px;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 10px;
    }

    .btn-back, .btn-back:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        margin-right: 20px;
        border: solid 0.5px #ceced4;
        background-color: #ff7f00;
    }

    .btn-cancel, .btn-cancel:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        border: solid 0.5px #ceced4;
        background-color: #ee2a27;
    }

    .btn-confirm, .btn-confirm:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        color: #ffffff;
        padding: 0;
        border: solid 0.5px #ceced4;
        background-color: #4f72e5;
    }

</style>

<form class="form_app_return" enctype="multipart/form-data"> 
   @csrf
    <div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding-top:17px">
        <div class="row m-0 mr-5" style="padding-left: 10px;padding-top:17px">
            <div class="col-5 p-0 pl-4">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-1">
                        <h5 class="text-head m-0 text-start">คำขอคืนครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                        <input type="text" class="input_room w-100 input_evr px-1" id="book_id" name="book_id" value="{{$booking_device['id']}}" style="display: none">
                    </div>
                </div>
            </div>
            <div class="col-7 d-flex justify-content-end align-items-center">
                @if ($booking_device['status'] == 'waiting')
                    <div class="status_warning mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">รออนุมัติ</span>
                    </div>
                @elseif($booking_device['status'] == 'approved')
                    <div class="status_approval mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">อนุมัติ</span>
                    </div>
                @elseif (($booking_device[0]['status'] == 'cancelled') && ($booking_device[0]['who_approve'] > 0))
                    <div class="status-not-approved mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">ไม่อนุมัติ</span>
                    </div>
                @elseif (($booking_device[0]['status'] == 'cancelled') && ($booking_device[0]['who_approve'] == 0))
                    <div class="status-cancel mr-4 d-flex justify-content-center align-items-center">
                        <span class="status_span">ยกเลิก</span>
                    </div>
                @endif
                <button class="btn-print">
                    <img src="/img/icon/print-material.png" alt="">
                </button>
            </div>
        </div>
        <div class="pt-0 forms_app_return" style="padding: 67px;">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
            <div class="row row_device m-0 p-0 text-14">
                <div class="col-12 text-16 mb-1">
                    <span class="text-key">ผู้ขอคืนครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>ชื่อ-สกุล</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['name']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>สำนัก</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['office_name']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>กลุ่มงาน</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['group']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ตำแหน่ง</span>
                        </div>
                        <div class="col-10 px-0">

                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['position']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ผู้บังคับบัญชา</span>
                        </div>
                        <div class="col-10">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['manager_name']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span>เบอร์โทรศัพท์ (ภายใน)</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$user['tel_internal']}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 text-16 mb-1">
                    <span class="text-key">รายละเอียดการขอคืนครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>เรื่อง</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$booking_device['title']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>เรียน</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$booking_device['to']}}">
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>วัตถุประสงค์</span>
                        </div>
                        <div class="col-10">
                            {{-- <select class="form-select text-14" aria-label="Default select example">
                                <option selected>ขอใช้ชั่วคราว</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select> --}}
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$booking_device['objective']}}">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>รูปแบบ</span>
                        </div>
                        <div class="col-10 px-0">
                            {{-- <select class="form-select text-14" aria-label="Default select example">
                                <option selected>ชั่วคราว</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select> --}}
                            <input type="text" class="input_room w-100 input_evr px-1" value="{{$booking_device['pattern']}}">
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-0 mt-3 d-flex text-16 align-items-center">
                    <span class="text-key">รายการขอคืน</span>
                </div>

                @foreach ($device as $key_device => $item_device)
                    <div class="col-12 d-flex mt-2 align-items-center add_device">
                        <div class="col-3 px-0 d-flex mr-4">
                            <div class="col-3 px-0 d-flex align-items-center mr-3">
                                <span>เลขครุภัณฑ์</span>
                            </div>
                            <div class="col-9 px-0">
                                <input type="text" class="input_room num_deivce_input px-1" value="{{$item_device['device_no']}}">
                            </div>
                        </div>
                        <div class="col-9 px-0 d-flex">
                            <div class="col-1 px-0 d-flex align-items-center">
                                <span>รายการ</span>
                            </div>
                            <div class="col-11 px-0">
                                {{-- <select class="form-select text-14 select_list_device" aria-label="Default select example">
                                    <option selected>กล้อง</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select> --}}
                                <input type="text" class="input_room name_deivce_input px-1" value="{{$item_device['name']}}">
                            </div>
                        </div>
                    </div>
                @endforeach

                {{-- results clone --}}
                <div class="p-0 results"></div>

            </div>
            <div class="row m-0 p-0 d-flex check justify-content-center mt-5">
                <button class="btn btn-back shadow" onclick="backpage()" type="button" style="width: 122px">ย้อนกลับ</button>
                @if ($booking_device['status'] == 'waiting')
                    <button class="btn btn-borrow shadow" type="submit" style="width: 122px">อนุมัติ</button>
                @endif
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.forms_app_return').find('input').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('.forms_app_return').find('select').attr('disabled', 'true').css('background-color', '#f3f3f3');

        $('.form_app_return').on('submit', function (e) {
            e.preventDefault();
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                    confirmButton: 'btn-confirm',
                    cancelButton: 'btn-cancel'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
    
                text: "คุณเห็นชอบคำขอคืนใช่หรือไม่ ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ไม่ใช่',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/equipment/manager/return',
                        type: 'POST',
                        data: new FormData(this),
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == 'true') {
                                $('.status_span').html('อนุมัติ');
                                $('.status_warning').addClass('status_approval');
                                $('.btn-borrow').hide();
                                // location.reload();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                        }

                    })
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    // 
                }
            })
        });
    });

    // function approve(_this) {

    //     var book_id = $(_this).parents('.card').find('#book_id').val();
    //     var this_test = $(_this).parents('.form_app_return');
    //     // console.log($(_this).parents('.card').find('#token').val());
    //     const swalWithBootstrapButtons = Swal.mixin({
    //         customClass: {
    //             confirmButton: 'btn-confirm',
    //             cancelButton: 'btn-cancel'
    //         },
    //         buttonsStyling: false
    //     })

    //     swalWithBootstrapButtons.fire({
   
    //         text: "คุณเห็นชอบคำขอคืนใช่หรือไม่ ?",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonText: 'ใช่',
    //         cancelButtonText: 'ไม่ใช่',
    //         reverseButtons: true
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             this_test.preventDefault();
    //             $.ajax({
    //                 url: '/equipment/manager/return',
    //                 type:'POST',
    //                 data: { id : '5' },
    //                 contentType:false,
    //                 cache:false,
    //                 processData:false,
    //                 success:function(datajson){
    //                     if(datajson.status == 'true'){
    //                         swalWithBootstrapButtons.fire(
    //                             '',
    //                             'คุณทำรายการสำเร็จแล้ว',
    //                             'success'
    //                         )
    //                         $('.status_span').html('อนุมัติ');
    //                         $('.status_warning').addClass('status_approval');
    //                         $('.btn-borrow').hide();
    //                         // window.location.href="/equipment";
    //                     }
    //                 }
    //             })


    //         } else if (
    //             /* Read more about handling dismissals below */
    //             result.dismiss === Swal.DismissReason.cancel
    //         ) {
    //             // 
    //         }
    //     })
    // }

    function backpage() {
        window.location.href = '/equipment/manager';
    }

</script>
@endsection
