@extends('layout.equipment')
@section('contentequipment')

{{-- <form class="device_lend" enctype="multipart/form-data"> --}}
    {{-- @csrf --}}
    <div class="card rounded-0 mb-4 border-0" style="min-height: 650px;padding: 31px;">
        <div class="row p-0 m-0">
            <div class="col-5 p-0">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-2">
                        <h5 class="text-head text-start m-0">งานครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                    </div>
                </div>
            </div>
            <div class="col-7 p-0 d-flex justify-content-end align-items-center">
                <div class="has-search position-relative">
                    <input type="text" class="input-search form-control shadow-sm border border-3"
                        placeholder=" ค้นหารายการ">
                    <img class="input-search-absolute" src="/img/icon/search-material-bule.png" />
                </div>
                <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
                <a href="/equipment/user/lend" class="btn btn-lend d-flex justify-content-center p-1 ml-2">+ ขอยืม</a>
            </div>
        </div>

        <table id="example" class="table mt-4" id="data_table">
            <thead>
                <tr class="text-start hr-head">
                    <th class="text-center" style="width: 6%">
                        {{-- <input class="all_device" type="checkbox" name="all_device"> --}}
                    </th>
                    <th class="thead_equipment" style="width: 40%">รายการที่ขอยืม</th>
                    <th class="thead_equipment text-center" style="width: 10%">จำนวนชิ้น</th>
                    <th class="thead_equipment text-center" style="width: 15%">เจ้าหน้าที่</th>
                    <th class="thead_equipment text-center" tyle="width: 15%">วันที่ขอยืม</th>
                    <th class="thead_equipment" style="width: 7%"></th>
                    <th class="thead_equipment text-center" style="width: 7%"></th>
                </tr>
            </thead>
            <tbody> 
                    @if ($device_lend != NULL)
                        @foreach ( $device_lend as $key_device_lend => $item_device_lend)
                            <tr class="text-center">
                                <td class="text-start"  colspan="3">
                                    <div class="col-12 w-100 mark_parents pl-2">
                                        <div class="col-12 px-0 d-flex">
                                            <div class="col-1">
                                                <input class="checkbox_order_device" type="checkbox" name="checkbox_order_device" value="{{ $item_device_lend['booking_id']}}">
                                            </div>
                                            <div class="col-10 drop_detial_equipment">
                                                <span class="mr-2">รายการที่ขอยืมครั้งที่ {{$key_device_lend +1 }}</span>
                                                <span class="drop_detial_room-arrow drop_detial_room-right"></span>
                                            </div>
                                            <div class="col-1">
                                                <span>{{$item_device_lend['count_device']}}</span>
                                            </div>
                                        </div>
                                        <div class="col-12 list_detial-equipment" style="display:none;">
                                            @foreach ( $item_device_lend['content'] as $key_device_content => $item_device_content)
                                                <div class="col-12 px-0 d-flex">
                                                    <div class="col-1 px-0">
                                                        <input class="checkbox_device" type="checkbox" name="order_id[{{$item_device_lend['booking_id']}}][{{$item_device_content['device_id']}}]" value="{{$item_device_content['device_id']}}">
                                                    </div>
                                                    <div class="col-10">
                                                        <span>{{$item_device_content['device_no']}}-{{$item_device_content['name']}}</span>
                                                    </div>
                                                    <div class="col-1" style="padding-left: 28px">
                                                        <span>1</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center"><span>{{$item_device_lend['who_app']}}</span></td>
                                <td class="text-center"><span>{{$item_device_lend['date_lend']}}</span></td>
                                <td class="text-center">
                                    <button class="bg-white border-0" type="button" onclick="to_detail({{$item_device_lend['booking_id']}})">
                                        <img src="/img/icon/description-material.png" alt="">
                                    </button>
                                </td>
                                <td class="text-center">
                                    <a class="btn_returnDevice">
                                        <button  class="btn-lend-mini vertical-center" type="button">
                                            <img src="/img/icon/reply-material-white.png"/>
                                            <span>ส่งคืน</span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
            </tbody>
        </table>
    </div>
{{-- </form> --}}


@endsection

@section('script')
<script>
    $(document).ready(function () {

        // $('.all_device').on('change', function () {
        //     if ($(this).is(':checked')) {
        //         $('.checkbox_order_device').prop('checked', true);
        //     } else {
        //         $('.checkbox_order_device').prop('checked', false);
        //     }
        // })
        


        $('.checkbox_order_device').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).parents('.mark_parents').find('.checkbox_device').prop('checked', true);

                var data = [];
                var device_arr = [];
                var device_id = $(this).parents('.mark_parents').find('.checkbox_device:checked');
                var book_id = $(this).val();
            
                $.each(device_id, function(key,data){
                    device_arr.push($(this).val());
                });

                $(this).parents('tr').find('.btn_returnDevice').attr('href','/equipment/user/return?device='+ btoa(device_arr)+'&book_id='+btoa(book_id));

            } else {
                $(this).parents('.mark_parents').find('.checkbox_device').prop('checked', false);
                $(this).parents('tr').find('.btn_returnDevice').attr('href','');

            }
        })

        $('.checkbox_device').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).parents('.mark_parents').find('.checkbox_order_device').prop('checked', true);

                var data = [];
                var device_arr = [];
                var device_id = $(this).parents('.mark_parents').find('.checkbox_device:checked');
                var book_id = $(this).parents('.mark_parents').find('.checkbox_order_device').val();
            
                $.each(device_id, function(key,data){
                    device_arr.push($(this).val());
                });

                $(this).parents('tr').find('.btn_returnDevice').attr('href','/equipment/user/return?device='+ btoa(device_arr)+'&book_id='+btoa(book_id));

            } else {
                // $(this).parents('.mark_parents').find('.checkbox_device').prop('checked', false);
                // $(this).parents('tr').find('.btn_returnDevice').attr('href','');

            }
        })

        $('.drop_detial_equipment').on('click', function () {
            $(this).parents('.mark_parents').find('.drop_detial_room-right').toggleClass('drop_detial_room-down');
            $(this).parents('.mark_parents').find('.list_detial-equipment').toggle();
        });

 

        // $('.btn_returnDevice').on('click', function (e) {
        //     e.preventDefault();
        //     // console.log($(this).parents('tr').find('.checkbox_order_device option:selected')).val();
        // });









       // $('.device_lend').on('submit', function (e) {
        //     e.preventDefault();

        //     $.ajax({
        //         url: '/equipment',
        //         type: 'POST',
        //         data: new FormData(this),
        //         cache: false,
        //         contentType: false,
        //         processData: false,
        //         success: function (data) {
        //             if (data.status == 'true') {
        //                 if (data.order != 'null'){
        //                     window.location.href = '/equipment/user/return?order='+ data.order;
        //                 }else{
        //                     alert('กรุณาเลือกรายการครุภัณฑ์');
        //                 }
        //             }
        //         },
        //         error: function (err) {
        //             console.log(err);
        //         }

        //     })
        // });

    });

    function to_detail(booking_id){
        window.location.href="/equipment/user/detail?booking_id="+ booking_id;
    }

</script>
@endsection
