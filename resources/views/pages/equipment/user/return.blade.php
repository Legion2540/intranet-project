@extends('layout.equipment')
@section('contentequipment')
<style>
    .input_evr {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_return {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
        width: 100%;
    }


    .form-select {
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .num_deivce_input {
        width: 159px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        /* background-color: #ffffff; */
        background-color: #f3f3f3;
    }

    .list_device {
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .name_type_input{
        width: 294px;
        height: 30px !important;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        /* background-color: #ffffff; */
        background-color: #f3f3f3;
    }

    .select_count_device {
        width: 55px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_btn {
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .btn-print{
        border: none;
        background: none;
    }

    .status{
        width: 100px;
        color: #ffffff;
        background-color:orange;
        border-radius: 10px;
    }

</style>


<form class="form_return">
    @csrf
    <div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding-top:17px">
        <div class="row m-0 mr-5" style="padding-left: 10px;padding-top:17px">
            <div class="col-5 p-0 pl-4">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-1">
                        <h5 class="text-head m-0 text-start">คำขอคืนครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                        <input type="text" class="input_room num_deivce_input px-1" id="book_id" name="book_id" value="{{$booking_devices->id}}" style="display: none">
                    </div>
                </div>
            </div>
            <div class="col-7 d-flex justify-content-end align-items-center">
                <div class="status mr-4 d-flex justify-content-center align-items-center">
                    <span class="status_span" style="display: none">รออนุมัติ</span>
                </div>
                <button class="btn-print" style="display: none">
                    <img src="/img/icon/print-material.png" alt="">
                </button>
            </div>
        </div>
        <div class="pt-0 cover_all_form" style="padding: 67px;">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
            <div class="row row_device m-0 p-0 text-14">
                <div class="col-12 text-16 mb-1">
                    <span class="text-key">ผู้ขอคืนครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>ชื่อ-สกุล</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_return px-1" value="{{Auth::user()->name}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>สำนัก</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_return px-1" value="{{Auth::user()->office_name}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>กลุ่มงาน</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_return px-1" value="{{Auth::user()->group}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ตำแหน่ง</span>
                        </div>
                        <div class="col-10 px-0">

                            <input type="text" class="input_return px-1" value="{{Auth::user()->position}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ผู้บังคับบัญชา</span>
                        </div>
                        <div class="col-10">
                            <input type="text" class="input_return px-1" value="{{Auth::user()->manager_name}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span>เบอร์โทรศัพท์ (ภายใน)</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_return px-1" value="{{Auth::user()->tel_internal}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 text-16 mb-1">
                    <span class="text-key">รายละเอียดการขอคืนครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>เรื่อง</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_room w-100 input_evr px-1" name="title" id="title" required>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>เรียน</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_room w-100 input_evr px-1" name="to" id="to" required>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>วัตถุประสงค์</span>
                        </div>
                        <div class="col-10">
                            {{-- <select class="form-select text-14" aria-label="Default select example">
                                <option selected>ขอใช้ชั่วคราว</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select> --}}
                            <input type="text" class="input_room w-100 input_evr px-1" name="objective" id="objective" required>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>รูปแบบ</span>
                        </div>
                        <div class="col-10 px-0">
                            {{-- <select class="form-select text-14" aria-label="Default select example">
                                <option selected>ชั่วคราว</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select> --}}
                            <input type="text" class="input_room w-100 input_evr px-1" name="pattern" id="pattern" required>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-0 mt-3 d-flex text-16 align-items-center">
                    <span class="text-key">รายการขอคืน</span>
                </div>


                @foreach ($device as $key=>$item)
                    <div class="col-12 d-flex mt-2 align-items-center add_device">
                            <div class="col-3 px-0 d-flex mr-4">
                                <div class="col-4 px-0 d-flex align-items-center">
                                    <span>เลขครุภัณฑ์</span>
                                </div>
                                <div class="col-8 px-0">
                                    <input type="text" class="input_room num_deivce_input px-1" id="no_device" name="no_device[]" value="{{$item->device_no}}" readonly>
                                    <input type="text" class="input_room num_deivce_input px-1" id="device_id" name="device_id[]" value="{{$item->id}}" style="display: none">
                                </div>
                            </div>
                            <div class="col-9 px-0 d-flex">
                                <div class="col-1 px-0 d-flex align-items-center">
                                    <span>รายการ</span>
                                </div>
                                <div class="col-11 px-0">
                                    <input type="text" class="input_room name_type_input px-1" id="name_type" name="name_type[]" value="{{$item->name_type}}" readonly>
                                    <input type="text" class="input_room name_type_input px-1" id="type_id" name="type_id[]" value="{{$item->type_id}}" style="display: none">
                                </div>
                            </div>
                    </div>
                @endforeach

                {{-- results clone --}}
                <div class="p-0 results"></div>

            </div>
            <div class="row m-0 p-0 d-flex check justify-content-center mt-5">
                <button class="btn btn-backpage shadow" type="button" onclick="backpage()" style="width: 122px">ย้อนกลับ</button>
                <button class="btn btn-edit-form shadow" type="button" onclick="edit()" style="width: 122px;display:none;">แก้ไข</button>
                <button class="btn btn-borrow shadow" id="btn-next" type="button" onclick="Next()" style="width: 122px">ถัดไป</button>
                <button class="btn btn-borrow shadow" id="btn-return" type="submit" style="width: 122px;display:none;">ส่งคำขอคืน</button>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        $('.form_return').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: '/equipment/user/return',
                type:'POST',
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(datajson){
                    if(datajson.status == 'true'){
                        Swal.fire({
                        icon: 'success',
                        title: 'ส่งคำขอคืนครุภัณฑ์สำเร็จ',
                        showConfirmButton: false,
                        timer: 1700
                    });
                        window.location.href="/equipment";
                    }
                }
            })

        });
    });

    function clone() {
        $('.add_device').last().clone().appendTo('.results');
        $('.add_btn').closest(".add_btn").not(':last').last().remove();
    }

    function Next() {
        $(".add_btn").hide();
        $('#btn-return').show();
        $('#btn-next').hide();


        $('.check .btn-backpage').css('display','none');
        $('.check .btn-edit-form').css('display','block');
        if ($('.btn-borrow').hasClass('afterreadonly')) {
            $('.status_span').css('display','block');
            $('.btn-print').css('display','block');

            $('.check .btn-borrow').css('display','none');
            $('.check .btn-edit-form').css('display','none');
            $('.check .btn-backpage').css('display','block');
        }else{
            $('.btn-borrow').addClass('afterreadonly');
        }

        $('.input_room').attr('readonly', true).css('background-color', '#f3f3f3');
        // $('select').attr('disabled', 'true').css('background-color', '#f3f3f3');
        $('textarea').attr('disabled', 'true').css('background-color', '#f3f3f3');
    }

    function backpage(){
        window.location.href='/equipment';
    }

    function edit(){
        show_display();
        $(".add_btn").show();
        // $('.btn-borrow').addClass('afterreadonly');
        $('.btn-borrow').removeClass('afterreadonly');
    }

    function show_display(){
        $('#btn-return').hide();
        $('#btn-next').show();
        $('.btn-backpage').show();
        $('.btn-edit-form').hide();

        $('.input_room').attr('readonly', false).css('background-color', '#ffffff');
        $('select').attr('disabled', false).css('background-color', '#ffffff');
        $('textarea').attr('disabled', false).css('background-color', '#ffffff');

        $('.num_deivce_input').attr('readonly', true).css('background-color', '#f3f3f3');
        $('.name_type_input').attr('readonly', true).css('background-color', '#f3f3f3');

        // btn control
        // $('.check .btn-edit-form').css('display','none');
        // $('.check .btn-borrow').css('display','block');
    }

</script>
@endsection
