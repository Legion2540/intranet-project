@extends('layout.equipment')
@section('contentequipment')
<style>
    .input_evr {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .form-select {
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .num_deivce_input {
        width: 108px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .list_device {
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_device {
        width: 385px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_count_device {
        width: 200px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_btn {
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        color: #4f72e5 !important;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #555555;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

</style>

<form class="form_2">
    @csrf
    <div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding-top:17px">
        <div class="row m-0" style="padding-left: 10px;;padding-top:17px">
            <div class="col-5 p-0 pl-4">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-1">
                        <h5 class="text-head m-0 text-start">คำขอยืมครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                        <input type="text" value="{{$booking_id}}" name="booking_id" id="booking_id" style="display: none">
                    </div>
                </div>
            </div>
        </div>
        <div class="pt-0" style="padding: 67px;">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
            <div class="row row_device m-0 p-0 text-14">
                <div class="col-12 my-3" style="padding: 0 300px;">
                    <ul class="timeline">
                        <li class="bf_active"></li>
                        <li class="active">เลือกรายการ</li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div class="col-12 text-16 mb-1">
                    <span class="text-key">ผู้ขอยืมครุภัณฑ์คอมพิวเตอร์</span>
                </div>
            </div>
            <table class="table text-14">
                <thead>
                    <tr>
                        <th class="text-center" style="width:50px;"></th>
                        <th style="width:50px;"></th>
                        <th style="width:930px;">รายการ</th>
                        <th class="text-center">จำนวน</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($type_devices as $key=>$type_device)
                        <tr>
                            <td class="text-center" style="vertical-align: middle;">
                                @if ($type_device->count_device == 0)
                                    <span style="color: red">หมด</span>
                                @else
                                    <input type="checkbox" name="" class="checkbox_test">
                                @endif
                            </td>
                            <td class="text-end" style="vertical-align: middle">{{$key+1}}</td>
                            <td class="text-start" style="vertical-align: middle">
                                <span>{{$type_device->name_type}}</span>
                            </td>
                            <td style="padding-left:60px;!important">
                                <select class="form-select" style="width: 65px;pointer-events: none;background-color: #ceced4;" name="type_id[{{$type_device->id}}]">
                                    @for ($i = 0; $i <= $type_device->count_device; $i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </td>
                            <td></td>

                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-end">จำนวนรวม</th>
                        <th class="text-center text_total">0</th>
                        <th class="text-center">ชิ้น</th>
                    </tr>
                </tfoot>
            </table>
            <div class="d-flex justify-content-center mt-5">
                <button class="btn btn-cancel shadow" type="button" onclick="back(this)">ย้อนกลับ</button>
                <button class="btn btn-borrow shadow" type="submit" style="width: 122px">ถัดไป</button>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.form_2').on('submit', function(e){
            // var booking_id = $(this).parents('.card').find('.form-select').val();

            var total_check = 0;
            $('select option:selected').each(function (key, value) {
                total_check += parseInt($(this).val());

                $('.text_total').text(total_check);
            });

            if(total_check > 0){
                e.preventDefault();
                $.ajax({
                    url: '/equipment/user/list',
                    type:'POST',
                    data: new FormData(this),
                    contentType:false,
                    cache:false,
                    processData:false,
                    success:function(datajson){
                        if(datajson.status == 'true'){
                            window.location.href="/equipment/user/check?booking_id="+ datajson.booking_id;
                        }
                    }
                })
            }else{
                Swal.fire({
                    position: 'center-center',
                    icon: 'warning',
                    title: 'โปรดเลือกจำนวนครุภัณฑ์',
                    showConfirmButton: false,
                    timer: 1500,
                })
            }

        });


        $('.checkbox_test').on('change', function () {
            
            if ($(this).prop('checked') == true) {
                $(this).parents('tr').find('select').css({'pointer-events':'auto','background-color': '#ffffff'});
                // // $(this).parents('tr').find('select').attr("disabled", false);


                var total = 0;
                $('select').change(function () {
                    total = 0;
                    var select = $(this).val();
            
                    $('select option:selected').each(function (key, value) {
                        total += parseInt($(this).val());

                        $('.text_total').text(total);
                    });
                });

            } else {
              
                $(this).parents('tr').find('select').css({'pointer-events':'none','background-color': '#ceced4'});
                // $(this).parents('tr').find('select').attr("disabled", true);
                $(this).parents('tr').find('select').val(0);

                var total = 0;
                $('select option:selected').each(function (key, value) {
                  
                    total += parseInt($(this).val());
                    $('.text_total').text(total);
                });
            }
        });
    });

    function back(_this) {
        // var booking_id = $(_this).parents('.form_2').find('#booking_id').val();

        // window.location.href = "/equipment/user/lend?booking_id="+ booking_id;

        window.history.back();
    }

</script>
@endsection
