@extends('layout.equipment')
@section('contentequipment')
<style>
    .input_lend_personal {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .input_lend_details {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .form-select{
        height: 30px !important;
        padding: 4px 11px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        font-size: 14px !important;
    }

    .num_deivce_input {
        width: 108px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .list_device {
        width: 294px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_device {
        width: 385px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select_count_device {
        width: 200px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .add_btn {
        width: 112px;
        height: 30px;
        padding: 4px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .timeline{
    counter-reset: year 0;
    position: relative;
    }

    .timeline li{
    list-style: none;
    float: left;
    color: #4f72e5 !important;
    width: 25%;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1){
    color: #4f72e5;
    }

    .timeline li:before{
    counter-increment: year;
    content: counter(year);
    width: 50px;
    height: 50px;
    border: 3px solid #4f72e5;
    border-radius: 50%;
    display: block;
    text-align: center;
    line-height: 50px;
    margin: 0 auto 10px auto;
    background: #ffffff;
    color: #4f72e5;
    transition: all ease-in-out .3s;
    cursor: pointer;
    font-size:25px; 
    position: relative;
    z-index: 1;
    }

    .timeline li:after{
    content: "";
    position: absolute;
    width: 100%;
    height: 3px;
    background-color: #4f72e5;
    top: 25px;
    left: -50%;
    z-index: 0;
    transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after{
    content: none;
    }
    .timeline li.active{
    color: #555555;
    }
    .timeline li.active:before{
    background: #4f72e5;
    width: 70px;
    height: 70px;
    padding-top: 7px;
    border: 3px solid;
    font-size: 40px;
    box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
    color: #ffffff;
    position: relative;
    z-index: 1;
    bottom: 10px;
    }

    .timeline li.active + li:after{
    background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active{
    color: #555555;
    }
    .timeline li.bf_active:before{
    background: #4f72e5;
    width: 50px;
    height: 50px;
    border: 3px solid;
    font-size: 25px;
    box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
    color: #ffffff;
    position: relative;
    z-index: 1;
    }

    .timeline li.bf_active + li:after{
    background: #4f72e5;
    }
</style>

<form class="form_1">
    @csrf
    <div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding-top:17px">
        <div class="row m-0" style="padding-left: 10px;;padding-top:17px">
            <div class="col-5 p-0 pl-4">
                <div class="row d-flex justify-content-start align-items-center">
                    <div class="col-1">
                        <img src="/img/icon/desktop-mac-material.png" style="width: 23px" />
                    </div>
                    <div class="col-11 pl-1">
                        <h5 class="text-head m-0 text-start">คำขอยืมครุภัณฑ์คอมพิวเตอร์ของฉัน</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="pt-0" style="padding: 67px;">
            <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
            <div class="row row_device m-0 p-0 text-14">
                <div class="col-12 my-3" style="padding: 0 300px;">
                        <ul class="timeline">
                        <li class="active">รายละเอียดการยืม</li>
                        <li></li>
                        <li></li>
                        <li></li>
                        </ul>
                </div>
                <div class="col-12 text-16 mb-1">
                    <span class="text-key">ผู้ขอยืมครุภัณฑ์คอมพิวเตอร์</span>
                    
                    <input type="text" value="{{@$bookking_deveces[0]->id}}" name="booking_id" id="booking_id" style="display: none">
                    <input type="text" value="{{Auth::user()->id}}" name="user_id" id="user_id" style="display: none">
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>ชื่อ-สกุล</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->name}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>สำนัก</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->office_name}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>กลุ่มงาน</span>
                        </div>
                        <div class="col-11">
                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->group}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ตำแหน่ง</span>
                        </div>
                        <div class="col-10 px-0">

                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->position}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>ผู้บังคับบัญชา</span>
                        </div>
                        <div class="col-10">
                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->manager_name}}" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span>เบอร์โทรศัพท์ (ภายใน)</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_lend_personal w-100  px-1" value="{{Auth::user()->tel_internal}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 text-16 mb-1">
                    <span class="text-key">รายละเอียดการขอยืมครุภัณฑ์คอมพิวเตอร์</span>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-1 px-0">
                            <span>เรื่อง</span>
                        </div>
                        <div class="col-11">
                            {{-- <select class="form-select" aria-label="Default select example" name="title" id="title">
                                <option selected value="ขออนุมัติยืมครุภัณฑ์คอมพิวเตอร์">ขออนุมัติยืมครุภัณฑ์คอมพิวเตอร์</option>
                                <option value="2">xxxxx</option>
                            </select> --}}
                            <input type="text" class="input_lend_details w-100  px-1" name="title" id="title" value="{{@$bookking_deveces[0]->title}}" required>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>เรียน</span>
                        </div>
                        <div class="col-10 px-0">
                            <input type="text" class="input_lend_details w-100  px-1" name="to" id="to" value="{{@$bookking_deveces[0]->to}}" required>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-2 d-flex align-items-center">
                    <div class="col-6 pl-0 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>วัตถุประสงค์</span>
                        </div>
                        <div class="col-10">
                            {{-- <select class="form-select" aria-label="Default select example" name="objective" id="objective">
                                <option selected value="ขอใช้ประจำตำแหน่ง">ขอใช้ประจำตำแหน่ง</option>
                                <option value="ขอใช้ชั่วคราว">ขอใช้ชั่วคราว</option>
                            </select> --}}
                            <input type="text" class="input_lend_details w-100  px-1" name="objective" id="objective" value="{{@$bookking_deveces[0]->objective}}" required>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span>รูปแบบ</span>
                        </div>
                        <div class="col-10 px-0">
                            {{-- <select class="form-select" aria-label="Default select example" name="pattern" id="pattern">
                                <option selected value="ประจำตำแหน่ง">ประจำตำแหน่ง</option>
                                <option value="ชั่วคราว">ชั่วคราว</option>
                            </select> --}}
                            <input type="text" class="input_lend_details w-100  px-1" name="pattern" id="pattern" value="{{@$bookking_deveces[0]->pattern}}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <button class="btn btn-borrow shadow" type="submit" style="width: 122px">ถัดไป</button>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        
        $('.form_1').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: '/equipment/user/lend',
                type:'POST',
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(datajson){
                    if(datajson.status == 'true'){
                        window.location.href="/equipment/user/list?booking_id="+ datajson.booking_id;
                    }
                }
            })
        });

    });

</script>
@endsection
