@extends('layout.equipment')
@section('contentequipment')

<div class="card rounded-0 mb-4 border-0" style="min-height: 756px;padding: 67px;padding-top:17px">
    <div class="row p-0 m-0">
        <div class="col-5 p-0">
            <div class="row d-flex justify-content-start align-items-center">
                <div class="col-12">
                    <h5 class="fw-bold text-head m-0 text-start">คำขอส่งซ่อมบำรุง</h5>
                </div>
            </div>
        </div>
    </div>
    <hr style="border-top: 1px dashed #8c8b8b;background-color: white;">
    <div class="row m-0 p-0 text-14">
        <div class="col-12 d-flex">
            <div class="col-6 m-0 p-0 ">
                <div class="m-0 mb-2 p-0 d-flex align-items-center">
                    <div class="m-0 p-0" style="width: 83px">
                        <span><b>เลขครุภัณฑ์</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <input type="text" class="form-control" style="width: 100%;height: 30px;">
                    </div>
                </div>
                <div class="m-0 mb-2 p-0 d-flex align-items-center">
                    <div class="m-0 p-0" style="width: 83px">
                        <span><b>ยี่ห้อ/รุ่น</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <input type="text" class="form-control" style="width: 100%;height: 30px;">
                    </div>
                </div>
                <div class="m-0 mb-2 p-0 d-flex align-items-center">
                    <div class="m-0 p-0" style="width: 83px">
                        <span><b>ผู้ส่งซ่อม</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <input type="text" class="form-control" style="width: 100%;height: 30px;">
                    </div>
                </div>
                <div class="m-0 mb-2 p-0 d-flex ">
                    <div class="m-0 p-0" style="width: 83px">
                        <span><b>อาการ</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <textarea class="form-control" id="validationTextarea" style="height: 60px"></textarea>
                    </div>
                </div>

            </div>
            <div class="col-6 m-0 ml-3 pr-3 p-0">
                <div class="m-0 mb-5 p-0 d-flex w-100 align-items-center">
                    <div class="m-0 p-0 d-flex" style="width: 115px">
                        <span><b>ประเภทครุภัณฑ์</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <select class="form-select" style="width: 100%;height: 30px;font-size:14px">
                            <option selected value="1">คอมพิวเตอร์โน้ตบุค</option>
                            <option value="2">คอมพิวเตอร์โน้ตบุค</option>
                            <option value="3">คอมพิวเตอร์โน้ตบุค</option>
                        </select>
                    </div>
                </div>
                <div class="m-0 p-0 d-flex w-100 align-items-center">
                    <div class="m-0 p-0 d-flex" style="width: 80px">
                        <span><b>หน่วยงาน</b></span>
                    </div>
                    <div class="ml-0 pl-0" style="width: 100%">
                        <input type="text" class="form-control" style="width: 100%;height: 30px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center mt-5">
        <button class="btn btn-borrow shadow" type="submit" id="createphoto" style="width: 122px">ตรวจสอบข้อมูล</button>
    </div>
</div>

@endsection

@section('script')

@endsection
