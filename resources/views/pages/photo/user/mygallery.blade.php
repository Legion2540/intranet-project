@extends('layout.photo')
@section('contentphoto')
<div class="card shadow-sm rounded-0 p-4 mb-4 border-0" style="min-height: 650px">
    <div class="row p-0 m-0">
        <div class="col-4">
            <div class="row m-0">
                <div class="col-1 d-flex justify-content-end align-items-center p-1">
                    <img src="/img/icon/image-collections-blue@2x.png" style="width: 20px" />
                </div>
                <div class="col-11 ps-1 pr-0">
                    <h3 class="text-head m-0">คลังภาพของฉัน</h3>
                </div>
            </div>
        </div>
        <div class="col-8 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class="form-control-feedback">
                        <img src="/img/icon/search-material-bule.png" />
                    </span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <a href="/photo/user/upload/album" class="btn-create d-flex justify-content-center p-1">+ สร้างอัลบั้ม</a>
        </div>
    </div>
    <div class="row m-0 p-1 pb-0 pt-3">
        <table class="table table-borderless mb-0">
            <tbody>
                @for ($j = 0; $j < 3; $j++)
                    <tr>
                        @for ($i = 0; $i < 4; $i++)
                            <td>
                                <div class="card p-0 m-0">
                                    <img class="pos-absolute-top" src="/img/icon/photo-library-material-copy.png" alt="">
                                    <div class="pos-absolute-center">
                                        <span class="mr-2 text-img"><img class="mb-1 mr-1" src="/img/icon/grade-material.png"
                                                alt="">5</span>
                                        <span class="mr-2 text-img"><img class="mr-1" src="/img/icon/comment-blue.png"
                                                alt="">12</span>
                                        <span class="mr-2 text-img"><img class="mr-1" src="/img/icon/visibility-material-bule.png"
                                                alt="">9999</span>
                                    </div>
                                    <img src="/img/icon/rectangle-copy-3@3x.png" class="img-fluid"
                                        style="background-color: rgb(71, 71, 71); width:203px;height:203px;object-fit: cover;">
                                </div>
                            </td>
                        @endfor
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>
<div>
    <div class="row next-bar">
        <div class="col-4 d-flex justify-content-start align-items-center">
            <span>แสดงผล 7 จากทั้งหมด 7</span>
        </div>
        <div class="col-4 d-flex justify-content-center align-items-center">
            <button type="button" class="btn btn-next" disabled>หน้าถัดไป &rarr;</button>
        </div>
        <div class="col-4 pr-5 next-back">
            <div class="row d-flex justify-content-end">
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                    style="height: 28px; width:33px;font-size:11px;margin-right: 10px;">
                <div class="d-flex align-items-center px-0" style="width: 15%">
                    <span>จาก 1</span>
                </div>
                <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                    <button type="button" class="btn btn-next">&lt;</button>
                    <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
