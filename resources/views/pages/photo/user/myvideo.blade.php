@extends('layout.photo')
@section('contentphoto')
<div class="card shadow-sm rounded-0 p-4 mb-4 border-0" style="min-height: 650px">
    <div class="row p-0 m-0">
        <div class="col-4">
            <div class="row m-0">
                <div class="col-1 d-flex justify-content-end align-items-center p-1">
                    <img src="/img/icon/attach-video-material-bule.png" style="width: 20px" />
                </div>
                <div class="col-11 ps-1 pr-0">
                    <h3 class="text-head m-0">คลังวีดีโอของฉัน</h3>
                </div>
            </div>
        </div>
        <div class="col-8 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class="form-control-feedback">
                        <img src="/img/icon/search-material-bule.png" />
                    </span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <button class="btn color-botton shadow" type="submit">+
                อัพโหลดวีดีโอ</button>
        </div>
    </div>
    <div class="row m-0 p-1 pb-0 pt-4">
        @for ($i = 0; $i < 6; $i++)
            <div class="col-3 p-1">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                </div>
                <div class="pl-1 pr-1">
                    <p class="mb-0 text-titel-video">รายงานสถานการณ์/โอกาศทางการค้าจีน</p>
                    <span style="font-size: 14px">Draisa Patcharachot</span>
                    <p style="font-size: 14px">17 มกราคม 2564</p>
                </div>
            </div>
        @endfor
    </div>
</div>
<div>
    <div class="row next-bar">
        <div class="col-4 d-flex justify-content-start align-items-center">
            <span>แสดงผล 3 จากทั้งหมด 3</span>
        </div>
        <div class="col-4 d-flex justify-content-center align-items-center">
            <button type="button" class="btn btn-next" disabled>หน้าถัดไป &rarr;</button>
        </div>
        <div class="col-4 pr-5 next-back">
            <div class="row d-flex justify-content-end">
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="1"
                    style="height: 28px; width:33px;font-size:11px;margin-right: 10px;">
                <div class="d-flex align-items-center px-0" style="width: 15%">
                    <span>จาก 1</span>
                </div>
                <div class="row justify-content-end pl-0 ml-0" style="width: 22%">
                    <button type="button" class="btn btn-next">&lt;</button>
                    <button type="button" class="btn btn-next" style="margin-left: 1px">&gt;</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
