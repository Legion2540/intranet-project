@extends('layout.uploadimage')
@section('contentuploadphoto')
<style>
    .position-relative .position-absolute-photoup{
        position: absolute;
        top: 25%;
    }

    .areadrop{
        width: 80px;
        height:80px;
        object-fit:contain; 
    }
    .col-form-label{
        font-size: 16!important;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .input-name{
        width: 518px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .textarea-detail{
        resize:none;
        width: 518px;
        height: 168px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .select-album{
        width: 215px;
        height: 30px;
        padding: 2px 8px 4px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .img-del{
        position: absolute;
        top: 5%;
        right: 15%;
        cursor: pointer;
    }

</style>
<div class="card rounded-0 mb-4 border-0" style="min-height: 650px;padding: 31px;">
    <div class="row p-0 m-0">
        <div class="col-5 p-0">
            <div class="row d-flex justify-content-start align-items-center">
                <div class="col-1 pl-4 py-0">
                    <img src="/img/icon/add-a-photo-material@2x.png" style="width: 23px" />
                </div>
                <div class="col-11">
                    <h5 class="text-head m-0 text-start">อัพโหลดรูปภาพ</h5>
                </div>
            </div>
        </div>
        <div class="col-7 p-0 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class="form-control-upload">
                        <img src="/img/icon/search-material-bule.png" />
                    </span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <a href="/photo/user/upload/album" class="btn btn-create d-flex justify-content-center p-1">+
                สร้างอัลบั้ม</a>
            <a href="/photo/user/upload/photo" class="btn btn-create d-flex justify-content-center p-1 ml-2">+
                อัพโหลดรูป</a>
        </div>
    </div>
    <hr style="color: #4f72e5">
    <div class="row m-0 p-1 pb-0 pt-4">
        <div class="col-5 dorp px-5 position-relative">
            <div class="w-100 h-100">
                <div class="dropArea w-100 h-100" style="border-style: dashed;cursor:pointer;border-color: #4f72e5;">
                    <div class="custom-file position-relative mb-3 w-100 h-100">
                        <input type="file" class="img-upload-input custom-file-input" id="customFile" name="filename"
                            accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple style="width: 100%;height: 100%;">
                        <button type="submit" id="createalbum" class="btn-upload-photo shadow" hidden>อัพโหลด</button>
                        <div class="row position-absolute-photoup" style="padding-left: 24%;padding-right: 24%;">
                            <div class="col-12 mb-3 d-flex justify-content-lg-center">
                                <img src="/img/icon/insert-photo-material.png" alt="" width="50" height="50">
                            </div>
                            <div>
                                <div class="row">
                                    <span style="font-size:20px;color:#4f72e5;text-align:center">Choose file from your
                                        device or drag image here.</span>
                                    <span class="d-flex justify-content-center" style="font-size:14px;color:#ee2a27;">(bmp,
                                        gif, jpg, jpeg, png)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-5 list_photo pl-3">
            <div class="pl-3" style="width: 500px;height: 480px;">
                <div class="row d-flex avatar">
                    {{-- <div class="col-3 mt-3" style="width: 80px;height:80px; object-fit:contain;">
                        <img src="/img/icon/rectangle.png" width="100%" height="100%"/>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="col-7 pl-4">
            <div class="row pl-2 pr-5 mr-1 align-items-center">
                <div class="row mb-2">
                    <div class="col-2 pr-0">
                        <label for="text" class="col-form-label">ชื่อรูป</label>
                    </div>
                    <div class="col-10 pl-0 ml-0 py-1">
                        <input type="text" id="inputPassword6" class="form-control input-name">
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 pr-0">
                        <label for="text" class="col-form-label">เพิ่มในอัลบั้ม</label>
                    </div>
                    <div class="col-10 pl-0 ml-0 py-1">
                        <select class="form-select select-album selectpicker" aria-label="Default select example">
                            <option selected data-content="<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEqSURBVHjaYvzPgAD/GBiYaxkY/oARkPMLzPiFwgAIwKG80wAAwlAADAnq0MNWGTipvfcpheWmmy9GNGUPey/RkskiWwP6mhDy5BVAYA0M//8/ecrw9y8QPfj8++/fv7//A9UBFYHIX39//fr3C8iQ5ZEF2gAQQCxgs/9DVAOVrXRe+R8HYGRkfKj6ECCAWIBOZQSqBjuB4fef/7gByJP//gEEENhJf0BmE9bAyPjnzx+AAAI76S/I+P+/f+HXwAi2BCCAwE4CGv/rF8hVv37hcQ+QBDoJIIBAGphBxv9m+A0KG2tr61+/fv3+/RtKgll//vz+8+eviorKxYsXAQIIpIHp9x8GSUmQnj9/Zf78hgCgcyEkEPyFAaA9AAHE+BUcp3AkzEAAAAQYAHrGaQbZnz4fAAAAAElFTkSuQmCC'> hi ">อัลบั้ม</option>
                            <option value="1">กิจกรรมวันปีใหม่</option>
                            <option value="2">อบรมแผนกลยุทธ์การตลาด</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 pr-0">
                        <label for="text" class="col-form-label">รายละเอียด</label>
                    </div>
                    <div class="col-10 pl-0 ml-0 py-1">
                        <textarea class="form-control textarea-detail" id="validationTextarea"></textarea>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-2 pr-0">
                        <label for="text" class="col-form-label">แชร์</label>
                    </div>
                    <div class="col-10 pl-0 ml-0 py-1">
                        <div class="dropdown">
                            <button
                                class="btn btn-uploads-dropdown dropdown-toggle p-2 d-flex justify-content-start align-items-center"
                                type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="/img/icon/public-material.png" alt="">
                                <span class="pl-2 w-100 d-flex align-items-start">ทุกคน</span>
                            </button>
                            <ul class="dropdown-menu py-1" aria-labelledby="dropdownMenuButton1" style="width: 35%;">
                                <li><a class="dropdown-item pl-2" href="#"><img class="mr-2"
                                            src="/img/icon/public-material.png" alt="">ทุกคน</a></li>
                                <li><a class="dropdown-item pl-2" href="#"><img class="mr-2"
                                            src="/img/icon/group-material.png" alt="">เฉพาะหน่วย...</a></li>
                                <li><a class="dropdown-item pl-2" href="#"><img class="mr-2"
                                            src="/img/icon/person-material.png" alt="">เฉพาะฉัน</a></li>
                                <li><a class="dropdown-item pl-2" href="#"><img class="mr-2"
                                            src="/img/icon/settings-material.png" alt="">กำหนดเอง</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2 pr-0">

                    </div>
                    <div class="col-10 pl-0 ml-0 py-1 mt-3">
                        <button type="submit" id="upload" class="btn w-25 color-botton shadow mr-2">อัพโหลด</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script>
    var countimg = 0; 
    $(document).ready(function () {
        $('.list_photo').hide();
        $('.dorp').show();

        $('#upload').on('click', function () {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    content: 'sweetalert-album',
                    confirmButton: 'btn btn-modal-comfirm ml-3',
                    cancelButton: 'btn btn-modal-close'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                text: "คุณต้องการที่จะอัพโหลดรูปภาพหรือไม่ ?",
                icon: 'warning',
                iconColor: 'red',
                showCancelButton: true,
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire({
                        text: "คุณอัพโหลดรูปภาพสำเร็จ",
                        icon: 'success',
                        confirmButtonText: 'ตกลง',
                        reverseButtons: true
                    })
                }
            })
        });


        // upload img
        var store_img = [];
        countimg = 0; 

        var readURL = function(input) {
            countimg += input.files.length;
            if(input.files.length <= 10 && countimg <= 10){
                $('.list_photo').show();
                $('.dorp').hide();

                $.each(input.files,function(key,img_input){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        store_img.push(e.target.result);
                        $('.avatar').append('<div class="col-3 mt-3 areadrop position-relative">'+
                                '<img class="profile-pic" width="100%" height="100%" style="image-rendering: pixelated;"/>'+
                                '<img class="img-del" src="/img/icon/close-material.png" onclick="remove_img(this)"/>' +
                            '</div>');

                            $('.class_add_photo').remove();

                            $('.avatar').append('<div class="class_add_photo col-3 mt-3" style="width: 80px;height:80px; object-fit:contain;cursor:pointer;">'+
                                '<div class="w-100 h-100 d-flex justify-content-center align-items-center" style="border-style: dashed;border-color: #4f72e5;" onclick="add_photo()">'+
                                '<img src="/img/icon/add-to-photos-material.png">'+
                                '</div>'+
                            '</div>');

                        $.each(store_img,function(key, value){
                            $('.profile-pic').last().attr('src', value);                        
                        });
                    }
                    reader.readAsDataURL(input.files[key]);
                });
            }else{
                countimg -= input.files.length;
                alert('Maximum 10 file');
            }
        }
        $(".img-upload-input").on('change', function(){
            readURL(this);
        });

        $(".btn-upload-photo").on('click', function() {
            $(".img-upload-input").click();
        });
    });

    function add_photo(){
        $(".img-upload-input").click();
    }

    function remove_img(_this){
        $(_this).parents('.areadrop').remove();
        countimg -= 1;
    }

    

</script>
@endsection
