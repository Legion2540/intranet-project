@extends('layout.photo')
@section('contentphoto')

{{-- card Gallery start--}}
<div class="card shadow rounded-0 p-4 mb-4 border-0">
    <div class="row pb-3 m-0">
        <div class="col-6 p-0">
            <div class="row m-0">
                <div class="col-1 d-flex justify-content-end align-items-center p-1">
                    <img src="/img/icon/image-collections-blue@2x.png" style="width: 22px"/>
                </div>
                <div class="col-11 ps-1 pr-0">
                    <h3 class="text-head m-0">คลังภาพ</h3>
                </div>
            </div>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row m-0">
        <div class="col-4 d-flex justify-content-start align-items-center">
            <span class="text-titel-photo m-0">อัลบั้มล่าสุด</span>
        </div>
        <div class="col-8 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class=" form-control-feedback"><img
                            src="/img/icon/search-material-bule.png" /></span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <a href="/photo/user/upload/album" class="btn btn-create p-1">+ สร้างอัลบั้ม</a>
        </div>
    </div>
    <div class="row m-0 p-1 pb-0 pt-3">
        <table class="table table-borderless mb-0">
            <tbody>
                @for ($j = 0; $j < 3; $j++)
                    <tr>
                        @for ($i = 0; $i < 4; $i++)
                            <td>
                                <div class="card p-0 m-0">
                                    <img class="pos-absolute-top" src="/img/icon/photo-library-material-copy.png" alt="">
                                    <div class="pos-absolute-center">
                                        <span class="mr-2"><img class="mb-1 mr-1" src="/img/icon/invalid-name-star-white.png"
                                                alt="">5</span>
                                        <span class="mr-2"><img class="mr-1" src="/img/icon/invalid-name-white.png"
                                                alt="">12</span>
                                        <span class="mr-2"><img class="mr-1" src="/img/icon/visibility-material-white.png"
                                                alt="">9999</span>
                                    </div>
                                    <img src="/img/icon/rectangle-copy-3@3x.png" class="img-fluid"
                                        style="background-color: rgb(71, 71, 71); width:203px;height:203px;object-fit: cover;">
                                </div>
                            </td>
                        @endfor
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>
{{-- card Gallery stop--}}

{{-- card  video start--}}
<div class="card shadow rounded-0 pt-4 pl-4 pb-4 pr-4 mb-4 border-0">
    <div class="row pb-3 m-0">
        <div class="col-6 p-0">
            <div class="row m-0">
                <div class="col-1 d-flex justify-content-end align-items-center p-1">
                    <img src="/img/icon/search-material.png" style="background-color: rgb(4, 0, 214);width:22px" />
                </div>
                <div class="col-11 ps-1 pr-0">
                    <h3 class="text-head m-0">คลังวีดีโอ</h3>
                </div>
            </div>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row m-0">
        <div class="col-4 d-flex justify-content-start align-items-center">
            <h5 class="text-titel-photo m-0">วีดีโอล่าสุด</h5>
        </div>
        <div class="col-8 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class=" form-control-feedback"><img
                            src="/img/icon/search-material-bule.png" /></span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <a href="#" class="btn btn-create p-1">+ อัพโหลดวีดีโอ</a>
        </div>
    </div>
    <div class="row m-0 p-1 pb-0 pt-4">
        <table class="table table-borderless mb-0">
            <tbody>
                @for ($a = 0; $a < 2; $a++)
                    <tr>
                        @for ($i = 0; $i < 4; $i++)
                            <td class="p-0 px-1">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=c9B4TPnak1A"
                                        allowfullscreen></iframe>
                                </div>
                                <div class="px-2">
                                    <p class="mb-0 text-titel-video">การหารือการขยายตลาดสินค้ากับผู้ประกอบการร้านอาหารไทย</p>
                                    <span style="font-size: 14px;">Draisa Patcharachot</span>
                                    <p style="font-size: 14px">18 มกราคม 2564</p>
                                </div>
                            </td>
                        @endfor
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>
{{-- card  video stop--}}

{{-- card  documents start--}}
<div class="card shadow rounded-0 pt-4 pl-4 pb-4 pr-4 mb-4 border-0">
    <div class="row pb-3 m-0">
        <div class="col-6 p-0">
            <div class="row m-0">
                <div class="col-1 d-flex justify-content-end align-items-center p-1">
                    <img src="/img/icon/attach-file-material-bule.png" />
                </div>
                <div class="col-11 ps-1 pr-0">
                    <h3 class="text-head m-0">คลังไฟล์</h3>
                </div>
            </div>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>
    <div class="row m-0">
        <div class="col-4 d-flex justify-content-start align-items-center">
            <h5 class="text-titel-photo m-0">ไฟล์ล่าสุด</h5>
        </div>
        <div class="col-8 d-flex justify-content-end align-items-center">
            <div class="has-search">
                <div class="row justify-content-end" style="padding-life:0; padding-right:16px">
                    <span class=" form-control-feedback"><img
                            src="/img/icon/search-material-bule.png" /></span>
                </div>
                <input type="text" class="input-search form-control shadow-sm border border-3"
                    placeholder=" ค้นหารายการ">
            </div>
            <img src="/img/icon/tune-material-copy-3.png" class="ms-3 me-3 w-auto" />
            <a href="#" class="btn btn-create p-1">+ อัพโหลดไฟล์</a>
        </div>
    </div>
    <div class="row m-0 p-1 pb-0 pt-4">
        <table class="table table-borderless mb-0">
            <tbody>
                @for ($i = 0; $i < 2; $i++)
                    <tr>
                        <td>
                            <div style="border-radius:3px;background-color: #eaeaea;">
                                <div class="row p-2 m-0">
                                    <div class="col-2 p-0 m-0">
                                        <img src="/img/type_files/group-20-copy-9@2x.png" alt="">
                                    </div>
                                    <div class="col-8">
                                        <p class="m-0 textover-documents" style="font-size: 14px ;width: inherit;">
                                            การล็อกดาวน์ช่วยกระทำให้ดีขึ้นจากเดิมมาก</p>
                                        <p class="m-0" style="font-size: 9px">57 KB</p>
                                    </div>
                                    <div class="col-1 p-0 ml-3 d-flex align-items-center justify-content-center">
                                        <img src="/img/icon/file-download-material-black.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="border-radius:3px;background-color: #eaeaea;">
                                <div class="row p-2 m-0">
                                    <div class="col-2 p-0 m-0">
                                        <img src="/img/type_files/group-20-copy-11@2x.png" alt="">
                                    </div>
                                    <div class="col-8">
                                        <p class="m-0 textover-documents" style="font-size: 14px ;width: inherit;">
                                            การล็อกดาวน์ช่วยกระทำให้ดีขึ้นจากเดิมมาก</p>
                                        <p class="m-0" style="font-size: 9px">21 KB</p>
                                    </div>
                                    <div class="col-1 p-0 ml-3 d-flex align-items-center justify-content-center">
                                        <img src="/img/icon/file-download-material-black.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="border-radius:3px;background-color: #eaeaea;">
                                <div class="row p-2 m-0">
                                    <div class="col-2 p-0 m-0">
                                        <img src="/img/type_files/group-20-copy-9@2x.png" alt="">
                                    </div>
                                    <div class="col-8">
                                        <p class="m-0 textover-documents" style="font-size: 14px ;width: inherit;">
                                            การล็อกดาวน์ช่วยกระทำให้ดีขึ้นจากเดิมมาก</p>
                                        <p class="m-0" style="font-size: 9px">21 KB</p>
                                    </div>
                                    <div class="col-1 p-0 ml-3 d-flex align-items-center justify-content-center">
                                        <img src="/img/icon/file-download-material-black.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
</div>
{{-- card  documents stop--}}

@endsection
