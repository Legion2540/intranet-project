@extends('layout.booking')
@section('style')
    <style>
        .waiting_approve {
            width: 100%;
            height: auto;
            padding: 0px 26px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #ffbf00;
            border-radius: 10px;
        }

        .approve {
            width: 100%;
            height: auto;
            padding: 0px 26px;
            object-fit: contain;
            background-color: #6dd005;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            border-radius: 10px;
        }

        .cancel_approve {
            width: fit-content;
            height: auto;
            padding: 1px 16px;
            object-fit: contain;
            background-color: #ee2a27;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            border-radius: 10px;
        }

        .nav-tabs .nav-item.show .nav-link,
        .nav-tabs .nav-link.active {
            background-color: #4f72e5 !important;
            color: #ffffff !important;
        }

        .change_booking_btn {
            background-color: #4f72e5 !important;
            color: #ffffff;
            font-family: Kanit-ExtraLight;
            font-size: 20px;
            font-weight: bold;
        }

        .change_not_btn {
            background-color: #ffffff !important;
            color: #4f72e5;
            font-family: Kanit-ExtraLight;
            font-size: 20px;
            font-weight: bold;
        }

        .display,
        .table {
            background-color: #ffffff !important;
        }

        .title_booking {
            font-size: 20px;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4f72e5;
        }

        #booking_car_btn {
            width: 128px;
            height: 30px;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #ffffff;
            border-radius: 8px;
            box-shadow: 2px 3px 4px 1px rgba(0, 0, 0, 0.1);
            background-image: linear-gradient(270deg, #4f72e5 9%, #314d7b 111%) !important;
        }

        .date_appr_booking {
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            width: 103px;
            height: 30px;
            color: #4f72e5;


        }

        /* Button span for Show Popover */
        .filter_car .popover {
            width: 100% !important;
            height: fit-content !important;
            max-width: 360px !important;
        }

        .filter_room .popover {
            width: 100% !important;
            height: fit-content !important;
            max-width: 360px !important;
        }

        .filter_calendar_room .popover {
            width: 100% !important;
            height: fit-content !important;
            max-width: 360px !important;
        }


        /* Popover CSS */
        .car_popover_content .text_filter {
            font-size: 14px;
            font-weight: 500;
            color: #4f72e5;
        }

        .room_popover_content .text_filter {
            font-size: 14px;
            font-weight: 500;
            color: #4f72e5;
        }

        .room_calendar_popover_content .text_filter {
            font-size: 14px;
            font-weight: 500;
            color: #4f72e5;
        }


        /* Arrow for change month */
        .arrow-booking {
            border: solid black;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 3px;
            margin: 0px 8px 1px 0px;
            height: 1px !important;
            width: 1px !important;
        }

        .left {
            transform: rotate(135deg);
            -webkit-transform: rotate(135deg);
        }

        .right {
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
        }

        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .filter_month {
            width: 103px;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .filter_province {
            width: 103px;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .filter_agency {
            width: 100%;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

    </style>
@endsection




@section('content')
    <div class="container mt-3" style="background-color:white;max-width:1370px">
        <div class="row">
            <nav>
                <div class="nav nav-tabs border-0" id="nav-tab" role="tablist">
                    <button class="nav-link  mr-2 change_not_btn" id="req_cars" data-bs-toggle="tab"
                        data-bs-target="#req_car" type="button" role="tab" aria-controls="req_car"
                        aria-selected="false">จองรถส่วนกลาง<span
                            class="badge rounded-pill bg-danger ml-2">{{ !empty($booking_car) ? count($booking_car) : 0 }}</span></button>
                    <button class="nav-link change_not_btn mr-2" id="history_rooms" data-bs-toggle="tab"
                        data-bs-target="#req_room" type="button" role="tab" aria-controls="history_room"
                        aria-selected="true">จองห้องประชุม<span
                            class="badge rounded-pill bg-danger ml-2">{{ !empty($booking_room) ? count($booking_room) : 0 }}</span></button>
                </div>
            </nav>

            <div class="tab-content pt-3" id="nav-tabContent" style="background-color:#ffffff;">


                {{-- ------------------------------------------ Tab Car ---------------------------------------------------- --}}
                <div class="tab-pane fade mx-4" id="req_car" role="tabpanel" aria-labelledby="req_car">

                    <div class="col-12 d-flex py-3 pl-0 pr-3 mb-2 ">
                        <div class="col-3 pl-0 text-start d-flex align-items-center">
                            <span class="title_booking">สถานะคำขอของฉัน</span>
                        </div>

                        <div class="col-9 align-items-center text-end pr-0">
                            <input type="search" class="p-2"
                                style="width: 420px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left: 905px; top: 6px;"
                                alt="">
                            {{-- Filter Car img --}}
                            <span class="filter_car" data-bs-placement="bottom" data-bs-toggle="popover"
                                data-bs-title="Filters">
                                <img src="/img/icon/tune-material-copy-3.png" class="ml-2" alt="">
                            </span>
                        </div>
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="booking_room"></th>
                                <th>รายการคำขอใช้ห้องประชุม</th>
                                <th>ห้องประชุม</th>
                                <th>วันที่เริ่มใช้</th>
                                <th>คำขอเพิ่มเติม</th>
                                <th>ผู้อนุมัติ</th>
                                <th><span class="d-flex justify-content-center">สถานะ</span></th>
                                <th><img src="/img/icon/description-material.png" alt=""></th>
                                <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                            </tr>
                        </thead>
                        <tbody>


                            @if (empty($booking_car))
                            <tr>
                                <td colspan="9" class="text-center">ไม่มีคำร้องขอ</td>
                            </tr>


                            @else
                                @foreach ($booking_car as $car)

                                    <tr>
                                        <td><input type="checkbox" name="booking_room"></td>
                                        <td>ประชุมทีม</td>
                                        <td>ห้องประชุม 1</td>
                                        <td>
                                            <span>21 มีนาคม 2564</span>
                                            <span>10.00 น. - 12:00 น.</span>
                                        </td>
                                        <td><span>VDO Conference</span></td>
                                        <td>นายกันต์ สินธร</td>
                                        <td class="text-center"><span class="waiting_approve">รออนุมัติ</span></td>
                                        <td><img src="/img/icon/description-material.png" alt=""></td>
                                        <td><img src="/img/icon/local-printshop-material-copy.png" alt=""></td>
                                    </tr>
                                @endforeach

                            @endif



                        </tbody>
                    </table>
                </div>
                {{-- ------------------------------------------ Tab Car ---------------------------------------------------- --}}



                {{-- ------------------------------------------ Tab Room ---------------------------------------------------- --}}
                <div class="tab-pane fade show active mx-4" id="req_room" role="tabpanel" aria-labelledby="req_room">
                    <div class="col-12 d-flex py-3 pl-0 pr-3 mb-2 ">
                        <div class="col-3 pl-0 text-start  d-flex align-items-center">
                            <span class="title_booking">สถานะคำขอของฉัน</span>
                        </div>
                        <div class="col-9 align-items-center text-end pr-0">
                            <input type="search" class="p-2"
                                style="width: 420px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left: 905px; top: 6px;"
                                alt="">
                            {{-- Filter Room img --}}
                            <span class="filter_room" data-bs-placement="bottom" data-bs-toggle="popover"
                                data-bs-title="Filters">
                                <img src="/img/icon/tune-material-copy-3.png" style="cursor: pointer;" class="ml-2" alt="">
                            </span>



                        </div>
                    </div>


                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th><input type="checkbox" name="booking_room"></th>
                                <th>รายการคำขอใช้ห้องประชุม</th>
                                <th>ห้องประชุม</th>
                                <th>วันที่เริ่มใช้</th>
                                <th>คำขอเพิ่มเติม</th>
                                <th>ผู้อนุมัติ</th>
                                <th><span class="d-flex justify-content-center">สถานะ</span></th>
                                <th><img src="/img/icon/description-material.png" alt=""></th>
                                <th><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                            </tr>
                        </thead>
                        <tbody>


                            @if (empty($booking_room))
                                <tr>
                                    <td colspan="9" class="text-center">ไม่มีคำร้องขอ</td>
                                </tr>

                            @else

                                @foreach ($booking_room as $room)

                                    <tr class="text-center">
                                        <td><input type="checkbox" name="booking_room"></td>
                                        <td>{{ @$room['objective'] }}</td>
                                        <td>{{ @$room['room_request'] }}</td> {{-- Need to use room_id and join table for show data --}}
                                        <td>

                                            @if (@$room['start_date'] == @$room['end_date'])
                                                <span>{{ @$room['start_date'] }}</span>
                                            @elseif(@$room['start_date'] != @$room['end_date'])
                                                <span>{{ @$room['start_date'] . ' - ' . @$room['end_date'] }}</span>
                                            @endif

                                            <span>{{ @$room['start_time'] . ' - ' . @$room['end_time'] }}</span>
                                        </td>

                                        <td>
                                            @foreach (@$room['device_id'] as $device)
                                                <span>{{ $device }}</span><br>
                                            @endforeach
                                        </td>


                                        <td>{{ @$room['request_to'] }}</td>

                                        @if (@$room['status_booking'] == 'waiting')
                                            <td class="text-center"><span class="waiting_approve">รออนุมัติ</span>

                                            @elseif(@$room['status_booking'] == 'approved')
                                            <td class="text-center"><span class="approve">อนุมัติ</span>

                                            @elseif(@$room['status_booking'] == 'cancelled')
                                            <td class="text-center"><span class="cancel_approve">ไม่อนุมัติ</span>
                                        @endif


                                        </td>

                                        {{-- show booking room page form 4 --}}
                                        <td>
                                            <a href="/reserve/room/form_room4?room_id={{ @$room['room_id'] }}">
                                                <img src="/img/icon/description-material.png" alt="">
                                            </a>
                                        </td>

                                        {{-- Export (PDF, Excel) --}}
                                        <td>
                                            <a href="#">
                                                <img src="/img/icon/local-printshop-material-copy.png" alt="">
                                            </a>
                                        </td>


                                    </tr>

                                @endforeach

                            @endif




                        </tbody>
                    </table>
                </div>
                {{-- ------------------------------------------ Tab Room ---------------------------------------------------- --}}



            </div>




        </div>
    </div>



    {{-- @if (Request::path() == 'reserve/car'); --}}
    @include('include/calender_booking_user/car')
    {{-- @elseif(Request::path() == "reserve/room") --}}
    @include('include/calender_booking_user/room')
    {{-- @endif --}}

    {{-- <div id="calendar_car" class="mt-4"></div>
<div id="calendar_room" class="mt-4"></div> --}}









    {{-- ------------------------------------ Popover Car ------------------------------ --}}
    <div id="car_popover_content" style="display: none;">
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ช่วงเวลา</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="all_room" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="small_room" id="" class="mr-2">
                <span class="text_filter">ช่วงเช้า</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="big_room" id="" class="mr-2">
                <span class="text_filter">ช่วงบ่าย</span>
            </div>
        </div>

        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="status_all" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_empty" id="" class="mr-2">
                <span class="text_filter">อนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">รออนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">ยกเลิกการใช้ห้องประชุม</span>
            </div>
        </div>


    </div>
    {{-- ------------------------------------ Popover Car ------------------------------ --}}


    {{-- ------------------------------------ Room ------------------------------------- --}}
    <div id="room_popover_content" style="display: none;">
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ช่วงเวลา</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="all_room" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="small_room" id="" class="mr-2">
                <span class="text_filter">ช่วงเช้า</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="big_room" id="" class="mr-2">
                <span class="text_filter">ช่วงบ่าย</span>
            </div>
        </div>

        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="status_all" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_empty" id="" class="mr-2">
                <span class="text_filter">อนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">รออนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">ยกเลิกการใช้ห้องประชุม</span>
            </div>
        </div>


    </div>
    {{-- ------------------------------------ Room ------------------------------------- --}}



    {{-- Calendar Room --}}
    <div id="room_calendar_popover_content" style="display: none;">
        {{-- Period Time --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ช่วงเวลา</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="all_room" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="small_room" id="" class="mr-2">
                <span class="text_filter">ช่วงเช้า</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="big_room" id="" class="mr-2">
                <span class="text_filter">ช่วงบ่าย</span>
            </div>
        </div>

        {{-- Size room --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ประเภทห้องประชุม</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="all_room" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="small_room" id="" class="mr-2">
                <span class="text_filter">ห้องประชุมเล็ก</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="big_room" id="" class="mr-2">
                <span class="text_filter">ห้องประชุมใหญ่</span>
            </div>
        </div>

        {{-- Status room --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="status_all" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_empty" id="" class="mr-2">
                <span class="text_filter">อนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">รออนุมัติ</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">ยกเลิกการใช้ห้องประชุม</span>
            </div>
        </div>

        {{-- Device in room --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">อุปกรณ์โสดทัศนูปกรณ์</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="status_all" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_empty" id="" class="mr-2">
                <span class="text_filter">โปรเจ็คเตอร์</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">ทีวี</span>
            </div>

            <div class="col-12 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">กระดานไวท์บอร์ด</span>
            </div>
        </div>

        {{-- Agency --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">อุปกรณ์โสดทัศนูปกรณ์</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-12 d-flex align-items-center p-0">
                <select class="form-select form-select-2x filter_agency" aria-label="ทั้งหมด">
                    <option selected>ทั้งหมด</option>
                    <option value="day">ราษฎร</option>
                    <option value="week">เอกสาร</option>
                    <option value="month">ท้องถื่น</option>
                </select>
            </div>
        </div>

        {{-- Food & drink room --}}
        <div class="row px-3 py-2">
            <div class="col-12 p-0">
                <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">อาหาร/อาหารว่าง</span>
            </div>
        </div>

        <div class="row px-3 py-2 mb-2 d-flex">
            <div class="col-6 d-flex align-items-center p-0">
                <input type="checkbox" name="status_all" id="" class="mr-2">
                <span class="text_filter">ทั้งหมด</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_empty" id="" class="mr-2">
                <span class="text_filter">มี</span>
            </div>

            <div class="col-6 d-flex align-items-center p-0 ">
                <input type="checkbox" name="status_not" id="" class="mr-2">
                <span class="text_filter">ไม่มี</span>
            </div>
        </div>

    </div>
    {{-- Calendar Room --}}









@endsection

@section('script')
    <script>
        $(document).ready(function() {

            $('.filter_room').popover({
                html: true,
                container: '.filter_room',
                sanitize: false,
                trigger: 'manual',
                content: function() {
                    return $('#room_popover_content').html();
                }
            }).on('click', function(e) {
                $(this).popover('show');
            });
            $('.filter_room').on('click', function(e) {
                e.stopPropagation();
            });



            $('.filter_car').popover({
                html: true,
                container: '.filter_car',
                sanitize: false,
                trigger: 'manual',
                content: function() {
                    return $('#car_popover_content').html();
                }
            }).on('click', function(e) {
                $(this).popover('show');
            });
            $('.filter_car').on('click', function(e) {
                e.stopPropagation();
            });





            $('.filter_calendar_room').popover({
                html: true,
                container: '.filter_calendar_room',
                sanitize: false,
                trigger: 'manual',
                content: function() {
                    return $('#room_calendar_popover_content').html();
                }
            }).on('click', function(e) {
                $(this).popover('show');
            });
            $('.filter_calendar_room').on('click', function(e) {
                e.stopPropagation();
            });








        });




        document.addEventListener('DOMContentLoaded', function() {
            // var calendarCar = document.getElementById('calendar_car');
            // var calendarRoom = document.getElementById('calendar_room');
            var calendarCar = $('#car_calendar')[0];
            var calendarRoom = $('#room_calendar')[0];

            var calendar_car = new FullCalendar.Calendar(calendarCar, {
                initialView: 'dayGridMonth'
            });
            var calendar_room = new FullCalendar.Calendar(calendarRoom, {
                initialView: 'dayGridMonth'
            });



            var pathname = window.location.pathname;


            if (pathname === "/reserve/car") {
                $('.calendar_car').css("display", "block");
                $('.calendar_room').css("display", "none");

                $('#req_cars').addClass('active  change_booking_btn');
                $('#history_rooms').removeClass('active  change_booking_btn');

                calendar_car.render();
                calendar_room.render();



            } else if (pathname === "/reserve/room") {
                $('.calendar_car').css("display", "none");
                $('.calendar_room').css("display", "block");

                $('#req_cars').removeClass('active  change_booking_btn');
                $('#history_rooms').addClass('active  change_booking_btn');

                calendar_car.render();
                calendar_room.render();
            }

            $('#req_cars').on('click', function() {
                $('.calendar_car').fadeIn(1000).css("display", "block");
                $('.calendar_room').css("display", "none");
                $(this).toggleClass('change_booking_btn');
                calendar_car.render();
                calendar_room.render();
            });


            $('#history_rooms').on('click', function() {
                $('.calendar_room').fadeIn(1000).css("display", "block");
                $('.calendar_car').css("display", "none");
                $(this).toggleClass('change_booking_btn');
                calendar_car.render();
                calendar_room.render();
            });

        });

        // Part Show Calendar event


































        $('html').click(function(e) {

            if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
                $('.filter_calendar_room').popover('hide');
                $('.filter_room').popover('hide');
                $('.filter_car').popover('hide');
            }
        });
    </script>
@endsection
