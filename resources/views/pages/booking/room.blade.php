@extends('layout.booking')
@section('style')
    <style>
        .dropdown_province {
            width: 127px;
            height: 30px;
            margin: 0 1px 8px 10px;
            padding: 2px 3px 2px 7px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            background-color: #ffffff;
        }

        .dropdown_province::after {
            margin-right: 5px;
        }



        .name_room {
            font-family: Kanit-ExtraLight;
            font-size: 16px;
            font-weight: bold;
            color: #4a4a4a;
        }

        .empty {
            width: 100px;
            height: 20px;
            padding: 0 30px;
            object-fit: contain;
            background-color: green;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            line-height: 1.07;
            text-align: center;
            color: #ffffff;
            border-radius: 8px
        }

        .not_empty {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            background-color: red;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            border-radius: 4px
        }

        .slide_img_room {
            width: 472px;
            height: 334px;
        }

        .slide_img_room .swiper-container {
            width: 100% !important;
            height: 100% !important;
        }

        .slide_img_room .swiper-container .swiper-button-next,
        .slide_img_room .swiper-container .swiper-button-prev {
            color: #ffffff;
            width: 30px !important;
            height: 30px !important;
        }

        .slide_img_room #img_room {
            object-fit: cover;
            width: 100%;
            height: 100%;
        }


        .title_bookingRoom {
            font-family: Kanit-Regular;
            font-size: 16px;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4a4a4a;
        }


        .drop_detial_room {
            cursor: pointer;
        }

        .drop_detial_room-arrow {
            border: solid blue;
            border-width: 0 1px 1px 0;
            display: inline-block;
            width: 7px;
            height: 7px;
            transition: .2s ease;
        }

        .drop_detial_room-right {
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
        }

        .drop_detial_room-down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .list_device ul {
            list-style: none;
            padding-left: 15px;
            margin: 5px;
        }

        .list_device ul li::before {
            content: "\2022";
            color: blue;
            font-weight: bold;
            display: inline-block;
            width: 1em;
        }

        .filter_booking_room .popover {
            width: 500px !important;
            height: fit-content !important;
        }

        .popover_content_wrapper .text_filter {
            font-size: 14px;
            font-weight: 500;
            color: #4f72e5;
        }

        .room_id,
        .room_id:hover {
            border: none;
            background-color: transparent;
            text-decoration: none;
            color: #ffffff;
            border: none;
        }
        .room_id,
        .room_id:disabled {
            cursor: not-allowed;
        }



    </style>

@endsection


@section('content')
    <div class="p-4 mt-3" style="background-color:white !important">
        <div class="row d-flex align-items-center">

            <div class="col-5">
                <img src="/img/icon/forum-material.png"
                    style="width: 23px; height: 23px; margin: 0 10px 0 0; color: #4f72e5;" alt="">
                <span style=" {
                        margin: 0 0px 39px 17px;
                        font-size: 26px;
                        font-weight: bold;
                        color: #4f72e5;">ห้องประชุม</span>
            </div>


            <div class="col-7 justify-content-end">
                <div class="row">

                    <div class="col-12 d-flex justify-content-end align-content-center">
                        <input type="search" class="px-2"
                            style="width: 420px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;"
                            placeholder="ค้นหารายการ">
                        <img src="/img/icon/search-material-bule.png" style="position: absolute; left: 73%; top: 6px;"
                            alt="">

                        {{-- Dropdown --}}
                        <select class="form-select form-select-1x dropdown_province" aria-label="ทุกหมวดหมู่">
                            <option selected>เมืองทอง</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>


                        {{-- Filter img --}}
                        <span class="filter_booking_room" data-bs-placement="bottom" data-bs-toggle="popover"
                            data-bs-title="Filters">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: fit-content; height: fit-content; color: #4f72e5; cursor: pointer;"
                                class="mr-0 ml-3" alt="">
                        </span>


                        <div id="popover_content_wrapper" style="display: none; ">
                            <div class="row px-3 py-2">
                                <div class="col-12 p-0">
                                    <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">ประเภทห้องประชุม</span>
                                </div>
                            </div>

                            <div class="row px-3 py-2 mb-2 d-flex">
                                <div class="col-6 d-flex align-items-center p-0">
                                    <input type="checkbox" name="all_room" id="" class="mr-2">
                                    <span class="text_filter">ทั้งหมด</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="small_room" id="" class="mr-2">
                                    <span class="text_filter">ห้องประชุมเล็ก</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="big_room" id="" class="mr-2">
                                    <span class="text_filter">ห้องประชุมใหญ่</span>
                                </div>
                            </div>

                            <div class="row px-3 py-2">
                                <div class="col-12 p-0">
                                    <span style="font-size: 14px;font-weight: 500; color: #4a4a4a;">สถานะ</span>
                                </div>
                            </div>
                            <div class="row px-3 py-2 mb-2 d-flex">
                                <div class="col-6 d-flex align-items-center p-0">
                                    <input type="checkbox" name="status_all" id="" class="mr-2">
                                    <span class="text_filter">ทั้งหมด</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="status_empty" id="" class="mr-2">
                                    <span class="text_filter">ว่าง</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="status_not" id="" class="mr-2">
                                    <span class="text_filter">ไม่ว่าง</span>
                                </div>
                            </div>


                            <div class="row px-3 py-2">
                                <div class="col-12 p-0">
                                    <span
                                        style="font-size: 14px;font-weight: 500; color: #4a4a4a;">อุปกรณ์โสดทัศนูปกรณ์</span>
                                </div>
                            </div>
                            <div class="row px-3 py-2 mb-2 d-flex">
                                <div class="col-6 d-flex align-items-center p-0">
                                    <input type="checkbox" name="device_all" id="" class="mr-2">
                                    <span class="text_filter">ทั้งหมด</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="device_projector" id="" class="mr-2">
                                    <span class="text_filter">โปรเจ็คเตอร์</span>
                                </div>

                                <div class="col-6 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="device_tv" id="" class="mr-2">
                                    <span class="text_filter">ทีวี</span>
                                </div>
                                <div class="col-12 d-flex align-items-center p-0 ">
                                    <input type="checkbox" name="device_whiteboard" id="" class="mr-2">
                                    <span class="text_filter">กระดานไวท์บอร์ด</span>
                                </div>
                            </div>



                        </div>



                    </div>


                    <div class="col-12 d-flex justify-content-end align-items-center">
                        {{-- Group input #1 --}}
                        {{-- Need to show Calendar --}}
                        <label class="mb-0 mr-1" for="date">วันที่</label>
                        <input type="search" class="px-2"
                            style="width: 222px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;">
                        <img src="/img/icon/date-range-material-copy-4.png" style="position: absolute; left: 77%;     top: 6px;
                                width: 15px;
                                height: 16px;
                                object-fit: contain;" alt="">

                        {{-- Group input #2 --}}
                        {{-- Need to show timepicker --}}
                        <label class="mb-0 mr-1 ml-2" for="date">เวลา</label>
                        <input type="search" class="px-2"
                            style="  width: 100px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                            placeholder="เวลา">
                        <img src="/img/icon/access-time-material.png" style="position: absolute; left: 95%; top: 8px;"
                            alt="">


                    </div>

                </div>

            </div>

            <div class="col-12" style="width: 1338px; height: 1px; margin: 22px 0 30px; border: solid 1px #4f72e5;"></div>

        </div>


        <form action="/reserve/room/form_room1" method="POST">
            @csrf
            @foreach ($room as $data)

                <div class="row d-flex align-items-start justify-content-around mb-4 px-3">
                    <div class="col-5 border p-0" style="max-width:469px">

                        <div class="col-12 px-3 pt-3 d-flex">
                            <div class="col-8 p-0">
                                <span class="name_room">หมายเลขห้อง {{ $data['no_room'] }} ชื่อห้อง
                                    {{ $data['name_room'] }} ชั้น {{ $data['level'] }} ตึก
                                    {{ $data['name_building'] }} สภานที่ {{ $data['place'] }}</span>
                            </div>

                            <div class="col-4 text-end">

                                @if ($data['status'] == 'ready')
                                    <span class="empty">ว่าง</span>
                                @elseif($data['status'] == 'not ready')
                                    <span class="not_empty">ไม่ว่าง</span>
                                @endif

                            </div>
                        </div>

                        <div class="col-12 pl-3 mb-2">
                            <img src="/img/icon/event-seat-material.png" style="  width: 16px;
                                height: 16px;
                                object-fit: contain;
                                font-family: material;
                                font-size: 16px;
                                color: #f5a623;" alt="">
                            <span>{{ $data['opacity'] }} ที่นั่ง</span>
                        </div>



                        <div class="col-12 slide_img_room px-0">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach ($data['img_room'] as $img)
                                        <div class="swiper-slide">
                                            <img id="img_room" src="/img/room/{{ $img }} " alt=""
                                                onerror="this.src='/img/icon/insert-photo-material@2x.png'">
                                        </div>
                                    @endforeach

                                </div>

                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>

                            </div>
                        </div>
                    </div>



                    <div class="col-7">
                        <div class="row detial_room">
                            {{-- Name & Button --}}
                            <div class="row">

                                <div class="col-12 d-flex">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">ชื่อ</span>
                                    </div>
                                    <div class="col-5 pl-2">
                                        <span>{{ $data['name_room'] }}</span>
                                    </div>


                                    <div class="col-5 text-end pr-0">
                                        <input type="hidden" name="room_id" value="{{ $data['id'] }}">
                                        @if($data['status'] == 'ready')
                                        <button class="room_id pr-0" data-id="{{ $data['id'] }}"><span class="btn_rent_room" >+
                                            จองห้องประชุมนี้</span></button>
                                        @else
                                        <button class="room_id pr-0" data-id="{{ $data['id'] }}" disabled><span class="btn_rent_room_disabled" >+
                                            จองห้องประชุมนี้</span></button>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-12 d-flex">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">หมายเลขห้อง</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <span>{{ $data['no_room'] }}</span>
                                    </div>
                                </div>

                                <div class="col-12 d-flex">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">ชั้น</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <span>{{ $data['level'] }}</span>
                                    </div>
                                </div>

                                <div class="col-12 d-flex">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">ตึก</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <span>{{ $data['name_building'] }}</span>
                                    </div>
                                </div>


                                <div class="col-12 d-flex">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">สภานที่</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <span>{{ $data['place'] }}</span>
                                    </div>
                                </div>

                                {{-- Size room --}}
                                <div class="col-12 d-flex mt-2">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">ขนาดห้อง</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        @if ($data['size'] >= 'small')
                                            <span>เล็ก</span>
                                        @elseif($data['size'] >= 'medium')
                                            <span>กลาง</span>
                                        @elseif($data['size'] > 'large')
                                            <span>ใหญ่</span>
                                        @endif
                                    </div>
                                </div>
                                {{-- Capacity people --}}
                                <div class="col-12 d-flex mt-2">
                                    <div class="col-2 pl-0">
                                        <span class="title_bookingRoom">จำนวนที่นั่ง</span>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <span>{{ $data['opacity'] }} ที่นั่ง</span>
                                    </div>
                                </div>
                            </div>







                            <div class="row">
                                {{-- Detials room --}}
                                <div class="col-12 d-flex mt-2 drop_detial_room">
                                    <div class="col-3 p-0 ">
                                        <span id="device"
                                            style="text-decoration: underline; color:blue">อุปกรณ์โสดทัศน์</span>
                                    </div>
                                    <div class="col-8 p-0 align-items-center">
                                        <span class="drop_detial_room-arrow drop_detial_room-right"></span>
                                    </div>
                                </div>


                                <div class="col-12 p-0 ml-3 mt-2 border list_device" style="display: none">
                                    <ul>
                                        @foreach ($data['device_id'] as $item)
                                            @if ($item != null)
                                                <li>{{ $item }}</li>
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            @endforeach
        </form>




    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.drop_detial_room').on('click', function() {
                $(this).find('.drop_detial_room-right').toggleClass('drop_detial_room-down');
                $(this).parents('.detial_room').find('.list_device').toggle();
            });

            const swiper = new Swiper('.slide_img_room .swiper-container', {
                direction: 'horizontal',
                loop: true,
                pagination: {
                    el: '.slide_img_room .swiper-container .swiper-pagination',
                },
                navigation: {
                    nextEl: '.slide_img_room .swiper-container .swiper-button-next',
                    prevEl: '.slide_img_room .swiper-container .swiper-button-prev',
                },

            });


            $('.filter_booking_room').popover({
                html: true,
                container: '.filter_booking_room',
                sanitize: false,
                trigger: 'manual',
                content: function() {
                    return $('#popover_content_wrapper').html();
                }
            }).on('click', function(e) {
                $(this).popover('show');
            });


            $('.filter_booking_room').on('click', function(e) {
                e.stopPropagation();
            });



            $('html').click(function(e) {

                if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
                    $('.filter_booking_room').popover('hide');
                }
            });
        });




        $(document).ready(function() {

            $('.room_id').on('click', function(e) {
                e.preventDefault();
                var room_id = $(this).data('id');

                $.ajax({
                    url: "/reserve/room/choose",
                    type: "post",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    data: {
                        room_id: room_id
                    },

                    success: function(data) {
                        console.log(data);

                        if (data.status == 'true') {
                            window.location.href = '/reserve/room/form_room1?book_id=' + data
                                .book_id;
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    }

                });
            });

        });
    </script>

    <script>


    </script>
@endsection
