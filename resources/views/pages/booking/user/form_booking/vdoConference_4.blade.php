@extends('layout.booking')
@section('style')
<style>
    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_evr:read-only {
        background-color: #f3f3f3 !important;
    }

    .input_date:read-only {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .form-select:disabled {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        line-height: 1;
        padding: 0 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }


    .device_check {
        margin-right: 5px;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;

    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4a4a4a;
    }


    /* .... */

    .title_room2 {
        font-family: Kanit-ExtraLight;
        font-size: 26px;
        font-weight: bold;
    }

    .select_form_room2 {
        width: 135px;
        height: 30px;
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .select_food {
        width: 301px;
        height: 30px;
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .form_time {
        width: 100px;
        height: 30px;
        margin: 0 0 0 8px;
        padding: 4px 4px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3 !important;

    }

    #input_price {
        background-color: ##f3f3f3;
        width: 100px;
        height: 30px;
        margin: 0 0 0 8px;
        padding: 4px 4px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .warning_span {
        font-size: 12px;
        line-height: 1.25;
        color: #ee2a27;
    }

    .comeback {
        width: 112px;
        height: 30px;
        margin: 0 17px 0 0;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ff7a00;
        color: #ffffff;
    }

    .cancel {
        width: 112px;
        height: 30px;
        /* margin: 0 17px 0 0;
        padding: 5px 5px 5px 15px; */
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
    }

    .approve_form {
        color: #ffffff;
        width: 112px;
        height: 30px;
        /* margin: 0 0 0 17px; */
        /* padding: 4px 41px; */
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(266deg, #4f72e5 36%, #314d7b 145%);
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        font-weight: bold;
        font-family: Kanit-ExtraLight;
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    #time_serve {
        height: 30px;
        width: 80px;
        border-radius: 8px;
        border: 1px solid #d5d4d4;
        padding-left: 10px;
    }

    .price_product {
        font-family: Kanit-ExtraLight;
        font-weight: bold;
        font-size: 16px;
        color: #4f72e5;
    }

    .status_waiting {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: gold;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_approve {
        width: 130px;
        height: 23px;
        color: #ffffff;
        background-color: green;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_cancel {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #ee2a27;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

</style>
@endsection

@section('content')


<div class="my-3 px-5 py-3" style="background-color:#ffffff;">
    <div class="row">
        <div class="col-12 d-flex align-items-center">
            <div class="col-9">
                <span
                    style="font-family: Kanit-Regular; font-size: 26px; font-weight: bold; color: #4f72e5;">รายละเอียดการจอง
                    VDO Conference</span>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-end status_form_room">

                @if($room['status_booking'] == 'waiting')
                    <span class="status_waiting mr-3">รออนุมัติ</span>
                @elseif($room['status_booking'] == 'approved')
                    <span class="status_approve mr-3">อนุมัติ</span>
                @elseif($room['status_booking'] == 'cancelled')
                    <span class="status_cancel mr-3">ยกเลิก</span>
                @endif

                <img src="/img/icon/local-printshop-material-copy.png" alt="">
            </div>
        </div>

    </div>
    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <div class="row">
        <div class="col-12 my-3" style="padding: 0 300px;">
            <ul class="timeline">
                <li class="bf_active"></li>
                <li class="bf_active"></li>
                <li class="bf_active"></li>
                <li class="active">ส่งคำขอและบันทึก</li>
            </ul>
        </div>
    </div>


    <div class="col-12 mt-3 d-flex">
        <span class="title_form">ผู้ขอจอง</span>
    </div>

        {{-- Name & Agency --}}
        <div class="row d-flex  mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">ชื่อ-สกุล</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" placeholder="ชื่อ-สกุล" name="name_user"
                        value="นางสาวดาริสา พัชรโชด" readonly>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0 ">
                    <span class="content_span">สำนัก</span>
                </div>

                <div class="col-10 ">
                    <input type="text" class="input_evr" value="สำนักประชาสัมพันธ์และสื่อสารองค์กร" name="office_id"
                        readonly>
                </div>
            </div>

        </div>

        {{-- Position & Group --}}
        <div class="row d-flex mt-3">

            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">กลุ่มงาน</span>
                </div>
                <div class="col-10">
                    <input type="text" class="input_evr" value="สื่อประชาสัมพันธ์" name="group_id" readonly>
                </div>
            </div>



            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">ตำแหน่ง</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" value="เจ้าหน้าที่สื่อประชาสัมพันธ์" name="position" readonly>
                </div>
            </div>
        </div>

        {{-- Number Phone --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-3 pr-0 ">
                    <span class="content_span">ผู้บังคับบัญชา</span>
                </div>

                <div class="col-9 pl-0">
                    <input class="input_evr" type="text" value="นายกันต์ สินธร" name="manager_id" readonly>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-4 pr-0 ">
                    <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                </div>

                <div class="col-8 pl-0">
                    <input class="input_evr" type="text" value="02-777-12000" name="tel_in_no" readonly>
                </div>
            </div>
        </div>




        <div class="col-12 mt-5">
            <span class="title_form">รายละเอียดการจองห้องประชุม</span>
        </div>




        {{-- Title & send to --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">เรื่อง</span>
                </div>
                <div class="col-10">
                    <select class="form-select" disabled>
                        <option selected>{{ $room['title'] }}</option>
                    </select>
                </div>
            </div>
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">เรียน</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" name="send_to" value="{{ $room['request_to'] }}" readonly>
                </div>
            </div>
        </div>

        {{-- Objective --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">วัตถุประสงค์</span>
                </div>
                <div class="col-10">
                    <input type="text" class="input_evr p-2" name="objective" id="objective"
                        value="{{ $room['objective'] }}" readonly>
                </div>
            </div>
        </div>

        {{-- Date & Time --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">วันที่เริ่มใช้</span>
                </div>

                <div class="col-4 pr-0">
                    <input class="input_date" type="text" name="date_start" value="{{ $room['start_date'] }}" readonly>
                </div>

                <div class="col-1">
                    <span class="content_span">เวลา</span>
                </div>
                <div class="col-2 px-0">
                    <input class="input_date" type="text" value="{{ $room['start_time'] }}" name="time_start" readonly>
                </div>

            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">วันที่สิ้นสุด</span>
                </div>

                <div class="col-4 pr-0">
                    <input class="input_evr" type="text" value="{{ $room['end_date'] }}" name="date_end" readonly>
                </div>

                <div class="col-1">
                    <span class="content_span">เวลา</span>
                </div>
                <div class="col-2 px-0">
                    <input class="input_evr" type="text" value="{{ $room['end_time'] }}" name="time_end" readonly>
                </div>

            </div>
        </div>

        {{-- Place &  Room --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">สถานที่</span>
                </div>

                <div class="col-10">
                    <select class="form-select input_evr" disabled>
                        <option selected>{{ $room['place'] }}</option>
                    </select>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">ห้องประชุม</span>
                </div>

                <div class="col-10">
                    <select class="form-select input_evr" disabled>
                        <option selected>{{ $room['room_request'] }}</option>
                    </select>
                </div>
            </div>
        </div>

        {{-- Device In Room  & Staff device--}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-start">
                <div class="col-3 pr-0">
                    <span class="content_span">อุปกรณ์โสดทัศนูปกรณ์</span>
                </div>

                <div class="col-10 d-flex pl-0 align-items-center">
                    <div class="row d-flex align-items-center mr-2">

                        <div class="row mx-2">
                            @foreach ($room['device_id'] as $device_id)

                            @php
                            $name_device = '';
                            if($device_id == 'projector'){
                            $name_device = 'เครื่องฉาย PROJECTOR';
                            }elseif($device_id == 'vdo'){
                            $name_device = 'เครื่องฉาย VDO';

                            }elseif($device_id == 'record'){
                            $name_device = 'บันทึกเทป';

                            }elseif($device_id == 'conference'){
                            $name_device = 'VDO Conference';

                            }elseif($device_id == 'visual'){
                            $name_device = 'เครื่องฉาย PROJECTOR';

                            }
                            @endphp


                            <div class="col-4 d-flex align-items-center p-0">
                                <input type="checkbox" name="device_room" value="{{ $device_id }}" id="projector"
                                    class="device_check" checked disabled>
                                <span class="content_span pl-2 mr-3">{{ $name_device }}</span>
                            </div>
                            @endforeach

                        </div>



                    </div>
                </div>
            </div>


            <div class="col-6 d-flex align-items-center">
                <div class="col-5 pr-0">
                    <span class="content_span">เจ้าหน้าที่ควบคุมอุปกรณ์โสดทัศนูปกรณ์</span>
                </div>

                <div class="col-6 d-flex pl-0 align-items-center">
                    <div class="row d-flex align-items-center mr-2">

                        <div class="d-flex align-items-center mr-2">
                            <input type="checkbox" name="staff_device"
                                value="{{ ($room['staff'] == 'need') ? 'ต้องการ' : 'ไม่ต้องการ' }}" id="need_devices"
                                class="device_check" checked disabled>
                            <span class="content_span mr-3">{{ ($room['staff'] == 'need') ? 'ต้องการ' : 'ไม่ต้องการ' }}</span>
                        </div>
                    </div>
                </div>

            </div>


        </div>


        {{-- Boss --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">ผู้บังคับบัญชา</span>
                </div>

                <div class="col-10">
                    <select class="form-select input_evr" disabled>
                        <option selected>{{ (($room['manager_id'] == 1)) ? 'นายวันชัย นาคทั่ง' : 'นายกันต์ สินธร' }}
                    </select>
                </div>
            </div>
        </div>



        {{-- Food & Drink --}}
        <div class="col-12 mt-3">
            <div class="col-12 d-flex align-items-center pl-0">
                <div class="px-0">
                    <span class="content_span">อาหารว่าง/เครื่องดื่มรับรองการประชุม</span>
                </div>

                <div class="d-flex pl-0 align-items-center">
                    <div class="d-flex align-items-center mr-2">

                        <div class="col-1 d-flex align-items-center">
                            <input type="checkbox" name="food_drink" value="{{ @$room['food_detail']->appertizer }}"
                                checked disabled id="need_food" class="device_check">
                        </div>
                        <div class="col-11 pl-2 pr-0">
                            <span class="content_span mr-3">{{ (@$room['food_detail']->appertizer == 'on') ? 'ต้องการ': 'ไม่ต้องการ' }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="col-12 mt-3">
        <span class="title_form">รายการอาหารว่าง อาหารกลางวัน และเครื่องดื่ม</span>
    </div>


        <table class="table">
            <thead>
                <tr>
                    <th style="width: 33px;"></th>
                    <th style="width: 190px;"><span>เลือกรายการ</span></span></th>
                    <th><span>รายการ</span></th>
                    <th class="text-center" style="width:120px"><span>ราคาต่อหน่วย</span></th>
                    <th style="width: 1px;"></th>
                    <th style="width:120px"><span>จำนวน (ชุด)</span></th>
                    <th class="text-end" style="width:120px"><span>จำนวนเงิน</span></th>
                    {{-- <th style="width: 1px;"></th> --}}
                </tr>
            </thead>
            <tbody>

                @if($room['food_detail'] == null)
                <tr>
                    <td colspan="8" class="text-center" style="height:100px;vertical-align: middle;">ไม่มีรายการ</td>
                </tr>

                @elseif($room['food_detail'] != null)

                @foreach($room['food_detail']->detail_food as $key => $item)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                        <select class="form-select select_form_room2 input_evr" aria-label="เลือกรายการ"
                            disabled="disabled">
                            <option selected>{{ $item->period }}</option>
                        </select>
                    </td>

                    <td>
                        <select class="form-select select_form_room2 input_evr" aria-label="เลือกรายการ"
                            disabled="disabled">
                            <option selected>{{ $item->set_food }}</option>
                        </select>
                    </td>

                    <td class="text-center">
                        <span class="price_product pr-2">{{ $item->price }}</span>
                    </td>

                    <td class="text-center"><span class="price_product">x</span></td>

                    <td>
                        <input type="text" name="count_food" class="text-end input_evr form_time"
                            value="{{ $item->count_food }}" readonly>
                    </td>

                    <td class="text-end pr-5">
                        <span>{{ $item->price * $item->count_food }}</span>
                    </td>



                </tr>

                @endforeach

                @endif







            </tbody>
            <tfoot>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-end"><span>รวม</span></td>
                    <td class="text-end"><span>{{ @$room['food_detail']->total }}</span></td>
                    {{-- <td><span class="text-end">บาท</span></td> --}}

                </tr>

            </tfoot>
        </table>

        <div class="row" style="margin: 100px 0 25px 0">
            <div class="col-12 d-flex justify-content-center">
                @if($room['status_booking'] == 'waiting')
                <span class="cancel text-center pt-1 mr-2" id="cancel_btn" style="cursor: pointer;">ยกเลิกคำขอ</span>
                @endif

                <span class="text-center approve_form pt-1"><a href="/"
                        style="color:#ffffff;text-decoration: none;">กลับหน้าหลัก</a></span>
            </div>
        </div>
        <input type="hidden" name="book_id" value="{{ $book_id }}">



    </div>

@endsection


@section('script')
<script>
    $(document).ready(function () {


        $('#cancel_btn').on('click',function(e){
            e.preventDefault();

            var book_id = $('input[name="book_id"]').val();

            $.ajax({
                url:'/reserve/vdoconference/vdoConference4',
                type:'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    book_id: book_id
                },
                success:function(data){
                    console.log(data);

                    if(data == 'success'){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'คำขอถูกยกเลิกแล้ว',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            $('.status_form_room').find('span').attr('class', 'status_cancel mr-3').text(
                                'ยกเลิก');
                            $('#cancel_btn').remove();
                        });
                    }
                }
            });

        });


        $('#cancel_btn').on('click', function (e) {
            e.preventDefault();

        });

    });

</script>
@endsection
