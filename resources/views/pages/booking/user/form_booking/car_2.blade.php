@extends('layout.booking')
@section('style')
<style>
    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .date_input {
        width: 226px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
        padding: 0 8px;
    }

    .time_input {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
        padding: 0 8px;
    }

    .form-select-city {
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #4a4a4a;
    }
    /* .map{
        position: absolute;
    } */

    .choosemap_btn {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        border: none;
        font-size: 16px;
        padding: 5px 6px;
    }

    .waiting_approve,
    .waiting_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 28px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
        background-color: rgb(252, 144, 2);
    }

    .valiedate_data,
    .valiedate_data:hover {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        /* padding: 0px 28px; */
        text-decoration: none;
    }

    .accept_location{
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 14px;
    }

    #objective {
        resize: none;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f7f7f7
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .form-select {
        height: 30px;
        line-height: 1;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

</style>
@endsection
@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <span style="font-family: Kanit-Regular; font-size: 26px; color: #4f72e5;">คำขอจองรถ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>



    <div class="row">
        <div class="col-12 my-3" style="padding: 0 260px;">
            <ul class="timeline">
                <li class="bf_active"></li>
                <li class="active">เลือกสถานที่</li>
                <li class=""></li>
                <li></li>
            </ul>
        </div>
    </div>


    <div class="col-12 mt-3 pl-0">
        <span class="title_form">ผู้ขอจองใช้รถ</span>
    </div>

    <form action="#" method="post">

        <div class="row mt-2">
            <div class="col-12 d-flex">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ชื่อสถานที่</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4; x"
                            type="text" placeholder="ชื่อสถานที่" name="location" value="โรงแรมเซนทารา แกรนด์">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ตำบล/แขวง</span>
                    </div>
                    <div class="col-10 pl-0">
                        <select class="form-select" aria-label="Default select example" name="county">
                            <option selected>ปทุมวัน</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">อำเภอ</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" aria-label="Default select example" name="district">
                            <option selected>ปทุมวัน</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">จังหวัด</span>
                    </div>
                    <div class="col-10 pl-0">
                        <select class="form-select" aria-label="Default select example" name="city">
                            <option selected>ปทุมวัน</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
            </div>



        </div>

        <div class="row mt-5">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <button class="choosemap_btn">Choose Map</button>
                <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                    style="object-fit: cover; width: 985px; height: 318px;border: solid 1px #4f72e5;" alt="">
            </div>
        </div>




        <div class="row" style="margin-top:150px; margin-bottom:80px;">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <a href="/reserve/car/form_car3" class="valiedate_data d-flex align-items-center justify-content-center">ถัดไป</a>
            </div>
        </div>



        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 1285px;">
                <div class="modal-content"  style="border: none;background-color:transparent;">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-0" style="background-color:#ffffff">
                        <div class="row">
                            <div class="col-12">
                                <input class="input_evr px-2"
                                    style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;"
                                    type="text" placeholder="22 January 2021" name="date_start" readonly>
                                <img src="/img/icon/search-material-bule.png"
                                    style="position: absolute; top: 22%; right: 2%;" alt="">
                            </div>

                            <div class="col-12 mt-3 px-0">
                                <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                                    style="object-fit: cover; width: 100%;height:460px" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12 mt-4 d-flex justify-content-center">
                            <button type="button" data-bs-dismiss="modal" class="accept_location">ตกลง</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>

</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('.choosemap_btn').on('click', function (e) {
            e.preventDefault();
            $('#exampleModal').modal('show');

        })
    });

</script>
@endsection
