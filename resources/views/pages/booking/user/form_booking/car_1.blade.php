@extends('layout.booking')
@section('style')
<style>
    .title_form {
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .date_input {
        width: 226px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding: 0 8px;

    }

    .time_input {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding: 0 8px;

    }

    .form-select-city {
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #4a4a4a;
    }

    .choosemap_btn {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        top: 50%;
        left: 42%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 16px;
        padding: 5px 6px;
    }

    .valiedate_data,
    .valiedate_data:hover {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 5px 6px;
        text-decoration: none;
        width: 112px;
        height: 30.1px;
        text-align: center;
    }

    .browse {
        width: 90px;
        height: 26px;
        font-size: 12px;
        color: #ffffff;
        border-radius: 8px;
        border: none;
        box-shadow: 0 0 3px 1px rgba(0, 0, 0, 0.15);
        background-image: linear-gradient(193deg, #4f72e5 47%, #314d7b 119%);
    }

    .extension_files {
        margin-left: 10px;
        font-family: Kanit-Regular;
        font-weight: bold;
        font-size: 12px;
        color: #ee2a27;
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }
</style>
@endsection
@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">

    <div class="row">
        <span style="font-family: Kanit-Regular; font-size: 26px;color: #4f72e5;">รายละเอียดคำขอจองใช้รถ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>


    <div class="row">
        <div class="col-12 my-3" style="padding: 0 260px;">
            <ul class="timeline">
                <li class="active">รายละเอียดการจอง</li>
                <li class=""></li>
                <li class=""></li>
                <li></li>
            </ul>
        </div>
    </div>



    <div class="col-12 mt-3 pl-0">
        <span class="title_form">ผู้ขอจองใช้รถ</span>
    </div>

    <form action="#" method="post">

        <div class="row mt-2">
            <div class="col-12 d-flex">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10 px-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="ชื่อ-สกุล" name="form_name_room" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">สำนัก</span>
                    </div>
                    <div class="col-10 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="สํานักบริหารกลาง" name="form_name_room" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">กลุ่มงาน</span>
                    </div>
                    <div class="col-10 px-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="สื่อประชาสัมพันธ์" name="group_id" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ตำแหน่ง</span>
                    </div>
                    <div class="col-10 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="เจ้าหน้าที่สื่อประชาสัมพันธ์" name="position" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>
                    <div class="col-9 px-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="นายกันต์  สินธร" name="manager_id" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                    </div>
                    <div class="col-9 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" placeholder="02-777-1200" name="tel_in_no" readonly>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-12 mt-3 pl-0">
            <span class="title_form">รายละเอียดการจอง</span>
        </div>

        <div class="row mt-2">

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span class="content_span">ขออนุญาตใช้</span>
                    </div>
                    <div class="col-10 pl-0 d-flex align-items-center">
                        <div class="col-4">
                            <input type="checkbox" name="type_car" id="car_center">
                            <span class="content_span">รถส่วนกลาง</span>
                        </div>
                        <div class="col-8">
                            <input type="checkbox" name="type_car" id="car_center">
                            <span class="content_span">รถรับรอง</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่เริ่มใช้</span>
                    </div>
                    <div class="col-5">
                        <input class="date_input"
                            style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;"
                            type="text" placeholder="22 January 2021" name="date_start">
                        <img src="/img/icon/date-range-material.png" style="position: absolute; top: 8px; left: 215px;"
                            alt="">
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <input class="time_input" type="text" placeholder="09:00" name="time_start">
                        <img src="/img/icon/access-time-material.png" style="position: absolute; top: 8px; left: 90px;"
                            alt="">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่สิ้นสุด</span>
                    </div>
                    <div class="col-5">
                        <input class="date_input" type="text" placeholder="22 January 2021" name="date_start">
                        <img src="/img/icon/date-range-material.png" style="position: absolute; top: 8px; left: 215px;"
                            alt="">
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <input class="time_input" type="text" placeholder="09:00" name="time_start">
                        <img src="/img/icon/access-time-material.png" style="position: absolute; top: 8px; left: 90px;"
                            alt="">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center w-100">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">วัตถุประสงค์</span>
                    </div>
                    <div class="col-10 pr-0">
                        <input type="text" class="input_evr px-2" name="purpose" id="purpose" value="ไปสัมนาวิชาการ">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center w-100">
                    <div class="col-3 px-0 align-items-start">
                        <span class="content_span">หลักฐานอื่นๆ (ถ้ามี)</span>
                    </div>
                    <div class="col-9 px-0 d-flex align-items-center">
                        <button type="button" class="browse px-2 py-1">Browse File</button>
                        <input type="file" name="evidence" id="evidence"
                            accept=".pdf,.doc,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                            style="display:none;">
                        <span class="extension_files">(pdf., docx., excel.)</span>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">จำนวนที่นั่ง</span>
                    </div>
                    <div class="col-10 pr-0">
                        <input type="text" class="input_evr px-2" name="member_seat" id="member_seat" value="2">
                    </div>
                </div>
            </div>




        </div>


        <div class="row" style="margin-top: 160px;">
            <div class="col-12 d-flex align-content-center justify-content-center ">
                <a class="valiedate_data" href="/reserve/car/form_car2">ถัดไป</a>
            </div>
        </div>

    </form>

</div>
@endsection



@section('script')
<script>
    $(document).ready(function () {
        $('.choosemap_btn').on('click', function (e) {
            e.preventDefault();
            $('#exampleModal').modal('show');

        })

        $('.browse').on('click', function () {
            $('#evidence').trigger('click');
        });



    });
</script>
@endsection
