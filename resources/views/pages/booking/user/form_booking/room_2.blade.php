@extends('layout.booking')
@section('style')
    <style>
        .title_room2 {
            font-family: Kanit-Regular;
            font-size: 26px;
            font-weight: bold;
            color: #4f72e5;
        }

        .select_form_room2 {
            width: 100%;
            height: 30px;
            padding: 5px 6.5px 3px 9px;
            border-radius: 8px;
            line-height: 1;
            border: solid 0.5px #ceced4;
        }

        .select_period {
            width: 140px;
            height: 30px;
            padding: 5px 6.5px 3px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        #form_time {
            width: 100px;
            height: 30px;
            margin: 0 0 0 8px;
            padding: 4px 4px 4px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;

        }

        #input_price {
            background-color: #f7f7f7;
            width: 100px;
            height: 30px;
            margin: 0 0 0 8px;
            padding: 4px 4px 4px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .warning_span {
            font-size: 12px;
            line-height: 1.25;
            color: #ee2a27;
        }

        .comeback,
        .comeback:hover {
            width: 112px;
            height: 30px;
            margin: 0 17px 0 0;
            border-radius: 8px;
            padding: 4px 0;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-color: #ee2a27;
            color: #ffffff;
            text-align: center;
            text-decoration: none;
        }

        .approve_form,
        .approve_form:hover {
            color: #ffffff;
            width: 112px;
            height: 30px;
            margin: 0 0 0 17px;
            padding: 4px 28px;
            border: none;
            border-radius: 8px;
            text-align: center;
            text-decoration: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(266deg, #4f72e5 36%, #314d7b 145%);
        }

        .price_product {
            font-family: Kanit-ExtraLight;
            font-weight: bold;
            font-size: 16px;
            color: #4f72e5;
        }

        .text_x {
            font-family: Kanit-ExtraLight;
            font-weight: bold;
            font-size: 16px;
            color: #4f72e5;
        }

        .count_food {
            width: 80px;
            height: 30px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            padding-left: 10px;
            text-align: right;

        }

        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }



        .timeline {
            counter-reset: year 0;
            position: relative;
        }

        .timeline li {
            list-style: none;
            float: left;
            width: 25%;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            font-family: 'Dosis', sans-serif;
        }

        ul:nth-child(1) {
            color: #4f72e5;
        }

        .timeline li:before {
            counter-increment: year;
            content: counter(year);
            width: 50px;
            height: 50px;
            border: 3px solid #4f72e5;
            border-radius: 50%;
            display: block;
            text-align: center;
            line-height: 50px;
            margin: 0 auto 10px auto;
            background: #ffffff;
            color: #4f72e5;
            transition: all ease-in-out .3s;
            cursor: pointer;
            font-size: 25px;
            position: relative;
            z-index: 1;
        }

        .timeline li:after {
            content: "";
            position: absolute;
            width: 100%;
            height: 3px;
            background-color: #4f72e5;
            top: 25px;
            left: -50%;
            z-index: 0;
            transition: all ease-in-out .3s;
        }

        .timeline li:first-child:after {
            content: none;
        }

        .timeline li.active {
            font-weight: bold;
            font-family: Kanit-Regular;
            color: #4f72e5;
        }

        .timeline li.active:before {
            background: #4f72e5;
            width: 70px;
            height: 70px;
            padding-top: 7px;
            border: 3px solid;
            font-size: 40px;
            box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
            color: #ffffff;
            position: relative;
            z-index: 1;
            bottom: 10px;
        }

        .timeline li.active+li:after {
            background: #4f72e5;
        }

        /* Bf active */
        .timeline li.bf_active {
            color: #555555;
        }

        .timeline li.bf_active:before {
            background: #4f72e5;
            width: 50px;
            height: 50px;
            border: 3px solid;
            font-size: 25px;
            box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
            color: #ffffff;
            position: relative;
            z-index: 1;
        }

        .timeline li.bf_active+li:after {
            background: #4f72e5;
        }

        .table td,
        .table th {
            padding: 9px !important;
        }

        .table thead th {
            border-top: 1px solid #4f72e5 !important;
            border-bottom: 1px solid #4f72e5 !important;
        }

        .table>:not(:last-child)>:last-child>* {
            border-top: 1px solid #4f72e5 !important;
            border-bottom: 1px solid #4f72e5 !important;
        }

        .table tfoot tr,
        .table tfoot th {
            border-top: 1px solid #4f72e5 !important;
            border-bottom: 1px solid #4f72e5 !important;
        }

        .add_menu {
            background-color: #ffffff;
            border-radius: 8px;
            border: 0.5px solid #d5d4d4;
            color: #4f72e5;
            height: 30px;
            margin-top: 10px;

            font-family: Kanit-ExtraLight;
            font-weight: bold;
            font-size: 16px;
        }

        .title_second {
            font-family: Kanit-ExtraLight;
            font-size: 16px;
            font-weight: bold;
        }

        .text_need {
            font-family: Kanit-ExtraLight;
            font-size: 14px;
        }

        .time_serve {
            height: 30px;
            width: 80px;
            border-radius: 8px;
            border: 1px solid #d5d4d4;
            padding-left: 10px;
        }

        input[type="time"]::-webkit-calendar-picker-indicator {
            opacity: 1;
            display: block;
            background: url("/img/icon/access-time-material.png") no-repeat;
            width: 20px;
            height: 20px;
            align-items: center;
            position: absolute;
            top: 8px;
            left: 65px;
            cursor: pointer;
        }

        .delete_menu {
            cursor: pointer;
        }

    </style>
@endsection

@section('content')
    <form class="form2" enctype="multipart/form-data">
        @csrf
        <div class="my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
            <div class="row">
                <span class="title_room2">คำขอจองห้องประชุม</span>
            </div>
            <div class="my-2" style="border:0.5px dashed #ceced4"></div>


            <div class="row">
                <div class="col-12 my-3" style="padding: 0 250px;">
                    <ul class="timeline">
                        <li class="bf_active"></li>
                        <li class="active">อาหารว่าง/เครื่องดื่ม</li>
                        <li class=""></li>
                        <li class=""></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 mt-3">
                <div class="col-12 pl-0">
                    <span class="title_second">รายการอาหารว่าง/เครื่องดื่ม</span>
                </div>

                <div class="col-12 d-flex">
                    <div class="px-0">
                        <span class="text_need">อาหารว่าง/เครื่องดื่มรับรองการประชุม</span>
                    </div>
                    <div class="d-flex align-items-center px-0 justify-content-between">

                        <div class="col-1 d-flex align-items-center">
                            <input type="radio" name="appertizer" id="need" value="need"
                                {{ @$room['food_detail']->appertizer == 'need' ? 'checked' : '' }}>
                        </div>

                        <div class="col-11 pl-2 pr-0">
                            <span class="text_need">ต้องการ</span>
                        </div>

                    </div>
                    <div class="d-flex align-items-center px-0 justify-content-between">

                        <div class="col-1 d-flex align-items-center">
                            <input type="radio" name="appertizer" id="no_need" value="no_need"
                                {{ @$room['food_detail']->appertizer == 'no_need' ? 'checked' : '' }}>
                        </div>

                        <div class="col-11 pl-2 pr-0">
                            <span class="text_need">ไม่ต้องการ</span>
                        </div>

                    </div>
                </div>

            </div>


            <div class="col-12 mt-3">
                <span class="title_second">รายการอาหารว่าง อาหารกลางวัน และเครื่องดื่ม</span>
            </div>

            {{-- <input type="hidden" id="food_arr" value="{{ $food }}"> --}}
            <input type="hidden" id="order_edit" value="{{ json_encode(@$room['food_detail']) }}">

            <table class="table" id="devices_table">
                <thead>

                    <tr>
                        <th style="width: 33px;"></th>
                        <th style="width: 190px;"><span>เลือกรายการ</span></span></th>
                        <th><span>รายการ</span></th>
                        <th>เวลาในการเสริฟ</th>
                        <th class="text-center" style="width:120px"><span>ราคาต่อหน่วย</span></th>
                        <th style="width: 1px;"></th>
                        <th style="width:120px"><span>จำนวน (ชุด)</span></th>
                        <th class="text-end" style="width:120px"><span>จำนวนเงิน</span></th>
                        <th style="width: 1px;"></th>
                    </tr>

                </thead>
                <tbody>
                    @foreach (@$food as $key => $item)
                        <tr class="choosed">
                            <td class="index_row_menu">{{ @$key + 1 }}</td>
                            <td>
                                <select class="form-select select_period w-75" name="period[]">
                                    <option>เลือกรายการ</option>
                                    <option value="{{ @$item->period }}">{{ @$item->period }}</option>
                                </select>

                                <button type="button" class="add_menu">
                                    <img class="mr-2" src="/img/icon/add-circle-material.png" alt="">เพิ่มรายการ
                                </button>
                            </td>

                            <td>
                                <select class="form-select select_form_room2 w-75 choose_menu" name="set_food[]">
                                    <option>เลือกเมนูอาหาร</option>
                                    @foreach (@$item['list_food'] as $list_food)
                                        <option value="{{ @$list_food[0] }}" data-price="{{ @$list_food[1] }}">
                                            {{ @$list_food[0] }}
                                        </option>
                                    @endforeach

                                </select>
                            </td>
                            <td>
                                <div class="col-12 d-flex align-items-center justify-content-start">
                                    <div class="d-flex align-items-center">
                                        <input type="time" name="time_serve[]" class="time_serve">
                                    </div>
                                </div>
                            </td>

                            <td class="text-end">
                                <span class="price_product pr-2">0</span>
                            </td>

                            <td><span class="text_x">x</span></td>

                            <td class="td_count_food">
                                <input type="number" class="count_food px-2" name="count_food[]" placeholder="ระบุ">
                            </td>

                            <td class="pr-5 text-end total_food_each"><span>0</span></td>
                            <input type="hidden" name="price[]" id="price" class="price_sum">
                            <td><img src="/img/icon/close-material.png" alt="" class="delete_menu"></td>
                        </tr>

                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-end">รวม</td>
                        <td class="pr-5 text-end">
                            <span class="text-end total_span">0</span>
                            <input type="hidden" name="total" class="total_input">
                        </td>
                        <td><span class="text-end">บาท</span></td>

                    </tr>

                </tfoot>

            </table>

            <div class="row" style="margin: 100px 0 25px 0">
                <div class="col-12 d-flex justify-content-center">
                    <a href="/reserve/room/form_room1?book_id={{ $book_id }}" class="comeback">ย้อนกลับ</a>
                    <button type="submit" class="approve_form">ส่งคำขอ</button>
                    {{-- <a href="/reserve/room/form_room3" class="approve_form">ส่งคำขอ</a> --}}
                </div>
            </div>

            <input type="hidden" name="book_id" value="{{ $book_id }}">
        </div>
    </form>

@endsection


@section('script')
    <script>
        function validateForm() {

            let array = [];

            if ($('input[type="radio"]:checked').val() == 'need') {
                $.each($('.choosed'), function(key, data) {
                    var period = $(this).find('.select_period option:selected').val().trim();
                    var menu = $(this).find('.choose_menu option:selected').val().trim();
                    var time = $(this).find('.time_serve').val().trim();
                    var count = $(this).find('.count_food').val().trim();


                    if (period === 'เลือกรายการ') {
                        $('.choosed:eq(' + key + ')').find('.select_period')
                            .fadeIn(1500, function() {
                                $(this).css('border-color', 'red');
                            });
                    }

                    if (menu === 'เลือกเมนูอาหาร') {
                        $('.choosed:eq(' + key + ')').find('.choose_menu')
                            .fadeIn(1500, function() {
                                $(this).css('border-color', 'red');
                            });
                    }

                    if (time === '') {
                        $('.choosed:eq(' + key + ')').find('.time_serve')
                            .fadeIn(1500, function() {
                                $(this).css('border-color', 'red');
                            });
                    }

                    if (count === '') {
                        $('.choosed:eq(' + key + ')').find('.count_food')
                            .fadeIn(1500, function() {
                                $(this).css('border-color', 'red');
                            });
                    }


                    if (period !== 'เลือกรายการ' && menu !== 'เลือกเมนูอาหาร' && time !== '' && count !== '') {
                        // console.log('true ready to access backend');
                        array.push(true);
                    } else {
                        // console.log('false not ready to access backend');
                        array.push(false);
                    }

                });
            }



            for (let i = 0; i < array.length; i++) {
                if (array[i] !== true) {

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'กรุณากรอกข้อมูล หรือ ลบรายการที่ไม่ได้กรอก',
                        showConfirmButton: false,
                        timer: 1500
                    });

                    return false;
                }
            }
            return true;
        }




        $(document).ready(function() {

            $('.form2').on('submit', function(e) {
                e.preventDefault();
                var book_id = $('input[name="book_id"]').val();

                if (validateForm() === true) {

                    $.ajax({
                        url: '/reserve/room/form_room2',
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            if (data == 'true') {
                                window.location.href =
                                    '/reserve/room/form_room3?book_id=' + book_id;
                            }
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });

                }

            });


            $('input[type="radio"]').on('click', function() {

                if ($(this).val() === 'no_need') {
                    $('input:not([type=radio]):not([name=_token]):not([name=book_id])').attr('disabled',
                        true);
                    $('select').attr('disabled', true);
                    $('.add_menu').css('cursor', 'not-allowed').attr('disabled', true);

                } else {
                    $('input:not([type=radio]):not([name=_token]):not([name=book_id])').attr('disabled',
                        false);
                    $('select').attr('disabled', false);
                    $('.add_menu').css('cursor', 'pointer').attr('disabled', false);
                }
            });



            get_length($('tbody tr').length);



            $('tbody').on('click', '.add_menu', function() {
                var count_list = $('.index_row_menu').last().text();
                var count = parseInt(count_list);
                var counted = count + 1;

                $('.choosed').last().clone().appendTo('tbody');
                $('.add_menu').not(':last').last().remove();
                $('.index_row_menu').not(':first').last().text(counted++);

                // // $('.choose_menu:first-child').attr('selected', true);
                // $('.choosed').find('.price_product').last().text(0);
                // $('.choosed').find('.count_food').last().val('');
                // $('.choosed').find('.total_food_each').last().text('');
                // $('.choosed').find('.price_sum').last().val('');

                var choosed = $('.choosed:last');

                var period = choosed.find('.select_period option:first-child');
                var menu = choosed.find('.choose_menu option:first-child');
                var time = choosed.find('.time_serve');
                var price = choosed.find('.price_product');
                var count = choosed.find('.count_food');
                var total_each = choosed.find('.total_food_each');
                var sum = choosed.find('.price_sum');


                period.attr('selected', true);
                menu.attr('selected', true);
                time.val('');
                price.text(0);
                count.val('');
                total_each.text('');
                sum.val('');



                get_length($('tbody tr').length);


            });







            var edit_order = JSON.parse($('#order_edit').val());

            if (edit_order != null) {
                var count_row = edit_order.detail_food.length;

                $.each(edit_order.detail_food, function(key, data) {

                    var count_list = $('.index_row_menu').last().text();
                    var count = parseInt(count_list);
                    var counted = count + 1;

                    $('.choosed').last().clone().appendTo('tbody');
                    $('.index_row_menu').not(':first').last().text(counted++);

                    var period = $('.select_period option:eq(' + (key) + ')').val();
                    var set_food = $('.choose_menu option:eq(' + (key + 1) + ')').val();
                    var time = $('.time_serve:eq(' + (key) + ')').val();
                    var count_food = $('.count_food:eq(' + (key) + ')').val();



                    $(".select_period option[value=" + data.period + "]:eq(" + (key) + ")").attr("selected",
                        true);
                    $('.choose_menu option[value="' + data.set_food + '"]:eq(' + (key) + ')').attr(
                        'selected', true);

                    $('.price_product:eq(' + (key) + ')').text(data.price);

                    $('.time_serve:eq(' + (key) + ')').val(data.time_serve);
                    $('.count_food:eq(' + (key) + ')').val(data.count_food);

                    var total = 0;
                    total = data.price * data.count_food;
                    $('.total_food_each:eq(' + (key) + ')').text(total);
                    $('.price_sum:eq(' + (key) + ')').val(data.price);


                    get_length($('tbody tr').length);
                });
                $('.total_span').text(edit_order.total);
                $('.total_input').val(edit_order.total);

                $('.choosed').last().remove();
                $('.add_menu').not(':last').remove();


            }











        });










        function get_length(index, checkData) {

            $('tr:eq(' + (index) + ')').on('change', '.choose_menu', function() {
                $(this).css('border-color', '');

                let price = $(this).find(":selected").data('price');

                if ($(this).val() === 'เลือกเมนูอาหาร') {
                    price = 0;
                }


                var total_each = 0;
                var selector = $(this).parents('tr');
                var count = parseInt(selector.find('.count_food').val());

                if (isNaN(count)) {
                    count = 0;
                    selector.find('.total_food_each').text(parseInt(count));

                } else {

                    total_each = parseInt(price) * count;
                    selector.find('.total_food_each').text(parseInt(total_each));
                }

                selector.find('.price_product').text(parseInt(price));
                // selector.find('.total_food_each').text(parseInt(total_each));
                selector.find('.price_sum').val(parseInt(price));

            });



            $('tr:eq(' + (index) + ')').on('keyup', '.count_food', function() {

                let count = $(this).val();
                let selector = $(this).parents('tr');
                let price = selector.find('.price_product').text();

                selector.find('.total_food_each').text(count * price);

                let total_all = parseInt(selector.find('.total_food_each').text());


                var total = 0;

                $('table tbody tr').each(function() {
                    var sum = 0;
                    $('.total_food_each').each(function() {
                        var value = parseInt($(this).text());
                        if (!isNaN(value)) {
                            sum += value;
                            total = sum;
                        }
                    });
                });



                $('.total_span').text(total);
                $('.total_input').val(total);

            });


            $('tr:eq(' + (index) + ')').on('click', '.delete_menu', function() {

                if ($('.choosed').length <= 1) {

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'ไม่สามารถลบแถวที่เหลือได้',
                        showConfirmButton: false,
                        timer: 1500
                    })

                } else {

                    $(this).parents('tr').fadeOut(300, function() {
                        $(this).remove();



                        $('.choosed:last td:eq(1)').append('<button type="button" class="add_menu">' +
                            '<img class="mr-2" src="/img/icon/add-circle-material.png" alt="">เพิ่มรายการ' +
                            '</button>');

                        if ($('.add_menu').length > 1) {
                            $('.add_menu').last().remove();
                        }
                    });
                }
            });



            $('tr:eq(' + (index) + ')').on('change', '.select_period', function() {
                $(this).css('border-color', '');
            });

            $('tr:eq(' + (index) + ')').on('change', '.time_serve', function() {
                $(this).css('border-color', '');
            });

            $('tr:eq(' + (index) + ')').on('change', '.count_food', function() {
                $(this).css('border-color', '');
            });


        }
    </script>
@endsection
