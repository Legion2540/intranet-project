@extends('layout.booking')
@section('style')
<style>
    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_evr:read-only {
        background-color: #f3f3f3 !important;
        /* font-weight: bold; */
    }

    .form-select {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        line-height: 1;
        padding: 0 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }


    .device_check {
        margin-right: 5px;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4a4a4a;
    }

    .comeback,
    .comeback:hover {
        width: 112px;
        height: 30px;
        margin: 0 17px 0 0;
        border-radius: 8px;
        padding: 4px 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }


    input[type="date"]::-webkit-calendar-picker-indicator {
        display: block;
        background: url("/img/icon/date-range-material.png") no-repeat;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 8px;
        left: 250px;
    }

    input[type="time"]::-webkit-calendar-picker-indicator {
        opacity: 1;
        display: block;
        background: url("/img/icon/access-time-material.png") no-repeat;
        width: 20px;
        height: 20px;
        align-items: center;
        position: absolute;
        top: 8px;
        left: 105px;
    }

</style>
@endsection
@section('content')
<div class="my-3 px-5 py-3" style="background-color:white">
    <div class="row">
        <span style="font-family: Kanit-Regular; font-size: 26px; font-weight: bold; color: #4f72e5;">คำขอจอง VDO
            Conference</span>
    </div>
    <div class="my-2" style="border:0.5px dashed #ceced4"></div>


    <div class="row">
        <div class="col-12 my-3" style="padding: 0 250px;">
            <ul class="timeline">
                <li class="active">รายละเอียดการจอง</li>
                <li class=""></li>
                <li class=""></li>
                <li></li>
            </ul>
        </div>
    </div>


    <div class="col-12 mt-3">
        <span class="title_form">ผู้ขอจอง</span>
    </div>

    <form class="confer1">
        @csrf

        {{-- Name & Agency --}}
        <div class="row d-flex  mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">ชื่อ-สกุล</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" placeholder="ชื่อ-สกุล" name="name_user"
                        value="นางสาวดาริสา พัชรโชด" readonly>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0 ">
                    <span class="content_span">สำนัก</span>
                </div>

                <div class="col-10 ">
                    <input type="text" class="input_evr" value="สำนักประชาสัมพันธ์และสื่อสารองค์กร" name="office_id"
                        readonly>
                </div>
            </div>

        </div>

        {{-- Position & Group --}}
        <div class="row d-flex mt-3">

            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">กลุ่มงาน</span>
                </div>
                <div class="col-10">
                    <input type="text" class="input_evr" value="สื่อประชาสัมพันธ์" name="group_id" readonly>
                </div>
            </div>



            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">ตำแหน่ง</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" value="เจ้าหน้าที่สื่อประชาสัมพันธ์" name="position" readonly>
                </div>
            </div>
        </div>

        {{-- Number Phone --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-3 pr-0 ">
                    <span class="content_span">ผู้บังคับบัญชา</span>
                </div>

                <div class="col-9 pl-0">
                    <input class="input_evr" type="text" value="นายกันต์ สินธร" name="manager_id" readonly>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-4 pr-0 ">
                    <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                </div>

                <div class="col-8 pl-0">
                    <input class="input_evr" type="text" value="02-777-12000" name="tel_in_no" readonly>
                </div>
            </div>
        </div>



        <div class="col-12 mt-3">
            <span class="title_form">รายละเอียดการจองห้องประชุม</span>
        </div>




        {{-- Title & send to --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">เรื่อง</span>
                </div>
                <div class="col-10">
                    <select class="form-select" name="title">
                        <option {{ (@$room['title'] == null) ? '' : 'selected' }} selected>กรุณาเลือกหัวข้อคำร้อง
                        </option>
                        <option {{ (@$room['title'] == 'ขออนุญาติใช้ห้องประชุมและโสดทัศนูปกรณ์') ? 'selected' : '' }}
                            value="ขออนุญาติใช้ห้องประชุมและโสดทัศนูปกรณ์">ขออนุญาติใช้ห้องประชุมและโสดทัศนูปกรณ์
                        </option>
                        <option {{ (@$room['title'] == '2') ? 'selected' : '' }} value="2">Two</option>
                        <option {{ (@$room['title'] == '3') ? 'selected' : '' }} value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">เรียน</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" name="request_to" value="{{ @$room['request_to'] }}">
                </div>
            </div>
        </div>

        {{-- Objective --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">วัตถุประสงค์</span>
                </div>
                <div class="col-10">
                    <input class="input_evr" type="text" name="objective" value="{{ @$room['objective'] }}">
                </div>
            </div>
        </div>

        {{-- Date & Time --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">วันที่เริ่มใช้</span>
                </div>

                <div class="col-6">
                    <input class="input_evr"
                        style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;" type="date"
                        name="start_date" value="{{ @$room['start_date'] }}">
                    {{-- <img src="/img/icon/date-range-material.png" style="position: absolute; top: 8px; left: 215px;"
                        alt=""> --}}
                </div>

                <div class="col-1">
                    <span class="content_span">เวลา</span>
                </div>
                <div class="col-3">
                    <input class="input_evr"
                        style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;" type="time"
                        name="start_time" value="{{ @$room['start_time'] }}">
                    {{-- <img src="/img/icon/access-time-material.png" style="position: absolute; top: 8px; left: 90px;"
                        alt=""> --}}
                </div>

            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">วันที่สิ้นสุด</span>
                </div>

                <div class="col-6">
                    <input class="input_evr"
                        style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;" type="date"
                        name="end_date" value="{{ @$room['end_date'] }}">
                    {{-- <img src="/img/icon/date-range-material.png" style="position: absolute; top: 8px; left: 215px;"
                        alt=""> --}}
                </div>

                <div class="col-1">
                    <span class="content_span">เวลา</span>
                </div>
                <div class="col-3">
                    <input class="input_evr"
                        style="border-radius: 8px; border: solid 0.5px #ceced4; background-color: #ffffff;" type="time"
                        name="end_time" alue="{{ @$room['end_time'] }}">
                    {{-- <img src="/img/icon/access-time-material.png" style="position: absolute; top: 8px; left: 90px;"
                        alt=""> --}}
                </div>

            </div>
        </div>

        {{-- Place &  Room --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center">
                <div class="col-2">
                    <span class="content_span">สถานที่</span>
                </div>

                <div class="col-10">
                    <select class="form-select" name="place">
                        <option {{ ( @$room['place'] == null) ? '' : 'selected' }}>กรุณาเลือกสถานที่</option>
                        <option {{ ( @$room['place'] == 'รัชดา') ? 'selected' : '' }} value="รัชดา">รัชดา</option>
                        <option {{ ( @$room['place'] == '2') ? 'selected' : '' }} value="2">Two</option>
                        <option {{ ( @$room['place'] == '3') ? 'selected' : '' }} value="3">Three</option>
                    </select>
                </div>
            </div>

            <div class="col-6 d-flex align-items-center">
                <div class="col-2 pr-0">
                    <span class="content_span">ห้องประชุม</span>
                </div>

                <div class="col-10">
                    <select class="form-select room_request" aria-label="สำนักบริหารกลาง" name="room_request">
                        <option {{ ( @$room['room_request'] == null) ? '' : 'selected' }}></option>
                        <option {{ ( @$room['room_request'] == 'ห้องประชุม 1') ? 'selected' : '' }} value="ห้องประชุม 1">
                            ห้องประชุม 1</option>
                        <option {{ ( @$room['room_request'] == '2') ? 'selected' : '' }} value="2">Two</option>
                        <option {{ ( @$room['room_request'] == '3') ? 'selected' : '' }} value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>


        {{-- Device In Room  & Staff device--}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-start">
                <div class="col-4 pr-0">
                    <span class="content_span">อุปกรณ์โสดทัศนูปกรณ์</span>
                </div>

                <div class="col-10 d-flex pl-0 align-items-center">
                    <div class="row d-flex align-items-center mr-2">

                        <div class="d-flex align-items-center mr-2">
                            <input type="hidden" id="device_id" value="{{ @$room['device_id'] }}">
                            <input type="checkbox" name="device_room[]" value="projector" id="" class="device_check">
                            <span class="content_span mr-3">เครื่องฉาย PROJECTOR</span>
                            <input type="checkbox" name="device_room[]" value="vdo" id="" class="device_check">
                            <span class="content_span mr-3">เครื่องฉาย VDO</span>
                            <input type="checkbox" name="device_room[]" value="record" id="" class="device_check">
                            <span class="content_span mr-3">อัดเทป</span>
                        </div>



                        <div class="d-flex align-items-center mr-2 pr-0">
                            <div>
                                <input type="checkbox" name="device_room[]" value="conference" id=""
                                    class="device_check">
                                <span class="content_span mr-4">VDO Conference</span>
                            </div>

                            <div>
                                <input type="checkbox" name="device_room[]" value="visual" id="" class="device_check">
                                <span class="content_span">เครื่องฉายVISUAL (ฉายกระดาษทีป-ใส)</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>


            <div class="col-6 d-flex align-items-start">
                <div class="col-6 pr-0">
                    <span class="content_span">เจ้าหน้าที่ควบคุมอุปกรณ์โสดทัศนูปกรณ์</span>
                </div>

                <div class="col-6 d-flex pl-0 align-items-center">
                    <div class="row d-flex align-items-center mr-2">

                        <div class="d-flex align-items-center mr-2">
                            <input type="radio" name="staff_device" {{ ( @$room['staff'] == "need") ? 'checked' : '' }} value="need" id="need_operator"
                                class="device_check">
                            <span class="content_span mr-3">ต้องการ</span>
                            <input type="radio" name="staff_device" {{ ( @$room['staff'] == "no_need") ? 'checked' : '' }} value="no_need" id="no_need_operator"
                                class="device_check">
                            <span class="content_span mr-3">ไม่ต้องการ</span>
                        </div>

                    </div>
                </div>

            </div>


        </div>


        {{-- Boss --}}
        <div class="row d-flex mt-3">
            <div class="col-6 d-flex align-items-center ml-3">
                <div style="width: 100px">
                    <span class="content_span">ผู้บังคับบัญชา</span>
                </div>

                <div class="col-10 pl-0">
                    <select class="form-select" name="mamager_id">
                        <option selected>กรุณาเลือกผู้บังคับบัญชา</option>

                        {{-- @foreach ($manager as $boos) --}}
                        <option {{ ( @$room['manager_id'] == '1') ? 'selected' : '' }} value="1">One</option>
                        <option {{ ( @$room['manager_id'] == '2') ? 'selected' : '' }} value="2">Two</option>
                        <option {{ ( @$room['manager_id'] == '3') ? 'selected' : '' }} value="3">Three</option>
                        {{-- @endforeach --}}

                    </select>
                </div>
            </div>
        </div>



        <div style="margin-bottom:100px"></div>

        <div class="col-12 d-flex justify-content-center mt-5">
            <a href="/reserve/room?" class="comeback">ย้อนกลับ</a>
            <button type="submit" class="btn btn_rent_room shadow">ถัดไป</button>

        </div>


    </form>



</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('.confer1').on('submit', function (e) {
            e.preventDefault();


            $.ajax({
                url: '/reserve/vdoconference/vdoConference1',
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {

                    var book_id = data.book_id

                    if (data.status == 'true') {
                        window.location.href =
                            '/reserve/vdoconference/vdoConference2?book_id=' + book_id;
                    }

                },
                error: function (err) {
                    console.log(err);
                }

            });
        });



        var data = JSON.parse($('#device_id').val());

        if (data) {
            $.each(data, function (key, value) {
                $('input[value="' + value + '"]').attr('checked', true);
            });
        }
    });

</script>
@endsection
