@extends('layout.booking')
@section('style')
    <style>
        .title_room {
            font-family: Kanit-Regular;
            font-size: 26px;
            font-weight: bold;
            color: #4f72e5;
        }

        .comeback,
        .comeback:hover {
            width: 112px;
            height: 30px;
            margin: 0 17px 0 0;
            border-radius: 8px;
            padding: 4px 0;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-color: #ee2a27;
            color: #ffffff;
            text-align: center;
            text-decoration: none;
        }

        .approve_form {
            color: #ffffff;
            width: 112px;
            height: 30px;
            margin: 0 0 0 17px;
            border-radius: 8px;
            text-align: center;
            border: none;
            text-decoration: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(266deg, #4f72e5 36%, #314d7b 145%);
        }

        .input_text {
            width: 100%;
            height: 30px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            background-color: #fff;
            padding: 0 8px;
        }

        .img_preview {
            width: 350px;
            height: 165px;
            border: solid 2px #4f72e5;
            background-color: #fff;
        }


        .preview_img {
            width: 50px;
            height: 80px;
        }

        .select_room {
            width: 155px;
            height: 30px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            background-color: #fff;
        }

        .upload_img_room {
            width: 110px;
            height: 30px;
            border: none;
            margin-left: 10px;
            color: #ffffff;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(194deg, #4f72e5 5%, #314d7b 132%);
        }

    </style>
@endsection

@section('content')
    <div class="my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
        <div class="row">
            <span class="title_room">รายละเอียดห้องประชุม</span>
        </div>
        <div class="my-2" style="border:0.5px dashed #ceced4"></div>



        <form id="create_room" action="/building/add" method="post" enctype="multipart/form-data">
            @csrf

            <div class="col-12 d-flex p-0">
                <div class="col-6 d-flex algin-content-center">
                    <div class="col-2 p-0">
                        <span>หมายเลขห้อง</span>
                    </div>
                    <div class="col-10 p-0">
                        <input type="text" name="number_room" class="input_text" placeholder="ระบุหมายเลขห้อง" value="">
                    </div>
                </div>
                <div class="col-6 d-flex algin-content-center">
                    <div class="col-2 p-0">
                        <span>ชื่อห้อง</span>
                    </div>
                    <div class="col-10 p-0">
                        <input type="text" name="name_room" class="input_text" placeholder="ระบุชื่อห้อง" value="">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex p-0 mt-3">
                <div class="col-6 d-flex p-0">
                    <div class="col-6 d-flex algin-content-center">
                        <div class="col-2 p-0">
                            <span>ชั้น</span>
                        </div>
                        <div class="col-10 p-0">
                            <input type="text" name="level" class="input_text" placeholder="ระบุชั้น" value="">
                        </div>
                    </div>
                    <div class="col-6 d-flex algin-content-center">
                        <div class="col-2 p-0">
                            <span>อาคาร</span>
                        </div>
                        <div class="col-10 p-0">
                            <input type="text" name="no_building" class="input_text" placeholder="ระบุอาคาร" value="">
                        </div>
                    </div>
                </div>
                <div class="col-6 d-flex algin-content-center">
                    <div class="col-2 p-0">
                        <span>สถานที่</span>
                    </div>
                    <div class="col-10 p-0 d-flex align-items-center">
                        <div class="col-3">
                            <input type="radio" name="place" value="รัชดา">&nbsp;
                            <span>รัชดา</span>
                        </div>
                        <div class="col-3">
                            <input type="radio" name="place" value="บางกระสอ">&nbsp;
                            <span>บางกระสอ</span>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-12 d-flex p-0 mt-3">
                <div class="col-6 d-flex p-0">
                    <div class="col-6 d-flex algin-content-center">
                        <div class="col-4 p-0">
                            <span>ประเภทห้อง</span>
                        </div>
                        <div class="col-8 p-0">
                            <select name="type_room" class="select_room" id="">
                                <option>เลือกรูปแบบห้อง</option>
                                <option value="meeting">ห้องประชุม</option>
                                <option value="conference">Conference</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 d-flex algin-content-center">
                        <div class="col-2 p-0">
                            <span>ขนาด</span>
                        </div>
                        <div class="col-10 p-0">
                            <select name="size_room" class="select_room" id="">
                                <option>เลือกขนาดห้อง</option>
                                <option value="เล็ก">เล็ก</option>
                                <option value="กลาง">กลาง</option>
                                <option value="ใหญ่">ใหญ่</option>
                            </select>
                        </div>
                    </div>
                </div>



                <div class="col-6 d-flex">
                    <div class="col-2 p-0">
                        <span>จำนวนรวม</span>
                    </div>
                    <div class="col-10 p-0 d-flex align-items-center">
                        <input type="text" name="member_count" value="" class="input_text w-25" placeholder="ระบุจำนวนคน">
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex p-0 mt-3">
                <div class="col-6 d-flex">

                    <div class="col-3 p-0">
                        <span>อุปกรณ์โสดทัศนูปกรณ์</span>
                    </div>

                    <div class="col-9 d-flex">
                        <div class="row">
                            <div class="col-6">
                                <input type="checkbox" name="device[เครื่องฉาย PROJECTOR]" value="เครื่องฉาย PROJECTOR">&nbsp;
                                <input type="hidden" name="device[เครื่องฉาย PROJECTOR]" value={{ null }}>
                                <span>เครื่องฉาย PROJECTOR</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[เครื่องฉาย VDO]" value="เครื่องฉาย VDO">&nbsp;
                                <input type="hidden" name="device[เครื่องฉาย VDO]" value={{ null }}>
                                <span>เครื่องฉาย VDO</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[อัดเทป]" value="อัดเทป">&nbsp;
                                <input type="hidden" name="device[อัดเทป]" value={{ null }}>
                                <span>อัดเทป</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[VDO Conference]" value="VDO Conference">&nbsp;
                                <input type="hidden" name="device[VDO Conference]" value={{ null }}>
                                <span>VDO Conference</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[เครื่องฉาย VISUAL (ฉายกระดาษทีป - ใส)]" value="เครื่องฉาย VISUAL (ฉายกระดาษทีป - ใส)">&nbsp;
                                <input type="hidden" name="device[เครื่องฉาย VISUAL (ฉายกระดาษทีป - ใส)]" value={{ null }}>
                                <span>เครื่องฉาย VISUAL (ฉายกระดาษทีป - ใส)</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[ไมค์โครโฟน]" value="ไมค์โครโฟน">&nbsp;
                                <input type="hidden" name="device[ไมค์โครโฟน]" value={{ null }}>
                                <span>ไมค์โครโฟน</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[กระดานไวท์บอร์ด]" value="กระดานไวท์บอร์ด">&nbsp;
                                <input type="hidden" name="device[กระดานไวท์บอร์ด]" value={{ null }}>
                                <span>กระดานไวท์บอร์ด</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[สาย HDMI]" value="สาย HDMI">&nbsp;
                                <input type="hidden" name="device[สาย HDMI]" value={{ null }}>
                                <span>สาย HDMI</span>
                            </div>
                            <div class="col-6">
                                <input type="checkbox" name="device[เครื่องกระจายเสียง]" value="เครื่องกระจายเสียง">&nbsp;
                                <input type="hidden" name="device[เครื่องกระจายเสียง]" value={{ null }}>
                                <span>เครื่องกระจายเสียง</span>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


            <div class="col-12 d-flex p-0 mt-3">
                <div class="col-4 d-flex align-items-end">
                    <div class="img_preview text-center">
                        <img src="" alt="" id="room_img" style="display: none;width:100%; height:100%; object-fit:contain;">

                        <div class="col-12 room_mockup mt-4">
                            <img src="/img/icon/insert-photo-material.png" alt="">
                            <div class="col-12 mt-2 room_mockup">
                                <span class="content_span_upload_file">Upload Image</span>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="upload_img_room">อัพโหลดไฟล์</button>
                    <input type="file" name="img_room" id="img_room" style="display:none;">
                </div>
            </div>


            <div class="row" style="margin: 100px 0 25px 0">
                <div class="col-12 d-flex justify-content-center">
                    <a href="/reserve/room/form_room1" class="comeback">ย้อนกลับ</a>
                    <button type="submit" class="approve_form">บันทึก </button>
                </div>
            </div>
        </form>

    </div>
@endsection


@section('script')
    <script>
        $(document).ready(function() {


            $('input[type="checkbox"]').on('click',function(){
                if($(this).is(':checked')){
                    $(this).next().attr('name','');
                }else{
                    $(this).next().attr('name',$(this).parent().find(':first-child').attr('name'));
                }
            });



            $('.upload_img_room').on('click', function() {
                $('input[type="file"]').click();

            });
            $('input[type="file"]').change(function() {
                readURL(this);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.img_preview').css({
                            'border': 'solid 1px #4f72e5',
                            'padding': '10px'
                        });
                        $('.room_mockup').css('display', 'none');
                        $('#room_img').css('display', 'block');
                        $('#room_img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    alert('select a file to see preview');
                    $('#room_img').attr('src', '');
                }
            }


















        });
    </script>
@endsection
