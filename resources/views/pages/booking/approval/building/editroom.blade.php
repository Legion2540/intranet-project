@extends('layout.booking')
@section('style')
    <style>
        .card-background {
            background-color: white;
            max-width: 1400px !important;
        }

        .title_room {
            font-family: Kanit-Regular;
            font-size: 18px;
            font-weight: bold;
            color: #4f72e5;
        }

        .title {
            font-family: Kanit-Regular;

        }

        .search {
            width: 420px;
            height: 30px;
            border-radius: 8px;
            padding: 0 8px;
            border: solid 0.5px #ceced4;
            background-color: #fff;
            position: relative;
        }

        #magnifier {
            position: absolute;
            right: 190px;
        }

        .export {
            width: 147px;
            height: 30px;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(188deg, #4f72e5 30%, #314d7b 204%);
            border: none;
            padding: 5px 40px;
            color: #fff;
        }

        .img_room {
            width: 236.5px;
            height: 140.7px;
            border: solid 1px #979797;
            position: relative;
        }

        .change_room_img {
            cursor: pointer;
            position: absolute;
            bottom: -11px;
            left: 238px;

        }

        .input_text {
            width: 137px;
            height: 30px;
            padding: 0 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            background-color: #fff;
        }

        .toggle-switch:checked {
            background-color: #4BD763;
            border-color: #4BD763;
            border: none;
        }

        .toggle-switch {
            background-color: #ceced4;
            border-color: #ceced4;
            border: none;

        }

        .cancelled_adjust_device {
            width: 90px;
            height: 30px;
            color: #fff;
            border: none;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-color: #ee2a27;
        }

        .submit_adjust_device {
            width: 90px;
            height: 30px;
            border: none;
            color: #fff;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(212deg, #4f72e5 42%, #314d7b 184%);
        }

        .edit_room {
            width: 147px;
            height: 30px;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(188deg, #4f72e5 30%, #314d7b 204%);
            border: none;
            padding: 0;
            color: #fff;
        }

        .room_img {
            display: none;
        }

    </style>
@endsection

@section('content')
    <div class="my-3 px-5 py-3 card-background">
        <div class="row">
            <div class="col-5 d-flex align-items-center">
                <img src="/img/icon/group-work-material@2x.png" alt="" class="mr-3">
                <span class="title_room">{{ $room['name_room'] }} ชั้น {{ $room['level'] }} อาคาร
                    {{ $room['name_building'] }}</span>

            </div>
            <div class="col-7 d-flex align-items-center justify-content-end text-center">
                <img src="/img/icon/date-range-material-copy-4.png" alt="" class="mx-3" style="cursor: pointer;">
                <img src="/img/icon/tune-material-copy-3.png" alt="" class="mx-3" style="cursor: pointer;">

                <input type="search" class="search mx-3" placeholder="ค้นหารายการ">
                <img src="/img/icon/search-material-bule.png" alt="" id="magnifier">

                <button class="export d-flex align-items-center"><img src="/img/icon/file_upload-material.png"
                        alt="">&nbsp;Export</button>
            </div>
        </div>

        <div class="row mt-4 mb-5">
            <div class="col-12 d-flex p-0">
                <div class="col-3"><span class="title">รูปภาพ</span></div>
                <div class="col-9"><span class="title">ข้อมูลห้องประชุม</span></div>
            </div>


            <form class="editroom" enctype="multipart/form-data">
                @csrf

                <div class="col-12 d-flex p-0">
                    <div class="col-3 mt-3">
                        <img src="{{ $room['img_room'][0] != '' ? '/img/room/' . $room['img_room'][0] : '/img/icon/insert-photo-material@2x.png' }}"
                            class="img_room" alt="" onerror="this.src='/img/icon/insert-photo-material@2x.png'">
                        <img src="/img/icon/icon-camera.png" alt="" class="change_room_img">
                        <input type="file" name="room_img" class="room_img">
                    </div>


                    <input type="hidden" name="room_id" value="{{ $room['id'] }}">

                    <div class="col-9">

                        <div class="row mt-2 d-flex align-items-center">
                            <div class="col-4 d-flex">
                                <div class="col-4 p-0">
                                    <span class="title">หมายเลขห้อง</span>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="input_text" name="no_room" id="no_room"
                                        value="{{ $room['no_room'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>

                            <div class="col-4 d-flex">
                                <div class="col-2 p-0">
                                    <span class="title">ชื่อห้อง</span>
                                </div>
                                <div class="col-10">
                                    <input type="text" class="input_text" name="name_room" id="name_room"
                                        value="{{ $room['name_room'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>

                            <div class="col-4">
                                <span class="title">จัดการอุปกรณ์โสดทัศน์</span>
                                <img src="/img/icon/border-color-material-copy.png" alt="" style="cursor: pointer"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal">



                            </div>

                        </div>



                        <div class="row mt-2 d-flex align-items-center">
                            <div class="col-4 d-flex">
                                <div class="col-4 p-0">
                                    <span class="title">ชั้น</span>
                                </div>

                                <div class="col-8">
                                    <input type="text" class="input_text" name="level" id="level"
                                        value="{{ $room['level'] }}">
                                    <img src="" alt="">
                                </div>

                            </div>

                            <div class="col-4 d-flex">
                                <div class="col-2 p-0">
                                    <span class="title">อาคาร</span>
                                </div>
                                <div class="col-10">
                                    <input type="text" class="input_text" name="name_building" id="name_building"
                                        value="{{ $room['name_building'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>

                            <div class="col-4 d-flex">
                                <div class="col-4 p-0">
                                    <span class="title">การแสดงผล</span>
                                </div>
                                <div class="col-6">
                                    <div class="form-switch">
                                        <input class="form-check-input toggle-switch" type="checkbox" name="status"
                                            value="{{ $room['status'] }}"
                                            {{ $room['status'] != 'not ready' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row mt-2 d-flex align-items-center">
                            <div class="col-4 d-flex">
                                <div class="col-4 p-0">
                                    <span class="title">ประเภทห้อง</span>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="input_text" name="type_room" id="type_room"
                                        value="{{ $room['type_room'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>

                            <div class="col-4 d-flex">
                                <div class="col-2 p-0">
                                    <span class="title">ขนาด</span>
                                </div>
                                <div class="col-10">
                                    <input type="text" class="input_text" name="size" id="size"
                                        value="{{ $room['size'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>
                        </div>


                        <div class="row mt-2 d-flex align-items-center">
                            <div class="col-4 d-flex">
                                <div class="col-4 p-0">
                                    <span class="title">จำนวนที่นั่ง</span>
                                </div>
                                <div class="col-8">
                                    <input type="text" class="input_text" name="member_count" id="member_count"
                                        value="{{ $room['opacity'] }}">
                                    <img src="" alt="">
                                </div>
                            </div>

                            <div class="col-4 d-flex">
                                <div class="col-2 p-0">
                                    <span class="title">ขนาด</span>
                                </div>
                                <div class="col-10 d-flex">
                                    <div class="col-4 pl-0">
                                        <input type="radio" name="place" value="รัชดา"
                                            {{ $room['place'] == 'รัชดา' ? 'checked' : '' }}>&nbsp;
                                        <span>รัชดา</span>
                                    </div>
                                    <div class="col-8 ">
                                        <input type="radio" name="place" value="บางกระสอ"
                                            {{ $room['place'] == 'บางกระสอ' ? 'checked' : '' }}>&nbsp;
                                        <span>บางกระสอ</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 d-flex">
                                <button type="submit" class="edit_room">บันทึกการแก้ไข</button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>

        </div>







        <table class="table" id="add_menu">
            <thead>

                <tr class="vertical-center">
                    <th></th>
                    <th><span>วันที่จอง</span></span></th>
                    <th>ผู้ขอใช้รถ</th>
                    <th><span>วัตถุประสงค์</span></thx>
                    <th><span>อุปกรณ์โสดทัศน์</span></th>
                    <th>เจ้าหน้าที่อุปกรณ์โสดทัศน์</th>
                    <th>อาหารว่าง</th>
                    <th>เวลาเริ่มใช้</th>
                    <th>เวลาสิ้นสุด</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

            </thead>
            <tbody>

                @if (empty($history))
                    <tr>
                        <td class="text-center" colspan="12">ไม่มีข้อมูล</td>
                    </tr>
                @else
                    @foreach ($history as $item)

                        <tr>
                            <td><input type="checkbox" name="check_id" class="check_id"></td>
                            <td>{{ $item['start_date'] }}</td>
                            <td>{{ $item->datauser->name }}</td>
                            <td>{{ $item['objective'] }}</td>
                            <td class="text-center">

                                @foreach(json_decode($item['device_id']) as $device)
                                <span>{{ $device }}</span><br>
                                @endforeach

                            </td>
                            <td>{{ $item->datauser->name }}</td>

                            @if(!empty($item['food_detail']))
                                <td><img src="/img/icon/check-material.png" alt=""></td>
                            @else
                                <td><img src="/img/icon/close-material.png" alt=""></td>
                            @endif

                            <td>{{ $item['start_time'] }}</td>
                            <td>{{ $item['end_time'] }}</td>
                            <td><img src="/img/icon/description-material.png" alt=""></td>
                            <td><img src="/img/icon/border-color-material-copy.png" alt=""></td>
                            <td><img src="/img/icon/delete-material-copy-25.png" alt=""></td>
                        </tr>
                    @endforeach
                @endif

            </tbody>

        </table>

    </div>







    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content p-3" style="width: 600px;">

                {{-- <form class="status_device" action="/building/status/device" method="post"> --}}
                <form class="status_device" method="post">
                    @csrf
                    <input type="hidden" name="room_id" value="{{ $room['id'] }}">

                    <div class="modal-body">
                        <table class="table" id="adjust_device">
                            <thead>
                                <tr>
                                    <th>จัดการอุปกรณ์โสดทัศน์</th>
                                    <th>ทั้งหมด</th>
                                    <th>
                                        <div class="form-switch all_check">
                                            <input class="form-check-input toggle-switch" type="checkbox">
                                            <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                        </div>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach ($room['device_id'] as $key => $item)
                                    <tr>
                                        <th>{{ $key }}</th>
                                        <th>การแสดงผล</th>
                                        <th>
                                            <div class="form-switch">
                                                <input class="form-check-input toggle-switch device_id" type="checkbox"
                                                    value="{{ $item }}" name="device_id[{{ $key }}]"
                                                    data-id="{{ $key }}"
                                                    data-name="device_id[{{ $key }}]"
                                                    {{ $item != null ? 'checked="checked"' : '' }}>

                                                <input type="hidden" class="device_id"
                                                    name="device_id[{{ $key }}]"
                                                    value={{ $item == null ? null : $item }}>

                                                <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <button type="submit" class="submit_adjust_device mr-3">บันทึก</button>
                        <button type="button" class="cancelled_adjust_device">ยกเลิก</button>
                    </div>
                </form>

            </div>
        </div>
    </div>




@endsection


@section('script')
    <script>
        $(document).ready(function() {

            let data = [];

            $('.device_id:first-child').each(function(key, value) {
                data.push($(this).val());
            });

            data.every(checkData);

            if (data.every(checkData) == true) {
                $('.all_check input').prop('checked', true);
            } else {
                $('.all_check input').prop('checked', false);
            }


            function checkData(data) {
                return data !== '';
            }


            $('.all_check input').on('click', function() {

                if ($(this).is(':checked')) {
                    $(this).prop('checked', true);

                    $('.device_id:first-child').each(function(key, value) {
                        $(this).val($(this).data('id'));
                        $(this).attr('checked', true);
                        $(this).prop('checked', true);
                    });

                    $('.device_id:nth-child(2)').each(function(key, value) {
                        $(this).attr('name', '');
                        $(this).attr('checked', false);
                        $(this).prop('checked', false);
                    });

                } else {
                    $(this).attr('checked', false);
                    $(this).prop('checked', false);

                    $('.device_id:first-child').each(function(key, value) {
                        $(this).val($(this).data('id'));
                        $(this).attr('checked', false);
                        $(this).prop('checked', false);
                    });

                    $('.device_id:nth-child(2)').each(function(key, value) {
                        $(this).attr('name', $(this).parent().find('input:first-child').data(
                            'name'));
                        $(this).attr('checked', true);
                        $(this).prop('checked', true);
                        $(this).val(null);
                    });
                }


            });




            $('.device_id').on('click', function() {


                if ($(".device_id:first-child").length == $(".device_id:checked").length) {
                    $(".all_check input").prop("checked", true);
                } else {
                    $(".all_check input").prop("checked", false);
                }


                if ($(this).is(':checked')) {
                    $(this).val($(this).data('id'));
                    $(this).attr('name', $(this).data('name'));
                    $(this).attr('checked', true);
                    $(this).prop('checked', true);

                    $(this).next().attr('name', '');
                    $(this).next().attr('checked', false);
                    $(this).next().prop('checked', false);

                } else {
                    $(this).next().attr('name', $(this).parent().find(':first-child').data('name'));
                    $(this).next().val(null);
                    $(this).next().attr('checked', true);
                    $(this).next().prop('checked', true);


                    $(this).attr('name', '');
                    $(this).attr('checked', false);
                    $(this).prop('checked', false);
                }
            });


            $('.editroom').on('submit', function(e) {
                e.preventDefault();

                $.ajax({
                    url: '/building/editroom',
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'แก้ไขรายละเอียดห้องสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            $('.modal').modal('hide');
                        });
                    }
                })
            });


            $('.status_device').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: '/building/status/device',
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(res) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'แก้ไขอุปกรณ์โสดทัศน์สำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(() => {
                            $('.modal').modal('hide');
                        });

                    }
                });
            })
            $('.cancelled_adjust_device').on('click', function() {
                $('.modal').modal('hide');
            });



            $('.change_room_img').on('click', function() {
                $('.room_img').click();
            });


            $('.room_img').change(function() {
                // readURL(this);
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.img_room').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                } else {
                    alert('select a file to see preview');
                    $('.img_room').attr('src', '');
                }
            });



        });
    </script>
@endsection
