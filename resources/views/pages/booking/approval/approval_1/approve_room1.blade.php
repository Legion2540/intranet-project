@extends('layout.booking')
@section('style')
    <style>
        .input_evr {
            width: 100%;
            height: 30px;
            border-radius: 8px;
            padding: 0 10px;
            background-color: #f3f3f3 !important;
        }

        .device_check {
            margin-right: 5px;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4a4a4a;
        }

        .title_form {
            font-family: Kanit-Regular;
            font-size: 16px;
            font-weight: 600;
            color: #4a4a4a;

        }

        .content_span {
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #4a4a4a;
        }


        /* .... */

        .title_room2 {
            font-family: Kanit-ExtraLight;
            font-size: 26px;
            font-weight: bold;
        }

        .select_form_room2 {
            width: 135px;
            height: 30px;
            padding: 5px 6.5px 3px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .select_food {
            width: 301px;
            height: 30px;
            padding: 5px 6.5px 3px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .form_time {
            width: 100px;
            height: 30px;
            margin: 0 0 0 8px;
            padding: 4px 4px 4px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
            background-color: #f3f3f3 !important;

        }

        #input_price {
            background-color: ##f3f3f3;
            width: 100px;
            height: 30px;
            margin: 0 0 0 8px;
            padding: 4px 4px 4px 9px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .warning_span {
            font-size: 12px;
            line-height: 1.25;
            color: #ee2a27;
        }

        .data_result {
            color: #4f72e5;
            font-size: 14px;
        }


        .comeback {
            width: 112px;
            height: 30px;
            margin: 0 17px 0 0;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-color: #ff7a00;
            color: #ffffff;
        }

        .cancel,
        .cancel:hover {
            width: 112px;
            height: 30px;
            font-size: 16px;
            padding: 4px 17px;
            border-radius: 8px;
            font-weight: 500;
            color: #ffffff;
            text-align: center;
            background-color: #ee2a27;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        .waiting_approve,
        .waiting_approve:hover {
            width: 112px;
            height: 30px;
            font-size: 16px;
            padding: 4px 17px;
            border-radius: 8px;
            font-weight: 500;
            color: #ffffff;
            text-align: center;
            background-color: #ffbf00;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            text-decoration: none;
        }

        /* Status Room after approve | Not Approve */
        .status_yellow {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #ffbf00;
            border-radius: 10px;
        }

        .status_red {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 10px;
        }

        .status_green {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #6dd005;
            border-radius: 10px;
        }

        .approve_view {
            width: 112px;
            height: 30px;
            background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
            color: #ffffff;
            border-radius: 8px;
            font-size: 16px;
            padding: 4px 18px;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        /* Status Room after approve | Not Approve */



        /* In Cancel Textarea Modal */
        .disapproval {
            width: 112px;
            height: 30px;
            font-size: 16px;
            padding: 4px 17px;
            border-radius: 8px;
            font-weight: 500;
            color: #ffffff;
            text-align: center;
            background-color: #ee2a27;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        .cancel_accept_btn {
            width: 112px;
            height: 30px;
            background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
            color: #ffffff;
            border-radius: 8px;
            font-size: 16px;
            padding: 4px 18px;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        /* In Cancel Textarea Modal */


        /* In Accept Cancel Modal */
        .accept_cancel_btn {
            width: 112px;
            height: 30px;
            background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
            color: #ffffff;
            border-radius: 8px;
            font-size: 16px;
            padding: 4px 18px;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        /* In Accept Cancel Modal */

        /* Accept Modal */
        .accept_approve_room {
            width: 112px;
            height: 30px;
            background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
            color: #ffffff;
            border-radius: 8px;
            font-size: 16px;
            padding: 4px 18px;
            border: none;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        }

        /* Accept Modal */



        .reason {
            font-family: Kanit-Regular;
            font-size: 14px;
            font-weight: bold;
            color: #4a4a4a;
        }

        .reason_value {
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 600;
            color: #ee2a27;
        }

        #room_approval_table td,
        .table th {
            vertical-align: bottom;
        }

        .time_serve {
            height: 30px;
            width: 80px;
            border-radius: 8px;
            border: 1px solid #d5d4d4;
            padding-left: 10px;
        }

    </style>
@endsection

@section('content')
    <div class="container mt-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
        <div class="row">
            <div class="col-12 d-flex align-items-center">
                <div class="col-9">
                    <span
                        style="font-family: Kanit-Regular; font-size: 26px; color: #4f72e5;">พิจารณาคำขอจองห้องประชุม</span>
                </div>
                <div class="col-3 d-flex text-end status_room">
                    <div class="col-12">


                        @if ($room['status_booking'] == 'waiting' && $room['manager_approved'] == null)
                            <span class="status_yellow mr-3">รออนุมัติ</span>

                        @elseif($room['manager_approved'] == 'approved')
                            <span class="status_green mr-3">อนุมัติ</span>

                        @elseif($room['manager_approved'] == 'cancelled')
                            <span class="status_red mr-3">ไม่อนุญาติ</span>

                        @endif


                        <img src="/img/icon/local-printshop-material-copy.png" alt="">


                    </div>
                </div>
            </div>

        </div>
        <div class="my-2" style="border:0.5px dashed #ceced4"></div>


        <div class="col-12 mt-3 d-flex">
            <span class="title_form">ผู้ขอจองห้องประชุม</span>
        </div>

        <form action="#" method="post">
            {{-- Name & Agency --}}
            <div class="row d-flex  mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10">
                        <span class="data_result">{{ $room->datauser->name }}</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-1 pr-0">
                        <span class="content_span">สำนัก</span>
                    </div>

                    <div class="col-11">
                        <span class="data_result">{{ $room->datauser->office_name }}</span>
                    </div>
                </div>

            </div>

            {{-- Position & Group --}}
            <div class="row d-flex mt-3">

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">กลุ่มงาน</span>
                    </div>
                    <div class="col-10">
                        <span class="data_result">{{ $room->datauser->group }}</span>
                    </div>
                </div>



                <div class="col-6 d-flex align-items-center">
                    <div class="col-1 pr-0">
                        <span class="content_span">ตำแหน่ง</span>
                    </div>
                    <div class="col-10">
                        <span class="data_result">{{ $room->datauser->position }}</span>
                    </div>
                </div>
            </div>

            {{-- Number Phone --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0 ">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>

                    <div class="col-10">
                        <span class="data_result">{{ $room->datauser->manager_name }}</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 pr-0">
                        <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                    </div>

                    <div class="col-9">
                        <span class="data_result">{{ $room->datauser->tel_internal }}</span>
                    </div>
                </div>
            </div>



            <div class="col-12 mt-3">
                <span class="title_form">รายละเอียดการจองห้องประชุม</span>
            </div>




            {{-- Title & send to --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-1">
                        <span class="content_span">เรื่อง</span>
                    </div>
                    <div class="col-11">
                        <span class="data_result">{{ $room['title'] }}</span>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-1 pr-0">
                        <span class="content_span">เรียน</span>
                    </div>
                    <div class="col-11">
                        <span class="data_result">{{ $room['request_to'] }}</span>
                    </div>
                </div>
            </div>

            {{-- Objective --}}
            <div class="row d-flex mt-3">
                <div class="col-12 d-flex align-items-center">
                    <div class="col-1">
                        <span class="content_span">วัตถุประสงค์</span>
                    </div>
                    <div class="col-11">
                        <span class="data_result">{{ $room['objective'] }}</span>
                    </div>
                </div>
            </div>

            {{-- Date & Time --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">วันที่เริ่มใช้</span>
                    </div>

                    <div class="col-6">
                        <span class="data_result">{{ $room['start_date'] }}</span>
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="data_result">{{ $room['start_time'] }}</span>
                    </div>

                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">วันที่สิ้นสุด</span>
                    </div>

                    <div class="col-6">
                        <span class="data_result">{{ $room['end_date'] }}</span>
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="data_result">{{ $room['end_time'] }}</span>
                    </div>

                </div>
            </div>

            {{-- Place &  Room --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">สถานที่</span>
                    </div>

                    <div class="col-10">
                        <span class="data_result">{{ $room['place'] }}</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">ห้องประชุม</span>
                    </div>

                    <div class="col-10">
                        <span class="data_result">{{ $room['room_request'] }}</span>
                    </div>
                </div>
            </div>


            {{-- Device In Room  & Staff device --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-start">
                    <div class="col-3 pr-0">
                        <span class="content_span">อุปกรณ์โสดทัศนูปกรณ์</span>
                    </div>
                    <div class="col-10 d-flex pl-0 align-items-center">
                        <div class="row d-flex align-items-center mr-2">

                            <div class="row mx-2">
                                @foreach (@$room['device_id'] as $device_id)

                                    @php
                                        $name_device = '';
                                        if ($device_id == 'projector') {
                                            $name_device = 'เครื่องฉาย PROJECTOR';
                                        } elseif ($device_id == 'vdo') {
                                            $name_device = 'เครื่องฉาย VDO';
                                        } elseif ($device_id == 'record') {
                                            $name_device = 'บันทึกเทป';
                                        } elseif ($device_id == 'conference') {
                                            $name_device = 'VDO Conference';
                                        } elseif ($device_id == 'visual') {
                                            $name_device = 'เครื่องฉาย PROJECTOR';
                                        }
                                    @endphp


                                    <div class="col-4 d-flex align-items-center p-0">
                                        <input type="checkbox" name="device_room" value="{{ @$device_id }}"
                                            id="projector" class="device_check" checked disabled>
                                        <span class="content_span pl-2 mr-3">{{ @$name_device }}</span>
                                    </div>
                                @endforeach

                            </div>



                        </div>
                    </div>
                </div>


                <div class="col-6 d-flex align-items-center">
                    <div class="col-5 pr-0">
                        <span class="content_span">เจ้าหน้าที่ควบคุมอุปกรณ์โสดทัศนูปกรณ์</span>
                    </div>

                    <div class="col-7 d-flex pl-0 align-items-center justify-content-center">
                        <div class="col-1 p-0 d-flex align-items-end">
                            <input type="checkbox" name="staff_device"
                                value="{{ @$room['staff'] == 'need' ? 'ต้องการ' : 'ไม่ต้องการ' }}" checked disabled>
                        </div>
                        <div class="col-11 p-0">
                            {{-- <span class="data_result">ต้องการ</span> --}}
                            <span
                                class="data_result mr-3">{{ @$room['staff'] == 'need' ? 'ต้องการ' : 'ไม่ต้องการ' }}</span>

                        </div>
                    </div>


                </div>


            </div>


            {{-- Boss --}}
            <div class="row d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>

                    <div class="col-10">
                        <span class="data_result">{{ $room->datauser->manager_name }}</span>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-3">
                <span class="title_form">รายละเอียดการจองห้องประชุม</span>
            </div>

            {{-- Food & Drink --}}
            <div class="row d-flex mt-3">
                <div class="col-12 d-flex align-items-center">
                    <div class="col-3 pr-0">
                        <span class="data_result pl-3">อาหารว่าง/เครื่องดื่มรับรองการประชุม</span>
                    </div>

                    <div class="col-9 d-flex pl-0 align-items-center">

                        <div class="col-12">
                            <input type="checkbox" class="mr-2" id="need_food" name="food_drink"
                                value="{{ @$room['food_detail']->appertizer }}" checked disabled>
                            <span
                                class="data_result mr-3">{{ @$room['food_detail']->appertizer == 'on' ? 'ต้องการ' : 'ไม่ต้องการ' }}</span>

                        </div>

                    </div>
                </div>
            </div>

        </form>

        <div class="col-12 mt-3">
            <span class="title_form">รายการอาหารว่าง อาหารกลางวัน และเครื่องดื่ม</span>
        </div>


        {{-- <form action="#" method="post"> --}}
        <table class="table" id="room_approval_table">
            <thead>
                <tr>
                    {{-- <th></th>
                <th style="width: 380px;"><span>เลือกรายการ</span></span></th>
                <th><span>รายการ</span></th>
                <th class="text-center"><span>ราคาต่อหน่วย</span></th>
                <th></th>
                <th><span>จำนวน (ชุด)</span></th>
                <th class="text-end" style="width:120px"><span>จำนวนเงิน</span></th>
                <th></th> --}}
                    <th style="width: 33px;"></th>
                    <th style="width: 190px;"><span>เลือกรายการ</span></span></th>
                    <th><span>รายการ</span></th>
                    <th style="width: 1px;">เวลาเสิร์ฟ</th>
                    <th class="text-center" style="width:120px"><span>ราคาต่อหน่วย</span></th>
                    <th style="width: 1px;"></th>
                    <th style="width:120px"><span>จำนวน (ชุด)</span></th>
                    <th class="text-end" style="width:120px"><span>จำนวนเงิน</span></th>
                </tr>
            </thead>
            <tbody>
                @if ($room['food_detail'] == null)
                    <tr>
                        <td colspan="8" class="text-center" style="height:100px;vertical-align: middle;">ไม่มีรายการ
                        </td>
                    </tr>

                @elseif($room['food_detail'] != null)

                    @foreach ($room['food_detail']->detail_food as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>
                                <select class="form-select select_form_room2 input_evr" aria-label="เลือกรายการ"
                                    disabled="disabled">
                                    <option selected>{{ $item->period }}</option>
                                </select>
                            </td>

                            <td>
                                <select class="form-select select_form_room2 input_evr" aria-label="เลือกรายการ"
                                    disabled="disabled">
                                    <option selected>{{ $item->set_food }}</option>
                                </select>
                            </td>

                            <td>
                                <input type="time" name="time_serve[]" value="{{ $item->time_serve }}" class="time_serve"
                                    disabled>
                            </td>

                            <td class="text-center">
                                <span class="price_product pr-2">{{ $item->price }}</span>
                            </td>

                            <td class="text-center"><span class="price_product">x</span></td>

                            <td>
                                <input type="text" name="count_food" class="text-end input_evr form_time"
                                    value="{{ $item->count_food }}" readonly>
                            </td>

                            <td class="text-end pr-5">
                                <span>{{ $item->price * $item->count_food }}</span>
                            </td>

                        </tr>
                    @endforeach

                @endif

            </tbody>
            <tfoot>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-end"><span>รวม</span></td>
                    <td class="text-end"><span>{{ @$room['food_detail']->total }}</span></td>
                </tr>

            </tfoot>
        </table>

        @if (isset($room['reason']))
            <div class="col-12 d-flex justify-content-start reason-cover ">
                <div class="col-1 text-end">
                    <span class="reason">เหตุผล : </span>
                </div>
                <div class="col-11 p-0">
                    <span class='reason_value'>{{ @$room['reason'] }}</span>
                </div>
            </div>
        @endif

        <div class="row mt-5">

            <div class="col-12 d-flex justify-content-center">

                @if ($room['status_booking'] == 'waiting' && $room['manager_approved'] == null)
                    <a href="/manager" class="waiting_approve mr-3">ย้อนกลับ</a>
                    <button class="cancel mr-3" id="cancel_btn" data-bs-toggle="modal" data-bs-target="#cancel_approve_room"
                        style="cursor: pointer;">ไม่อนุมัติ</button>

                    <button class="text-center approve_view pt-1 mr-3" data-bs-toggle="modal"
                        data-bs-target="#approve_room">อนุมัติิ</button>


                @elseif($room['manager_approved'] == 'approved' || $room['manager_approved'] == 'cancelled')
                    <a href="/manager" class="waiting_approve mr-3">ย้อนกลับ</a>

                @endif

            </div>
        </div>
        {{-- </form> --}}
    </div>




    {{-- Cancel Textarea Modal --}}
    <form action="/manager/approved_room/cancelled" method="post">
        @csrf
        <div class="modal fade" id="cancel_approve_room" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="cancel_approve_room" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/close-material.png" alt="" style="width: 55px;
                                                            height: 55px; object-fit:cover;">
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <span style="font-size: 16px; font-weight: 600; color: #4a4a4a;">ไม่อนุมัติเนื่องจาก</span>
                            </div>
                            <div class="col-12 d-flex justify-content-center">
                                <textarea name="reason" class="px-2" id="reason" cols="50" rows="4" style="resize: none;border-radius: 3px;
                                                                border: solid 1px #cfd2d4;
                                                                background-color: #ffffff;"
                                    placeholder="เหตุผล..."></textarea>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <button type="button" data-bs-dismiss="modal" class="disapproval mr-3">ยกเลิก</button>
                                <button type="button" class="cancel_accept_btn ">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Accept Cancel Modal --}}
        <div class="modal fade" id="accept_cancel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="accept_cancel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/close-material.png" alt="" style="width: 55px;
                                                            height: 55px; object-fit:cover;">
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <span
                                    style="font-size: 16px; font-weight: 600; color: #4a4a4a;">คุณยืนยันการปฏิเสธคำขอใช้รถใช่หรือไม่
                                    ?</span>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <input type="hidden" name="book_id" value="{{ $room['id'] }}">
                                <button type="button" data-bs-dismiss="modal" class="disapproval mr-3">ยกเลิก</button>
                                <button type="submit" class="accept_cancel_btn ">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>





    {{-- Approve Modal --}}
    <div class="modal fade" id="approve_room" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="approve_room" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">

                        <form action="/manager/approved_room/approved" method="post">
                            @csrf
                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/correct-sign.png" alt="" style="width: 55px;
                                                            height: 55px; object-fit:cover;">
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-3">
                                <span class="title_form">คุณยืนยันการอนุมัติการใช้ห้องประชุมใช่หรือไม่ ?</span>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <input type="hidden" name="book_id" value="{{ $room['id'] }}">
                                <button type="button" data-bs-dismiss="modal" class="disapproval mr-3">ยกเลิก</button>
                                <button type="submit" class="accept_approve_room ">ตกลง</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('script')
    <script>
        $(document).ready(function() {

            $('.cancel_accept_btn').on('click', function() {
                if ($('#reason').val().trim() == '') {
                    alert('กรุณาใส่เหตุผลในการยกเลิก');

                } else {
                    $('#cancel_approve_room').modal('hide');
                    $('#accept_cancel').modal('show');
                    $('.accept_cancel_btn').on('click', function() {
                        $('#accept_cancel').modal('hide');
                        $('#cancel_btn').remove();
                        $('.approve_view').remove();
                        $('.status_room').find('span').toggleClass('status_red');
                        $('.status_room').find('span').text('ไม่อนุญาติ');
                        var textReason = $('#reason').val().trim();
                        console.log(textReason);
                        $('.reason_value').text(textReason);
                        $('.reason-cover').css('display', 'block');
                    });
                }
            });

            $('#accept_cancel .disapproval').on('click', function() {
                $('#reason').val("");
            });

            $('.accept_approve_room').on('click', function() {
                $('#approve_room').modal('hide');
                $('#cancel_btn').remove();
                $('.approve_view').remove();
                $('.status_room').find('span').toggleClass('status_green');
                $('.status_room').find('span').text('อนุญาติ');
            });

        });
    </script>
@endsection
