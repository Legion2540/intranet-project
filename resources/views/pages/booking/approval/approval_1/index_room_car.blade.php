@extends('layout.booking')
@section('style')
<style>
    .waiting_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ffbf00;
    }

    .cancel_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ee2a27;
    }

    .approve_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #6dd005;
    }

    .count_num {
        font-size: 50px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
    }

    .container {
        margin-right: auto !important;
        margin-left: 270px !important;
        max-width: 1400px !important;
    }

    .text_request {
        font-size: 18px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #ffffff;
        margin-left: 14px;
    }

    .nav-pills .nav-item.show .nav-link,
    .nav-pills .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    #request_car {
        font-size: 20px;
        font-weight: 600;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4f72e5;
    }

    #request_room {
        font-size: 20px;
        font-weight: 600;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4f72e5;
    }

    #approved_all {
        font-size: 20px;
        font-weight: 600;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #4f72e5;
    }

    .filter_all {
        width: 133px;
        height: 35px;
        margin: 0 40px 0 29px;
        padding: 3px 4px 3px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .form-select-1x {
        line-height: normal !important;
    }

    #search_request {
        width: 288px;
        height: 35px;
        padding: 5px 4px 0 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .waiting_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 4px
    }

    .approve_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 4px
    }

    .cancel_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        border-radius: 4px
    }

    .thead_car {
        font-size: 16px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 0.94;
        letter-spacing: normal;
        color: #4a4a4a;
    }

</style>
@endsection

@section('content')
<div class="my-4">
    <div class="row d-flex align-items-center mb-3">
        <div class="waiting_request">
            <span class="count_num">10</span>

            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/white-access-time-material.png" alt="">
                <span class="text_request">รออนุมัติ</span>
            </div>
        </div>

        <div class="approve_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/times-circle-outlined-font-awesome.png" alt="">
                <span class="text_request">ไม่อนุมัติ</span>
            </div>
        </div>

        <div class="cancel_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/ion-android-checkbox-outline-ionicons.png" alt="">
                <span class="text_request">อนุมัติ</span>
            </div>
        </div>
    </div>

    <div class="row" style="background-color: #ffffff;">

        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="request_car" data-bs-toggle="pill" data-bs-target="#car"
                    type="button" role="tab" aria-controls="car" aria-selected="true">คำขอการใช้รถ</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="request_room" data-bs-toggle="pill" data-bs-target="#room" type="button"
                    role="tab" aria-controls="room" aria-selected="false">คำขอใช้ห้องประชุม</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="approved_all" data-bs-toggle="pill" data-bs-target="#all" type="button"
                    role="tab" aria-controls="all" aria-selected="false">ประวัติการอนุมัติทั้งหมด</button>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="car" role="tabpanel" aria-labelledby="request_car">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-size: 18px;
                            font-weight: bold;
                            font-stretch: normal;
                            font-style: normal;
                            line-height: normal;
                            letter-spacing: normal;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="margin: 3px 21px 3px 40px;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <input type="text" name="request_search" id="search_request" placeholder="ค้นหา">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:94%" alt="">
                        </div>
                    </div>
                </div>
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car">ผู้ขอใช้</th>
                            <th class="thead_car">วัตถุประสงค์</th>
                            <th class="thead_car">สถานที่</th>
                            <th class="thead_car text-center">จำนวนคน</th>
                            <th class="thead_car">วันที่ใช้</th>
                            <th class="thead_car">แผนก</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i<3; $i++) <tr>
                            <td><input type="checkbox" name="key_row" id="key_row"></td>
                            <td><a href="/manager/approved_car1">นางสาวดาริสา พัชรโชต</a></td>
                            <td>ไปสัมนาวิชาการ</td>
                            <td>โรงแรมเซนทารา แกรนด์</td>
                            <td class="text-center">4</td>
                            <td>
                                <span>22 มกราคม 2564</span><br>
                                <span>เวลา 10.00 - 13.00 น.</span>
                            </td>
                            <td>ฝ่ายบุคคล</td>
                            <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>
                            <td class="text-center"><span class="waiting_status">อนุมัติ</span></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>

            </div>


            <div class="tab-pane fade" id="room" role="tabpanel" aria-labelledby="request_room">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-size: 18px;
                            font-weight: bold;
                            font-stretch: normal;
                            font-style: normal;
                            line-height: normal;
                            letter-spacing: normal;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="margin: 3px 21px 3px 40px;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <input type="text" name="request_search" id="search_request" placeholder="ค้นหา">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:94%" alt="">
                        </div>
                    </div>
                </div>
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car">ผู้ขอใช้</th>
                            <th class="thead_car">วัตถุประสงค์</th>
                            <th class="thead_car">สถานที่</th>
                            <th class="thead_car text-center">จำนวนคน</th>
                            <th class="thead_car">วันที่ใช้</th>
                            <th class="thead_car">แผนก</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i<3; $i++) <tr>
                            <td><input type="checkbox" name="key_row" id="key_row"></td>
                            <td><a href="/manager/approved_room1">นางสาวดาริสา พัชรโชต</a></td>
                            <td>ไปสัมนาวิชาการ</td>
                            <td>โรงแรมเซนทารา แกรนด์</td>
                            <td class="text-center">4</td>
                            <td>
                                <span>22 มกราคม 2564</span><br>
                                <span>เวลา 10.00 - 13.00 น.</span>
                            </td>
                            <td>ฝ่ายบุคคล</td>
                            <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>
                            <td class="text-center"><span class="waiting_status">อนุมัติ</span></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>


            <div class="tab-pane fade" id="all" role="tabpanel" aria-labelledby="approved_all">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-size: 18px;
                            font-weight: bold;
                            font-stretch: normal;
                            font-style: normal;
                            line-height: normal;
                            letter-spacing: normal;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="margin: 3px 21px 3px 40px;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <input type="text" name="request_search" id="search_request" placeholder="ค้นหา">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:94%" alt="">
                        </div>
                    </div>
                </div>
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car">ผู้ขอใช้</th>
                            <th class="thead_car">วัตถุประสงค์</th>
                            <th class="thead_car">สถานที่</th>
                            <th class="thead_car text-center">จำนวนคน</th>
                            <th class="thead_car">วันที่ใช้</th>
                            <th class="thead_car">แผนก</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i<3; $i++) <tr>
                            <td><input type="checkbox" name="key_row" id="key_row"></td>
                            <td>นางสาวดาริสา พัชรโชต</td>
                            <td>ไปสัมนาวิชาการ</td>
                            <td>โรงแรมเซนทารา แกรนด์</td>
                            <td class="text-center">4</td>
                            <td>
                                <span>22 มกราคม 2564</span><br>
                                <span>เวลา 10.00 - 13.00 น.</span>
                            </td>
                            <td>ฝ่ายบุคคล</td>
                            <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>
                            <td class="text-center"><span class="waiting_status">อนุมัติ</span></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>



        </div>











    </div>
</div>
@endsection

@section('script')
<script>


</script>
@endsection
