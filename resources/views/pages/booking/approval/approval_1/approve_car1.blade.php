@extends('layout.booking')
@section('style')
<style>
    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .date_input {
        width: 226px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .time_input {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .form-select-city {
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #4a4a4a;
    }

    .choosemap_btn {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        top: 50%;
        left: 42%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 16px;
        padding: 5px 6px;
    }

    .waiting_approve,
    .waiting_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        background-color: #ffbf00;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        text-decoration: none;
    }

    .approved_status {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        background-color: #6dd005;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        text-decoration: none;
    }

    .cancel_approve {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        background-color: #ee2a27;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }


    .valiedate_data {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 4px 18px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .cancel_valiedate_data {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 4px 18px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .data_result {
        color: #4f72e5;
        font-size: 14px;
    }

    .really_cancel_approve {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        background-color: #ee2a27;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .really_valiedate_data {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 4px 18px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .reason {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .reason_value {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 600;
        color: #ee2a27;
    }

</style>
@endsection
@section('content')
<div class="container my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
    <div class="row">
        <div class="col-8">
            <span
                style="font-family: Kanit-Regular; font-size: 26px; font-weight: bold; color: #4f72e5;">พิจารณาคำขอจองใช้รถ</span>
        </div>
        <div class="col-4 d-flex align-items-center justify-content-end status_approve_room">
            <span class="waiting_approve">รออนุมัติ</span>
            <img src="/img/icon/print-material.png" class="ml-4" alt="">
        </div>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <div class="col-12 mt-3 pl-0">
        <span class="title_form">ผู้ขอจองใช้รถ</span>
    </div>

    <form action="#" method="post">
        <div class="row mt-2">
            <div class="col-12 d-flex">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10 pl-0">
                        <span class="data_result">นาย PHP Laravel</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-4 px-0">
                        <span class="content_span">สำนัก / หน่วยงาน / องค์กร</span>
                    </div>
                    <div class="col-8 pl-0">
                        <span class="data_result">สำนักงานบริหารกลาง</span>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-1 px-0">
                        <span class="content_span">ตำแหน่ง</span>
                    </div>
                    <div class="col-10">
                        <span class="data_result">Programmer</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-4 px-0">
                        <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                    </div>
                    <div class="col-8 pl-0">
                        <span class="data_result">098-289-1087</span>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>
                    <div class="col-9 pl-0">
                        <span class="data_result">นายวันชัย นาคทั่ง</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-4 px-0">
                        <span class="content_span">จำนวนผู้เดินทาง </span>
                    </div>
                    <div class="col-8 pl-0">
                        <span class="data_result">4</span>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span class="content_span">ขออนุญาตใช้</span>
                    </div>
                    <div class="col-10 pl-0 d-flex align-items-center">
                        <div class="col-4">
                            <input type="checkbox" name="type_car" id="car_center" checked
                                style="background-color:#f7f7f7">
                            <span class="data_result">รถส่วนกลาง</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่เริ่มใช้</span>
                    </div>
                    <div class="col-5">
                        <span class="data_result">22 มกราคม 2564</span>
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="data_result">09:00</span>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่สิ้นสุด</span>
                    </div>
                    <div class="col-5">
                        <span class="data_result">22 มกราคม 2564</span>
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="data_result">17:00</span>
                    </div>
                </div>




            </div>
        </div>

        <div class="row">
            <div class="col-6">

                <div class="col-12 mt-3 d-flex">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">วัตถุประสงค์</span>
                    </div>
                    <div class="col-9">
                        <span class="data_result">ไปสัมนาวิชาการ</span>
                    </div>
                </div>

                <div class="col-12 mt-3 d-flex">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">หลักฐานอื่นๆ</span>
                    </div>
                    <div class="col-9">
                        <span class="data_result">-</span>
                    </div>
                </div>





                <div class="col-12 pl-0 mt-5">
                    <span class="title_form">สถานที่</span>
                </div>

                <div class="col-12 d-flex align-items-center mt-3">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">ชื่อสถานที่</span>
                    </div>
                    <div class="col-9 px-0">
                        <span class="data_result">โรงแรมเซนทารา</span>
                    </div>
                </div>


                <div class="col-12 d-flex align-items-center mt-3">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">จังหวัด</span>
                    </div>
                    <div class="col-9 px-0">
                        <span class="data_result">กรุงเทพมหานคร</span>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-center mt-3">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">อำเภอ/เขต</span>
                    </div>
                    <div class="col-9 px-0">
                        <span class="data_result">ปทุมวัน</span>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-center mt-3">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">ตำบล/แขวง</span>
                    </div>
                    <div class="col-9 px-0">
                        <span class="data_result">ปทุมวัน</span>
                    </div>
                </div>

            </div>




            <div class="col-6 d-flex align-items-end mt-4">
                <div class="col-12">
                    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                        style="object-fit: cover; width: 475px; height: 244px;" alt="">

                </div>
            </div>
        </div>

        <div class="col-12 d-flex justify-content-start reason_div mt-5" style="display: none !important;">
            <span class="reason">เหตุผล : </span>
            <span class="reason_value"></span>
        </div>

        <div class="row multi_btn" style="margin-top:150px; margin-bottom:80px;">
            <div class="col-12 align-items-center d-flex justify-content-center">
                {{-- <button class="choosemap_btn">แก้ไข</button> --}}
                {{-- <button class="choosemap_btn">ส่งคำขอ</button> --}}

                {{-- <a href="/reserve/car/form_car1" class="cancel_approve mr-3">ยกเลิกคำขอ</a> --}}
                <a href="/manager" type="button" class="waiting_approve mr-3">ย้อนกลับ</a>
                <button type="button" class="cancel_approve mr-3" data-bs-toggle="modal"
                    data-bs-target="#cancel_modal">ไม่อนุมัติ</button>
                <button type="button" class="valiedate_data " data-bs-toggle="modal"
                    data-bs-target="#approve_modal">อนุมัติ</button>
                {{-- <a href="/reserve/car/form_car3" class="valiedate_data">ส่งคำขอ</a> --}}
            </div>
        </div>



        <div class="modal fade" id="cancel_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">


                        {{-- <form action="#" id="reason_cancel" method="POST"> --}}
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/close-material.png" alt="" style="width: 55px;
                                height: 55px; object-fit:cover;">
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <span
                                    style="font-size: 16px; font-weight: 600; color: #4a4a4a;">ไม่อนุมัติเนื่องจาก</span>
                            </div>
                            <div class="col-12 d-flex justify-content-center">
                                <textarea name="reason" class="px-2" id="reason" cols="50" rows="4" style="resize: none;border-radius: 3px;
                                    border: solid 1px #cfd2d4;
                                    background-color: #ffffff;" placeholder="เหตุผล..."></textarea>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <button type="button" data-bs-dismiss="modal"
                                    class="cancel_approve mr-3">ยกเลิก</button>
                                <button type="button" class="cancel_valiedate_data ">ตกลง</button>
                            </div>
                        </div>
                        {{-- </form> --}}


                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="really_cancel_modal" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/close-material.png" alt="" style="width: 55px;
                                height: 55px; object-fit:cover;">
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <span
                                    style="font-size: 16px; font-weight: 600; color: #4a4a4a;">คุณยืนยันการปฏิเสธคำขอใช้รถใช่หรือไม่
                                    ?</span>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <button type="button" data-bs-dismiss="modal"
                                    class="really_cancel_approve mr-3">ยกเลิก</button>
                                <button type="button" class="cancel_valiedate_data ">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="approve_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-12 d-flex justify-content-center">
                                <img src="/img/icon/correct-sign.png" alt="" style="width: 55px;
                                height: 55px; object-fit:cover;">
                            </div>

                            <div class="col-12 d-flex justify-content-center mt-3">
                                <span class="title_form">คุณยืนยันการอนุมัติการใช้ห้องประชุมใช่หรือไม่ ?</span>
                            </div>


                            <div class="col-12 d-flex justify-content-center mt-4 mb-2">
                                <button type="button" data-bs-dismiss="modal"
                                    class="really_cancel_approve mr-3">ยกเลิก</button>
                                <button type="button" class="really_valiedate_data ">ตกลง</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>


</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {

        // multi_btn // 3 btn bottom
        // status_approve_room row cover span text
        // waiting_approve ย้อนกลับ
        // cancel_approve ไม่อนุมัติ
        // valiedate_data อนุมัติ

        //  Cancel Function
        $('.cancel_valiedate_data').on('click', function () {
            if ($('#reason').val().trim() == '') {
                alert('กรุณาใส่เหตุผลในการยกเลิก');
                return false;
            } else {
                $('#cancel_modal').modal('hide');
                $('#really_cancel_modal').modal('show');
                $('#really_cancel_modal').find('.cancel_valiedate_data').on('click', function () {
                    $('#really_cancel_modal').modal('hide');
                    $('.status_approve_room').find('span').toggleClass('really_cancel_approve');
                    $('.status_approve_room').find('span').text('ไม่อนุมัติ');
                    $('.cancel_approve').remove();
                    $('.valiedate_data').remove();
                    $('.reason_div').css('display','block');
                    $('.reason_value').text($('#reason').val().trim());
                });
            }
        });

        $('.valiedate_data').on('click', function () {
            $('.really_valiedate_data').on('click', function () {
                $('#approve_modal').modal('hide');
                $('.status_approve_room').find('span').toggleClass('approved_status');
                $('.status_approve_room').find('span').text('อนุมัติ');
                $('.cancel_approve').remove();
                $('.valiedate_data').remove();


            });

        });





    });

</script>
@endsection
