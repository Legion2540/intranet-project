@extends('layout.booking')
@section('style')
<style>
    .title_form_pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        color: #4a4a4a;
        background-color: #f3f3f3;
        border: solid 0.5px #ceced4;

    }

    .date_input {
        width: 226px;
        height: 30px;
        border-radius: 8px;
        padding: 0 5px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .time_input {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        padding: 0 5px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .form-select-city {
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #4a4a4a;
    }

    .choosemap_btn {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        top: 50%;
        left: 42%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 16px;
        padding: 5px 6px;
    }

    .waiting_approve,
    .waiting_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 28px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-decoration: none;
        text-align: center;
        text-decoration: none;
        background-color: #ff7f00;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);

    }

    .cancel_approve,
    .cancel_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
        background-color: #ee2a27;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .valiedate_data,
    .valiedate_data:hover {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff !important;
        border-radius: 8px;
        font-size: 16px;
        padding: 3px 23px;
        text-align: center;
        border: none;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .comback_index,
    .comback_index:hover {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 4px 18px;
        border: none;
        text-align: center;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .browse {
        width: 90px;
        height: 26px;
        font-size: 12px;
        color: #ffffff;
        border-radius: 8px;
        border: none;
        box-shadow: 0 0 3px 1px rgba(0, 0, 0, 0.15);
        background-image: linear-gradient(193deg, #4f72e5 47%, #314d7b 119%);
    }

    .extension_files {
        margin-left: 10px;
        font-family: Kanit-Regular;
        font-weight: bold;
        font-size: 12px;
        color: #ee2a27;
    }

    .form-select:disabled {
        height: 30px;
        line-height: 1;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .status_waiting {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #ffbf00;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_approve {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #6dd005;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_cancel {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #ee2a27;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .type_car {
        font-size: 14px;
        font-weight: 500;
        color: #4a4a4a;
    }

    #age_driver {
        width: 40px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .btn_cancel_sForm,
    .btn_cancel_sForm:hover {
        width: 112px;
        height: 33px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        border: none;
        color: #ffffff;
    }

    .btn_submit_sForm,
    .btn_submit_sForm:hover {
        width: 112px;
        height: 33px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(193deg, #4f72e5 57%, #314d7b 138%);
        border: none;
        color: #ffffff;

    }

    .btn_cancel_cForm,
    .btn_cancel_cForm:hover {
        width: 112px;
        height: 33px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        border: none;
        color: #ffffff;
    }

    .btn_submit_cForm,
    .btn_submit_cForm:hover {
        width: 112px;
        height: 33px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(193deg, #4f72e5 57%, #314d7b 138%);
        border: none;
        color: #ffffff;

    }

    .title_submit_modal {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .object {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    #result_object {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: 600;
        color: #ee2a27;
    }

</style>
@endsection
@section('content')
<div class="my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
    <div class="row">
        <div class="col-10">
            <span class="title_form_pages">ใบสั่งจ่าย</span>
        </div>
        <div class="col-2 d-flex align-items-center justify-content-end status_form">
            <span class="status_waiting mr-5">รออนุมัติ</span>
            <img src="/img/icon/print-material.png" alt="">
        </div>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <div class="col-12 mt-3 pl-0">
        <span class="title_form">ผู้ขอจองใช้รถ</span>
    </div>

    <form action="#" method="post">
        <div class="row mt-2">

            <div class="col-12 d-flex">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10 px-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" name="form_name_room" value="นางสาวดาริสา พัชรโชด" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">สำนัก</span>
                    </div>
                    <div class="col-10 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" name="form_name_room" value="สำนักงานบริหารกลาง" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ตำแหน่ง</span>
                    </div>
                    <div class="col-10 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" name="position" value="ธุรการ" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                    </div>
                    <div class="col-9 pl-0">
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" name="tel_in_no" value="02-777-1200" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>
                    <div class="col-9 px-0">

                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="text" name="manager_id" value="นายกันต์ สินธร" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">จำนวนที่นั่ง</span>
                    </div>
                    <div class="col-10 pr-0">
                        <input type="text" class="input_evr px-2" name="member_seat" id="member_seat" value="4"
                            disabled>
                    </div>
                </div>
            </div>

        </div>


        <div class="row mt-2">
            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span class="content_span">ขออนุญาตใช้</span>
                    </div>
                    <div class="col-10 pl-0 d-flex align-items-center">
                        <div class="col-4">
                            <input type="checkbox" name="type_car" id="car_center" class="mr-2" checked>
                            <span class="type_car">รถส่วนกลาง</span>
                        </div>
                        <div class="col-4">
                            <input type="checkbox" name="type_car" id="car_guest" class="mr-2" disabled>
                            <span class="type_car">รถรับรอง</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่เริ่มใช้</span>
                    </div>
                    <div class="col-5">
                        <input class="date_input" type="text" name="date_start" value="22 มกราคม 2564">
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <input class="time_input" type="text" name="time_start" value="09.00">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่สิ้นสุด</span>
                    </div>
                    <div class="col-5">
                        <input class="date_input" type="text" name="date_start" value="22 มกราคม 2564">

                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <input class="time_input" type="text" name="time_start" value="09.00">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center w-100">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">วัตถุประสงค์</span>
                    </div>
                    <div class="col-10 pr-0">
                        <input type="text" class="input_evr px-2" name="purpose" id="purpose" value="ไปสัมนาวิชาการ">
                    </div>
                </div>
            </div>


            <div class="col-12 mt-5">
                <span class="title_form">สถานที่ไป</span>
            </div>


            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 d-flex px-0">
                        <span class="content_span">ชื่อสถานที่</span>
                    </div>

                    <div class="col-10 d-flex align-items-center pr-0">
                        <input type="text" class="input_evr px-2" name="location" id="location"
                            value="โรงแรมเซนทารา แกรนด์" disabled>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">จังหวัด</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">

                        <input type="text" class="input_evr px-2" name="location" id="location" value="กรุงเทพมหานคร"
                            disabled>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">อำเภอ/เขต</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="location" id="location" value="ปทุมวัน"
                            disabled>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 ">
                        <span class="content_span">ตำบล/แขวง</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="location" id="location" value="ปทุมวัน"
                            disabled>
                    </div>
                </div>
            </div>
        </div>



        <div class="row mt-2">
            <div class="col-12 mt-5">
                <span class="title_form">รายละเอียดรถและคนขับรถ</span>
            </div>
            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 ">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="name_driver" id="location"
                            value="นายซุเช เท่เสมอ" disabled>
                    </div>
                </div>
                <div class="d-flex align-items-center ml-3">
                    <div class="">
                        <span class="content_span mr-2">อายุ</span>
                    </div>
                    <div class="d-flex align-items-center">
                        <input type="text" class="px-2 mr-2" name="age_driver" id="age_driver" value="28" disabled>
                        <span class="content_span">ปี</span>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 ">
                        <span class="content_span">เบอร์โทรศัพท์</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" style="width: 78%;" name="tel_driver" id="tel_driver"
                            value="02-777-12xx" disabled>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ยี่ห้อ / รุ่นรถ</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="series_car" id="series_car"
                            value="Toyota Vios ปี 2020" disabled>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 ">
                        <span class="content_span">เลขทะเบียน</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="car_no" id="car_no" value="ปทุมวัน" disabled>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-2">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">จำนวนที่นั่ง</span>
                    </div>
                    <div class="col-10 pr-0 d-flex align-items-center">
                        <input type="text" class="input_evr px-2" name="member_seat" id="member_seat" value="4"
                            disabled>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 object" style="display: none;">
            <div class="col-12 d-flex align-items-center">
                <div>
                    <span>เหตุผลที่ไม่อนุมัติ : </span>
                </div>
                <div class="ml-3">
                    <span id="result_object"></span>
                </div>
            </div>

        </div>



        <div class="row" style="margin-top: 90px;margin-bottom: 25px;">
            <div class="col-12 align-items-center d-flex justify-content-center">

                <a href="/manage_queueCar" class="waiting_approve mr-3">ย้อนกลับ</a>
                <button type="button" class="cancel_approve mr-3" data-bs-toggle="modal"
                    data-bs-target="#cancel_form">ไม่อนุมัติ</button>
                <button type="button" class="valiedate_data mr-3" data-bs-toggle="modal"
                    data-bs-target="#submit_form">อนุมัติ</button>


            </div>
        </div>


        <div class="modal fade" id="submit_form" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="col-12">
                            <div class="col-12 d-flex align-items-center justify-content-center">
                                <img src="/img/icon/error-outline-material@3x.png" alt="">
                            </div>
                            <div class="col-12 d-flex align-content-center justify-content-center mt-3 mb-4">
                                <span class="title_submit_modal">คุณเห็นชอบการขอใช้ห้องประชุมใช่หรือไม่ ?</span>
                            </div>
                            <div class="col-12 d-flex align-content-center justify-content-center">

                                <div class="col-6 text-end">
                                    <button type="button" class="btn_cancel_sForm"
                                        data-bs-dismiss="modal">ไม่ใช่</button>
                                </div>

                                <div class="col-6">
                                    <button type="button" class="btn_submit_sForm">ใช่</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="cancel_form" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="col-12">
                            <div class="col-12 d-flex align-items-center justify-content-center">
                                <img src="/img/icon/error-outline-material@3x.png" alt="">
                            </div>
                            <div class="col-12 d-flex align-content-center justify-content-center mt-3 mb-4">
                                <span class="title_submit_modal">คุณไม่เห็นชอบการขอใช้ห้องประชุมใช่หรือไม่ ?</span>
                            </div>
                            <div class="col-12 d-flex align-content-center justify-content-center">

                                <div class="col-6 text-end">
                                    <button type="button" class="btn_cancel_cForm"
                                        data-bs-dismiss="modal">ไม่ใช่</button>
                                </div>

                                <div class="col-6">
                                    <button type="button" class="btn_submit_cForm" data-bs-toggle="modal"
                                        data-bs-target="#purpose_modal">ใช่</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="purpose_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="col-12">
                            <div class="col-12 d-flex align-items-center justify-content-center">
                                <img src="/img/icon/error-outline-material@3x.png" alt="">
                            </div>
                            <div class="col-12 d-flex align-content-center justify-content-center my-2">
                                <span class="title_submit_modal">ไม่เห็นชอบเนื่องจาก</span>
                            </div>

                            <div class="col-12 d-flex align-content-center justify-content-center my-2">
                                <textarea name="object_text" id="object_text" cols="50" rows="5"
                                    style="resize: none;"></textarea>
                            </div>



                            <div class="col-12 d-flex align-content-center justify-content-center mt-3 form_purpose">
                                <div class="col-6 text-end">
                                    <button type="button" class="btn_cancel_cForm"
                                        data-bs-dismiss="modal">ไม่ใช่</button>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn_submit_cForm">ใช่</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>

</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('.btn_submit_sForm').on('click', function () {
            $('#submit_form').modal('hide');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'คุณทำรายการสำเร็จแล้ว',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                $('.cancel_approve').remove();
                $('.valiedate_data').remove();
                $('.status_form').find('span').attr('class', 'status_approve mr-5').text(
                    'อนุมัติ');
            })
        });


        $('.btn_submit_cForm').on('click', function () {
            $('#cancel_form').modal('hide');
            $('#purpose_modal').modal('show');
        });

        $('.form_purpose .btn_submit_cForm').on('click', function () {
            var purpose = $('#object_text').val();
            if (purpose != '') {
                $('#purpose_modal').modal('hide');
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'คุณทำรายการสำเร็จแล้ว',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    $('.cancel_approve').remove();
                    $('.valiedate_data').remove();
                    $('.status_form').find('span').attr('class', 'status_cancel mr-5').text(
                        'ไม่อนุมัติ');
                    $('.object').css('display', 'block');
                    $('#result_object').text(purpose);

                })
            }else{
                alert('กรุณากรอกเหตุผล');
            }

        });




    });

</script>
@endsection
