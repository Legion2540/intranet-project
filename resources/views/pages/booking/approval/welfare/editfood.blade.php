@extends('layout.booking')
@section('style')
<style>
    .title_room2 {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }


    .name_food {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #input_price {
        background-color: #f7f7f7;
        width: 100px;
        height: 30px;
        margin: 0 0 0 8px;
        padding: 4px 4px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }


    .comeback,
    .comeback:hover {
        width: 112px;
        height: 30px;
        margin: 0 17px 0 0;
        border-radius: 8px;
        padding: 4px 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
    }

    .approve_form {
        color: #ffffff;
        width: 112px;
        height: 30px;
        margin: 0 0 0 17px;
        border-radius: 8px;
        text-align: center;
        border: none;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(266deg, #4f72e5 36%, #314d7b 145%);
    }


    .count_food {
        width: 80px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        padding-left: 10px;
        text-align: right;

    }

    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }


    .add_menu {
        background-color: #ffffff;
        border-radius: 8px;
        border: 0.5px solid #d5d4d4;
        height: 30px;
        width: fit-content;
    }

    .title_second {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
    }

    .text_need {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
    }

    .price_menu {
        height: 30px;
        width: 80px;
        border-radius: 8px;
        border: 1px solid #d5d4d4;
        padding-left: 10px;
    }

    #add_menu tr th {
        font-family: Kanit-Regular;

    }

    .form-select {
        width: 70%;
        height: 30px;
        line-height: 1;
        border-radius: 8px;
    }


    .add_menu {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
    }

</style>
@endsection

@section('content')
<div class="my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
    <div class="row">
        <span class="title_room2">ลงทะเบียนอาหารว่าง / เครื่องดื่ม</span>
    </div>
    <div class="my-2" style="border:0.5px dashed #ceced4"></div>


    <div class="col-12 mt-3 mb-2">
        <div class="col-12 pl-0">
            <span class="title_second">รายการอาหารว่าง อาหารกลางวัน และเครื่องดื่ม</span>
        </div>
    </div>

    <form action="/welfare/edit?food_id={{ $food['id'] }}" method="post">
        @csrf
        <table class="table" id="add_menu">
            <thead>

                <tr class="vertical-center">
                    <th style="width: 33px;"></th>
                    <th style="width: 300px;"><span>รายการ</span></span></th>
                    <th style="width: 100px"></th>
                    <th style="width:530px"><span>ประเภท</span></th>
                    <th style="width: 110px;text-align: center;"><span>จำนวนเงิน</span></th>
                    <th style="width: 1px;"></th>
                    {{-- <th style="width: 1px;"></th> --}}
                </tr>

            </thead>
            <tbody>

                @foreach($food['list_food'] as $item)
                <tr class="choosed vertical-center">
                    <td></td>
                    <td><input type="text" name="name_food[]" class="name_food px-2" value="{{ $item[0] }}"
                            placeholder="ระบุเมนูอาหาร">
                    </td>
                    <td></td>
                    <td>
                        <select class="form-select" name="type_food[]" aria-label="Default select example">
                            <option>กรุณาเลือกรายการ</option>
                            <option value="อาหารกลางวัน" {{ ($food['period'] == 'อาหารกลางวัน') ? 'selected' : '' }}>อาหารกลางวัน</option>
                            <option value="อาหารว่าง" {{ ($food['period'] == 'อาหารว่าง') ? 'selected' : '' }}>อาหารว่าง</option>
                            <option value="เครื่องดื่ม" {{ ($food['period'] == 'เครื่องดื่ม') ? 'selected' : '' }}>เครื่องดื่ม</option>
                        </select>

                    </td>
                    <td class="text-center"><input type="text" name="price_menu[]" class="price_menu text-end px-2"
                            value="{{ $item[1] }}" placeholder="ราคา"></td>
                    <td><span class="currency">บาท</span></td>

                    {{-- <td><img class="delete_menu" src="/img/icon/close-material.png" alt="" onclick="delete_menu(this)"
                            style="cursor:pointer;"></td> --}}
                </tr>
                @endforeach







            </tbody>

            <tfoot>
                <tr>

                    <td></td>
                    <td>
                        {{-- <button type="button" class="add_menu price_product"><img class="mr-2"
                                src="/img/icon/add-circle-material.png" style="object-fit:contain;"
                                alt="">เพิ่มรายการ</button> --}}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>


                </tr>

            </tfoot>
        </table>

        <div class="row" style="margin: 100px 0 25px 0">
            <div class="col-12 d-flex justify-content-center">
                <a href="/reserve/room/form_room1" class="comeback">ย้อนกลับ</a>
                {{-- <button type="submit" class="approve_form">ส่งคำขอ</button> --}}
                <button type="submit" class="approve_form">บันทึก</button>
            </div>
        </div>
    </form>

</div>
@endsection


@section('script')
<script>
    $(document).ready(function () {

        $('.add_menu').on('click', function () {
            $('.choosed').last().clone().appendTo('tbody').each(function () {
                $(this).find('input').val('');
                // $(this).find('select').find('option:first').attr('selected','selected');
            });
        });

        $('.approve_form').on('click', function () {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'ทำการเพิ่มเมนูอาหารสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = '/welfare';
            })
        });



    });



    function delete_menu(e) {
        if ($('.choosed').length > 1) {
            $(e).parents('tr').remove();
        }
    }

</script>
@endsection
