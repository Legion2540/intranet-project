@extends('layout.booking')
@section('style')
    <style>
        .waiting_request {
            width: 253px;
            height: 111px;
            margin: 0 34px 0 0;
            border-radius: 2px;
            text-align: center;
            align-content: center;
            box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
            background-color: #ffbf00;
        }

        .cancel_request {
            width: 253px;
            height: 111px;
            margin: 0 34px 0 0;
            border-radius: 2px;
            text-align: center;
            align-content: center;
            box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
            background-color: #ee2a27;
        }

        .approve_request {
            width: 253px;
            height: 111px;
            margin: 0 34px 0 0;
            border-radius: 2px;
            text-align: center;
            align-content: center;
            box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
            background-color: #6dd005;
        }

        .count_num {
            font-size: 50px;
            font-weight: bold;
            text-align: center;
            color: #ffffff;
        }

        .container {
            margin-right: auto !important;
            margin-left: 270px !important;
            max-width: 1400px !important;
        }

        .text_request {
            font-size: 18px;
            font-weight: bold;
            color: #ffffff;
            margin-left: 14px;
        }

        .nav-pills .nav-item.show .nav-link,
        .nav-pills .nav-link.active {
            background-color: #4f72e5 !important;
            color: #ffffff !important;
        }

        #history_food {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        #request_room {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        #snack_list {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        #drink_list {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        #lunch_meal_list {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        .change_booking_btn {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        #approved_all {
            font-size: 20px;
            font-weight: 600;
            color: #4f72e5;
        }

        .filter_all {
            width: 133px;
            height: 35px;
            margin: 0 20px 0 15px;
            padding: 3px 4px 3px 10px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .form-select-1x {
            line-height: normal !important;
        }

        .search_request {
            width: 100%;
            height: 30px;
            padding: 5px 4px 0 11.5px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .waiting_status {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #ffbf00;
            border-radius: 8px
        }

        .approve_status {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #6dd005;
            border-radius: 8px
        }

        .cancel_status {
            width: 98px;
            height: 33px;
            padding: 0 16px;
            object-fit: contain;
            font-family: Kanit-ExtraLight;
            font-size: 14px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.07;
            letter-spacing: normal;
            text-align: center;
            color: #ffffff;
            background-color: #ee2a27;
            border-radius: 8px
        }

        .thead_car {
            font-family: Kanit-Regular;
            font-size: 16px;
            font-weight: bold;
            line-height: 0.94;
            color: #4a4a4a;
        }

        .export_file {
            width: 117px;
            height: 30px;
            color: #ffffff;
            border: none;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
        }


        .add_meal,
        .add_meal:hover {
            width: 260px;
            height: 30px;
            color: #ffffff;
            border: none;
            font-size: 15px;
            text-decoration: none;
            border-radius: 8px;
            box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
            background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
        }

        .date {
            font-size: 14px;
        }

        .time {
            font-size: 12px;
            color: #ceced4;
        }

        /* Arrow for change month */
        .arrow-booking {
            border: solid black;
            border-width: 0 2px 2px 0;
            display: inline-block;
            padding: 3px;
            margin: 0px 8px 1px 0px;
            height: 1px !important;
            width: 1px !important;
        }

        .left {
            transform: rotate(135deg);
            -webkit-transform: rotate(135deg);
        }

        .right {
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
        }

        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .filter_month {
            width: 103px;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .filter_province {
            width: 103px;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .filter_agency {
            width: 100%;
            height: 30px;
            margin: 5.5px 9.5px 5.5px 3.5px;
            padding: 2px 3px 2px 8px;
            border-radius: 8px;
            border: solid 0.5px #ceced4;
        }

        .toggle-switch:checked {
            background-color: #4BD763;
            border-color: #4BD763;
            border: none;
        }

        .toggle-switch {
            background-color: #ceced4;
            border-color: #ceced4;
            border: none;

        }

    </style>
@endsection

@section('content')
    <div class="my-4">
        <div class="row d-flex align-items-center mb-3">
            <div class="waiting_request">
                <span class="count_num">10</span>

                <div class="col-12 d-flex align-items-center justify-content-center">
                    <img src="/img/icon/white-access-time-material.png" alt="">
                    <span class="text_request">รออนุมัติ</span>
                </div>
            </div>

            <div class="approve_request">
                <span class="count_num">10</span>
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <img src="/img/icon/times-circle-outlined-font-awesome.png" alt="">
                    <span class="text_request">ไม่อนุมัติ</span>
                </div>
            </div>

            <div class="cancel_request">
                <span class="count_num">10</span>
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <img src="/img/icon/ion-android-checkbox-outline-ionicons.png" alt="">
                    <span class="text_request">อนุมัติ</span>
                </div>
            </div>
        </div>





        <div class="row" style="background-color: #ffffff;">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="request_room" data-bs-toggle="pill" data-bs-target="#room"
                        type="button" role="tab" aria-controls="room" aria-selected="true">คำขอใช้ห้องประชุม</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="history_food" data-bs-toggle="pill" data-bs-target="#food" type="button"
                        role="tab" aria-controls="food" aria-selected="false">ประวัติการสั่งอาหาร</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">

                <div class="tab-pane fade show active" id="room" role="tabpanel" aria-labelledby="request_room">

                    <div class="row d-flex">
                        <div class="col-12 d-flex">
                            <div class="col-6 d-flex align-items-center">
                                <span
                                    style="font-family:Kanit-Regular; font-size: 18px;
                                                                                                font-weight: bold;
                                                                                                color: #4a4a4a;"><b>ประจำเดือน</b></span>

                                <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                    <option selected>ทั้งหมด</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>

                                <span style="font-family:Kanit-Regular;
                                                                                                font-size: 18px;
                                                                                                font-weight: bold;
                                                                                                color: #4a4a4a;">ปี</span>
                                <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                    <option selected>ทั้งหมด</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>


                            </div>
                            <div class="col-6 d-flex justify-content-end align-items-center">
                                {{-- <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt=""> --}}
                                <input type="text" name="request_search" id="search_request" class="search_request"
                                    placeholder="ค้นหารายการ">
                                <img src="/img/icon/search-material-bule.png" style="position: absolute; left:74%" alt="">
                                <img src="/img/icon/tune-material-copy-3.png"
                                    style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                                <button type="button" class="export_file vertical-center"><img
                                        src="/img/icon/file_upload-material.png" class="mr-2" alt="">Export</button>
                            </div>
                        </div>
                    </div>
                    <table class="table mt-3" id="request_food_table">
                        <thead>
                            <tr class="text-center">
                                <th><input type="checkbox" name="key_thead"></th>
                                <th class="thead_car text-start">ผู้ขอใช้</th>
                                <th class="thead_car">ห้องประชุม</th>
                                <th class="thead_car">วันที่ใช้</th>
                                <th class="thead_car text-center">เวลาเสิร์ฟ</th>
                                <th class="thead_car">จำนวน</th>
                                <th class="thead_car">สำนัก</th>
                                <th class="thead_car">วันที่จอง</th>
                                <th class="thead_car"><img src="/img/icon/form-black.png" alt=""></th>
                                <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                                <th class="thead_car text-center">สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (empty($room))
                                <tr>
                                    <td colspan="11" class="text-center">ไม่มีคำร้องขอ</td>
                                </tr>
                            @else

                                @foreach ($room as $data)

                                    <tr class="text-center vertical-center">
                                        <td><input type="checkbox" name="list" class="list"></td>
                                        <td class="text-start"><a href="/welfare/approved?book_id={{ $data['id'] }}"">{{ $data->datauser->name }}</a></td>
                                                                <td><span>{{ $data['room_request'] }}</span></td>
                                                                <td>
                                                                    <div class=" col-12">
                                                <span
                                                    class="date">{{ $data['start_date'] . ' - ' . $data['end_date'] }}</span>
                </div>
                <div class="col-12">
                    <span class="time">{{ $data['start_time'] }} - {{ $data['end_time'] }}
                        น.</span>
                </div>
                </td>
                <td>
                    @foreach ($data['food_detail']->detail_food as $food)
                        <span>{{ $food->time_serve }}</span>
                    @endforeach
                </td>
                <td>{{ $data['member_count'] }}</td>
                <td><span>{{ $data->datauser->office_name }}</span></td>​
                <td>
                    <div class="col-12">
                        <span class="date">{{ $data['created_at'] }}</span>
                    </div>
                </td>
                <td><img src="/img/icon/form-black.png" alt=""></td>
                <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>



                @if ($data['welfare_approved'] == null)
                    <td><span class="waiting_status">รออนุมัติ</span></td>

                @elseif($data['welfare_approved'] == 'approved')
                    <td><span class="approve_status">อนุมัติ</span></td>

                @elseif($data['welfare_approved'] == 'cancelled')
                    <td><span class="cancel_status">ไม่อนุมัติ</span></td>

                @endif

                </tr>

                @endforeach
                @endif

                </tbody>
                </table>

            </div>


            <div class="tab-pane fade" id="food" role="tabpanel" aria-labelledby="history_food">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span
                                style="font-family:Kanit-Regular; font-size: 18px; color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span
                                style="font-family:Kanit-Regular; font-size: 18px; font-weight: bold; color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            {{-- <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt=""> --}}
                            <input type="text" name="request_search" id="search_history" class="search_request"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:74%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <button type="button" class="export_file vertical-center"><img
                                    src="/img/icon/file_upload-material.png" class="mr-2" alt="">Export</button>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_food_table">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-start">ผู้ขอใช้</th>
                            <th class="thead_car">ห้องประชุม</th>
                            <th class="thead_car">วันที่ใช้</th>
                            <th class="thead_car text-center">เวลาเสิร์ฟ</th>
                            <th class="thead_car">จำนวน</th>
                            <th class="thead_car">สำนัก</th>
                            <th class="thead_car">วันที่จอง</th>
                            <th class="thead_car"><img src="/img/icon/form-black.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>


                        @if (!isset($his_room))
                            <tr>
                                <td colspan="11" class="text-center">ไม่มีคำร้องขอ</td>
                            </tr>
                        @else


                            @foreach ($his_room as $data)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="list" class="list"></td>
                                    <td class="text-start"><a
                                            href="/welfare/approved?book_id={{ $data['id'] }}">{{ $data->datauser->name }}</a>
                                    </td>
                                    <td><span>{{ $data['room_request'] }}</span></td>
                                    <td>
                                        <div class="col-12">
                                            <span
                                                class="date">{{ $data['start_date'] . ' - ' . $data['end_date'] }}</span>
                                        </div>

                                        <div class="col-12">
                                            <span class="time">{{ $data['start_time'] }} - {{ $data['end_time'] }}
                                                น.</span>
                                        </div>
                                    </td>
                                    <td>

                                        @foreach ($data['food_detail']->detail_food as $food)
                                            <span>{{ $food->time_serve }}</span>
                                        @endforeach
                                    </td>
                                    <td>{{ $data['member_count'] }}</td>
                                    <td><span>{{ $data->datauser->office_name }}</span></td>​
                                    <td>
                                        <div class="col-12">
                                            <span class="date">{{ $data['updated_at'] }}</span>
                                        </div>
                                    </td>
                                    <td><img src="/img/icon/form-black.png" alt=""></td>
                                    <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>


                                    @if ($data['welfare_approved'] == 'approved')
                                        <td><span class="approve_status">อนุมัติ</span></td>

                                    @elseif($data['welfare_approved'] == 'cancelled')
                                        <td><span class="cancel_status">ไม่อนุมัติ</span></td>

                                    @endif


                                </tr>

                            @endforeach

                        @endif

                    </tbody>
                </table>
            </div>



        </div>
    </div>




    <div class="mt-4 event_food" style="background-color:white;">
        <div class="row">
            <nav>
                <div class="nav nav-tabs border-0" id="nav-tab" role="tablist">
                    <button class="nav-link  mr-2 change_booking_btn" id="rooms" data-bs-toggle="tab"
                        data-bs-target="#calendar_room" typ e="button" role="tab" aria-controls="calendar_room"
                        aria-selected="false">ปฎิทินการใช้ห้องประชุม</button>
                </div>
            </nav>

            <div class="tab-content p-2" id="nav-tabContent" style="background-color:#ffffff;">
                <div class="tab-pane fade show active m-0 show_calendar" id="calendar_room" role="tabpanel"
                    aria-labelledby="calendar_room">
                    <div class="col-12 d-flex align-content-center p-0">
                        <div class="col-4 d-flex align-content-center pr-0">
                            <div class="d-flex align-items-center">
                                <span class="arrow-booking left"></span>
                            </div>
                            <div class="px-2 w-auto ">
                                <span
                                    style="font-family: Kanit-Regular; font-size: 25px; font-weight: bold; color: #4a4a4a;">December
                                    2021</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="arrow-booking right"></span>
                            </div>

                            <img class="ml-3" src="/img/icon/date-range-material-copy-4.png"
                                style="object-fit:contain; width:20px" alt="">
                        </div>
                        <div class="col-8 d-flex align-content-center pl-0 justify-content-end pr-0">

                            <div class="d-flex align-items-center">
                                <input type="search" class="p-2 mr-1" name="calendar_room"
                                    style="width: 320px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                                    placeholder="ค้นหารายการ">
                                <img src="/img/icon/search-material-bule.png"
                                    style="position: absolute; left: 67%; top: 13px;" alt="">
                            </div>
                            <div class="d-flex align-items-center">
                                {{-- Filter Calendar img --}}
                                <span class="filter_calendar_room" data-bs-placement="bottom" data-bs-toggle="popover"
                                    data-bs-title="Filters">
                                    <img src="/img/icon/tune-material-copy-3.png" class="mx-2" alt="">
                                </span>

                                <select class="form-select form-select-1x filter_month" aria-label="Month">
                                    <option selected>Month</option>
                                    <option value="day">Day</option>
                                    <option value="week">Week</option>
                                    <option value="month">Month</option>
                                </select>

                                <select class="form-select form-select-1x filter_province" aria-label="เมืองทอง">
                                    <option selected>เมืองทอง</option>
                                    <option value="1">รัชดา</option>
                                    <option value="2">ทองหล่อ</option>
                                    <option value="3">สีลม</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div id="food_calendar" class="mt-4"></div>
                </div>
            </div>
        </div>
    </div>





    <div class="row mt-5" style="background-color: #ffffff;">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="snack_list" data-bs-toggle="pill" data-bs-target="#snack" type="button"
                    role="tab" aria-controls="snack" aria-selected="true">ข้อมูลอาหารว่าง</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="drink_list" data-bs-toggle="pill" data-bs-target="#drink" type="button"
                    role="tab" aria-controls="drink" aria-selected="false">ข้อมูลเครื่องดื่ม</button>
            </li>

            <li class="nav-item" role="presentation">
                <button class="nav-link" id="lunch_meal_list" data-bs-toggle="pill" data-bs-target="#lunch_meal"
                    type="button" role="tab" aria-controls="lunch_meal" aria-selected="false">ข้อมูลอาหารกลางวัน</button>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="snack" role="tabpanel" aria-labelledby="snack_list">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            {{-- <img src="/img/icon/date-range-material-copy-4.png"
                                style="width: 19px; height: 15px; object-fit: contain;  margin: 0 14px 0 12px;" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt=""> --}}
                            <input type="text" name="request_search" id="search_snack" class="search_request"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/welfare/add?period=snack" type="button" class="add_meal text-center pt-1">+
                                ลงทะเบียนอาหารว่าง</a>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_food_table">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-start">No.</th>
                            <th class="thead_car">รายการ</th>
                            <th class="thead_car">ราคาต่อหน่วย</th>
                            <th class="thead_car"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/border-color-material-copy.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/close-material.png" alt=""></th>
                            <th class="thead_car text-center">การแสดงผล</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (empty($snack))
                            <tr>
                                <td class="text-center" colspan="8">ไม่มีข้อมูล</td>
                            </tr>
                        @else
                            @foreach (@$snack as $key => $item)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="snack_checkbox" class="snack_checkbox"></td>
                                    <td class="text-start"><span>{{ @$key + 1 }}</span></td>
                                    <td class="text-center">

                                        @foreach (json_decode($item['list_food']) as $list_food)
                                            <span>{{ $list_food[0] }}</span><br>
                                        @endforeach

                                    </td>
                                    <td>

                                        @foreach (json_decode($item['list_food']) as $list_food)
                                            <span>{{ $list_food[1] }}</span><br>
                                        @endforeach

                                    </td>
                                    <td><img src="/img/icon/description-material.png" alt=""></td>
                                    <td><a href="/welfare/edit?food_id={{ @$item['id'] }}"><img
                                                src="/img/icon/border-color-material-copy.png" alt=""></a>
                                    </td>
                                    <td><img src="/img/icon/close-material.png" alt=""></td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input toggle-switch snack_status" type="checkbox"
                                                {{ @$item['status_food'] == 'ready' ? ' checked' : '' }}
                                                data-id="{{ @$item['id'] }}">
                                            <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif


                    </tbody>
                </table>

            </div>


            <div class="tab-pane fade" id="drink" role="tabpanel" aria-labelledby="drink_list">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="request_search" id="search_drink" class="search_request"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/welfare/add?period=drink" type="button" class="add_meal text-center pt-1">+
                                ลงทะเบียนอาหารว่าง</a>

                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_food_table">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-start">No.</th>
                            <th class="thead_car">รายการ</th>
                            <th class="thead_car">ราคาต่อหน่วย</th>
                            <th class="thead_car"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/border-color-material-copy.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/close-material.png" alt=""></th>
                            <th class="thead_car text-center">การแสดงผล</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($drink))
                            <tr>
                                <td class="text-center" colspan="8">ไม่มีข้อมูล</td>
                            </tr>
                        @else
                            @foreach (@$drink as $key => $item)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="snack_checkbox" class="snack_checkbox"></td>
                                    <td class="text-start"><span>{{ @$key + 1 }}</span></td>
                                    <td class="text-center"><span>{{ @$item['list_food'] }}</span></td>
                                    <td>{{ @$item['price_food'] }}</td>
                                    <td><img src="/img/icon/description-material.png" alt=""></td>
                                    <td><a href="/welfare/edit?food_id={{ @$item['id'] }}"><img
                                                src="/img/icon/border-color-material-copy.png" alt=""></a>
                                    </td>
                                    <td><img src="/img/icon/close-material.png" alt=""></td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input toggle-switch drink_status" type="checkbox"
                                                {{ @$item['status_food'] == 'ready' ? ' checked' : '' }}
                                                data-id="{{ @$item['id'] }}">
                                            <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>


            <div class="tab-pane fade" id="lunch_meal" role="tabpanel" aria-labelledby="lunch_meal_list">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="request_search" id="search_lunch" class="search_request"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/welfare/add?period=lunch" type="button" class="add_meal text-center pt-1">+
                                ลงทะเบียนอาหารว่าง</a>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_food_table">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-start">No.</th>
                            <th class="thead_car">รายการ</th>
                            <th class="thead_car">ราคาต่อหน่วย</th>
                            <th class="thead_car"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/border-color-material-copy.png" alt=""></th>
                            <th class="thead_car"><img src="/img/icon/close-material.png" alt=""></th>
                            <th class="thead_car text-center">การแสดงผล</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (empty($lunch))
                            <tr>
                                <td class="text-center" colspan="8">ไม่มีข้อมูล</td>
                            </tr>
                        @else

                            @foreach (@$lunch as $key => $item)
                                <tr class="text-center vertical-center">
                                    <td><input type="checkbox" name="snack_checkbox" class="snack_checkbox"></td>
                                    <td class="text-start"><span>{{ @$key + 1 }}</span></td>
                                    <td class="text-center">

                                        @foreach (json_decode($item['list_food']) as $list_food)
                                            <span>{{ $list_food[0] }}</span><br>
                                        @endforeach

                                    </td>

                                    <td>

                                        @foreach (json_decode($item['list_food']) as $list_food)
                                            <span>{{ $list_food[1] }}</span><br>
                                        @endforeach

                                    </td>
                                    <td><img src="/img/icon/description-material.png" alt=""></td>
                                    <td><a href="/welfare/edit?food_id={{ @$item['id'] }}"><img
                                                src="/img/icon/border-color-material-copy.png" alt=""></a>
                                    </td>
                                    <td><img src="/img/icon/close-material.png" alt=""></td>
                                    <td>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input toggle-switch lunch_status" type="checkbox"
                                                {{ @$item['status_food'] == 'ready' ? ' checked' : '' }}
                                                data-id="{{ @$item['id'] }}">
                                            <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>













    </div>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var food_calendar = $('#food_calendar')[0];

            var calendar_food = new FullCalendar.Calendar(food_calendar, {
                initialView: 'dayGridMonth'
            });
            calendar_food.render();
        });


        $(document).ready(function() {


            $('.snack_status').on('click', function() {
                let id = $(this).data('id');
                if ($(this).is(':checked')) {

                    let status = $(this).val();


                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });



                } else {
                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'not ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }
            });




            $('.drink_status:checked').on('click', function() {
                let id = $(this).data('id');


                if ($(this).is(':checked')) {
                    let status = $(this).val();

                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });



                } else {
                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'not ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }
            });




            $('.lunch_status:checked').on('click', function() {
                let id = $(this).data('id');

                if ($(this).is(':checked')) {
                    let status = $(this).val();

                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });



                } else {
                    $.ajax({
                        url: '/welfare/status',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            food_id: id,
                            status: 'not ready',
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }

            });




        })
    </script>
@endsection
