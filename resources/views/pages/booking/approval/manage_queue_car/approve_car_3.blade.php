@extends('layout.booking')
@section('style')
<style>
    .title_room2 {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }

    .head_date_tab {
        font-family: Kanit-Regular;
    }

    .select_form_room2 {
        width: 100%;
        height: 30px;
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        line-height: 1;
        border: solid 0.5px #ceced4;
    }

    .select_period {
        width: 140px;
        height: 30px;
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    #form_time {
        width: 100px;
        height: 30px;
        margin: 0 0 0 8px;
        padding: 4px 4px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;

    }

    #input_price {
        background-color: #f7f7f7;
        width: 100px;
        height: 30px;
        margin: 0 0 0 8px;
        padding: 4px 4px 4px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .warning_span {
        font-size: 12px;
        line-height: 1.25;
        color: #ee2a27;
    }

    .comeback,
    .comeback:hover {
        width: 112px;
        height: 30px;
        margin: 0 17px 0 0;
        border-radius: 8px;
        padding: 4px 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
    }

    .approve_form,
    .approve_form:hover {
        color: #ffffff;
        width: 112px;
        height: 30px;
        margin: 0 0 0 17px;
        padding: 4px 28px;
        border-radius: 8px;
        text-align: center;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(266deg, #4f72e5 36%, #314d7b 145%);
    }

    .price_product {
        font-family: Kanit-ExtraLight;
        font-weight: bold;
        font-size: 16px;
        color: #4f72e5;
    }


    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }



    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        font-weight: bold;
        font-family: Kanit-Regular;
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th,
    .show_car th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .add_menu {
        background-color: #ffffff;
        border-radius: 8px;
        border: 0.5px solid #d5d4d4;
        color: #4f72e5;
        height: 30px;
        margin-top: 10px;
    }

    .title_second {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
    }

    .text_need {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
    }

    #time_serve {
        height: 30px;
        width: 80px;
        border-radius: 8px;
        border: 1px solid #d5d4d4;
        padding-left: 10px;
    }

    .input_date {
        width: 222px;
        height: 30px;
        padding: 5px 9px 3px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .input_time {
        width: 100px;
        height: 30px;
        padding: 5px 9px 3px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .waiting_status {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: gold;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .approve_status {
        width: 100px;
        height: 23px;
        color: #000000;
        background-color: #90ee90;
        border: none;
        padding: 1px 10px 1px 10px;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .cancel_status {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: red;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .show_car tr td {
        vertical-align: middle;
        font-size: 14px;
    }

</style>
@endsection

@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <div class="col-10">
            <span class="title_room2">จัดการรถ</span>
        </div>
        <div class="col-2 d-flex align-items-center justify-content-end status_form">
            <span class="waiting_status mr-5">รออนุมัติ</span>
            <img src="/img/icon/print-material.png" alt="">
        </div>
    </div>
    <div class="my-2" style="border:0.5px dashed #ceced4"></div>


    <div class="row">
        <div class="col-12 my-3" style="padding: 0 250px;">
            <ul class="timeline">
                <li class="bf_active"></li>
                <li class="active">เลือกรถ</li>
                <li class=""></li>
                <li class=""></li>
            </ul>
        </div>
    </div>

    <div class="col-12 mt-3 d-flex align-items-center mb-1">
        <div class="col-4 px-0">
            <span class="head_date_tab">จากวันที่</span>
        </div>
        <div class="col-4">
            <span class="head_date_tab">ถึงวันที่</span>
        </div>
        <div class="col-4">
            <span class="head_date_tab">เลือกรถที่ต้องใช้</span>
        </div>
    </div>

    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-4 d-flex align-items-center px-0">
            <div class="col-7">
                <input type="text" name="date_car_start" id="date_car_start" class="input_date"
                    placeholder="22 January 2021">
                <img src="/img/icon/date-range-material-copy-4.png" alt=""
                    style="position:absolute; right: 21px; top: 5px;">
            </div>

            <div class="col-4 d-flex align-items-center justify-content-between px-0">
                <span>เวลา</span>
                <input type="text" name="time_car_start" id="time_car_start" class="input_time" placeholder="09.00">
                <img src="/img/icon/access-time-material.png" alt="" style="position:absolute; right: 8px; top: 8px;">
            </div>
        </div>
        <div class="col-4 d-flex align-items-center px-0">
            <div class="col-7">
                <input type="text" name="date_car_end" id="date_car_end" class="input_date"
                    placeholder="22 January 2021">
                <img src="/img/icon/date-range-material-copy-4.png" alt=""
                    style="position:absolute; right: 21px; top: 5px;">
            </div>

            <div class="col-4 d-flex align-items-center justify-content-between px-0">
                <span>เวลา</span>
                <input type="text" name="time_car_end" id="time_car_end" class="input_time" placeholder="09.00">
                <img src="/img/icon/access-time-material.png" alt="" style="position:absolute; right: 8px; top: 8px;">
            </div>
        </div>
        <div class="col-4 px-0 d-flex align-items-center">
            <div class="col-4 d-flex align-items-center px-0 justify-content-center">
                <div class="col-1">
                    <input type="checkbox" name="all_status_car" id="all_status_car">
                </div>
                <div class="col-11 pl-1">
                    <span>สถานะทั้งหมด</span>
                </div>
            </div>
            <div class="col-2 d-flex align-items-center px-0">
                <div class="col-1">
                    <input type="checkbox" name="free_status_car" id="free_status_car">
                </div>
                <div class="col-11 pl-1">
                    <span>ว่าง</span>
                </div>
            </div>
            <div class="col-2 d-flex align-items-center px-0">
                <div class="col-1">
                    <input type="checkbox" name="notfree_status_car" id="notfree_status_car">
                </div>
                <div class="col-11 pl-1">
                    <span>ไม่ว่าง</span>
                </div>
            </div>
        </div>
    </div>







    <form action="/reserve/room/form_room3" method="post">
        <table class="table show_car mt-3">
            <thead>

                <tr>
                    <th><input type="checkbox" name="all_car" id="all_car"></th>
                    <th></th>
                    <th><span>ยี่ห้อ / รุ่น</span></th>
                    <th><span>ทะเบียน</span></th>
                    <th><span>เลขไมล์ปัจจุบัน</span></th>
                    <th><span>ประเภทน้ำมัน</span></th>
                    <th><span>จำนวนที่นั่ง</span></th>
                    <th class="text-center"><span>ที่นั่ง</span></th>
                    <th class="text-center"><img src="/img/icon/description-material.png" alt=""></th>
                </tr>

            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" name="car_id" id="car_id"></td>
                    <td><img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                            style="object-fit:cover;width: 137px;height: 73px;" alt=""></td>
                    <td><span>Toyota Vios</span></td>
                    <td><span>2กง 2415</span></td>
                    <td><span>250,000</span></td>
                    <td><span>แก๊ซโซฮอล์ 95</span></td>
                    <td><span style="font-family:Kanit-Regular ; color:#4f72e5;">0</span><span> / 4 คน</span></td>

                    <td class="text-center"><span class="approve_status">สถานะว่าง</span></td>
                    <td class="text-center"><img src="/img/icon/description-material.png" alt=""></td>
                </tr>
            </tbody>
        </table>
        <div class="col-12 d-flex justify-content-end">
            <span class="mr-2" style="color: #4f72e5; font-family:Kanit-Regular">
                <</span> <span class="mr-2">1
            </span>
            <span style="color: #4f72e5; font-family:Kanit-Regular">></span>
        </div>


        <div class="row" style="margin: 100px 0 25px 0">
            <div class="col-12 d-flex justify-content-center">
                <a href="/manage_queueCar/approve_2" class="comeback">ย้อนกลับ</a>
                {{-- <button type="submit" class="approve_form">ส่งคำขอ</button> --}}
                <a href="/manage_queueCar/approve_4" class="approve_form">ถัดไป</a>
            </div>
        </div>
    </form>

</div>
@endsection


@section('script')
<script>
    $(document).ready(function () {

        $(".first_select_period").change(function () {
            $('.not_choose').remove();
            $('.choosed').toggle();
            $('.select_period').val($(this).val());
        });

    });

    function click_add() {
        var count_list = $('.index_row_menu').last().text();
        var count = parseInt(count_list);
        var counted = count + 1;


        $('.choosed').last().clone().appendTo('tbody');
        $('.add_menu').not(':last').last().remove();
        $('.index_row_menu').not(':first').last().text(counted++);
    }

</script>
@endsection
