@extends('layout.booking')
@section('style')
<style>
    .title_form {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        color: #4f72e5;
    }

    .date_input {
        width: 226px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .time_input {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .form-select-city {
        padding: 5px 6.5px 3px 9px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        color: #4a4a4a;
    }

    .choosemap_btn {
        position: absolute;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        top: 50%;
        left: 42%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        font-size: 16px;
        padding: 5px 6px;
    }

    .waiting_approve,
    .waiting_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 28px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-decoration: none;
        text-align: center;
        text-decoration: none;
        background-color: #ff7f00;
    }

    .cancel_approve,
    .cancel_approve:hover {
        width: 112px;
        height: 30px;
        font-size: 16px;
        padding: 4px 17px;
        border-radius: 8px;
        font-weight: 500;
        color: #ffffff;
        text-align: center;
        text-decoration: none;
        background-color: #ee2a27;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .valiedate_data,
    .valiedate_data:hover {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 3px 23px;
        border: none;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .comback_index,
    .comback_index:hover {
        width: 112px;
        height: 30px;
        background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);
        color: #ffffff;
        border-radius: 8px;
        font-size: 16px;
        padding: 4px 18px;
        border: none;
        text-align: center;
        text-decoration: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
    }

    .timeline {
        counter-reset: year 0;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 25%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        font-family: 'Dosis', sans-serif;
    }

    ul:nth-child(1) {
        color: #4f72e5;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4f72e5;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #ffffff;
        color: #4f72e5;
        transition: all ease-in-out .3s;
        cursor: pointer;
        font-size: 25px;
        position: relative;
        z-index: 1;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: #4f72e5;
        top: 25px;
        left: -50%;
        z-index: 0;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #4f72e5;
    }

    .timeline li.active:before {
        background: #4f72e5;
        width: 70px;
        height: 70px;
        padding-top: 7px;
        border: 3px solid;
        font-size: 40px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
        bottom: 10px;
    }

    .timeline li.active+li:after {
        background: #4f72e5;
    }

    /* Bf active */
    .timeline li.bf_active {
        color: #555555;
    }

    .timeline li.bf_active:before {
        background: #4f72e5;
        width: 50px;
        height: 50px;
        border: 3px solid;
        font-size: 25px;
        box-shadow: 0 0px 0px 0 rgb(0 0 0 / 50%), 0 0px 9px 0 rgb(0 0 0 / 50%);
        color: #ffffff;
        position: relative;
        z-index: 1;
    }

    .timeline li.bf_active+li:after {
        background: #4f72e5;
    }

    .table td,
    .table th {
        padding: 9px !important;
    }

    .table thead th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table>:not(:last-child)>:last-child>* {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .table tfoot tr,
    .table tfoot th {
        border-top: 1px solid #4f72e5 !important;
        border-bottom: 1px solid #4f72e5 !important;
    }

    .browse {
        width: 90px;
        height: 26px;
        font-size: 12px;
        color: #ffffff;
        border-radius: 8px;
        border: none;
        box-shadow: 0 0 3px 1px rgba(0, 0, 0, 0.15);
        background-image: linear-gradient(193deg, #4f72e5 47%, #314d7b 119%);
    }

    .extension_files {
        margin-left: 10px;
        font-family: Kanit-Regular;
        font-weight: bold;
        font-size: 12px;
        color: #ee2a27;
    }

    .form-select:disabled {
        height: 30px;
        line-height: 1;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #f3f3f3;
    }

    .status_waiting {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #ffbf00;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_approve {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #6dd005;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

    .status_cancel {
        width: 100px;
        height: 23px;
        color: #ffffff;
        background-color: #ee2a27;
        border: none;
        border-radius: 8px;
        text-align: center;
        line-height: 1.4;
    }

</style>
@endsection
@section('content')
<div class="container my-3 px-5 py-3" style="background-color:white; max-width: 1400px !important;">
    <div class="row">
        <div class="col-10">
            <span class="title_form">จัดารคิวรถ</span>
        </div>
        <div class="col-2 d-flex align-items-center justify-content-end status_form">
            <span class="status_waiting mr-5">รออนุมัติ</span>
            <img src="/img/icon/print-material.png" alt="">
        </div>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <div class="row">
        <div class="col-12 my-3" style="padding: 0 360px;">
            <ul class="timeline">
                <li class="active">รายละเอียดการจอง</li>
                <li class=""></li>
                <li class=""></li>
                <li></li>
            </ul>
        </div>
    </div>

    <div class="col-12 mt-3 pl-0">
        <span class="title_form">ผู้ขอจองใช้รถ</span>
    </div>

    <form action="#" method="post">
        <div class="row mt-2">

            <div class="col-12 d-flex">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10 px-0">
                        <span class="input_evr">นางสาวดาริสา พัชรโชด</span>
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="hidden" name="form_name_room" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">สำนัก</span>
                    </div>
                    <div class="col-10 pl-0">
                        <span class="input_evr">สำนักงานบริหารกลาง</span>
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="hidden" name="form_name_room" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">ตำแหน่ง</span>
                    </div>
                    <div class="col-10 pl-0">
                        <span class="input_evr">ธุรการ</span>
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="hidden" name="position" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">เบอร์โทรศัพท์ (ภายใน)</span>
                    </div>
                    <div class="col-9 pl-0">
                        <span class="input_evr">02-777-1200</span>
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="hidden" name="tel_in_no" readonly>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3 px-0">
                        <span class="content_span">ผู้บังคับบัญชา</span>
                    </div>
                    <div class="col-9 px-0">
                        <span class="input_evr">นายกันต์ สินธร</span>
                        <input class="input_evr px-2" style="border-radius: 8px; border: solid 0.5px #ceced4;"
                            type="hidden" name="manager_id" readonly>
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">จำนวนที่นั่ง</span>
                    </div>
                    <div class="col-10 pr-0">
                        <span class="input_evr">4</span>
                        <input type="hidden" class="input_evr px-2" name="member_seat" id="member_seat" value="2"
                            disabled>
                    </div>
                </div>
            </div>

        </div>


        <div class="row mt-2">
            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex">
                    <div class="col-2 px-0">
                        <span class="content_span">ขออนุญาตใช้</span>
                    </div>
                    <div class="col-10 pl-0 d-flex align-items-center">
                        <div class="col-4">
                            <input type="checkbox" name="type_car" id="car_center" checked>
                            <span class="input_evr">รถส่วนกลาง</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่เริ่มใช้</span>
                    </div>
                    <div class="col-5">
                        <span class="input_evr">22 มกราคม 2564</span>
                        <input class="date_input" type="hidden" name="date_start">
                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="input_evr">09.00</span>
                        <input class="time_input" type="hidden" placeholder="09:00" name="time_start">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <span class="content_span">วันที่สิ้นสุด</span>
                    </div>
                    <div class="col-5">
                        <span class="input_evr">22 มกราคม 2564</span>
                        <input class="date_input" type="hidden" name="date_start">

                    </div>

                    <div class="col-1">
                        <span class="content_span">เวลา</span>
                    </div>
                    <div class="col-3">
                        <span class="input_evr">09.00</span>
                        <input class="time_input" type="hidden" name="time_start">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center w-100">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">วัตถุประสงค์</span>
                    </div>
                    <div class="col-10 pr-0">
                        <span class="input_evr">09.00</span>
                        <input type="hidden" class="input_evr px-2" name="purpose" id="purpose" value="ไปสัมนาวิชาการ">
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3 d-flex">
                <div class="col-6 d-flex align-items-center w-100">
                    <div class="col-2 px-0 align-items-start">
                        <span class="content_span">หลักฐานอื่นๆ</span>
                    </div>
                    <div class="col-9  d-flex align-items-center">
                        <span class="input_evr">-</span>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex">

                <div class="col-6 mt-3">
                    <div class="row">
                        <span class="title_form px-0">สถานที่</span>
                    </div>
                    <div class="col-12 d-flex mt-3 px-0">
                        <div class="col-2 d-flex px-0">
                            <span class="content_span">ชื่อสถานที่</span>
                        </div>

                        <div class="col-10 d-flex align-items-center pr-0">
                            <span class="input_evr">โรงแรมเซนทารา แกรนด์</span>
                            <input type="hidden" class="input_evr px-2" name="location" id="location"
                                value="โรงแรมเซนทารา แกรนด์" disabled>
                        </div>
                    </div>

                    <div class="col-12 d-flex mt-3 px-0">
                        <div class="col-2 px-0">
                            <span class="content_span">จังหวัด</span>
                        </div>
                        <div class="col-10 pr-0 d-flex align-items-center">
                            <span class="input_evr">กรุงเทพมหานคร</span>
                            <input type="hidden" class="input_evr px-2" name="location" id="location"
                                value="กรุงเทพมหานคร" disabled>
                        </div>

                    </div>

                    <div class="col-12 d-flex mt-3 px-0">
                        <div class="col-2 px-0">
                            <span class="content_span">อำเภอ/เขต</span>
                        </div>
                        <div class="col-10 pr-0 d-flex align-items-center">
                            <span class="input_evr">ปทุมวัน</span>
                            <input type="hidden" class="input_evr px-2" name="location" id="location" value="ปทุมวัน"
                                disabled>
                        </div>
                    </div>

                    <div class="col-12 d-flex mt-3 px-0">
                        <div class="col-2 px-0 ">
                            <span class="content_span">ตำบล/แขวง</span>
                        </div>
                        <div class="col-10 pr-0 d-flex align-items-center">
                            <span class="input_evr">ปทุมวัน</span>
                            <input type="hidden" class="input_evr px-2" name="location" id="location" value="ปทุมวัน"
                                disabled>
                        </div>
                    </div>


                </div>

                <div class="col-6 d-flex align-items-center justify-content-start">
                    <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                        style="object-fit: cover; width: 475px; height: 244px;" alt="">

                </div>
            </div>


        </div>





        <div class="row" style="margin-top:150px; margin-bottom:80px;">
            <div class="col-12 align-items-center d-flex justify-content-center">

                <a href="/manage_queueCar" class="cancel_approve mr-3">ย้อนกลับ</a>
                <a href="/manage_queueCar/approve_3" class="valiedate_data">จัดการรถ</a>

            </div>
        </div>



    </form>

</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {


    });

</script>
@endsection
