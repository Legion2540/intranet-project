@extends('layout.booking')
@section('style')
<style>
    .title_pages {
        font-family: Kanit-Regular;
        font-size: 18px;
        color: #4f72e5;
    }

    .title_pages_modal {
        font-family: Kanit-Regular;
        font-size: 26px;
        color: #4f72e5;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .content_span_modal {
        font-size: 14px;
        color: #4a4a4a;
    }

    .data_result_modal {
        font-size: 14px;
        color: #4f72e5;
    }

    .search_datial_car {
        width: 420px;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .export_history,
    .export_history:hover {
        width: 147px;
        height: 30px;
        color: #ffffff;
        border: none;
        font-size: 15px;
        text-decoration: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }

    .content_head_span {
        font-family: Kanit-Regular;
        font-size: 16px;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        /* width: 168px; */
        height: 30px;
        padding: 0 5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input_color {
        width: 94px;
        height: 30px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .switch_display .form-check-input:checked {
        background-color: #4cd964;
        border: none;
    }


    .waiting_status {
        width: 98px;
        height: 33px;
        padding: 0 36px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        /* line-height: 1.07; */
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 8px
    }

    .approve_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 8px
    }

    .cancel_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        border-radius: 8px
    }

    #history_car {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

    #history_car tr th,
    #history_car tr td {
        font-size: 14px;

    }


    .detail_request .nav-pills .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .detail_request .nav-item {
        margin: 0 0 0 10px;
    }

    .detail_request .nav-pills .nav-link {
        font-size: 18px;
        font-weight: bold;
        background-color: #ffffff !important;
        color: #4f72e5 !important;
        border-radius: 0px;
        border-radius: 4px 4px 0px 0px;
        padding-left: 95px;
        padding-right: 95px;
    }

    .close_modal {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        border: none;
        color: #ffffff;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ff7f00;
    }

</style>



@section('content')
<div class="my-4 p-3" style="background-color:#ffffff">
    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-4 d-flex align-items-center px-0">
            <img src="/img/icon/desktop-mac-material.png" alt="" class="mr-2">
            <span class="title_pages">Toyota Vios</span>
            <span class="title_pages ml-2">2กง 2415</span>
        </div>

        <div class="col-8 d-flex align-items-center justify-content-end pr-0">

            <img src="/img/icon/date-range-material-copy-4.png" alt="" style="cursor: pointer;">
            <img src="/img/icon/tune-material-copy-3.png"
                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;cursor: pointer;" alt="">

            <input type="text" name="search_datial_car" id="search_datial_car" class="search_datial_car mr-3"
                placeholder="ค้นหารายการ">
            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:76.6%" alt="">

            <a href="/manage_queueCar/add/car" type="button" class="export_history text-center pt-1">
                <img src="/img/icon/file-download-material.png" alt="">
                Export</a>
        </div>
    </div>

    <div class="col-12 d-flex mt-3 px-0">

        <div class="col-3 pl-0">
            <div class="col-12 px-0">
                <span class="content_head_span">รูปภาพ</span>
            </div>
            <div class="col-12 mt-1 px-0">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="width: 294.6px; height: 157px; object-fit: cover" alt="">
            </div>
        </div>


        <div class="col-9 px-0">
            <div class="col-12 px-0">
                <span class="content_head_span">ข้อมูลรถ</span>
            </div>


            <div class="col-12 px-0">
                <div class="col-12 d-flex align-items-center mt-2 px-0 ">

                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0">
                            <span>หมายเลขรถ</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="car_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0 mr-3">
                            <span>หมายเลขเครื่อง</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="engine_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mx-3">
                        <div class="col-5 px-0">
                            <span>หมายเลขตัวถัง</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="body_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-2 px-0">
                            <span>สี</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="color" class="input_color">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 px-0">
                <div class="col-12 d-flex align-items-center mt-2 px-0 ">

                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0">
                            <span>ประเภทรถ</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="type_car" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0 mr-3">
                            <span>ยี่ห้อ/รุ่น</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="text" name="brand_car" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mx-3">
                        <div class="col-6 px-0">
                            <span>ประเภทน้ำมันที่ใช้</span>
                        </div>

                        <div class="col-6 px-0">
                            <input type="text" name="type_gas" class="input_evr">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 px-0">
                <div class="col-12 d-flex align-items-center mt-2 px-0 ">

                    <div class="col-3 d-flex align-items-center justify-content-between px-0 mr-3">
                        <div class="col-5 px-0">
                            <span>วันที่จดทะเบียน</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="start_plate_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0 mr-3">
                            <span>ทะเบียนหมดอายุ</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="end_plate_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mx-3">
                        <div class="col-6 px-0">
                            <span>เลขทะเบียน</span>
                        </div>

                        <div class="col-6 px-0">
                            <input type="text" name="plate_no" class="input_evr">
                        </div>
                    </div>
                    <div class="col-3 d-flex align-items-center">
                        <img src="/img/icon/error-outline-material.png" alt="">
                    </div>
                </div>
            </div>


            <div class="col-12 px-0">
                <div class="col-12 d-flex align-items-center mt-2 px-0 ">

                    <div class="col-3 d-flex align-items-center justify-content-between px-0 mr-3">
                        <div class="col-5 px-0">
                            <span>วันที่ทำ พ.ร.บ.</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="start_license_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0 mr-3">
                            <span>พ.ร.บ. หมดอายุ</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="end_license_no" class="input_evr">
                        </div>
                    </div>

                </div>
            </div>




            <div class="col-12 px-0">
                <div class="col-12 d-flex align-items-center mt-2 px-0 ">

                    <div class="col-3 d-flex align-items-center justify-content-between px-0 mr-3">
                        <div class="col-5 px-0">
                            <span>วันที่ทำประกัน</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="start_insurance_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mr-3">
                        <div class="col-5 px-0 mr-3">
                            <span>ประกันหมดอายุ</span>
                        </div>

                        <div class="col-7 px-0">
                            <input type="date" name="end_insurance_no" class="input_evr">
                        </div>
                    </div>


                    <div class="col-3 d-flex align-items-center px-0 mx-3">
                        <div class="col-6 px-0">
                            <span>การแสดงผล</span>
                        </div>

                        <div class="col-6 px-0">
                            <div class="form-check form-switch switch_display">
                                <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                            </div>
                        </div>
                    </div>

                </div>
            </div>




        </div>





    </div>




    <div class="col-12">
        <table class="table mt-3" id="history_car">
            <thead>
                <tr>
                    <th class="text-center"><input type="checkbox" name="all_his_car"></th>
                    <th>วันที่ใช้รถ</th>
                    <th>คนขับ</th>
                    <th>ผู้ขอใช้รถ</th>
                    <th>วัตถุประสงค์</th>
                    <th>สถานที่</th>
                    <th class="text-center">เวลาเริ่มใช้</th>
                    <th class="text-center">เวลาคืนรถ</th>
                    <th class="text-center">เลขไมล์ (ล่าสุด)</th>
                    <th class="text-center">การเติมน้ำมัน</th>
                    <th class="text-center">จำนวนลิตร</th>
                    <th class="text-center">สถานะ</th>
                    <th class="text-center"><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                    <th class="text-center"><img src="/img/icon/description-material.png" alt=""></th>
                    <th class="text-center"><img src="/img/icon/delete-material-copy-25.png" alt=""></th>
                </tr>
            </thead>
            <tbody>
                @for($i=0; $i<7; $i++) <tr>
                    <td><input type="checkbox" name="car_id" class="car_id"></td>
                    <td><span>01/04/2564</span></td>
                    <td><span>นายสมศักดิ์ แต้มใจ</span></td>
                    <td><span>นางสาวดาริสา พัชรโชด</span></td>
                    <td><span>สัมนาวิชาการ</span></td>
                    <td><span>โรงแรมเซนทารา แกรนด์</span></td>
                    <td class="text-center"><span>09 : 00</span></td>
                    <td class="text-center"><span>16 : 00</span></td>
                    <td class="text-center"><span>500</span></td>
                    <td class="text-center"><span>500</span></td>
                    <td class="text-center"><span>18.76</span></td>
                    <td class="text-center"><span class="approve_status">คืนรถแล้ว</span></td>
                    <td class="text-center"><img src="/img/icon/local-printshop-material-copy.png" alt=""></td>
                    <td class="text-center"><img src="/img/icon/description-material.png" alt=""
                            style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#detail_history"></td>
                    <td class="text-center"><img src="/img/icon/delete-material-copy-25.png" alt=""></td>
                    </tr>
                    @endfor
            </tbody>
        </table>









    </div>

</div>



<div class="modal fade" id="detail_history" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 1400px !important;">
        <div class="modal-content" style="background-color: transparent;border: none;">

            <div class="modal-body p-0 detail_request">
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="request_details" data-bs-toggle="pill"
                            data-bs-target="#request_detail" type="button" role="tab" aria-controls="request_detail"
                            aria-selected="true">คำขอใช้รถ</button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="return_details" data-bs-toggle="pill"
                            data-bs-target="#return_detail" type="button" role="tab" aria-controls="return_detail"
                            aria-selected="false">บันทึกคืนรถ</button>
                    </li>
                </ul>


                <div class="tab-content p-3" id="pills-tabContent" style="background-color:#ffffff;">

                    <div class="tab-pane fade show active" id="request_detail" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="col-12 d-flex align-items-center">
                            <div class="col-6 d-flex align-items-center ">
                                <span class="title_pages_modal">คำขอจองใช้รถ</span>
                            </div>
                            <div class="col-6 d-flex align-items-center justify-content-end">
                                <div class="col-3 px-0 text-end">
                                    <span class="approve_status">คืนรถแล้ว</span>
                                </div>
                                <div class="col-1 px-0 text-end">
                                    <img src="/img/icon/local-printshop-material-copy.png" alt="">
                                </div>
                            </div>

                        </div>
                        <div class="my-2" style="border:0.5px dashed #ceced4"></div>

                        <div class="col-12">
                            <div class="col-12 mt-3 pl-0">
                                <span class="title_form">ผู้ขอจองใช้รถ</span>
                            </div>

                            <div class="row mt-2">
                                <div class="col-12 d-flex">
                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-2 px-0">
                                            <span class="content_span_modal">ชื่อ-สกุล</span>
                                        </div>
                                        <div class="col-10 pl-0">
                                            <span class="data_result_modal">นาย PHP Laravel</span>
                                        </div>
                                    </div>

                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-4 px-0">
                                            <span class="content_span_modal">สำนัก / หน่วยงาน / องค์กร</span>
                                        </div>
                                        <div class="col-8 pl-0">
                                            <span class="data_result_modal">สำนักงานบริหารกลาง</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex mt-3">
                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-1 px-0">
                                            <span class="content_span_modal">ตำแหน่ง</span>
                                        </div>
                                        <div class="col-10">
                                            <span class="data_result_modal">Programmer</span>
                                        </div>
                                    </div>

                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-4 px-0">
                                            <span class="content_span_modal">เบอร์โทรศัพท์ (ภายใน)</span>
                                        </div>
                                        <div class="col-8 pl-0">
                                            <span class="data_result_modal">098-289-1087</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex mt-3">
                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-2 px-0">
                                            <span class="content_span_modal">ผู้บังคับบัญชา</span>
                                        </div>
                                        <div class="col-9 pl-0">
                                            <span class="data_result_modal">นายวันชัย นาคทั่ง</span>
                                        </div>
                                    </div>

                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-4 px-0">
                                            <span class="content_span_modal">จำนวนผู้เดินทาง </span>
                                        </div>
                                        <div class="col-8 pl-0">
                                            <span class="data_result_modal">4</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex mt-3">
                                    <div class="col-6 d-flex">
                                        <div class="col-2 px-0">
                                            <span class="content_span_modal">ขออนุญาตใช้</span>
                                        </div>
                                        <div class="col-10 pl-0 d-flex align-items-center">
                                            <div class="col-4">
                                                <input type="checkbox" name="type_car" id="car_center" checked
                                                    style="background-color:#f7f7f7">
                                                <span class="data_result_modal">รถส่วนกลาง</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 d-flex mt-3">
                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-2 px-0">
                                            <span class="content_span_modal">วันที่เริ่มใช้</span>
                                        </div>
                                        <div class="col-5">
                                            <span class="data_result_modal">22 มกราคม 2564</span>
                                        </div>

                                        <div class="col-1">
                                            <span class="content_span_modal">เวลา</span>
                                        </div>
                                        <div class="col-3">
                                            <span class="data_result_modal">09:00</span>
                                        </div>
                                    </div>

                                    <div class="col-6 d-flex align-items-center">
                                        <div class="col-2 px-0">
                                            <span class="content_span_modal">วันที่สิ้นสุด</span>
                                        </div>
                                        <div class="col-5">
                                            <span class="data_result_modal">22 มกราคม 2564</span>
                                        </div>

                                        <div class="col-1">
                                            <span class="content_span_modal">เวลา</span>
                                        </div>
                                        <div class="col-3">
                                            <span class="data_result_modal">17:00</span>
                                        </div>
                                    </div>




                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">

                                    <div class="col-12 mt-3 d-flex">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">วัตถุประสงค์</span>
                                        </div>
                                        <div class="col-9">
                                            <span class="data_result_modal">ไปสัมนาวิชาการ</span>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3 d-flex">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">หลักฐานอื่นๆ</span>
                                        </div>
                                        <div class="col-9">
                                            <span class="data_result_modal">-</span>
                                        </div>
                                    </div>





                                    <div class="col-12 pl-0 mt-5">
                                        <span class="title_form">สถานที่</span>
                                    </div>

                                    <div class="col-12 d-flex align-items-center mt-3">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">ชื่อสถานที่</span>
                                        </div>
                                        <div class="col-9 px-0">
                                            <span class="data_result_modal">โรงแรมเซนทารา</span>
                                        </div>
                                    </div>


                                    <div class="col-12 d-flex align-items-center mt-3">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">จังหวัด</span>
                                        </div>
                                        <div class="col-9 px-0">
                                            <span class="data_result_modal">กรุงเทพมหานคร</span>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex align-items-center mt-3">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">อำเภอ/เขต</span>
                                        </div>
                                        <div class="col-9 px-0">
                                            <span class="data_result_modal">ปทุมวัน</span>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex align-items-center mt-3">
                                        <div class="col-2 px-0 align-items-start">
                                            <span class="content_span_modal">ตำบล/แขวง</span>
                                        </div>
                                        <div class="col-9 px-0">
                                            <span class="data_result_modal">ปทุมวัน</span>
                                        </div>
                                    </div>

                                </div>




                                <div class="col-6 d-flex align-items-end mt-4">
                                    <div class="col-12">
                                        <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                                            style="object-fit: cover; width: 475px; height: 244px;" alt="">

                                    </div>
                                </div>




                                <div class="col-12 text-center" style="margin:150px 0 25px 0">
                                    <button class="close_modal">ปิด</button>
                                </div>
                            </div>





                        </div>
                    </div>



                    <div class="tab-pane fade" id="return_detail" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="col-12 d-flex align-items-center">
                            <div class="col-6 d-flex align-items-center ">
                                <span class="title_pages_modal">บันทึกการคืนรถ</span>
                            </div>
                            <div class="col-6 d-flex align-items-center justify-content-end">
                                <div class="col-3 px-0 text-end">
                                    <span class="approve_status">คืนรถแล้ว</span>
                                </div>
                                <div class="col-1 px-0 text-end">
                                    <img src="/img/icon/local-printshop-material-copy.png" alt="">
                                </div>
                            </div>

                        </div>


                        <div class="my-2" style="border:0.5px dashed #ceced4"></div>


                        <div class="row px-4 py-3">
                            <div class="col-12 d-flex">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">เลขทะเบียน</span>
                                    </div>
                                    <div class="col-10 pl-0">
                                        <span class="data_result_modal">กง 1789</span>
                                    </div>
                                </div>

                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-4 px-0">
                                        <span class="content_span_modal">หมายเลขรถ</span>
                                    </div>
                                    <div class="col-8 pl-0">
                                        <span class="data_result_modal">TYT789456001</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-1 px-0">
                                        <span class="content_span_modal">ยี่ห้อ/รุ่น</span>
                                    </div>
                                    <div class="col-10">
                                        <span class="data_result_modal">Toyota Vios</span>
                                    </div>
                                </div>

                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-4 px-0">
                                        <span class="content_span_modal">ประเภทน้ำมันที่ใช้</span>
                                    </div>
                                    <div class="col-8 pl-0">
                                        <span class="data_result_modal">แก๊ซโซฮอล 95</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">หมายเลขเครื่อง</span>
                                    </div>
                                    <div class="col-9 pl-0">
                                        <span class="data_result_modal">12345678912345678</span>
                                    </div>
                                </div>

                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-4 px-0">
                                        <span class="content_span_modal">หมายเลขตัวถัง</span>
                                    </div>
                                    <div class="col-8 pl-0">
                                        <span class="data_result_modal">13456789211</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">ประเภทรถ</span>
                                    </div>
                                    <div class="col-10 pl-0 d-flex align-items-center">
                                        <span class="data_result_modal">รถซีดาน</span>
                                    </div>
                                </div>
                                <div class="col-6 d-flex">
                                    <div class="col-4 px-0">
                                        <span class="content_span_modal">จำนวนที่นั่ง(ไม่รวมคนขับ)</span>
                                    </div>
                                    <div class="col-8 pl-0 d-flex align-items-center">
                                        <span class="data_result_modal">4</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">วันที่ใช้</span>
                                    </div>
                                    <div class="col-5">
                                        <span class="data_result_modal">01 เมษายน 2564</span>
                                    </div>

                                    <div class="col-2">
                                        <span class="content_span_modal">วันที่ส่งคืน</span>
                                    </div>
                                    <div class="col-3">
                                        <span class="data_result_modal">01 เมษายน 2564</span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">เวลาเริ่ม</span>
                                    </div>
                                    <div class="col-5">
                                        <span class="data_result_modal">09:00</span>
                                    </div>

                                    <div class="col-2">
                                        <span class="content_span_modal">เวลาส่งคืน</span>
                                    </div>
                                    <div class="col-3">
                                        <span class="data_result_modal">16:00</span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">เลขไมล์ล่าสุด</span>
                                    </div>
                                    <div class="col-9 pl-0">
                                        <span class="data_result_modal">500</span>
                                    </div>
                                </div>

                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-1 px-0">
                                        <span class="content_span_modal">ผู้ขับรถ</span>
                                    </div>
                                    <div class="col-11 pl-0">
                                        <span class="data_result_modal">นายสมศักดิ์ แต้มใจ</span>
                                    </div>
                                </div>
                            </div>




                            <div class="col-12 d-flex mt-3">
                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">จำนวนน้ำมันก่อนใช้</span>
                                    </div>
                                    <div class="col-5">
                                        <span class="data_result_modal">500</span>
                                    </div>
                                    <div class="col-2">
                                        <span class="content_span_modal">การเติมน้ำมัน</span>
                                    </div>
                                    <div class="col-3 px-0 d-flex align-items-center">
                                        <div class="col-6 d-flex align-items-center px-0">
                                            <input type="checkbox" checked class="mr-2">
                                            <span class="content_span_modal">เติม</span>
                                        </div>

                                        <div class="col-6 d-flex align-items-center px-0">
                                            <input type="checkbox" checked class="mr-2">
                                            <span class="content_span_modal">ไม่ได้เติม</span>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-6 d-flex align-items-center">
                                    <div class="col-2 px-0">
                                        <span class="content_span_modal">จำนวนลิตร</span>
                                    </div>
                                    <div class="col-1 px-0">
                                        <span class="data_result_modal">18.76</span>
                                    </div>
                                    <div class="col-2 px-0 text-center">
                                        <span class="content_span_modal">ลิตร</span>
                                    </div>

                                    <div class="col-1 px-0">
                                        <span class="content_span_modal">เติม</span>
                                    </div>
                                    <div class="col-2 px-0">
                                        <span class="data_result_modal">500</span>
                                    </div>
                                    <div class="col-4">
                                        <span class="content_span_modal">บาท</span>
                                    </div>


                                </div>



                            </div>



                            <div class="col-12 mt-3">
                                <div class="col-12">
                                    <span class="content_span_modal">รายละเอียดเพิ่มเติม</span>
                                </div>
                                <div class="col-12">
                                    <span class="content_span_modal d-none">รายละเอียดเพิ่มเติม</span>
                                </div>
                            </div>



                            <div class="col-12 text-center" style="margin:150px 0 25px 0">
                                <button class="close_modal">ปิด</button>
                            </div>
                        </div>



                    </div>










                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection





@section('script')
<script>
    $(document).ready(function () {

    });

</script>
@endsection
