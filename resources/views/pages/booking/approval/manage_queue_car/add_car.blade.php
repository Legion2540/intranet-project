@extends('layout.booking')
@section('style')
<style>
    .title_form_pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;

    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .content_span_upload_file {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        line-height: 1.13;
        color: #4f72e5;
    }

    .form-select {
        height: 30px;
        line-height: 1 !important;
    }

    .detail_option_form {
        border-radius: 8px;
    }

    .area_input {
        width: 165px;
        height: 165px;
        padding: 35px 10px 35px 10px;
        border: dashed 2px #4f72e5;
        background-color: #ffffff;
    }

    .upload_img_car {
        width: 110px;
        height: 25px;
        border: none;
        margin-left: 10px;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 85%, #314d7b 132%);
    }

    .validate_data {
        width: 120px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

    .submit_addCar {
        display: none;
        width: 120px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

</style>




@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <span class="title_form_pages">รายละเอียดรถ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <form action="#" method="post">
        <div class="row">
            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">เลขทะเบียน</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="plate_car" id="plate_car" value="กง 1789">
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">หมายเลขรถ</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="car_no" id="car_no" value="TYT789456001">
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ยี่ห้อ/รุ่น</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="brand" id="brand" value="Toyota Vios">
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">ประเภทน้ำมัน</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_gas" id="type_gas">
                            <option>เลือกประเภทเชื้อเพลิง</option>
                            <option value="gas95">แก๊ซโซฮอล์ 95</option>
                            <option value="gas91">แก๊ซโซฮอล์ 91</option>
                            <option value="e20">แก๊ซโซฮอล์ E20</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">หมายเลขเครื่อง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="engine_no" id="engine_no" value="123456789123456">
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">หมายเลขตัวถัง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="body_no" id="body_no" value="134567893211">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ประเภทรถ</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_car" id="type_car">
                            <option>เลือกประเภทรถ</option>
                            <option value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-8 px-0">
                            <span class="content_span">จำนวนที่นั่ง (ไม่รวมคนชับรถ)</span>
                        </div>
                        <div class="col-5">
                            <input class="input_evr" type="text" name="member_seat" id="member_seat" value="4">
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2">
                            <span class="content_span">สี</span>
                        </div>
                        <div class="col-6">
                            <input class="input_evr" type="text" name="color_id" id="color_id" value="ขาว">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-3 d-flex align-items-center">
                    <div class="col-5">
                        <span class="content_span">วันที่จดทะเบียน</span>
                    </div>
                    <div class="col-8">
                        <input class="input_evr" type="date" name="date_start_licencs" id="date_start_licencs"
                            value="22 มกราคม 2564">
                    </div>
                </div>
                <div class="col-3 d-flex align-items-center">
                    <div class="col-5">
                        <span class="content_span">ทะเบียนหมดอายุ</span>
                    </div>
                    <div class="col-8">
                        <input class="input_evr" type="text" name="date_end_licens" id="date_end_licens"
                            value="22 มกราคม 2565">
                    </div>
                </div>
                <div class="col-3 d-flex align-items-center">
                    <img src="/img/icon/error-outline-material.png" alt="">
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-8 d-flex align-items-start">
                    <div class="col-2">
                        <span class="content_span">รายละเอียดเพิ่มเติม</span>
                    </div>
                    <div class="col-10">
                        <textarea name="detail_option_form" id="detail_option_form" cols="70" rows="3"
                            style="resize: none;  border: solid 0.5px #ceced4; border-radius:8px;"></textarea>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-8 d-flex align-items-start">
                    <div class="col-2">
                        <span class="content_span">เลขไมล์เริ่มต้น</span>
                    </div>
                    <div class="col-8">
                        <input type="text" class="input_evr" name="start_km_meter" id="start_km_meter">
                    </div>
                    <div class="col-2">
                        <img src="/img/icon/error-outline-material.png" alt="">
                    </div>
                </div>

                <div class="col-4 d-flex align-items-end">
                    <div class="area_input vertical-center text-center">
                        <img src="" alt="" id="car_img"
                            style="display: none;width:145px; height:145px; object-fit:contain;">

                        <div class="col-12 upload_mockup">
                            <img src="/img/icon/insert-photo-material.png" alt="">
                        </div>
                        <div class="col-12 mt-2 upload_mockup">
                            <span class="content_span_upload_file">Upload Image</span>
                        </div>
                    </div>

                    <button type="button" class="upload_img_car">อัพโหลดไฟล์</button>
                    <input type="file" name="img_car" id="img_car" style="display:none;">
                </div>
            </div>

            <div class="col-12 d-flex justify-content-center" style="margin-top: 80px;">
                <button type="button" class="validate_data">ตรวจสอบข้อมูล</button>
                <button type="button" class="submit_addCar">บันทึกข้อมูล</button>
            </div>
        </div>
    </form>


</div>
@endsection




@section('script')
<script>
    $(document).ready(function () {
        $('.validate_data').on('click', function () {

            $('input').css('background-color', '#f3f3f3');
            $('input').attr('readonly', true);

            $('textarea').css('background-color', '#f3f3f3');
            $('textarea').attr('readonly', true);

            $('select').css('background-color', '#f3f3f3');
            $('select').attr('disabled', true);
            $(this).remove();
            $('.submit_addCar').css('display', 'block');
        });

        $('.submit_addCar').on('click', function () {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                console.log("Need to add ajax");
                window.location.href = "/manage_queueCar";
            });
        });





        $('.upload_img_car').on('click', function () {
            $('input[type="file"]').click();

        });
        $('input[type="file"]').change(function () {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.area_input').css({
                        'border': 'solid 1px #4f72e5',
                        'padding': '10px'
                    });
                    $('.upload_mockup').css('display', 'none');
                    $('#car_img').css('display', 'block');
                    $('#car_img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                alert('select a file to see preview');
                $('#car_img').attr('src', '');
            }
        }

    });

</script>
@endsection
