@extends('layout.booking')
@section('style')
<style>
    .waiting_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ffbf00;
    }

    .cancel_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #ee2a27;
    }

    .approve_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #6dd005;
    }

    .fix_request {
        width: 253px;
        height: 111px;
        margin: 0 34px 0 0;
        border-radius: 2px;
        text-align: center;
        align-content: center;
        box-shadow: 0 2px 13px 6px rgba(0, 0, 0, 0.07);
        background-color: #cfd2d4;
    }

    .count_num {
        font-size: 50px;
        font-weight: bold;
        text-align: center;
        color: #ffffff;
    }

    .container {
        margin-right: auto !important;
        margin-left: 270px !important;
        max-width: 1400px !important;
    }

    .text_request {
        font-size: 18px;
        font-weight: bold;
        color: #ffffff;
        margin-left: 14px;
    }




    .manage_car .nav-pills .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .manage_car .nav-item {
        margin: 0 0 0 10px;
    }

    .manage_car .nav-pills .nav-link {
        font-size: 18px;
        background-color: #ffffff !important;
        color: #4f72e5 !important;
        border-radius: 0px;
        border-radius: 4px 4px 0px 0px;
    }



    .event_car .nav-tabs .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .event_car .nav-item {
        margin: 0 0 0 10px;
    }

    .event_car .nav-tabs .nav-link {
        font-size: 18px;
        background-color: #4f72e5 !important;
        color: #ffffff !important;
        border-radius: 0px;
        margin-left: 10px;
        border-radius: 4px 4px 0px 0px;
        padding-left: 65px !important;
        padding-right: 65px !important;
    }



    .car_driver .nav-pills .nav-link.active {
        background-color: #4f72e5 !important;
        color: #ffffff !important;
    }

    .car_driver .nav-item {
        margin: 0 0 0 10px;
    }

    .car_driver .nav-pills .nav-link {
        font-size: 18px;
        font-weight: bold;
        background-color: #ffffff !important;
        color: #4f72e5 !important;
        border-radius: 0px;
        border-radius: 4px 4px 0px 0px;
        padding-left: 95px;
        padding-right: 95px;
    }






    #request_car {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #return_car {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #history_car {
        width: 300px;
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }



    #room_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #drink_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #lunch_meal_list {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    .change_booking_btn {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    #approved_all {
        font-size: 20px;
        font-weight: 600;
        color: #4f72e5;
    }

    .filter_all {
        width: 133px;
        height: 35px;
        margin: 0 20px 0 15px;
        padding: 3px 4px 3px 10px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .form-select-1x {
        line-height: normal !important;
    }

    .search_request_car {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_datial_car {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_return_car {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .search_history_car {
        width: 100%;
        height: 30px;
        padding: 5px 4px 5px 11.5px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .waiting_status {
        width: 98px;
        height: 33px;
        padding: 0 36px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ffbf00;
        border-radius: 8px
    }

    .approve_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #6dd005;
        border-radius: 8px
    }

    .cancel_status {
        width: 98px;
        height: 33px;
        padding: 0 16px;
        object-fit: contain;
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.07;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;
        background-color: #ee2a27;
        border-radius: 8px
    }

    .thead_car {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        line-height: 0.94;
        color: #4a4a4a;
    }

    .export_file {
        width: 117px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }


    .add_car,
    .add_car:hover {
        width: 260px;
        height: 30px;
        color: #ffffff;
        border: none;
        font-size: 15px;
        text-decoration: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }

    .add_driver,
    .add_driver:hover {
        width: 260px;
        height: 30px;
        color: #ffffff;
        border: none;
        font-size: 15px;
        text-decoration: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(195deg, #4f72e5 55%, #314d7b 128%);
    }

    /* Arrow for change month */
    .arrow-booking {
        border: solid black;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
        height: 1px !important;
        width: 1px !important;
    }

    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }

    .filter_month {
        width: 103px;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .filter_province {
        width: 103px;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .filter_agency {
        width: 100%;
        height: 30px;
        margin: 5.5px 9.5px 5.5px 3.5px;
        padding: 2px 3px 2px 8px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
    }

    .toggle-switch:checked {
        background-color: #4BD763;
        border-color: #4BD763;
        border: none;
    }

    .toggle-switch {
        background-color: #ceced4;
        border-color: #ceced4;
        border: none;

    }

    #request_car_table td {
        vertical-align: middle;
        font-size: 14px;
    }

    #return_car_table td {
        vertical-align: middle;
        font-size: 14px;
    }

    #request_car_history td {
        vertical-align: middle;
        font-size: 14px;
    }

</style>
@endsection

@section('content')
<div class="my-4">
    <div class="row d-flex align-items-center mb-3">
        <div class="waiting_request">
            <span class="count_num">10</span>

            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/white-access-time-material.png" alt="">
                <span class="text_request">รออนุมัติ</span>
            </div>
        </div>

        <div class="cancel_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/times-circle-outlined-font-awesome.png" alt="">
                <span class="text_request">ไม่อนุมัติ</span>
            </div>
        </div>

        <div class="approve_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/ion-android-checkbox-outline-ionicons.png" alt="">
                <span class="text_request">อนุมัติ</span>
            </div>
        </div>

        <div class="fix_request">
            <span class="count_num">10</span>
            <div class="col-12 d-flex align-items-center justify-content-center">
                <img src="/img/icon/ion-android-checkbox-outline-ionicons.png" alt="">
                <span class="text_request">ซ่อมบำรุง</span>
            </div>
        </div>
    </div>





    <div class="row manage_car" style="background-color: transparent;">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="request_car" data-bs-toggle="pill" data-bs-target="#request_cars"
                    type="button" role="tab" aria-controls="room" aria-selected="true">คำขอใช้รถ</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="return_car" data-bs-toggle="pill" data-bs-target="#return_cars"
                    type="button" role="tab" aria-controls="food" aria-selected="false">บันทึกคืนรถ</button>
            </li>

            <li class="nav-item" role="presentation">
                <button class="nav-link" id="history_car" data-bs-toggle="pill" data-bs-target="#history_cars"
                    type="button" role="tab" aria-controls="food" aria-selected="false">ประวัติการใช้รถ</button>
            </li>
        </ul>

        <div class="tab-content pt-3" id="pills-tabContent" style="background-color:#ffffff">
            <div class="tab-pane fade show active" id="request_cars" role="tabpanel" aria-labelledby="request_car">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-family:Kanit-Regular; font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="font-family:Kanit-Regular;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_return_car" id="search_return_car" class="search_return_car"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:74%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <button type="button" class="export_file vertical-center"><img
                                    src="/img/icon/file_upload-material.png" class="mr-2" alt="">Export</button>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_car_table">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_req_car"></th>
                            <th class="thead_car text-start">ผู้ขอใช้</th>
                            <th class="thead_car text-start">วัตถุประสงค์</th>
                            <th class="thead_car text-start">สถานที่</th>
                            <th class="thead_car">จำนวนคน</th>
                            <th class="thead_car text-start">วันที่ใช้</th>
                            <th class="thead_car text-start">สำนัก</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center vertical-center">
                            <td><input type="checkbox" name="list_request_car" class="list_request_car"></td>
                            <td class="text-start"><a href="/manage_queueCar/approve_1">นางสาวดาริสา พัชรโชด</a></td>
                            <td class="text-start"><span>ไปสัมนาวิชาการ</span></td>
                            <td class="text-start"><span>โรงแรมเซนทารา แกรนด์</span></td>
                            <td><span>4</span></td>
                            <td class="text-start" style="line-height: 1.4">
                                <span>20 ธันวาคม 2563</span><br>
                                <span>เวลา 10.00 - 13.00 น.</span>
                            </td>
                            <td class="text-start"><span>ฝ่ายบุคคล</span></td>​
                            <td><a href="/manage_queueCar/sendForm"><img
                                        src="/img/icon/local-printshop-material-copy.png" alt=""></a></td>
                            <td><span class="waiting_status">รออนุมัติ</span></td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div class="tab-pane fade" id="return_cars" role="tabpanel" aria-labelledby="return_car">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-family:Kanit-Regular; font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="font-family:Kanit-Regular;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_return_car" id="search_return_car" class="search_return_car"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:74%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <button type="button" class="export_file vertical-center"><img
                                    src="/img/icon/file_upload-material.png" class="mr-2" alt="">Export</button>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="return_car_table">
                    <thead>
                        <tr class="text-start">
                            <th><input type="checkbox" name="key_return_car"></th>
                            <th class="thead_car">รถยนต์</th>
                            <th class="thead_car">คนขับ</th>
                            <th class="thead_car">ผู้ขอใช้</th>
                            <th class="thead_car">วัตถุประสงค์</th>
                            <th class="thead_car">สถานที่</th>
                            <th class="thead_car text-center">เวลาคืน</th>
                            <th class="thead_car">สำนัก</th>
                            <th class="thead_car">อาหาร/ของวา่ง</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-start vertical-center">
                            <td><input type="checkbox" name="list" class="list"></td>
                            <td class="text-start"><span>กง 2789 Toyota Vios</span></td>
                            <td><span>นายสมศักดิ์ แต้มใจ</span></td>
                            <td><a href="/manage_queueCar/return"><span>นางสาวดาริสา พัชรโชด</span></a></td>
                            <td><span>ไปสัมนาวิชาการ</span></td>
                            <td>โรงแรมเซนทารา แกรนด์</td>
                            <td style="line-height: 1.4">
                                <span>02 เมษายน 2564</span><br>
                                <span>เวลา 10.000 น.</span>
                            </td>​

                            <td class="text-center"><span>-</span></td>

                            <td><span>ฝ่ายบุคคล</span></td>
                            <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>
                            <td><span class="waiting_status">รออนุมัติ</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="history_cars" role="tabpanel" aria-labelledby="history_car">
                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6 d-flex align-items-center">
                            <span style="font-family:Kanit-Regular; font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;"><b>ประจำเดือน</b></span>

                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>

                            <span style="font-family:Kanit-Regular;
                            font-size: 18px;
                            font-weight: bold;
                            color: #4a4a4a;">ปี</span>
                            <select class="form-select form-select-1x filter_all" aria-label="Default select example">
                                <option selected>ทั้งหมด</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>


                        </div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_history_car" id="search_history_car"
                                class="search_history_car" placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:74%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <button type="button" class="export_file vertical-center"><img
                                    src="/img/icon/file_upload-material.png" class="mr-2" alt="">Export</button>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_car_history">
                    <thead>
                        <tr class="text-center">
                            <th><input type="checkbox" name="key_his_car"></th>
                            <th class="thead_car text-start">ผู้ขอใช้</th>
                            <th class="thead_car text-start">วัตถุประสงค์</th>
                            <th class="thead_car text-start">สถานที่</th>
                            <th class="thead_car">จำนวนคน</th>
                            <th class="thead_car text-start">วันที่ใช้</th>
                            <th class="thead_car text-start">สำนัก</th>
                            <th class="thead_car"><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center vertical-center">
                            <td><input type="checkbox" name="list_his_car" class="list_his_car"></td>
                            <td class="text-start"><a href="#">นางสาวดาริสา พัชรโชด</a></td>
                            <td class="text-start"><span>ไปสัมนาวิชาการ</span></td>
                            <td class="text-start"><span>โรงแรมเซนทารา แกรนด์</span></td>
                            <td><span>4</span></td>
                            <td class="text-start" style="line-height: 1.4">
                                <span>20 ธันวาคม 2563</span><br>
                                <span>เวลา 10.00 - 13.00 น.</span>
                            </td>
                            <td class="text-start"><span>ฝ่ายบุคคล</span></td>​
                            <td><img src="/img/icon/local-printshop-material-copy-5.png" alt=""></td>
                            <td><span class="approve_status">จัดการแล้ว</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>




    <div class="mt-4 event_car" style="background-color:transparent;">
        <div class="row">
            <nav class="p-0">
                <div class="nav nav-tabs border-0" id="nav-tab" role="tablist">
                    <button class="nav-link change_booking_btn" id="rooms" data-bs-toggle="tab"
                        data-bs-target="#calendar_room" typ e="button" role="tab" aria-controls="calendar_room"
                        aria-selected="false">ปฎิทินการใช้รถ</button>
                </div>
            </nav>

            <div class="tab-content p-2" id="nav-tabContent" style="background-color:#ffffff;">
                <div class="tab-pane fade show active m-0 show_calendar" id="calendar_room" role="tabpanel"
                    aria-labelledby="calendar_room">
                    <div class="col-12 d-flex align-content-center p-0">
                        <div class="col-4 d-flex align-content-center pr-0">
                            <div class="d-flex align-items-center">
                                <span class="arrow-booking left"></span>
                            </div>
                            <div class="px-2 w-auto ">
                                <span
                                    style="font-family: Kanit-Regular; font-size: 25px; font-weight: bold; color: #4a4a4a;">December
                                    2021</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span class="arrow-booking right"></span>
                            </div>

                            <img class="ml-3" src="/img/icon/date-range-material-copy-4.png"
                                style="object-fit:contain; width:20px" alt="">
                        </div>
                        <div class="col-8 d-flex align-content-center pl-0 justify-content-end pr-0">

                            <div class="d-flex align-items-center">
                                <input type="search" class="p-2 mr-1" name="calendar_room"
                                    style="width: 320px; height: 30px; border-radius: 8px; border: solid 0.5px #ceced4;"
                                    placeholder="ค้นหารายการ">
                                <img src="/img/icon/search-material-bule.png"
                                    style="position: absolute; left: 67%; top: 13px;" alt="">
                            </div>
                            <div class="d-flex align-items-center">
                                {{-- Filter Calendar img --}}
                                <span class="filter_calendar_room" data-bs-placement="bottom" data-bs-toggle="popover"
                                    data-bs-title="Filters">
                                    <img src="/img/icon/tune-material-copy-3.png" class="mx-2" alt="">
                                </span>

                                <select class="form-select form-select-1x filter_month" aria-label="Month">
                                    <option selected>Month</option>
                                    <option value="day">Day</option>
                                    <option value="week">Week</option>
                                    <option value="month">Month</option>
                                </select>

                                <select class="form-select form-select-1x filter_province" aria-label="เมืองทอง">
                                    <option selected>เมืองทอง</option>
                                    <option value="1">รัชดา</option>
                                    <option value="2">ทองหล่อ</option>
                                    <option value="3">สีลม</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div id="event_car" class="mt-4"></div>
                </div>
            </div>
        </div>
    </div>



    <div class="row car_driver mt-5" style="background-color: transparent;">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="detail_cars" data-bs-toggle="pill" data-bs-target="#detail_car"
                    type="button" role="tab" aria-controls="detail_car" aria-selected="true">ข้อมูลรถยนต์</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="detail_drivers" data-bs-toggle="pill"
                    data-bs-target="#detail_driver" type="button" role="tab" aria-controls="detail_driver"
                    aria-selected="false">ข้อมูลคนขับ</button>
            </li>


        </ul>
        <div class="tab-content pt-3" id="pills-tabContent" style="background-color:#ffffff;">

            <div class="tab-pane fade show active" id="detail_car" role="tabpanel" aria-labelledby="detail_cars">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_datial_car" id="search_datial_car" class="search_datial_car"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/manage_queueCar/add/car" type="button" class="add_car text-center pt-1">+
                                ลงทะเบียนรถใหม่</a>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_car_history">
                    <thead>
                        <tr class="text-start">
                            <th class="text-center"><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-center"></th>
                            <th class="thead_car">ทะเบียนรถ</th>
                            <th class="thead_car">ยี่ห้อ / รถ</th>
                            <th class="thead_car">น้ำมัน / เชื้อเพลิง</th>
                            <th class="thead_car">เลขไมล์ (ล่าสุด)</th>
                            <th class="thead_car">ใช้งานล่าสุด</th>
                            <th class="thead_car text-center"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car text-center"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car text-center"><img src="/img/icon/border-color-material-copy.png"
                                    alt=""></th>
                            <th class="thead_car text-center">สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><input type="checkbox" name="car_checkbox" class="car_checkbox"></td>
                            <td><img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                                    style="width:120px; height:80px; object-fit:containl;" alt=""></td>
                            <td class="text-start"><span>2กง 2415</span></td>
                            <td class="text-start"><span>Toyota Vios</span></td>
                            <td class="text-start"><span>เบนซิล 95</span></td>
                            <td class="text-start"><span>50,000</span></td>
                            <td class="text-start"><span>22 / 03 / 2021</span></td>

                            <td><a href="/manage_queueCar/requestFix"><img src="/img/icon/description-material.png"
                                        alt=""></a></td>
                            <td><a href="/manage_queueCar/historycar"><img src="/img/icon/description-material.png" alt=""></a></td>
                            <td><a href="/manage_queueCar/edit/car"><img src="/img/icon/border-color-material-copy.png"
                                        alt=""></a></td>
                            <td><span class="waiting_status w-100">ว่าง</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="detail_driver" role="tabpanel" aria-labelledby="detail_drivers">

                <div class="row d-flex">
                    <div class="col-12 d-flex">
                        <div class="col-6"></div>
                        <div class="col-6 d-flex justify-content-end align-items-center">
                            <input type="text" name="search_datial_car" id="search_datial_car" class="search_datial_car"
                                placeholder="ค้นหารายการ">
                            <img src="/img/icon/search-material-bule.png" style="position: absolute; left:63%" alt="">
                            <img src="/img/icon/tune-material-copy-3.png"
                                style="width: 19px; height: 15px; object-fit: contain;   margin: 0 14px 0 12px;" alt="">
                            <a href="/manage_queueCar/add/driver" type="button" class="add_driver text-center pt-1">+
                                ลงทะเบียนคนชับรถใหม่</a>
                        </div>
                    </div>
                </div>
                <table class="table mt-3" id="request_car_history">
                    <thead>
                        <tr class="text-start">
                            <th class="text-center"><input type="checkbox" name="key_thead"></th>
                            <th class="thead_car text-center"></th>
                            <th class="thead_car">ชื่อ-สกุล</th>
                            <th class="thead_car">อายุ</th>
                            <th class="thead_car">เบอร์โทรศัพท์</th>
                            <th class="thead_car">ออกรอบล่าสุด</th>

                            <th style="width:250px;"></th>
                            <th class="thead_car text-center">สถานะ</th>
                            <th class="thead_car text-center"></th>
                            <th class="thead_car text-center"></th>
                            {{-- <th class="thead_car text-center"><img src="/img/icon/description-material.png" alt=""></th>
                            <th class="thead_car text-center"><img src="/img/icon/border-color-material-copy.png" alt=""></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><input type="checkbox" name="car_checkbox" class="car_checkbox"></td>
                            <td><img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                                    style="width:62px; height:60px; object-fit:containl; border-radius: 50%" alt="">
                            </td>
                            <td class="text-start"><span>จอร์แดน วิลสัน</span></td>
                            <td class="text-start"><span>28 ปี</span></td>
                            <td class="text-start"><span>087-456-7894</span></td>
                            <td class="text-start"><span>21 / 03 / 2021</span></td>
                            <td></td>

                            <td><span class="waiting_status w-100">ว่าง</span></td>
                            <td><a href="#"><img src="/img/icon/description-material.png" alt=""></a></td>
                            <td><a href="/manage_queueCar/edit/driver"><img
                                        src="/img/icon/border-color-material-copy.png" alt=""></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </div>










</div>
@endsection

@section('script')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var event_car = $('#event_car')[0];

        var calendar_food = new FullCalendar.Calendar(event_car, {
            initialView: 'dayGridMonth'
        });
        calendar_food.render();
    });

</script>
@endsection
