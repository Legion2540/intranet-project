@extends('layout.booking')
@section('style')
<style>
    .title_form_pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 1 6px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
    }

    .input_evr:read-only {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #f3f3f3;
        border: solid 0.5px #ceced4;
    }

    .form-select {
        height: 30px;
        line-height: 1 !important;
        border-radius: 8px;
    }

    .form-select:disabled {
        height: 30px;
        line-height: 1 !important;
        border-radius: 8px;
        background-color: #f3f3f3;
    }





    .amount {
        width: 90px;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
        text-align: end;
    }

    .amount_piece {
        width: 90px;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
        text-align: end;

    }

    .amount_piece_span {
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .add_fix_list {
        width: 135px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .submit {
        width: 112px;
        height: 30px;
        color:#ffffff;
        border-radius: 8px;
        font-size:14px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

    .send_data {
        width: 112px;
        height: 30px;
        color:#ffffff;
        border-radius: 8px;
        border: none;
        font-size:14px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

</style>
@endsection


@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <span class="title_form_pages">บันทึกคืนรถ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <form action="#" method="post">
        <div class="row">


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">เลขทะเบียน</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="plate_car" id="plate_car" value="กง 1789" readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">หมายเลขรถ</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="car_no" id="car_no" value="TYT789456001" readonly>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ยี่ห้อ/รุ่น</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="brand" id="brand" value="Toyota Vios" readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">ประเภทน้ำมัน</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_gas" id="type_gas" disabled>
                            <option>เลือกประเภทเชื้อเพลิง</option>
                            <option selected value="gas95">แก๊ซโซฮอล์ 95</option>
                            <option value="gas91">แก๊ซโซฮอล์ 91</option>
                            <option value="e20">แก๊ซโซฮอล์ E20</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">หมายเลขเครื่อง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="engine_no" id="engine_no" value="123456789123456"
                            readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">หมายเลขตัวถัง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="body_no" id="body_no" value="134567893211" readonly>
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ประเภทรถ</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_car" id="type_car" disabled>
                            <option>เลือกประเภทรถ</option>
                            <option selected value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-8 px-0">
                            <span class="content_span">จำนวนที่นั่ง (ไม่รวมคนชับรถ)</span>
                        </div>
                        <div class="col-5">
                            <input class="input_evr" type="text" name="member_seat" id="member_seat" value="4" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2">
                            <span class="content_span">สี</span>
                        </div>
                        <div class="col-6">
                            <input class="input_evr" type="text" name="color_id" id="color_id" value="ขาว" readonly>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">

                    <div class="col-6 d-flex align-items-center px-0">
                        <div class="col-4">
                            <span class="content_span">วันที่ใช้</span>
                        </div>

                        <div class="col-8">
                            <input type="date" class="input_evr" name="date_start_use" id="date_start_use">
                        </div>
                    </div>

                    <div class="col-6 d-flex align-items-center">
                        <div class="col-4">
                            <span class="content_span">วันที่ส่งคืน</span>
                        </div>

                        <div class="col-8">
                            <input type="date" class="input_evr" name="date_end_use" id="date_end_use">
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">

                    <div class="col-6 d-flex align-items-center px-0">
                        <div class="col-4">
                            <span class="content_span">เวลาเริ่มต้น</span>
                        </div>

                        <div class="col-8">
                            <input type="time" class="input_evr" name="time_start_use" id="time_start_use">
                        </div>
                    </div>

                    <div class="col-6 d-flex align-items-center">
                        <div class="col-4">
                            <span class="content_span">เวลาส่งคืน</span>
                        </div>

                        <div class="col-8">
                            <input type="time" class="input_evr" name="time_end_use" id="time_end_use">
                        </div>
                    </div>

                </div>
            </div>





            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">เลขไมล์ส่งซ่อม</span>
                    </div>
                    <div class="col-10">
                        <input type="text" class="input_evr" name="mile_start_fix" id="mile_start_fix">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ผู้ขับรถ</span>
                    </div>
                    <div class="col-10">
                        <input type="text" class="input_evr" name="man_send_id" id="man_send_id">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-7 d-flex align-items-start">
                    <div class="col-3 pr-0">
                        <span class="content_span">รายละเอียดเพิ่มเติม</span>
                    </div>
                    <div class="col-9 px-0">
                        <textarea name="problem" id="problem" cols="80" rows="2"
                            style="resize:none; border: solid 0.5px #ceced4; border-radius: 8px;"
                            style="resize: none; border-radius:8px;"></textarea>
                    </div>
                </div>
            </div>
        </div>





        <div class="col-12 text-center" style="margin: 150px 0 20px;">
            {{-- <button class="submit" type="submit">ตรวจสอบข้อมูล</button> --}}
            <button class="submit" type="button">ตรวจสอบข้อมูล</button>
            <button class="send_data" type="button" style="display: none;">บันทึกข้อมูล</button>

        </div>


    </form>


</div>

@endsection






@section('script')
<script>
    $(document).ready(function () {


        $('.submit').on('click',function(){
            $('input').css('background-color','#f3f3f3');
            $('date').css('background-color','#f3f3f3');
            $('time').css('background-color','#f3f3f3');
            $('textarea').css('background-color','#f3f3f3');

            $(this).remove();
            $('.send_data').toggle();


        });

        $('.send_data').on('click',function(){
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'ส่งคำขอถูกยกเลิกแล้ว',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = "/manage_queueCar";
            });
        });



    });





</script>
@endsection
