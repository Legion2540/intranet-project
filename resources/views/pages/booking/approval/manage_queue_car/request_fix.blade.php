@extends('layout.booking')
@section('style')
<style>
    .title_form_pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;
    }

    .title_form {
        font-family: Kanit-Regular;
        font-size: 1 6px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
    }

    .input_evr:read-only {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #f3f3f3;
        border: solid 0.5px #ceced4;
    }

    .form-select {
        height: 30px;
        line-height: 1 !important;
        border-radius: 8px;
    }

    .form-select:disabled {
        height: 30px;
        line-height: 1 !important;
        border-radius: 8px;
        background-color: #f3f3f3;
    }





    .amount {
        width: 90px;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
        text-align: end;
    }

    .amount_piece {
        width: 90px;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
        text-align: end;

    }

    .amount_piece_span {
        font-family: Kanit-Regular;
        color: #4a4a4a;
    }

    .add_fix_list {
        width: 135px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .submit {
        width: 112px;
        height: 30px;
        color:#ffffff;
        border-radius: 8px;
        font-size:14px;
        border: none;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

    .send_data {
        width: 112px;
        height: 30px;
        color:#ffffff;
        border-radius: 8px;
        border: none;
        font-size:14px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

</style>
@endsection


@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <span class="title_form_pages">รายละเอียดรถ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <form action="#" method="post">
        <div class="row">


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">เลขทะเบียน</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="plate_car" id="plate_car" value="กง 1789" readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">หมายเลขรถ</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="car_no" id="car_no" value="TYT789456001" readonly>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ยี่ห้อ/รุ่น</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="brand" id="brand" value="Toyota Vios" readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">ประเภทน้ำมัน</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_gas" id="type_gas" disabled>
                            <option>เลือกประเภทเชื้อเพลิง</option>
                            <option selected value="gas95">แก๊ซโซฮอล์ 95</option>
                            <option value="gas91">แก๊ซโซฮอล์ 91</option>
                            <option value="e20">แก๊ซโซฮอล์ E20</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">หมายเลขเครื่อง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="engine_no" id="engine_no" value="123456789123456"
                            readonly>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">หมายเลขตัวถัง</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="body_no" id="body_no" value="134567893211" readonly>
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ประเภทรถ</span>
                    </div>
                    <div class="col-10">
                        <select class="form-select" name="type_car" id="type_car" disabled>
                            <option>เลือกประเภทรถ</option>
                            <option selected value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                            <option value="seadan">รถซีดาน</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 d-flex align-items-center">
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-8 px-0">
                            <span class="content_span">จำนวนที่นั่ง (ไม่รวมคนชับรถ)</span>
                        </div>
                        <div class="col-5">
                            <input class="input_evr" type="text" name="member_seat" id="member_seat" value="4" readonly>
                        </div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="col-2">
                            <span class="content_span">สี</span>
                        </div>
                        <div class="col-6">
                            <input class="input_evr" type="text" name="color_id" id="color_id" value="ขาว" readonly>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-4 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">วันที่ส่งซ่อม</span>
                    </div>

                    <div class="col-8">
                        <input type="date" class="input_evr" name="start_date_send_fix" id="start_date_send_fix">
                    </div>
                </div>

                <div class="col-4 d-flex align-items-center">
                    <div class="col-3">
                        <span class="content_span">คาดว่าเสร็จ</span>
                    </div>

                    <div class="col-8">
                        <input type="date" class="input_evr" name="random_date_send_fix" id="random_date_send_fix">
                    </div>
                </div>

                <div class="col-4 d-flex align-items-center">
                    <div class="col-3 pr-0">
                        <span class="content_span">วันที่เสร็จจริง</span>
                    </div>

                    <div class="col-8">
                        <input type="date" class="input_evr" name="end_date_send_fix" id="end_date_send_fix">
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2 pr-0">
                        <span class="content_span">เลขไมล์ส่งซ่อม</span>
                    </div>
                    <div class="col-10">
                        <input type="text" class="input_evr" name="mile_start_fix" id="mile_start_fix">
                    </div>
                </div>

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ผู้ส่งซ่อม</span>
                    </div>
                    <div class="col-10">
                        <input type="text" class="input_evr" name="man_send_id" id="man_send_id">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-7 d-flex align-items-start">
                    <div class="col-2">
                        <span class="content_span">อาการ</span>
                    </div>
                    <div class="col-10 px-0">
                        <textarea name="problem" id="problem" cols="80" rows="2"
                            style="resize:none; border: solid 0.5px #ceced4; border-radius: 8px;"
                            style="resize: none; border-radius:8px;"></textarea>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-12 d-flex mt-4">
            <span class="title_form">รายการซ่อม</span>
        </div>
        <table class="table mt-3" id="list_fix">
            <thead>
                <tr>
                    <th style="width:1px"></th>
                    <th style="width:60%"><span>รายการ</span></th>
                    <th class="text-center"><span>จำนวน</span></th>
                    <th class="text-center"><span>ราคา/ชิ้น</span></th>
                    <th class="text-center"><span>จำนวนเงิน</span></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td><span class="index_row">1</span></td>
                    <td>
                        <input type="text" name="problem_list" class="problem_list input_evr w-50"
                            placeholder="ระบุอะไหล่">
                        <br>
                        <button type="button" class="mt-2 add_fix_list" onclick="add_list(this)">
                            <img class="mr-2" src="/img/icon/add-circle-material.png" alt=""><span
                                class="content_span">เพิ่มรายการ</span>
                        </button>

                    </td>

                    <td class="text-center">
                        <input type="text" name="amount" class="amount" placeholder="จำนวน">
                    </td>
                    <td class="text-center">
                        <input type="text" name="amount_piece" class="amount_piece" placeholder="ราคา"> <span
                            class="amount_piece_span">x 1</span>
                    </td>


                    <td class="text-center">
                        <span class="amount_total"></span>
                        <input type="hidden" name="amount_total">
                    </td>
                    <td></td>
                </tr>




            </tbody>



            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-end"><span>รวม</span></td>
                    <td class="text-end"><span class="total"></span></td>
                    <td class="text-center"><span>บาท</span></td>
                </tr>

            </tfoot>

        </table>

        <div class="col-12 text-center" style="margin: 20px 10px 0">
            {{-- <button class="submit" type="submit">ตรวจสอบข้อมูล</button> --}}
            <button class="submit" type="button">ตรวจสอบข้อมูล</button>
            <button class="send_data" type="button" style="display: none;">บันทึกข้อมูล</button>

        </div>


    </form>


</div>

@endsection






@section('script')
<script>
    $(document).ready(function () {


        $('.amount , .amount_piece').on('keyup keydown', function () {
            sum_piece();
        });



        $('.submit').on('click',function(e){
            e.preventDefault();
            $('input').attr('readonly',true).css('background-color','#f3f3f3');
            $('select').attr('disabled',true).css('background-color','#f3f3f3');
            $('textarea').attr('disabled',true).css('background-color','#f3f3f3');
            $(this).remove();
            $('.add_fix_list').remove();
            $('.send_data').toggle();


        });


        $('.send_data').on('click',function(){
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'ส่งคำขอถูกยกเลิกแล้ว',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = "/manage_queueCar";
            });
        });



    });





    function sum_piece() {
        var amount = $('.amount').val();
        var price = $('.amount_piece').val();

        var total = parseInt(price) * parseInt(amount);
        // console.log(total);

        if (!isNaN(total)) {
            $('.amount_total').text(total);
            total_sum();

        } else {
            $('.amount_total').text('');

        }

    }

    function total_sum() {
        var total_all = 0;
        var total = 0;

        total_all = $('.amount_total').text();
        total_all = parseInt(total_all);

        total += total_all;


        if (!isNaN(total_all)) {
            $('.total').text(total);
        } else {
            $('.total').text('');
        }

    }







    function add_list() {
        var count_list = $('.index_row').last().text();
        var count = parseInt(count_list);
        var counted = count + 1;


        $('tbody tr').last().clone().appendTo('tbody');
        $('.add_fix_list').not(':last').last().remove();
        $('.index_row').not(':first').last().text(counted++);

    }

</script>
@endsection
