@extends('layout.booking')
@section('style')
<style>
    .title_form_pages {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: 600;
        color: #4f72e5;

    }

    .input_evr {
        width: 100%;
        height: 30px;
        border-radius: 8px;
        padding: 0 8px;
        color: #4a4a4a;
        background-color: #ffffff;
        border: solid 0.5px #ceced4;
    }

    .content_span {
        font-size: 14px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .content_span_upload_file {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        line-height: 1.13;
        color: #4f72e5;
    }

    .form-select {
        height: 30px;
        line-height: 1 !important;
        border-radius: 8px !important;
    }

    .detail_option_form {
        border-radius: 8px;
    }

    .area_input {
        width: 165px;
        height: 165px;
        padding: 35px 10px 35px 10px;
        border: dashed 2px #4f72e5;
        background-color: #ffffff;
    }

    .upload_img_car {
        width: 110px;
        height: 25px;
        border: none;
        margin-left: 10px;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 85%, #314d7b 132%);
    }

    .validate_data {
        width: 120px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

    .submit_addCar {
        display: none;
        width: 120px;
        height: 30px;
        color: #ffffff;
        border: none;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b 132%);
    }

    .area_input {
        width: 165px;
        height: 165px;
        padding: 35px 10px 35px 10px;
        border: dashed 2px #4f72e5;
        background-color: #ffffff;
    }

    .upload_img_driver{
        width: 110px;
        height: 30px;
        border: none;
        margin-left: 10px;
        font-size: 14px;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 30%, #314d7b 100%);
    }

</style>




@section('content')
<div class="my-3 px-5 py-3" style="background-color:white;">
    <div class="row">
        <span class="title_form_pages">รายละเอียนคนขับ</span>
    </div>

    <div class="my-2" style="border:0.5px dashed #ceced4"></div>

    <form action="#" method="post">
        <div class="row">
            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">คำนำหน้า</span>
                    </div>

                    <div class="col-2 d-flex align-items-center px-0">
                        <div class="col-3 px-0">
                            <input type="radio" name="title_name" value="mr">
                        </div>
                        <div class="col-6 px-0">
                            <span class="content_span">นาย</span>
                        </div>
                    </div>

                    <div class="col-2 d-flex align-items-center px-0">
                        <div class="col-3 px-0">
                            <input type="radio" name="title_name" value="mrs">
                        </div>
                        <div class="col-6 px-0">
                            <span class="content_span">นาง</span>
                        </div>
                    </div>

                    <div class="col-2 d-flex align-items-center px-0">
                        <div class="col-3 px-0">
                            <input type="radio" name="title_name" value="miss">
                        </div>
                        <div class="col-6 px-0">
                            <span class="content_span">นางสาว</span>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-12 d-flex mt-3">

                <div class="col-6 d-flex align-items-center">
                    <div class="col-2">
                        <span class="content_span">ชื่อ-สกุล</span>
                    </div>
                    <div class="col-10">
                        <input class="input_evr" type="text" name="brand" id="brand" value="Toyota Vios">
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-6 d-flex align-items-center ">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-2 px-0">
                            <span class="content_span">อายุ</span>
                        </div>
                        <div class="col-6">
                            <input class="input_evr" type="text" name="age" id="age" value="24">
                        </div>
                        <div class="col-4 px-0">
                            <span class="content_span">ปี</span>
                        </div>
                    </div>
                    <div class="col-8 d-flex align-items-center">
                        <div class="col-2">
                            <span class="content_span">สัญชาติ</span>
                        </div>
                        <div class="col-6">
                            <input class="input_evr" type="text" name="national" id="national" value="ไทย">
                        </div>
                        <div class="col"></div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-10 d-flex align-items-center">
                    <div class=" d-flex align-items-center mr-4">
                        <div class="px-0 mx-3">
                            <span class="content_span">ที่อยู่ตามทะเบียนบ้านเลขที่</span>
                        </div>
                        <div style="width:102px; height: 30px">
                            <input type="text" class="input_evr" type="text" name="house_no">
                        </div>
                    </div>
                    <div class="d-flex align-items-center mr-4">
                        <div class="px-0 mr-3">
                            <span class="content_span">ตรอก/ซอย</span>
                        </div>
                        <div style="width: 300px; height: 30px;">
                            <input type="text" class="input_evr" type="text" name="soi">
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="px-0 mr-3">
                            <span class="content_span">ถนน</span>
                        </div>
                        <div style="width: 290px; height: 30px;">
                            <input type="text" class="input_evr" type="text" name="street">
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-12 d-flex mt-3">
                <div class="col-10 d-flex align-items-center">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">ตำบล/แขวง</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_evr" type="text" name="county">
                        </div>
                    </div>

                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">อำเภอ/เขต</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" class="input_evr" type="text" name="district">
                        </div>
                    </div>

                    <div class="col-4 d-flex align-items-center pr-0">
                        <div class="col-2 px-0">
                            <span class="content_span">จังหวัด</span>
                        </div>
                        <div class="col-10 pr-0">
                            <input type="text" class="input_evr" type="text" name="city">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex mt-3">
                <div class="col-10 d-flex align-items-center">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">รหัสไปรษณีย์</span>
                        </div>
                        <div class="col-9 px-0">
                            <input type="text" class="input_evr" type="text" name="post_code">
                        </div>
                    </div>

                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">เบอร์โทรศัพท์</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="text" class="input_evr" type="text" name="tel_no">
                        </div>
                    </div>

                </div>
            </div>





            <div class="col-12 d-flex mt-3">
                <div class="col-10 d-flex align-items-center">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">ประเภทใบขับขี่</span>
                        </div>
                        <div class="col-9 px-0">
                            {{-- <input type="text" class="input_evr" type="text" name="type_license_drive"> --}}
                            <select class="form-select" name="type_license_drive" id="type_license_drive">
                                <option checked>ประเภทใบขับขี่</option>
                                <option value="car">รถยนต์ส่วนตัว</option>
                                <option value="motocycle">รถจักรยานยนต์ส่วนตัว</option>

                            </select>
                        </div>
                    </div>

                    <div class="col-4 d-flex align-items-center">
                        <div class="col-3 px-0">
                            <span class="content_span">วันหมดอายุ</span>
                        </div>
                        <div class="col-9 pr-0">
                            <input type="date" class="input_evr" type="text" name="end_date_license">
                        </div>
                    </div>

                    <div class="col-4 d-flex align-items-center pr-0">
                        <div class="col-4 px-0">
                            <span class="content_span">ยานพาหนะที่ขับขี่ได้</span>
                        </div>
                        <div class="col-8 pr-0">
                            <input type="text" class="input_evr" type="text" name="can_drive">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 d-flex align-items-end mt-3 mx-3">
                <div class="area_input vertical-center text-center mx-3">
                    <img src="" alt="" id="driver_img"
                        style="display: none;width:100%; height:100%; object-fit:contain;">

                    <div class="col-12 upload_mockup">
                        <img src="/img/icon/insert-photo-material.png" alt="">
                    </div>
                    <div class="col-12 mt-2 upload_mockup">
                        <span class="content_span_upload_file">Upload Image</span>
                    </div>
                </div>

                <button type="button" class="upload_img_driver">อัพโหลดไฟล์</button>
                <input type="file" name="img_driver" id="img_driver" style="display:none;">

            </div>






            <div class="col-12 d-flex justify-content-center" style="margin-top: 80px;">
                <button type="button" class="validate_data">ตรวจสอบข้อมูล</button>
                <button type="button" class="submit_addCar">บันทึกข้อมูล</button>
            </div>
        </div>
    </form>


</div>
@endsection




@section('script')
<script>
    $(document).ready(function () {
        $('.validate_data').on('click', function () {

            $('input').css('background-color', '#f3f3f3');
            $('input').attr('readonly', true);

            $('input[type="radio"]').attr('disabled', true);

            $('textarea').css('background-color', '#f3f3f3');
            $('textarea').attr('readonly', true);

            $('select').css('background-color', '#f3f3f3');
            $('select').attr('disabled', true);

            $('.upload_img_driver').attr('disabled', true).css('cursor','not-allowed');

            $(this).remove();
            $('.submit_addCar').css('display', 'block');
        });

        $('.submit_addCar').on('click', function () {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                console.log("Need to add ajax");
                window.location.href = "/manage_queueCar";
            });
        });





        $('.upload_img_driver').on('click', function () {
            $('input[type="file"]').click();

        });
        $('input[type="file"]').change(function () {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.area_input').css({
                        'border': 'solid 2px #4f72e5',
                        'object-fit':'contain',
                        'padding':'1px',

                    });
                    $('.upload_mockup').css('display', 'none');
                    $('#driver_img').css('display', 'block');
                    $('#driver_img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                alert('select a file to see preview');
                $('#driver_img').attr('src', '');
            }
        }

    });

</script>
@endsection
