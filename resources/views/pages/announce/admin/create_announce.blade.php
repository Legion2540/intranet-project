@extends('layout.announce')
@section('style')
<style>
    .forms-announce {
        padding: 22px 67px 60px 30px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
    }


    .text-head-forms {
        color: #4f72e5;
        font-size: 26px;
        font-family: Kanit-Regular;
    }

    .input-time {
        width: 100px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-date {
        width: 222px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .input-url {
        width: 792px;
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        padding: 10px;
    }

    .text-key-announce {
        font-size: 16px;
        color: #4a4a4a;
        font-family: Kanit-Regular;
    }


    .textarea-message {
        width: 792px;
        height: 85px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        resize: none;
    }

    .btn-post {
        width: 112px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(268deg, #4f72e5 1%, #4362c6 99%);
    }


    .btn-cancel {
        width: 112px;
        height: 30px;
        border: 0;
        color: #ffffff;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
    }

    input[type="date"]::-webkit-calendar-picker-indicator {
        background-image: url('/img/icon/date-range-material-copy-4.png');
        object-fit: contain;
    }

    input[type="time"]::-webkit-calendar-picker-indicator {
        background-image: url('/img/icon/access-time-material.png');
        object-fit: contain;
    }

</style>
@endsection


@section('content_announce')
    
<div class="forms-announce">
    <div class="col-12 px-0 d-flex align-items-center">
        <img src="/img/icon/border-color-material-copy.png" alt="" width="23px" height="23px">
        <span class="text-head-forms ml-3">สร้างประกาศ</span>
    </div>
    <div class="col-12 px-0 pt-4 d-flex">
        <div class="px-0 d-flex align-items-center" style="width:120px">
            <span class="text-key-announce">วัน/เวลา ที่ประกาศ</span>
        </div>
        <div class="pl-3 d-flex">
            <input type="date" class="input-date form-control">
            <span class="mx-3">เวลา</span>
            <input type="time" class="input-time form-control">
        </div>
    </div>
    <div class="col-12 px-0 pt-4 d-flex">
        <div class="px-0 d-flex align-items-start" style="width:120px">
            <span class="text-key-announce">ข้อความ</span>
        </div>
        <div class="pl-3 d-flex">
            <textarea class="textarea-message"></textarea>
        </div>
    </div>
    <div class="col-12 px-0 pt-4 d-flex">
        <div class="px-0 d-flex align-items-start" style="width:120px">
            <span class="text-key-announce">แนบลิ้งค์</span>
        </div>
        <div class="pl-3 d-flex">
            <input type="text" class="input-url" placeholder="ระบุ URL">
        </div>
    </div>
</div>

<div class="col-12 mt-5 d-flex justify-content-center">
    <button class="btn-cancel">ยกเลิก</button>
    <button class="btn-post ml-3">โพสต์ประกาศ</button>
</div>

@endsection


@section('script')
<script>
    
</script>
@endsection
