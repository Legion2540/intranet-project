@extends('layout/kwonledge')
@section('style')

<style>
    .head_comment {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }

    .title_comment {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .input_title {
        height: 30px;
        border-radius: 8px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .text_choose_img {
        font-family: Kanit-ExtraLight;
        font-size: 20px;
        font-weight: bold;
        color: #4f72e5;
    }

    .lastname_file {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        color: #ee2a27;
    }


    .input_url_span {
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        color: #d5001a;
    }

    .cancel_btn,
    .cancel_btn:hover {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-color: #ee2a27;
        color: #ffffff;
        text-decoration: none;
    }

    .submit_btn {
        width: 112px;
        height: 30px;
        border-radius: 8px;
        border: 0;
        box-shadow: -2px 3px 7px 1px rgba(0, 0, 0, 0.27);
        background-image: linear-gradient(194deg, #4f72e5 86%, #314d7b -15%);
        color: #ffffff;
    }

    .input_files_div input {
        display: none;
    }

    .input_files_div img {
        cursor: pointer;
    }

    /* .suggestion_file {
        position: relative;
    } */

    .suggestion_file .tooltip_blog {
        width: 181px;
        height: fit-content;
        border-radius: 3px;
        box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.11);
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
        position: absolute;
        z-index: 10;
        top: -68px;
        right: 232px;
    }


    .tooltip_blog .type_file_span {
        font-size: 12px;
        font-weight: 600;
        color: #4f72e5;
    }

    .tooltip_blog .detials_span {
        font-family: Kanit-ExtraLight;
        font-size: 10px;
        color: #4a4a4a;
    }

    .ck-content {
        width: 100%;
    }

    .file-input {
        width: 100%;
    }
    .ck ,.ck-reset ,.ck-editor ,.ck-rounded-corners{
        width: 100%;
    }

</style>
@endsection

@section('content')
<div class="row comment px-3 py-4">
    <div class="col-12">

        <div class="col-12 d-flex px-0 align-items-center">
            <div class="col-6 d-flex justify-content-startpx-0">
                <img src="/img/icon/border-color-material-copy.png" alt="" class="mr-3" style="object-fit: contain;">
                <span class="head_comment">โพสต์บทความ/เอกสาร

                    {{-- <i class="fas fa-upload"></i> --}}

                </span>
            </div>
            <div class="col-6 d-flex justify-content-end align-items-center">
                <img src="/img/icon/comment-blue@2x.png" alt="" class="mr-2">
                <span class="count_comment">0</span>
            </div>
        </div>

        <form action="/upload" method="POST" role="form" enctype="multipart/form-data">
            @csrf

            {{-- หมวดหมู่ --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">หมวดหมู่</span>
                </div>
                <div class="col-5 px-0 d-flex align-items-center justify-content-start">
                    <select class="form-select form-select-2x pl-3" aria-label="ระบุหมวดหมู่โพสต์">
                        <option selected>ระบุหมวดหมู่โพสต์</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>

            {{-- สำนัก --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">สำนัก</span>
                </div>
                <div class="col-5 px-0 d-flex align-items-center justify-content-start">
                    <select class="form-select form-select-2x pl-3" aria-label="สำนักประชาสัมพันธ์และสื่อสารองค์กร">
                        <option selected>สำนักประชาสัมพันธ์และสื่อสารองค์กร</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>

            {{-- ชื่อหัวเรื่อง --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">ชื่อหัวเรื่อง</span>
                </div>
                <div class="col-11 px-0 d-flex align-items-center justify-content-start">
                    <input type="text" name="name_title" class="w-100 input_title pl-3"
                        placeholder="ระบุชื่อบทความ หัวเรื่อง">
                </div>
            </div>

            {{-- ข้อความ --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">ข้อความ</span>
                </div>
                <div class="col-11 px-0 d-flex align-items-center justify-content-start">
                    <textarea name="content" id="editor"></textarea>
                    <figure class="image">
                        <img src="" alt="">
                    </figure>
                </div>
            </div>


            {{-- แนบรูป --}}
            <div class="col-12 d-flex justify-content-start align-items-start mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">แนบรูป</span>
                </div>
                <div class="col-11 px-0 d-flex align-items-center justify-content-start py3 file-loading">
                    {{-- <div class="col-4 px-0 text-end">
                        <img src="/img/icon/insert-photo-material.png" alt=""
                            style="width: 48px;height: 48px; object-fit:contain;">
                    </div>
                    <div class="col-8">
                        <div class="col-12 text-start">
                            <span class="text_choose_img">Choose file from your device or drag image here.</span>
                        </div>
                        <div class="col-12 text-start">
                            <span class="lastname_file">(bmp, gif, jpg, jpeg, png)</span>
                        </div>
                    </div> --}}
                    {{-- <input type="file" name="img_blog[]" class="file" data-show-upload="false" data-show-caption="true"
                    data-msg-placeholder="Select {files} for upload..." multiple> --}}
                    {{-- <input type="file" name="img_blog[]" class="file" id="input-id" data-preview-file-type="text" multiple> --}}
                    <input type="file" name="img_blog[]" id="input-fas" multiple>

                </div>
            </div>


            {{-- แนบอัลบั้ม --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">แนบอัลบั้ม</span>
                </div>
                <div class="col-5 px-0 d-flex align-items-center justify-content-start">
                    <select class="form-select form-select-2x pl-3" aria-label="เลือกอัลบั้ม">
                        <option selected>เลือกอัลบั้ม</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>


            {{-- แนบไฟล์ --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">แนบไฟล์</span>
                </div>
                <div class="col-11 px-0 d-flex align-items-center justify-content-start">
                    <div class="mr-2">
                        <label for="img" style="cursor: pointer;" class="m-0">
                            <img src="/img/icon/group-25-copy-14.png" alt="">
                        </label>
                        <input type="file" id="img" name="files" style="display: none;" accept=".rar, .zip, .jpg, .png,
                            .gif, .pdf, .psd,.ai,
                            .doc, .xls, .ppt, .mp3,
                            .mp4, .mov, .avi .xlsx" multiple>
                        <span class="count_files"></span>

                    </div>
                    <div class="col-6 text-start px-0 suggestion_file">
                        <img src="/img/icon/error-outline-material.png" alt="" class="tooltip_img">
                        <div class="tooltip_blog" style="display: none;">
                            <div class="col-12">
                                <span class="type_file_span">ประเภทไฟล์ที่รองรับ</span>
                            </div>
                            <div class="col-12">
                                <p class="detials_span">.RAR .ZIP .JPG .PNG .GIF .PDF .PSD .AI .DOC .XLS .PPT .MP3 .MP4
                                    .MOV
                                    .AVI</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {{-- ULR YOUTUBE --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">แนบลิงค์</span>
                </div>
                <div class="col-5 px-0 d-flex align-items-center justify-content-start mr-2">
                    <input type="text" name="url_youtube" class="w-100 input_title pl-3"
                        placeholder="ระบุชื่อบทความ หัวเรื่อง">
                </div>
                <div class="col-6 px-0 d-flex justify-content-start">
                    <span class="input_url_span">ระบุ URL ของวีดิโอจาก Youtube</span>
                </div>
            </div>



            {{-- TAG --}}
            <div class="col-12 d-flex justify-content-start align-items-center mt-3">
                <div class="col-1 px-0 d-flex align-items-center justify-content-start">
                    <span class="title_comment">ข้อความ</span>
                </div>
                <div class="col-11 px-0 d-flex align-items-center justify-content-start">
                    <input type="text" name="tag" class="w-100 input_title pl-3" placeholder="ระบุชื่อบทความ หัวเรื่อง">
                </div>
            </div>

            <div class="col-12 d-flex justify-content-center align-items-center mt-3">
                <a href="#" class="cancel_btn pt-1 mr-3">ยกเลิก</a>
                <button class="submit_btn">โพสต์</button>
            </div>

        </form>

    </div>
</div>
@endsection


@section('script')

<script src="{{asset('/js/ck_editor/editor.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#img').bind('change', function () {
            var name_file = $(this)[0].files;
            $.each(name_file, function (key, data) {
                // Show name File
                console.log(data.name);
            });

            // var newNameFiles = $(this).val().replace(/^.*\\/, "");

            var filesSize = $(this)[0].files.length;
            console.log(filesSize);
            $('.count_files').html(filesSize + " files");


        });




        $('.tooltip_img').hover(function () {
            $('.tooltip_blog').toggle();
        }, function () {
            $('.tooltip_blog').toggle();
        });





        $("#input-fas").fileinput({
            uploadUrl: "/site/test-upload",
            enableResumableUpload: true,
            initialPreviewAsData: true,
            theme: 'fas',
            deleteUrl: '/site/file-delete',
            fileActionSettings: {
                showZoom: function (config) {
                    if (config.type === 'pdf' || config.type === 'image') {
                        return true;
                    }
                    return false;
                }
            }
        });

    });

</script>
@endsection
