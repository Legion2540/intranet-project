@extends('layout/kwonledge')
@section('style')
<style>
    .title {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }

    .sub_title {
        font-family: Kanit-ExtraLight;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    #search_knowledge {
        width: 122px;
        height: 29px;
        border-radius: 8px;
        padding: 0 0 0 28px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }

    .tag_office {
        font-family: Kanit-ExtraLight;
        font-size: 10px;
        font-weight: 500;
        color: #4f72e5;
        text-decoration: underline;
    }

    .text_content {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .by_user {
        font-family: Kanit-ExtraLight;
        font-size: 10px;
        font-weight: 500;
        color: #4a4a4a;
    }

    .arrow-knowledge-pages {
        border: solid #ffffff;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        margin: 0px 8px 1px 0px;
        height: 1px !important;
        width: 1px !important;
    }

    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }

    .right {
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
    }

    .count_knowledge_list {
        font-family: Kanit-Regular;
        font-size: 12px;
        font-weight: 500;
        color: #4a4a4a;

    }

    .next_page_knowledge,
    .next_page_knowledge:hover {
        color: #ffffff;
        width: 183px !important;
        height: 35px;
        border-radius: 3px;
        background-color: #4f72e5;
        text-decoration: none;
    }

    .text_pages {
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: bold;
        color: #4a4a4a;
    }

    input[name="page_knowledge"] {
        width: 34px;
        height: 23px;
        border-radius: 4px;
        border: solid 0.5px #ceced4;
        background-color: #ffffff;
    }


    .page_prev {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;
        margin-right: 1px;
    }

    .page_next {
        width: 23px;
        height: 23px;
        background-color: #4f72e5;
        border: none;

    }

    .more {
        cursor: pointer;
    }

    .more:hover .dropdown-knowledge {
        display: inline-block;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

</style>
@endsection



@section('content')

{{-- Title Knowledge Center --}}
<div class="col-12 d-flex p-0 align-content-start">
    <div class="p-0 d-flex mr-3">
        <img src="/img/icon/library-books-material-bule@2x.png" style="object-fit:contain;" alt="">
    </div>
    <div class="p-0 text-start">
        <span class="title">Knowledge Center</span>
    </div>
</div>

<div class="col-12 d-flex align-items-center mt-1 pl-0 pr-5">
    {{-- About Filters --}}
    <div class="col-12 p-0 d-flex align-items-center justify-content-start">
        <div class="text-start p-0" style="width:130px;">
            <span class="sub_title">ราการโพสต์</span>
        </div>
        <div class="text-start p-0">
            <img src="/img/icon/search-material-copy.png" alt=""
                style="position: absolute; width:14px;height:14px;transform: translate(8px, 7px);">
            <input type="text" name="search_knowledge" id="search_knowledge" placeholder="ค้นหา">
        </div>

        <div class="text-start p-0 ml-2" style="width: 205px;">
            <select class="form-select form-select-2x" aria-label="แสดงหมวดทั้งหมด">
                <option selected>แสดงหมวดทั้งหมด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="text-start p-0 ml-2" style="width: 205px;">
            <select class="form-select form-select-2x" aria-label="แสดงสำนักทั้งหมด">
                <option selected>แสดงสำนักทั้งหมด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="text-start p-0 ml-2" style="width: 127px;">
            <select class="form-select form-select-2x" aria-label="เรียงจากใหม่สุด">
                <option selected>เรียงจากใหม่สุด</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="text-start p-0 ml-2" style="width: 127px;">
            <select class="form-select form-select-2x" aria-label="แสดงที่มีไฟล์แนบ">
                <option class="three_dots" selected>แสดงที่มีไฟล์แนบ</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

    </div>
</div>

{{-- Files --}}
<div style="min-height:870px; max-height: 870px;overflow:hidden">
@for($i=1; $i<=3;$i++)
<div class="col-12 p-0 d-flex align-items-center mt-3 pr-5 ">
    <div class="col-12 d-flex p-0">
        <div class="d-flex align-items-center justify-content-center"
            style="width:70px;height:70px; background-color:#96cfee">
            <img src="/img/icon/group-8-copy.png" style="object-fit: contain" alt="">
        </div>
        <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
            <div class="col-5 p-0">
                <div class="col-12 text-start">
                    <a href="#" class="tag_office">สำนักตลาดพาณิชย์ดิจิทัล</a>
                </div>
                <div class="col-12 text-start d-flex align-items-start">
                    <span class="text_content three_dots">ขั้นตอนการส่งออก : กล้องถ่ายรูปและชิ้นส่วน</span>
                </div>
            </div>
            <div class="col-3 d-flex">
                <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                <img src="/img/icon/group-20-copy-9.png" alt="" class="mr-2">
            </div>
            <div class="col-2">
                <div class="col-12">
                    <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                </div>
                <div class="col-12">
                    <span class="by_user">19 ม.ค. 2563 09:00</span>
                </div>
            </div>
            <div class="col-2 more_knowledge">
                <img src="/img/icon/flag-material-copy.png" alt="" class="mr-4">
                <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1">

                <ul class="dropdown-menu" style="display: none">
                    <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                            style="background-color:#4f72e5;">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                </div>

                            </div>
                        </a></li>
                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                </div>
                            </div>

                        </a>
                    </li>
                    <li><a class="dropdown-item py-1 px-2" href="# ">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                </div>
                                <div class="col-2 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>


            </div>
        </div>
    </div>
</div>


<div class="col-12 p-0 d-flex align-items-center mt-3 pr-5 ">
    <div class="col-12 d-flex p-0">
        <div class="d-flex align-items-center justify-content-center"
            style="width:70px;height:70px; background-color:#a4b6f0">
            <img src="/img/icon/group-18.png" style="object-fit: contain" alt="">
        </div>
        <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
            <div class="col-5 p-0">
                <div class="col-12 text-start">
                    <a href="#" class="tag_office">กลุ่มตรวจสอบภายใน</a>
                </div>
                <div class="col-12 text-start d-flex align-items-start">
                    <span class="text_content">ขั้นตอนการเตรียมเอกสารเพื่อยื่นภาษีเงินได้ส่วนบุคคล ประจำปี 2563
                    </span>
                </div>
            </div>
            <div class="col-3 d-flex">
                <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                <img src="/img/type_files/group-20-copy-6.png" alt="" class="mr-2">
                <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                <img src="/img/type_files/group-20-copy-3.png" alt="" class="mr-2">
                <img src="/img/type_files/group-20-copy-7.png" alt="" class="mr-2">
                <img src="/img/type_files/group-20-copy-16.png" alt="" class="mr-2">
                <img src="/img/icon/more-vert-material.png" style="object-fit: contain" alt="" class="mr-2">
            </div>
            <div class="col-2">
                <div class="col-12">
                    <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                </div>
                <div class="col-12">
                    <span class="by_user">19 ม.ค. 2563 09:00</span>
                </div>
            </div>
            <div class="col-2 more_knowledge">
                <img src="/img/icon/group-work-material.png" alt="" class="mr-4">
                <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1">

                <ul class="dropdown-menu" style="display: none">
                    <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                            style="background-color:#4f72e5;">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                </div>

                            </div>
                        </a></li>
                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                </div>
                            </div>

                        </a>
                    </li>
                    <li><a class="dropdown-item py-1 px-2" href="# ">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                </div>
                                <div class="col-2 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>


<div class="col-12 p-0 d-flex align-items-center mt-3 pr-5 ">
    <div class="col-12 d-flex p-0">
        <div class="d-flex align-items-center justify-content-center"
            style="width:70px;height:70px; background-color:#e9bdee">
            <img src="/img/icon/group-3.png" style="object-fit: contain" alt="">
        </div>
        <div class="w-100 p-0 d-flex align-items-center" style="border-radius: 2px; background-color: #eaeaea;">
            <div class="col-5 p-0">
                <div class="col-12 text-start">
                    <a href="#" class="tag_office">สำนักส่งเสริมการค้าาสินค้าไลฟ์สไตล์</a>
                </div>
                <div class="col-12 text-start d-flex align-items-start">
                    <span class="text_content">รายละเอียดและระยะเวลาดำเนินการ โครงการ “STYLE Bangkok”
                        งานแสดงสินค้าไลฟ์สไตล์ ประจำปี 2564</span>
                </div>
            </div>
            <div class="col-3 d-flex">
                <img src="/img/icon/attach-file-material-copy.png" alt="" class="mr-3">
                <img src="/img/type_files/group-20-copy-5.png" alt="" class="mr-2">
                <img src="/img/type_files/group-20-copy-9.png" alt="" class="mr-2">
                {{-- <img src="/img/icon/more-vert-material.png" style="object-fit: contain" alt="" class="mr-2"> --}}
            </div>
            <div class="col-2">
                <div class="col-12">
                    <span class="by_user">by กฤตวิทย์ เสนาลอย</span>
                </div>
                <div class="col-12">
                    <span class="by_user">19 ม.ค. 2563 09:00</span>
                </div>
            </div>
            <div class="col-2 more_knowledge">
                <img src="/img/icon/group-work-material.png" alt="" class="mr-4">
                <img src="/img/icon/more-horiz-material-copy-7.png" alt="" class="more ml-1">

                <ul class="dropdown-menu" style="display: none">
                    <li><a class="dropdown-item py-1 px-2" href="/knowleadgeCenter/page/1"
                            style="background-color:#4f72e5;">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/see-files-more.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#ffffff; font-weight:bold; font-size:14px;">ดูบทความ/เอกสาร</span>
                                </div>

                            </div>
                        </a></li>
                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/group-work-material.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span
                                        style="color:#4f72e5; font-weight:bold; font-size:14px;">บันทึกไว้ในคลัง</span>
                                </div>
                            </div>

                        </a>
                    </li>
                    <li><a class="dropdown-item py-1 px-2" href="# ">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-1">
                                </div>
                                <div class="col-10 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">ดาวน์โหลดไฟล์</span>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li><a class="dropdown-item py-1 px-2" href="#">
                            <div class="d-flex">
                                <div class="col-2 px-1">
                                    <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-1">​
                                </div>
                                <div class="col-2 px-1">
                                    <span style="color:#4f72e5; font-weight:bold; font-size:14px;">พิมพ์</span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>


            </div>
        </div>
    </div>
</div>
@endfor
</div>


{{-- Footer || Paginations Knowledge Center Pages --}}
<div class="col-12 p-0 d-flex align-items-center mt-3 pr-5">
    <div class="col-12 p-0 d-flex justify-content-between align-items-center">
        <div class="col-4 text-start p-0">
            <span class="count_knowledge_list">แสดงผล <b>10</b> จากทั้งหมด <b>150</b></span>
        </div>
        <div class="col-4 d-flex align-items-center justify-content-center">
            <a href="#" class="d-flex justify-content-center align-items-center next_page_knowledge">
                <img src="/img/icon/group-14.png" alt="">
            </a>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center pr-0">
            <span class="text_pages mr-2">หน้่า</span>
            <input type="text" name="page_knowledge" class="text_pages mr-2 text-center" value="1">
            <span class="text_pages mr-2">จาก 15</span>

            <button class="d-flex align-items-center page_prev pl-2" type="button">
                <span class="arrow-knowledge-pages left"></span>
            </button>

            <button class="d-flex align-items-center page_next" type="button">
                <span class="arrow-knowledge-pages right"></span>
            </button>


        </div>
    </div>
</div>





@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.more').on('click', function () {
            $(this).parents('.more_knowledge').find('.dropdown-menu').toggle();
        });
    });

</script>
@endsection
