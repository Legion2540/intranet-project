@extends('layout/kwonledge')
@section('style')
<style>
    .head_three_menu {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 2.29;
        letter-spacing: normal;
        color: #4f72e5;
    }

    .title {
        font-family: Kanit-Regular;
        font-size: 24px;
        font-weight: bold;
        color: #4a4a4a;
        line-height: 1px;
    }

    .date_blog {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        line-height: 2.29;
        color: #4a4a4a;
    }

    .content_1 {
        font-family: Kanit-ExtraLight;
        font-size: 18px;
        font-weight: 500;
        line-height: 1.89;
        color: #4a4a4a;
    }

    .input_files {
        border-radius: 2px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
        border-radius: 8px;
    }

    .files_input_text {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .count_files {
        font-family: Kanit-ExtraLight;
        font-size: 12px;
        font-weight: 600;
        line-height: 2.67;
        text-align: right;
        color: #4a4a4a;
    }

    .name_files {
        font-family: Kanit-Regular;
        font-size: 14px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .size_files {
        font-family: Kanit-ExtraLight;
        font-size: 10px;
        color: #4a4a4a;
    }

    .tag {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: 600;
        color: #4a4a4a;
    }

    .blue {
        color: #4f72e5;
    }

    .img_profile {
        width: 53px;
        height: 53px;
        border: solid 3px #ffffff;
        border-radius: 50%;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
    }

    .by_files {
        font-family: Kanit-Regular;
        font-size: 14px;
        color: #4a4a4a;
    }

    .by_user {
        font-family: Kanit-Regular;
        font-size: 16px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .position_office {
        font-family: Kanit-ExtraLight;
        font-size: 14px;
        color: #4a4a4a;
    }

    .comment {
        border-radius: 2px;
        box-shadow: 2px 3px 10px 1px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
        border-radius: 8px;
    }

    .head_comment {
        font-family: Kanit-Regular;
        font-size: 26px;
        font-weight: bold;
        color: #4f72e5;
    }

    .count_comment {
        font-family: Kanit-Regular;
        font-size: 20px;
        font-weight: bold;
        color: #4a4a4a;
    }

    .input_comment {
        padding: 16px 333px 15px 12px;
        object-fit: contain;
        border-radius: 8px;
        border: solid 0.5px #8a8c8e;
    }

</style>
@endsection


@section('content')
<div class="row pl-0">
    <div class="col-12 pl-0">
        <div class="col-12 d-flex align-content-center justify-content-between pl-0">
            <div class="col-6 text-start px-0">
                <a href="#" style="text-decoration: underline">สำนักตลาดพาณิช์ดิจิทัล</a>
            </div>
            <div class="col-6 text-end">
                <a href="#" style="text-decoration: none;">
                    <img src="/img/icon/group-work-material.png" alt="" class="mr-2">
                    <span class="head_three_menu mr-3">เอาออกจากคลัง</span>
                </a>
                <a href="#" style="text-decoration: none">
                    <img src="/img/icon/files-download-materail-blue.png" alt="" class="mr-2">
                    <span class="head_three_menu mr-3">ดาวน์โหลดไฟล์</span>
                </a>
                <a href="#" style="text-decoration: none">
                    <img src="/img/icon/local-printshop-material-copy.png" alt="" class="mr-2">
                    <span class="head_three_menu mr-3">พิมพ์</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row pl-0">
    <div class="col-12 text-start pl-0">
        <span class="title">ขั้นตอนการส่งออก : กล้องถ่ายรูปและชิ้นส่วน</span>
    </div>
    <div class="col-12 text-start pl-0 d-flex align-items-center">
        <img src="/img/icon/access-time-material.png" alt="" style="object-fit: contain; width:14px; height:14px;"
            class="mr-1">
        <span class="date_blog">19 ม.ค. 2563 09:00</span>
    </div>
    <div class="col-12 text-start pl-0 d-flex align-content-center mt-3">
        <span
            class="content_1">จากความยอดนิยมของการถ่ายภาพในยุคปัจจุบันและความจำเป็นในการใช้กล้องถ่ายภาพเป็นส่วนหนึ่งในรูปแบบการทำงานแบบต่างๆ
            ไม่ว่าจะการใช้งานเพื่อสื่อประชาสัมพันธ์ การบันทึกภาพ
            การถ่ายเพื่อทำไปใช้ประกอบเป็นส่วนหนึ่งของการโฆษณาต่างๆทุกการดำเนินการธุรกิจ
            จำเป็นต้องมีภาพถ่ายเพื่อดึงดูดผู้บริโภคอยู่เสมอถ่ายส่งผลให้มูลค่าทางการตลาดสูงขึ้นตลอดและเป็นสินค้าที่ยังคงสามารถสร้างรายได้ได้อย่างต่อเนื่องจึงทำให้เกิดการส่งออกต่างๆมากมาย
            โดยสามารถติดตามได้ที่เอกสารด้านล่างนี้</span>
    </div>
    <div class="col-12 d-flex align-items-center mt-3 pl-0">
        <embed src="" type="application/pdf" class="" style="background-color: #eaeaea" height="721px" width="100%">
    </div>
</div>

<div class="row input_files mt-4" style="height:154px;">
    <div class="col-12">
        <div class="col-12 d-flex p-3">
            <div class="col-6 text-start px-0">
                <span class="files_input_text">ไฟล์แนบ</span>
            </div>
            <div class="col-6 text-end px-0">
                <span class="count_files">1 ไฟล์</span>
            </div>
        </div>
        <div class="col-12 d-flex">
            @for($i=1; $i<=4;$i++) <div class="col-3 d-flex align-items-center">
                <div class="col-12 d-flex align-items-center p-2" style="border-radius: 8px; background-color:#eaeaea">
                    <div class="col-2">
                        <img src="/img/type_files/group-20-copy-9.png" alt="">
                    </div>
                    <div class="col-8 text-start p-0">
                        <div class="col-12 d-flex align-items-center pr-0">
                            <span class="name_files three_dots">ข่าวความเคลื่อนไหวตลอดเวลา</span>
                        </div>
                        <div class="col-12 d-flex align-items-center pr-0">
                            <span class="size_files">57 KB</span>
                        </div>
                    </div>
                    <div class="col-2 p-0">
                        <img src="/img/icon/file-download-material-black.png" alt="">
                    </div>
                </div>
        </div>
        @endfor
    </div>
</div>
</div>

<div class="row mt-2">
    <div class="col-12 d-flex px-0 mt-4">
        <span class="tag">Tag :</span>
        <span class="tag blue"> กล้อง, กล้องดิจิตอล, กล้องถ่ายรูป, การส่งออก </span>
    </div>
</div>

<div class="row mt-5 mb-5">
    <div class="col-12 text-start px-0">
        <span class="by_files">เอกสารโดย :</span>
    </div>
    <div class="col-12 d-flex align-items-center px-0">
        <div class="col-1 px-0 d-flex justify-content-start">
            <img class="img_profile" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
        </div>
        <div class="col-11 px-0">
            <div class="col-12 text-start px-0">
                <span class="by_user">กฤตวิทย์ เสนาลอย</span>
            </div>
            <div class="col-12 text-start px-0">
                <span class="position_office">สำนักตลาดพาณิชย์ดิจิทัล</span>
            </div>
        </div>
    </div>
</div>


<div class="row mt-4 comment px-3 py-4">
    <div class="col-12">
        <div class="col-12 d-flex px-0">
            <div class="col-6 text-start px-0">
                <span class="head_comment">คุณรู้สึกอย่างไรกับบทความนี้</span>
            </div>
            <div class="col-6 d-flex justify-content-end align-items-center">
                <img src="/img/icon/comment-blue@2x.png" alt="" class="mr-2">
                <span class="count_comment">0</span>
            </div>
        </div>
        <div class="col-12 d-flex align-items-center mt-3 px-0">
            <div class="col-1 px-0">
                <img class="img_profile" src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    alt="">
            </div>
            <div class="col-11">
                <input type="text" class="input_comment px-3 w-100" placeholder="แสดงความคิดเห็น...">
            </div>
        </div>

    </div>
</div>


@endsection




@section('script')
<script>

</script>

@endsection
