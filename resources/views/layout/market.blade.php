<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/market.css')}}">
</head>

<body style="background-color: #f4f4f4">
    @include('include.header')

    <div class="container p-0" style="max-width:1390px;">
        <div class="row padding_body d-flex justify-content-center">
            <div class="px-0" style="width:967px;">
                @yield('contentmarket')
            </div>
            <div style="width: 423px;padding-left:50px">
                @include('include.market.profile_card')
                @include('include.market.popular_card')
            </div>
        </div>
    </div>

    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
