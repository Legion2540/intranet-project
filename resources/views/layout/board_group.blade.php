<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    <link rel="stylesheet" href="/css/pages/ck_editor.css" type="text/css">
    {{-- <link rel="stylesheet" href="{{asset('css/pages/main.css')}}"> --}}
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>



</head>

<body style="background-color:#f4f4f4;">
    @include('include.header')
    @yield('style')

    <link href="/css/krajee/fileinput.min.css" rel="stylesheet">
    <link href="/css/krajee/themes/explorer/theme.css" rel="stylesheet">
    <script src="/js/krajee/fileinput.js"></script>
    <script src="/css/krajee/themes/explorer/theme.js"></script>


    <div class="container p-0 my-4" style="max-width:1400px !important;">
        <div class="col-12 d-flex justify-content-center" style="width: 100%; height: 360px;">
            @yield('title_image')
        </div>
    </div>

    <div class="container p-0 my-4" style="max-width:1400px !important;">
        <div class="col-12 d-flex justify-content-between">
            <div class="px-0" style="max-width: 990px;">
                @yield('content')
            </div>
            <div class="pr-0" style="max-width: 358px;margin-left:22px;">
                @include('/include/blog/sidebar_blog_group')
            </div>
        </div>
    </div>


    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
