<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
    <link rel="stylesheet" href="{{asset('css/pages/photo.css')}}">

</head>

<body style="background-image: linear-gradient(to bottom, #4f72e5, #314d7b 119%);">
    @yield('style')
    <div class="container-fluid p-0">
        @yield('content')
    </div>
    @yield('script')

</body>

</html>
