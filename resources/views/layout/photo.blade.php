<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/photo.css')}}">
</head>

<body style="background-color: #f4f4f4">
    @include('include.header')

    <div class="container p-0" style="max-width: 1340px;">
        <div class="row padding_body d-flex justify-content-center">
            <div class="p-0" style="width: 970px">
                @yield('contentphoto')
            </div>
            <div class="p-0" style="width:373px;margin-left:22px">
                @include('include.pupularcard')
            </div>
        </div>
    </div>

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
