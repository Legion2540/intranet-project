<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    <link rel="stylesheet" href="/css/pages/ck_editor.css" type="text/css">
    {{-- <link rel="stylesheet" href="{{asset('css/pages/main.css')}}"> --}}
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>



</head>

<body>
    @include('include.header')
    @yield('style')

    <link href="/css/krajee/fileinput.min.css" rel="stylesheet">
    <link href="/css/krajee/themes/explorer/theme.css" rel="stylesheet">
    <script src="/js/krajee/fileinput.js"></script>
    <script src="/css/krajee/themes/explorer/theme.js"></script>



    <div class="container-fluid p-0">
        <div class="row text-center d-flex justify-content-center mt-4 mb-5"
            style="margin-left: 270px; margin-right: 270px;">

            <div class="col-9">
                @yield('content')
            </div>

            <div class="col-3 ">
                <div class="col-12  px-3 py-3 mb-3" style="background-color:#4f72e5;">
                    @include('include/knowledge/sideRightbar')
                </div>
                <div class="col-12 px-3 py-3" style="background-color:#eaeaea; height:228px !important;">
                    @include('include/knowledge/sideRightbottom')
                </div>
            </div>
        </div>
    </div>
    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
