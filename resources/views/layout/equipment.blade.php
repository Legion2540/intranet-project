<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/equipment.css')}}">

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body style="background-color: #f4f4f4">
    @include('include.header')

    <div class="container p-0" style="max-width: 1340px;">
        <div class="row padding_body">
            <div class="p-0" style="width: 100%">
                @yield('contentequipment')
            </div>
        </div>
    </div>
    @yield('script')
    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
