<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    {{-- <link rel="stylesheet" href="{{asset('css/pages/main.css')}}"> --}}
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
    <link rel="stylesheet" href="{{asset('css/pages/photo.css')}}">

</head>

<body style="background:#f7f7f7">
    @include('include.header')
    @yield('style')
    <div class="container p-0" style="max-width: 1370px;">
        <div class="row padding_body d-flex align-items-start" style="background:#f7f7f7">
            @yield('content')
        </div>
        <div class="news" style="position: absolute;left: -14px;">
            @yield('news')
        </div>
        <div class="row padding_body d-flex align-items-start" style="background:#f7f7f7; margin-top: 480px;">
            @yield('media')
        </div>
    </div>
    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>
</body>

</html>
