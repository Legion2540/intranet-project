<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/pm.css')}}">
    <link rel="stylesheet" href="/css/pages/ck_editor.css" type="text/css">
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script> --}}
</head>

<body style="background-color: #f4f4f4">
    @include('include.header')
    @yield('style')
    <div class="container p-0" style="max-width: 1365px">
        <div class="row padding_body d-flex">
            <div class="px-0" style="width:294px;margin-right: 25px;background-color: #e1e1e1;">
                @include('include.pm_card')
            </div>
            <div style="width: 1076px;background-color: #ffffff;">
                @yield('content_pm')
            </div>
        </div>
    </div>

    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
