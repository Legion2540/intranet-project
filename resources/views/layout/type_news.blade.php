<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    <link rel="stylesheet" href="/css/pages/ck_editor.css" type="text/css">
    {{-- <link rel="stylesheet" href="{{asset('css/pages/main.css')}}"> --}}
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>



</head>

<body style="background-color:#f4f4f4;">
    @include('include.header')
    @yield('style')

    <link href="/css/krajee/fileinput.min.css" rel="stylesheet">
    <link href="/css/krajee/themes/explorer/theme.css" rel="stylesheet">
    <script src="/js/krajee/fileinput.js"></script>
    <script src="/css/krajee/themes/explorer/theme.js"></script>



    <div class="container p-0 my-4" style="max-width:1340px !important;">
        <div class="row d-flex justify-content-between">
            <div class="col-9 pr-4 pl-1">
                @yield('content')
            </div>
            <div class="col-3 d-flex justify-content-start pr-0" style="width: 335px;">
                @include('/include/news/sidebar_type_news')
            </div>
        </div>
    </div>




    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
