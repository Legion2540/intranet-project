<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
</head>

<body style="background-color: #f4f4f4">
    @include('include.header')

    <div class="container p-0" style="max-width:1390px;">
        <div class="row padding_body d-flex justify-content-center">
            <div class="p-0" style="width: 237px; height: 392px;">
                @include('include.faqcard')
            </div>
            <div class="p-0 ml-3" style="width:1126px;">
                @yield('contentfaqs')
            </div>
        </div>
    </div>

    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
