<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{ asset('css/pages/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/main_old.css') }}">
    {{-- <link rel="stylesheet" href="{{asset('css/pages/main.css')}}"> --}}
    <script src='{{ asset('/js/tracking_calendar/main.js') }}'></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

</head>

<body>
    @include('include.header')
    @yield('style')
    <div class="container-fluid p-0">
        <div class="row text-center d-flex justify-content-center m-0" style="background:#f7f7f7">

            @if (Auth::user()->type == 'employee')
                @include('include/profile/user/top_profile')

            @elseif(Auth::user()->type == 'approval')
                @include('include/profile/approval/appr_top_profile')

            @elseif(Auth::user()->type == 'admin')
                @include('include/profile/approval/appr_top_profile')
                
            @endif

            @yield('content')
        </div>
    </div>
    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
