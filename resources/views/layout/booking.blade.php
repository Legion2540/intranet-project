<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/pages/main_old.css')}}">
    <script src='{{asset('/js/tracking_calendar/main.js')}}'></script>
</head>

<body style="background:#f7f7f7">
    @include('include.header')
    @yield('style')
    <div class="container p-0" style="max-width:1340px !important">
        <div class="row">
            @yield('content')
        </div>
    </div>
    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>