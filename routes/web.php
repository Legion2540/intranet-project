<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();


Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/forgotpassword', 'Auth\ForgotPasswordController@index');
Route::post('/forgotpassword', 'Auth\ForgotPasswordController@forgotpassword');


Route::get('/resetpassword', 'Auth\ResetPasswordController@index');



Route::prefix('/ldap')->group(function () {
    Route::get('/', 'IndexController@ldap_authentication');
});




// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'IndexController@indexpage');


    Route::prefix('/profile')->group(function () {
        Route::get('/board', 'ProfileController@board');
        Route::get('/blog', 'ProfileController@blog');
        Route::get('/media', 'ProfileController@media');
        Route::get('/booking', 'ProfileController@booking');
        Route::get('/product', 'ProfileController@product');
        Route::get('/group', 'ProfileController@group');
        Route::get('/approved', 'ProfileController@approved');

        // Route::get('/setting', 'ProfileController@setting')->middleware('auth');
        Route::get('/setting', 'ProfileController@setting');
        Route::post('/updateprofile', 'ProfileController@updateprofile');
    });

    // Approval 1
    Route::prefix('/manager')->group(function () {
        Route::get('/', 'ManagerEmpController@index_approval');

        Route::get('/approved_car1', 'ManagerEmpController@approved_car');
        Route::get('/approved_room', 'ManagerEmpController@approved_room');
        Route::post('/approved_room/approved', 'ManagerEmpController@approved_room_approved');
        Route::post('/approved_room/cancelled', 'ManagerEmpController@approved_room_cancelled');

    });


    Route::prefix('/knowleadgeCenter')->group(function () {
        Route::get('/', 'KnowledgeController@index');
        Route::get('/myfiles', 'KnowledgeController@myfiles');
        Route::get('/instorage', 'KnowledgeController@instorage');
        // TODO : pass data on url /page/id_know
        Route::get('/page/{id_know}', 'KnowledgeController@content_knowledge');
        Route::get('/creatKnowledge', function () {
            return view('pages/knowledgeCenter/createBlog');
        });
        // TODO : submit after create Knowledge
        Route::get('/creatKnowledge/submit', 'KnowledgeController@createKnowledge');
    });



    // Booking Room (User)
    Route::prefix('/reserve/room')->group(function () {

        Route::get('/', 'Booking\Room\UserController@booking_room_index');
        Route::get('/choose', 'Booking\Room\UserController@choose_room');

        Route::post('/choose', 'Booking\Room\UserController@choose_room_update');

        Route::get('/form_room1', 'Booking\Room\UserController@form_room1_view');
        Route::post('/form_room1', 'Booking\Room\UserController@form_room1_update');

        Route::get('/form_room2', 'Booking\Room\UserController@form_room2_view');
        Route::post('/form_room2', 'Booking\Room\UserController@form_room2_update');

        Route::get('/form_room3', 'Booking\Room\UserController@form_room3_view');
        Route::post('/form_room3', 'Booking\Room\UserController@form_room3_update');

        Route::get('/form_room4', 'Booking\Room\UserController@form_room4_view');
        Route::post('/form_room4', 'Booking\Room\UserController@form_room4_update');
    });


    // Booking Deivce
    Route::prefix('/reserve/vdoconference')->group(function () {
        Route::get('/vdoConference1', 'Booking\Room\UserController@form_welfare1_view');
        Route::post('/vdoConference1', 'Booking\Room\UserController@form_welfare1_update');

        Route::get('/vdoConference2', 'Booking\Room\UserController@form_welfare2_view');
        Route::post('/vdoConference2', 'Booking\Room\UserController@form_welfare2_update');

        Route::get('/vdoConference3', 'Booking\Room\UserController@form_welfare3_view');
        Route::post('/vdoConference3', 'Booking\Room\UserController@form_welfare3_update');

        Route::get('/vdoConference4', 'Booking\Room\UserController@form_welfare4_view');
        Route::post('/vdoConference4', 'Booking\Room\UserController@form_welfare4_update');
    });




    // Booking Car (User)
    Route::prefix('/reserve/car')->group(function () {
        Route::get('/', 'Booking\Car\UserController@booking_car_index');

        // Form : Input
        Route::get('/form_car1', 'Booking\Car\UserController@form_car1');
        Route::get('/form_car2', 'Booking\Car\UserController@form_car2');
        Route::get('/form_car3', 'Booking\Car\UserController@form_car3');
        // Form : Input
    });



    // Approval Food & Drink
    Route::prefix('/welfare')->group(function () {
        Route::get('/', 'Booking\Room\WelfareController@index_welfare');

        Route::get('/approved', 'Booking\Room\WelfareController@approved_welfare');
        Route::post('/approved', 'Booking\Room\WelfareController@approved_welfare_approved');
        Route::post('/cancelled', 'Booking\Room\WelfareController@approved_welfare_cancelled');

        Route::get('/add', 'Booking\Room\WelfareController@add_welfare');
        Route::post('/add', 'Booking\Room\WelfareController@add_welfate_update');


        Route::get('/edit', 'Booking\Room\WelfareController@edit_welfare');
        Route::post('/edit', 'Booking\Room\WelfareController@edit_welfare_update');


        Route::post('/status', 'Booking\Room\WelfareController@status_food');
    });



    // Approval Building
    Route::prefix('/building')->group(function () {
        Route::get('/', 'Booking\Room\ManagerRoomController@index_building');

        Route::get('/approved', 'Booking\Room\ManagerRoomController@approved_room');
        Route::post('/approved', 'Booking\Room\ManagerRoomController@approved_room_approved');
        Route::post('/cancelled', 'Booking\Room\ManagerRoomController@approved_room_cancelled');



        Route::get('/add', 'Booking\Room\ManagerRoomController@add_room');
        Route::post('/add', 'Booking\Room\ManagerRoomController@add_room_update');


        Route::get('/detail_room', 'Booking\Room\ManagerRoomController@detail_room');

        Route::post('/editroom', 'Booking\Room\ManagerRoomController@edit_room');
        Route::post('/status/device', 'Booking\Room\ManagerRoomController@status_device');

    });

    





    // Approval Manager Queue Car
    Route::prefix('/manage_queueCar')->group(function () {
        Route::get('/', 'Booking\Car\QueueCarController@index_queueCar');

        Route::get('/approve_1', 'Booking\Car\QueueCarController@queue_approve_1');
        Route::get('/approve_2', 'Booking\Car\QueueCarController@queue_approve_2');
        Route::get('/approve_3', 'Booking\Car\QueueCarController@queue_approve_3');
        Route::get('/approve_4', 'Booking\Car\QueueCarController@queue_approve_4');
        Route::get('/approve_5', 'Booking\Car\QueueCarController@queue_approve_5');


        Route::get('/historycar', 'Booking\Car\QueueCarController@history');


        Route::prefix('/add')->group(function () {
            Route::get('/car', 'Booking\Car\QueueCarController@add_car');
            Route::get('/driver', 'Booking\Car\QueueCarController@add_driver');
        });

        Route::prefix('/edit')->group(function () {
            Route::get('/car', 'Booking\Car\QueueCarController@edit_car');
            Route::get('/driver', 'Booking\Car\QueueCarController@edit_drive');
        });


        Route::get('/requestFix', 'Booking\Car\QueueCarController@request_fix');
        Route::get('/sendForm', 'Booking\Car\QueueCarController@sendForm');
        Route::get('/return', 'Booking\Car\QueueCarController@return_car');
    });



    // Approval Manager Car
    Route::get('/manager/car', 'ManagerEmpController@manager_approve');




    // News
    Route::prefix('/news')->group(function () {
        Route::get('/', 'NewsController@index');
        Route::get('/{type_news}', 'NewsController@type_news');
        Route::get('/{type_news}/{id}', 'NewsController@content_news');
    });

    // Blog
    Route::prefix('/blog')->group(function () {
        Route::get('/', 'BlogController@index');
        Route::get('/other', 'BlogController@blog_other');
        Route::get('/myblog', 'BlogController@myblog');
        Route::get('/myblog/{id}', 'BlogController@content_blog');
        Route::get('/createblog', 'BlogController@createBlog');
    });





    // Board
    Route::prefix('/board')->group(function () {
        Route::get('/', 'BoardController@index');
        Route::get('/myboard', 'BoardController@myboard');
        Route::get('/myboard/{id}', 'BoardController@content_board');
        Route::get('/createboard', 'BoardController@createboard');
        Route::get('/group/{namegroup}', 'BoardController@group_board');
    });


    // Organization
    Route::prefix('/organization')->group(function () {
        Route::get('/', 'OrganizationController@index');
        Route::get('/section/{namesection}', 'OrganizationController@section');
        Route::get('/editsection', 'OrganizationController@edit_section');
    });








    // -------- Start Photo ------------

    // part:photo
    Route::prefix('/photo')->group(function () {
        Route::get('/', 'PhotoController@photo_index');

        // part:photo/user
        Route::prefix('/user')->group(function () {
            Route::get('/gallery', 'PhotoController@photo_gallery');
            Route::get('/video', 'PhotoController@photo_video');
            Route::get('/files', 'PhotoController@photo_files');

            // part:photo/user/upload
            Route::prefix('/upload')->group(function () {
                Route::get('/photo', 'PhotoController@photo_up_photo');
                Route::get('/album', 'PhotoController@photo_up_album');
            });
        });
    });


    // -------- End Photo ------------



    // -------- Start Equipment ------------

    // part:equipment
    Route::prefix('/equipment')->group(function () {
        Route::get('/', 'EquipmentController@equipment_index');
        Route::post('/', 'EquipmentController@equipment_index_update');

        Route::prefix('/user')->group(function () {
            Route::get('/detail', 'EquipmentController@equipment_detail');
            Route::post('/detail', 'EquipmentController@equipment_detail_update');

            Route::get('/return', 'EquipmentController@equipment_user_return');
            Route::post('/return', 'EquipmentController@equipment_user_return_update');

            Route::get('/lend', 'EquipmentController@equipment_user_lend');
            Route::post('/lend', 'EquipmentController@equipment_user_lend_update');

            // Route::get('/list','EquipmentController@equipment_user_list');
            Route::get('/list', 'EquipmentController@equipment_user_list');
            Route::post('/list', 'EquipmentController@equipment_user_list_update');

            Route::get('/check', 'EquipmentController@equipment_user_check');
            Route::post('/check', 'EquipmentController@equipment_user_check_update');
        });

        Route::prefix('/manager')->group(function () {
            Route::get('/', 'EquipmentController@equipment_manager_index');

            Route::get('/adddevice', 'EquipmentController@equipment_manager_adddevice');
            Route::post('/adddevice', 'EquipmentController@equipment_manager_adddevice_update');

            Route::get('/detail', 'EquipmentController@equipment_manager_detail');

            Route::get('/edit', 'EquipmentController@equipment_manager_edit_device');
            Route::post('/edit', 'EquipmentController@equipment_manager_edit_device_update');

            Route::get('/return', 'EquipmentController@equipment_manager_return');
            Route::post('/return', 'EquipmentController@equipment_manager_return_update');

            Route::get('/lend', 'EquipmentController@equipment_manager_lend');
            Route::post('/lend', 'EquipmentController@equipment_manager_lend_update');

            Route::post('/cancel', 'EquipmentController@equipment_manager_lend_cancel');
        });
    });




    // -------- End Equipment ------------




    // -------- Start Form ------------
    Route::prefix('/forms')->group(function () {
        Route::get('/', 'FormsController@forms_index');
        Route::get('/1', 'FormsController@forms_1');
        Route::get('/2', 'FormsController@forms_2');
        Route::get('/3', 'FormsController@forms_3');
        Route::get('/4', 'FormsController@forms_4');
        Route::get('/5', 'FormsController@forms_5');
        Route::get('/6', 'FormsController@forms_6');
    });
    // -------- End Form ------------




    // -------- Start PM ------------
    Route::prefix('/pm')->group(function () {
        Route::get('/inbox', 'PMController@pm_inbox');
        Route::get('/chat', 'PMController@pm_chat');
    });

    // -------- End PM ------------


    // -------- Start FAQs ------------

    // part:faq
    Route::prefix('/faq')->group(function () {
        Route::get('/', 'FAQController@faq_index');
    });

    // -------- End FAQs ------------


    // -------- Start Market ------------

    // part:market
    Route::prefix('/market')->group(function () {
        Route::get('/', 'MarketController@market_index');
        Route::get('/myproduct', 'MarketController@market_myproduct');
        Route::get('/product', 'MarketController@market_product');
        Route::get('/sell', 'MarketController@market_sell');
    });

    // -------- End Market ------------


    // -------- Start Question ------------

    // part:question
    Route::prefix('/question')->group(function () {
        Route::get('/', 'QuestionController@question_index');
        Route::get('/create', 'QuestionController@question_create');
    });

    // -------- End Question ------------


    // -------- Start Announce ------------

    // part:announce
    Route::prefix('/announce')->group(function () {
        Route::get('/', 'AnnounceController@announce_index');
        Route::get('/create', 'AnnounceController@announce_create');
    });

    // -------- End Announce ------------

});
