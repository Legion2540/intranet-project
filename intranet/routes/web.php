<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@indexpage');



Route::prefix('/profile')->group(function () {
    Route::get('/{blog}','ProfileController@profile');
    Route::get('/setting','ProfileController@setting');
});



Route::prefix('/knowleadgeCenter')->group(function () {
    Route::get('/','KnowledgeController@index');
    Route::get('/myfiles','KnowledgeController@myfiles');
    Route::get('/instorage','KnowledgeController@instorage');
    // TODO : pass data on url /page/id_know
    Route::get('/page/{id_know}','KnowledgeController@content_knowledge');
    Route::get('/creatKnowledge',function(){
        return view('pages/knowledgeCenter/createBlog');
    });
    // TODO : submit after create Knowledge
    Route::get('/creatKnowledge/submit','KnowledgeController@createKnowledge');
});



// Booking Room (User)
Route::prefix('/reserve/room')->group(function () {
    Route::get('/','Booking\Room\UserController@booking_room_index');
    Route::get('/choose','Booking\Room\UserController@choose_room');
    // Route::get('/form_room1/{room_id}',function(){
    //     return view('pages/booking/user/form_booking/room_1');
    // });
    Route::get('/form_room1/{room_id}','Booking\Room\UserController@form_room1');
    Route::post('/form_room2','Booking\Room\UserController@form_room2');
    Route::get('/form_room3','Booking\Room\UserController@form_room3');
    Route::get('/form_room4','Booking\Room\UserController@form_room4');
});


// Booking Deivce
Route::prefix('/reserve/vdoconference')->group(function () {
    Route::get('/vdoConference1','Booking\Room\UserController@form_welfare1');
    Route::get('/vdoConference2','Booking\Room\UserController@form_welfare2');
    Route::get('/vdoConference3','Booking\Room\UserController@form_welfare3');
    Route::get('/vdoConference4','Booking\Room\UserController@form_welfare4');
});




// Booking Car (User)
Route::prefix('/reserve/car')->group(function () {
    Route::get('/','Booking\Car\UserController@booking_car_index');

    // Form : Input
    Route::get('/form_car1','Booking\Car\UserController@form_car1');
    Route::get('/form_car2','Booking\Car\UserController@form_car2');
    Route::get('/form_car3','Booking\Car\UserController@form_car3');
    // Form : Input
});



// Approval Food & Drink
Route::prefix('/welfare')->group(function(){
    Route::get('/','Booking\Room\WelfareController@index_welfare');
    Route::get('/approved','Booking\Room\WelfareController@approved_welfare');
    Route::get('/add','Booking\Room\WelfareController@add_welfare');
    Route::get('/edit','Booking\Room\WelfareController@edit_welfare');
});



// Approval Building
Route::prefix('/building')->group(function(){
    Route::get('/','Booking\Room\ManagerRoomController@index_building');
    Route::get('/approved','Booking\Room\ManagerRoomController@approved_room');
    Route::get('/add','Booking\Room\ManagerRoomController@add_room');
    Route::get('/edit','Booking\Room\ManagerRoomController@edit_room');
});



// Approval 1
Route::prefix('/manager')->group(function(){
    Route::get('/','ManagerEmpController@index_approval');

    Route::get('/approved_car1','ManagerEmpController@approved_car');
    Route::get('/approved_room1','ManagerEmpController@approved_room');
});


// Approval Manager Queue Car
Route::prefix('/manage_queueCar')->group(function(){
    Route::get('/','Booking\Car\QueueCarController@index_queueCar');

    Route::get('/approve_1','Booking\Car\QueueCarController@queue_approve_1');
    Route::get('/approve_2','Booking\Car\QueueCarController@queue_approve_2');
    Route::get('/approve_3','Booking\Car\QueueCarController@queue_approve_3');
    Route::get('/approve_4','Booking\Car\QueueCarController@queue_approve_4');
    Route::get('/approve_5','Booking\Car\QueueCarController@queue_approve_5');


    Route::get('/historycar','Booking\Car\QueueCarController@history');


    Route::prefix('/add')->group(function(){
        Route::get('/car','Booking\Car\QueueCarController@add_car');
        Route::get('/driver','Booking\Car\QueueCarController@add_driver');
    });

    Route::prefix('/edit')->group(function(){
        Route::get('/car','Booking\Car\QueueCarController@edit_car');
        Route::get('/driver','Booking\Car\QueueCarController@edit_drive');
    });


    Route::get('/requestFix','Booking\Car\QueueCarController@request_fix');
    Route::get('/sendForm','Booking\Car\QueueCarController@sendForm');
    Route::get('/return','Booking\Car\QueueCarController@return_car');

});



// Approval Manager Car
Route::get('/manager/car','ManagerEmpController@manager_approve');




// News
Route::prefix('/news')->group(function(){
    Route::get('/','NewsController@index');
    Route::get('/{type_news}', 'NewsController@type_news');
    Route::get('/{type_news}/{id}','NewsController@content_news');
});

// Blog
Route::prefix('/blog')->group(function(){
    Route::get('/','BlogController@index');
    Route::get('/other','BlogController@blog_other');
    Route::get('/myblog','BlogController@myblog');
    Route::get('/myblog/{id}','BlogController@content_blog');
    Route::get('/createblog','BlogController@createBlog');
});





// Board
Route::prefix('/board')->group(function(){
    Route::get('/','BoardController@index');
    Route::get('/myboard','BoardController@myboard');
    Route::get('/myboard/{id}','BoardController@content_board');
    Route::get('/createboard','BoardController@createboard');
    Route::get('/group/{namegroup}','BoardController@group_board');
});


// Organization
Route::prefix('/organization')->group(function(){
    Route::get('/','OrganizationController@index');
    Route::get('/section/{namesection}','OrganizationController@section');
    Route::get('/editsection','OrganizationController@edit_section');
});








// -------- Start Photo ------------

// part:photo
Route::prefix('/photo')->group(function () {
    Route::get('/','PhotoController@photo_index');

    // part:photo/user
    Route::prefix('/user')->group(function () {
        Route::get('/gallery','PhotoController@photo_gallery');
        Route::get('/video','PhotoController@photo_video');
        Route::get('/files','PhotoController@photo_files');

        // part:photo/user/upload
        Route::prefix('/upload')->group(function () {
            Route::get('/photo','PhotoController@photo_up_photo');
            Route::get('/album','PhotoController@photo_up_album');
        });
    });
});


// -------- End Photo ------------



// -------- Start Equipment ------------

// part:equipment
Route::prefix('/equipment')->group(function () {
    Route::get('/','EquipmentController@equipment_index');

    Route::prefix('/user')->group(function () {
        Route::get('/return','EquipmentController@user_return');
        Route::get('/lend','EquipmentController@user_lend');
        Route::get('/list','EquipmentController@user_list_device');
        Route::get('/check','EquipmentController@user_check');

    });

    Route::prefix('/manager')->group(function () {
        Route::get('/','EquipmentController@index_manger_device');
        Route::get('/adddevice','EquipmentController@manager_add_device');
        Route::get('/return','EquipmentController@manager_return');
        Route::get('/lend','EquipmentController@manager_lend');
    });


});




// -------- End Equipment ------------




// -------- Start Form ------------
Route::prefix('/forms')->group(function () {
    Route::get('/','FormsController@forms_index');
    Route::get('/1','FormsController@forms_1');
    Route::get('/2','FormsController@forms_2');
    Route::get('/3','FormsController@forms_3');
    Route::get('/4','FormsController@forms_4');
    Route::get('/5','FormsController@forms_5');
    Route::get('/6','FormsController@forms_6');
});
// -------- End Form ------------




// -------- Start PM ------------
Route::prefix('/pm')->group(function () {
    Route::get('/inbox','PMController@pm_inbox');
    Route::get('/chat','PMController@pm_chat');
});

// -------- End PM ------------


// -------- Start FAQs ------------

// part:faq
Route::prefix('/faq')->group(function () {
    Route::get('/','FAQController@faq_index');
});

// -------- End FAQs ------------


// -------- Start Market ------------

// part:market
Route::prefix('/market')->group(function () {
    Route::get('/','MarketController@market_index');
    Route::get('/myproduct','MarketController@market_myproduct');
    Route::get('/product','MarketController@market_product');
    Route::get('/sell','MarketController@market_sell');
});

// -------- End Market ------------


// -------- Start Question ------------

// part:question
Route::prefix('/question')->group(function () {
    Route::get('/','QuestionController@question_index');
    Route::get('/create','QuestionController@question_create');
});

// -------- End Question ------------


// -------- Start Announce ------------

// part:announce
Route::prefix('/announce')->group(function () {
    Route::get('/','AnnounceController@announce_index');
    Route::get('/create','AnnounceController@announce_create');
});

// -------- End Announce ------------





