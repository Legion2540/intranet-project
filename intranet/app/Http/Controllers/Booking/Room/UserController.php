<?php

namespace App\Http\Controllers\Booking\Room;

use App\BookingRoom;
use App\Device;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use Illuminate\Validation\Validator;

class UserController extends Controller
{
    public function booking_room_index()
    {

        $booking_room = BookingRoom::all();
        return view('pages/booking/booking', compact('booking_room'));
    }


    public function choose_room()
    {
        $data_room = [];
        $room_get = Room::orderByRaw("FIELD(status,'ready','not ready') ASC")->paginate(5);
        $data_room['room_get'] = $room_get;
        $id_device = json_decode($room_get[0]->device_id);
        foreach ($id_device as $id) {
            $dev = Device::where('id', $id)->select('name', 'brand', 'device_no', 'type_device', 'display', 'time_use', 'time_own', 'img', 'content', 'status', 'qrcode', 'owner')->first();
            $data_room['room_get'][0]['device'] = $dev;
        }

        return view('pages/booking/room', $data_room);
    }



    // Form
    public function form_room1(Request $request)
    {
        dd($request->all());
        dd('yest');
        return view('pages/booking/user/form_booking/room_1');
    }
    public function form_room2(Request $request)
    {

        // dd($request);
        // $validator = Validator::make($request->all(), [
        //     'user_id' => ['required', 'string', 'max:255'],


        // ],[
        //     'name.required' => 'กรุณากรอกชื่อ',
        //     'name.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'surname.required' => 'กรุณากรอกนามสกุล',
        //     'surname.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'tel.required' => 'กรุณากรอกเบอร์โทรศัพท์',
        //     'tel.regex' => 'รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง( 012-345-6789)',

        //     'address1.required' => 'กรุณากรอกที่อยู่',
        //     'address1.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'county.required' => 'กรุณากรอกอำเภอ',
        //     'county.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'district.required' => 'กรุณากรอกตำบล',
        //     'district.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'city.required' => 'กรุณากรอกชื่อจังหวัด',
        //     'city.max' => 'จำนวนตัวอักษรยาวเกินไป',

        //     'zipcode.required' => 'กรุณากรอกรหัสไปรษณีย์',
        //     'zipcode.digits' => 'จำนวนรหัสไปรษณีย์ไม่ครบ',



        // ]);
        // if ($validator->fails()) {
        //     return response()->json(['error' => $validator->errors()], 401);
        // }
        return view('pages/booking/user/form_booking/room_2');
    }
    public function form_room3()
    {
        return view('pages/booking/user/form_booking/room_3');
    }
    public function form_room4()
    {
        return view('pages/booking/user/form_booking/room_4');
    }
    // Form



    //Booking Welfare
    public function form_welfare1()
    {
        return view('/pages/booking/user/form_booking/vdoConference_1');
    }
    public function form_welfare2()
    {
        return view('pages/booking/user/form_booking/vdoConference_2');
    }
    public function form_welfare3()
    {
        return view('pages/booking/user/form_booking/vdoConference_3');
    }
    public function form_welfare4()
    {
        return view('pages/booking/user/form_booking/vdoConference_4');
    }
    //Booking Welfare


}
