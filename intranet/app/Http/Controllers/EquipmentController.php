<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function equipment_index(){
        return view('pages.equipment.myequipment');
    }


    public function equipment_user_return(){
        return view('pages.equipment.user.return');
    }
    public function equipment_user_lend(){
        return view('pages.equipment.user.lend');
    }
    public function equipment_user_list(){
        return view('pages.equipment.user.lend_list');
    }
    public function equipment_user_check(){
        return view('pages.equipment.user.lend_check');
    }


    public function equipment_manager_index(){
        return view('pages.equipment.approval.index');
    }
    public function equipment_manager_adddevice(){
        return view('pages.equipment.approval.add_device');
    }
    public function equipment_manager_return(){
        return view('pages.equipment.approval.app_return');
    }
    public function equipment_manager_lend(){
        return view('pages.equipment.approval.app_lend');
    }
}
