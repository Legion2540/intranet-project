<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingDeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('booking_devices')->insert(
            [
                'user_id' => 1,
                'details' => json_encode([
                    "title" => "2",
                    "to" => "1",
                    "objective" => "ขอใช้ชั่วคราว",
                    "pattern" => "ชั่วคราว",
                ]),
                'devices' => json_encode([
                    '0' => [
                        'id' => '1',
                        'count' => '5'
                    ],
                    '1' => [
                        'id' => '2',
                        'count' => '4'
                    ]
                ]),
                'type' => 'lend',
                'status' => 'waiting',
                'status_print' => 'not print'
            ]
        );
    }
}
