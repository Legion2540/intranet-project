<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AlbumSeeder::class,
            BlogSeeder::class,
            CarSeeder::class,
            // BookingDeviceSeeder::class,
            Category_albumSeeder::class,
            DevicesSeeder::class,
            DriverSeeder::class,
            FoodSeeder::class,
            KnowledgeSeeder::class,
            // OfficeSeeder::class,
            PhotoSeeder::class,
            RoomSeeder::class,
            Type_productSeeder::class,
            Type_devicesSeeder::class,
            VideoSeeder::class


        ]);


    }
}
