<?php

use Illuminate\Database\Seeder;

class Type_productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_product')->insert([
            'name_type' => 'มือถือ/แทปเลต',
            'img_type_product' => 'device-1.png'
        ],
        [
            'name_type' => 'เสื้อผ้ามือสอง',
            'img_type_product' => 'fashion-1.png'
        ],
        [
            'name_type' => 'แม่และเด็ก',
            'img_type_product' => 'toys-1.png'
        ],
        [
            'name_type' => 'คอมพิวเตอร์',
            'img_type_product' => 'desktop-1.png'
        ],
        [
            'name_type' => 'เครื่องประดับ',
            'img_type_product' => 'ring.png'
        ],
        [
            'name_type' => 'กล้อง',
            'img_type_product' => 'photo-camera.png'
        ],
        [
            'name_type' => 'เครื่องใช้ไฟฟ้า',
            'img_type_product' => 'electrical-appliances.png'
        ],
        [
            'name_type' => 'ของสะสม/ของเล่น',
            'img_type_product' => 'box.png'
        ],
        [
            'name_type' => 'เฟอร์นิเจอร์',
            'img_type_product' => 'furnitures.png'
        ],
        );
    }
}
