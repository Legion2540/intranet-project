<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DriverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('driver')->insert(
            // [

            //     "tel"=> "0123456789",
            //     "front_name" => "นางสาว",
            //     "name" => "สมปอง รักษ์ดี",
            //     "citizen_code" => "0123456789012",
            //     "birthday"=> "01/01/2564",
            //     "address"=>"เลขที่ 1 ซอย 2 ถนน 3 จ.4",
            //     "type_license" => json_encode(["รถยนต์ส่วนบุคคล","รถจักรยานยนต์ส่วนบุคคล"]),
            //     "type_car" => "car",
            //     "img_driver" => "profile1.jpg",
            //     "status" => "ready"
            // ]



            // $table->integer('tel');
            // $table->enum('front_name',['นาย','นาง','นางสาว']);
            // $table->string('name');
            // $table->integer('citizen_code');
            // $table->string('birthday');
            // $table->string('address');

            // // merge type license & date Expire license
            // $table->json('type_license');

            // $table->enum('type_car',['car','passenger']);
            // $table->string('img_driver');
            // $table->enum('status',['ready','not ready']);




            [
                'tel' => '0981234568',
                'front_name' => 'นาย',
                'name' => 'wanchai',
                'citizen_code' => 123456789012,
                'birthday' => '01/01/2564',
                'address' => 'เลขที่ 1 ซอย 2 ถนน 3 จ.4',
                'type_license' => json_encode(["รถยนต์ส่วนบุคคล", "รถจักรยานยนต์ส่วนบุคคล"]),
                'type_car' => 'car',
                'img_driver' => 'img.png',
                'status' => 'ready'


            ]
        );
    }
}
