<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert(
            [
                'user_id' => 1,
                'title' => 'การช่วงชิงอำนาจของรัฐบาล โดย บิ๊กตู่',
                'cover_photo' => 'background.png',
                'content' => 'การชิงอำนวจในครั้งนี้ คนส่วนใหญ่มองว่าเป็นการทำให้ประเทศล้มสะลาย',
                'img_blog' => json_encode(['iamge1.png','image2.jpg','image3.jpeg']),
                'album_id' => 1,
                'files' => json_encode(['files1.pdf','files2.xlxs','files3.docx']),
                'tag' => json_encode(['ม33','บิ๊กตู่','บิ๊กป้อม','โอริโอ']),
                'count_vote' => 20,
                'count_comments' => 3,
                'count_view' => 30,
                'type_blog_id' => 2,
            ]
        );
    }
}
