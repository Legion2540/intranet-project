<?php

use Illuminate\Database\Seeder;

class Category_albumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_album')->insert([
            'cateory_name' => 'ประชาสัมพันธ์',
            'type' => 'public'
        ],
        [
            'cateory_name' => 'กิจกรรมภายใน',
            'type' => 'private'
        ],
        [
            'cateory_name' => 'อบรมและสัมมนา',
            'type' => 'espercially'
        ],
        [
            'cateory_name' => 'ทั่วไป',
            'type' => 'custom'
        ]
        
        );
    }
}
