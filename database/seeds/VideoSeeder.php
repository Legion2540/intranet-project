<?php

use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('video')->insert([
            'video_name' => 'รายงานสถานการณ์/โอกาศทางการค้าจีน',
            'role_name' => 1,
            'url' => 'www.youtube.com/1'
        ],
        [
            'video_name' => 'รายงานสถานการณ์โควิด',
            'role_name' => 1,
            'url' => 'www.youtube.com/2'
        ],
        [
            'video_name' => 'รายงานสถานการณ์ทางการเมือง',
            'role_name' => 1,
            'url' => 'www.youtube.com/3'
        ]
        );
    }
}
