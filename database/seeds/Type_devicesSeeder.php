<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Type_devicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_devices')->insert(
            [
                'no_type' => '0005-027',
                'name_type' => 'เครื่องคอมพิวเตอร์กระเป๋าหิ้ว (Note Book)',
                'depreciation' => 33,
            ]
        );
        DB::table('type_devices')->insert(
            [
                'no_type' => '0005-049',
                'name_type' => 'อุปกรณ์คอมพิวเตอร์อื่น ๆ',
                'depreciation' => 33,
            ]
        );

    }
}
