<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room')->insert(
            [
                'type_room' => 'meeting',
                'no_room' => 'ห้องประชุม 1',
                'name_room' => 'ห้องประชุม 1',
                'level' => '1',
                'name_building' => 'A',
                'size' => 'small',
                'opacity' => '4',
                'img_room' => json_encode(['176449336_294627282067968_8570528169035308806_n.jpeg', '107073556_3164050847020715_7388594928252935091_n.jpeg']),
                'device_id' => json_encode(['1']),
                'place' => 'รัชดา',
                'status' => 'ready',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
    }
}
