<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('office')->insert(
            [
                'office_name' => 'กลุ่มกระทรวงวัฒนธรรม',
            ],
            [
                'office_name' => 'กลุ่มพัฒนาชุมชน',
            ],
            [
                'office_name' => 'กลุ่มการแม่บ้าน',
            ],
            [
                'office_name' => 'กลุ่มการจัดการของเสียในชุมชน',
            ],
            [
                'office_name' => 'กลุ่มดูแลผู้สูงอายุ',
            ]
        );
    }
}
