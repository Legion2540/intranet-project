<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // [
        //     'type_food' => 'อาหารเช้า',
        //     'list_food' => 'โดนัท/กาแฟ',
        //     'price_food' => 35,
        //     'status_food' => 'ready'
        // ],
        // 'status_food' => 'ready'

        DB::table('food')->insert(

            [
                'period' => 'อาหากลางวัน',
                'list_food' => json_encode([
                    '0' => ['ข้าวกระเพราหมูสับ', 35],
                    '1' => ['แซนวิส/กาแฟ', 35],
                    '2' => ['ขมปังไส้สังขยา/กาแฟ', 25]
                ]),
                'status_food' => 'ready'
            ],

        );
    }
}
