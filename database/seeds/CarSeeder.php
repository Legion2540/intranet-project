<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car')->insert(
            [   
                'detail_car' => json_encode([
                    'no_car' => '1ก 111',
                    'no_engine' => '111111111',
                    'no_body' => '2222222222',
                    'color' => 'ขาว'
                ]),
                'img_car' => '123',
                'status' => 'ready',
                'insulance_id' => json_encode([
                    'test'=> 'test'
                ]),
                'type' => 'seadan'
            ]




            // [
            //     'detail_car' => {
            //         'no_car' => '2ก 222',
            //         'no_engine' => '33333333',
            //         'no_body' => '444444444',
            //         'color' => 'ขาว'
            //     },
            //     'img_car' => '123',
            //     'status' => 'ready',
            //     'insulance_id' => {
            //         'test'=> 'test'
            //     },
            //     'type' => 'seadan'
            // ],
            // [
            //     'detail_car' => {
            //         'no_car' => '2ก 333',
            //         'no_engine' => '555555555',
            //         'no_body' => '666666666',
            //         'color' => 'ขาว'
            //     },
            //     'img_car' => '123',
            //     'status' => 'ready',
            //     'insulance_id' => {
            //         'test'=> 'test'
            //     },
            //     'type' => 'seadan'
            // ]

            
        );
        
      



    }
}
