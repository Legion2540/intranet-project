<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('devices')->insert(
            [
                'device_no' => '123456789',
                'name' => 'Apple Macbook Pro 2019 13 นิ้ว ',
                'type_device' => 1,
                'brand' => 'Apple',
                'price' => '45000',
                'date_buy' => '2021-07-13',
                'date_exp' => '2023-07-13',
                'date_warranty_exp' => '2023-07-13',
                'content' => '1.aaaaa',
                'img' => '123456789.png',
                'qrcode' => '123456789',
                'display' => 1,
                'who_lend' => 0
            ]
        );



        // status : 0 = คลัง , 1 = อยู่ระหว่างใช้งาน
    }
}
