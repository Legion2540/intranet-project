<?php

use Illuminate\Database\Seeder;

class KnowledgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('knowledgecenter')->insert(
            [
                'category_id' => 1,
                'office_id' => 1,
                'user_id' => 1,
                'title' => 'การระบาดของโรคโควิด',
                'content' => 'การแพร่เชื้อของโรคโควิด-19 มีอัตราการแพร่ระบาด',
                'img' => json_encode(['img.png', 'profile.jpg']),
                'album_id' => 2,
                'files' => json_encode(['files.pdf', 'test.xlxs']),
                'tag' => json_encode(['files', 'covid', 'test']),
                'subscribe' => "true",
                'url_youtube' => json_encode(['https:/www.youtube.com/test'])

            ],
            [
                'category_id' => 2,
                'office_id' => 2,
                'user_id' => 2,
                'title' => 'ยายหมีถูกหัว',
                'content' => 'ยายหมีถูกหัวเป็นจำนวนเงิน 100,000,000 บาท แต่ยายหมีขอบริจาคให้กับ วัดและทำบุญกับคนจร',
                'img' => json_encode(['img.png', 'profile.jpg']),
                'album_id' => 3,
                'files' => json_encode(['files.pdf', 'test.xlxs']),
                'tag' => json_encode(['files', 'covid', 'test']),
                'subscribe' => "true",
                'url_youtube' => json_encode(['https:/www.youtube.com/test'])

            ],
            [
                'category_id' => 3,
                'office_id' => 3,
                'user_id' => 3,
                'title' => 'กัญชาถูกกฎหมายในไทยแล้ว สามารถนำมาใช้ในการสูบได้ ทางรัฐบาลอนุมัติแล้ว',
                'content' => 'กัญชาได้รับการอนุมัติในการสูบ คล้ายกับบุหรี่ได้ในประเทศไทยแล้วประชากรเฮ! ยกใหญ่ พืชที่คนไทยต้องการให้ปลดออกจากยาเสพติด',
                'img' => json_encode(['img.png', 'profile.jpg']),
                'album_id' => 4,
                'files' => json_encode(['files.pdf', 'test.xlxs']),
                'tag' => json_encode(['files', 'covid', 'test']),
                'subscribe' => "true",
                'url_youtube' => json_encode(['https:/www.youtube.com/test'])

            ]
        );
    }
}
