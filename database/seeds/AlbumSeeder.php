<?php

use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('album')->insert([
            'category_album_id' => 1,
            'album_name' => 'วิว',
            'content' => 'เชียงใหม่',
            'role_album_id' => 1,
            'count_vote' => 10,
            'comment_id' => '1'
        ],
        [
            'category_album_id' => 2,
            'album_name' => 'รถ',
            'content' => 'มอเตอร์ไซต์',
            'role_album_id' => 2,
            'count_vote' => 1,
            'comment_id' => '1'
        ],
        [
            'category_album_id' => 3,
            'album_name' => 'บ้าน',
            'content' => 'บ้านใหม่',
            'role_album_id' => 3,
            'count_vote' => 0,
            'comment_id' => '1'
        ],
        [
            'category_album_id' => 4,
            'album_name' => 'มือถือ',
            'content' => '',
            'role_album_id' => 4,
            'count_vote' => 110,
            'comment_id' => '1'
        ]);
    }
}
