<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingDeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('booking_devices')->insert(
            [
                'user_id' => 1,
                'title' => 'ขออนุมัติยืมครุภัณฑ์คอมพิวเตอร์',
                'to' => 'หัวหน้า',
                'objective' => 'ขอใช้ประจำตำแหน่ง',
                'pattern' => 'ประจำตำแหน่ง',
                'devices' => json_encode([
                    '0' => [
                        'type_id' => 1,
                        'devices_id' => [
                            '0' => 1,
                            '1' => 2
                        ]
                    ],
                    '1' => [
                        'type_id' => 1,
                        'devices_id' => [
                            '0' => 3,
                            '1' => 4,
                            '2' => 5,
                        ]
                    ]
                ]),
                'type' => 'lend',
                'status' => 'waiting',
                'status_print' => 'not print',
                'form_status' => 1
            ]
        );
    }
}

