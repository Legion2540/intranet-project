<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_room');
            $table->string('no_room');
            $table->string('name_room');
            $table->string('level');
            $table->string('name_building');
            $table->string('size')->comment("small , medium , large ");
            $table->integer('opacity');
            $table->json('img_room');
            $table->json('device_id');
            $table->string('place');
            $table->enum('status',['ready','not ready']);
            $table->timestamps();
        });






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
