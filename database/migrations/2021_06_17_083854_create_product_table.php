<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('comment_id');

            // make migration = type_product_id
            $table->integer('type_product_id');

            $table->string('title');
            $table->json('img_product');
            $table->string('content');
            $table->integer('price');
            $table->enum('quality',['first hand','second hand']);
            $table->enum('status_product',['ready','not ready']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
