<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tel');
            $table->enum('front_name',['นาย','นาง','นางสาว']);
            $table->string('name');
            $table->bigInteger('citizen_code');
            $table->string('birthday');
            $table->string('address');

            // merge type license & date Expire license
            $table->json('type_license');

            $table->enum('type_car',['car','passenger']);
            $table->string('img_driver');
            $table->enum('status',['ready','not ready']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver');
    }
}
