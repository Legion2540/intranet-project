<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnowledgecenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledgecenter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
            $table->integer('office_id');
            $table->integer('user_id');
            // $table->integer('position_id');
            $table->string('title');
            $table->string('content');
            $table->json('img');
            $table->integer('album_id');
            $table->json('files');
            $table->string('tag');
            $table->enum('subscribe',['true','false']);
            $table->json('url_youtube');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledgecenter');
    }
}
