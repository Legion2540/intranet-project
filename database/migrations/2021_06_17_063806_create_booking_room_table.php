<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('room_id');
            $table->integer('manager_id');
            $table->enum('type_booking',['room','conference']);
            $table->json('device_id')->nullable();
            $table->string('staff')->nullable();
            $table->string('group_name')->nullable()->comment('Is group/position in job');
            $table->string('title')->nullable();
            // $table->enum('period',['morning','lunch','evening']);
            $table->json('food_detail')->nullable();
            $table->string('place')->nullable();
            $table->string('objective')->nullable();
            $table->string('request_to')->nullable();
            $table->string('room_request')->nullable();
            $table->enum('status_booking',['approved','cancelled','waiting'])->default('waiting');
            $table->integer('form_status')->default(0)->comment('0 = incomplete, 1 = complete');
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();

            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();

            $table->string('manager_approved')->nullable();
            $table->string('welfare_approved')->nullable();
            $table->string('device_approved')->nullable();
            $table->string('room_approved')->nullable();

            $table->integer('member_count')->nullable()->comment('count member in meeting room');
            $table->string('reason')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_room');
    }
}
