<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('device_no');
            $table->string('name');
            $table->integer('type_device');
            $table->string('brand');
            $table->string('price');
            $table->date('date_buy');
            $table->date('date_exp');
            $table->date('date_warranty_exp');
            $table->longText('content')->nullable();
            $table->string('img');
            $table->string('qrcode');
            $table->integer('display');
            $table->integer('who_lend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
