<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('to');
            $table->string('objective');
            $table->string('pattern');
            $table->json('devices')->nullable();
            $table->enum('type',['lend','return']);
            $table->enum('status',['approved','cancelled','waiting','return']);
            $table->enum('status_print',['print','not print']);
            $table->integer('form_status');
            $table->integer('who_approve')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_devices');
    }
}
