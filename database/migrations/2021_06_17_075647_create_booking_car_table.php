<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_car', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->json('approval');
            $table->string('start_date');
            $table->string('end_date');
            $table->enum('type_car',['public car','guest car']);
            $table->integer('count_member');
            $table->string('purpose');
            $table->json('location');
            $table->enum('status_approved',['approved','cancelled','waiting']);
            $table->integer('car_id');
            $table->integer('driver_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_car');
    }
}
