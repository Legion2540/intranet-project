<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food', function (Blueprint $table) {
            // $table->bigIncrements('id');
            // $table->string('type_food');
            // $table->string('list_food');
            // $table->integer('price_food');
            // $table->enum('status_food',['ready','not ready']);
            // $table->timestamps();


            $table->bigIncrements('id');
            $table->string('period');
            $table->json('list_food');
            $table->enum('status_food',['ready','not ready']);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food');
    }
}
