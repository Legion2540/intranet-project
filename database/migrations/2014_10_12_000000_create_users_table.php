<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type')->default('employee');
            $table->string('role')->nullable()->comment('building, welfare, device');
            $table->string('name')->nullable();
            $table->string('nickname')->nullable();
            $table->string('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('introduce')->nullable()->comment('introduce yourself');
            $table->string('office_name')->nullable()->comment('สำนักงาน');
            $table->string('group')->nullable()->comment('กลุ่มงาน');
            $table->string('position')->nullable()->comment('ตำแหน่ง');
            $table->string('manager_name')->nullable()->comment('หัวหน้างาน');
            $table->string('tel_internal')->nullable()->comment('เบอร์โทรภายใน');
            $table->string('pic_profile')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
