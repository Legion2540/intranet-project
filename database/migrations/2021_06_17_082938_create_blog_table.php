<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('cover_photo')->nullable();
            $table->string('content')->nullable();
            $table->string('img_blog')->nullable();
            $table->json('album_id')->nullable();
            $table->json('files')->nullable();
            $table->json('tag')->nullable();
            $table->integer('count_vote')->nullable();
            $table->integer('count_comments')->nullable();
            $table->integer('count_view')->nullable();
            $table->integer('type_blog_id')->nullable();
            $table->json('url_youtube')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
